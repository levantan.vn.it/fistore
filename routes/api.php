<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/ping', 'Api\ApiController@send_response');

Route::post('/product/create', 'Api\ProductApiController@create');
// API V1
Route::group(['prefix' => 'v1.0'], function(){
	Route::group(['prefix' => 'afrcall'], function(){
		Route::post('productUpdate', 'Api\ProductApiController@productUpdate');
		Route::post('deliveryStatusUpdate', 'Api\OrderApiController@deliveryStatusUpdate');
	});
});