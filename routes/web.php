<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Admin Auth Route
 * Trình xác thực người dùng cho nhóm Quản trị
 */
Route::group(['prefix' => 'admin', 'middleware' => 'session.prevent'], function() {
	Route::get('login', 'Auth\Admin\LoginController@showLoginForm');
	Route::post('login', 'Auth\Admin\LoginController@login')->name('admin.login');

	Route::get('popup','Admin\PopupController@index')->name('admin.popup');
	Route::post('popup','Admin\PopupController@savepopup')->name('admin.popup.savepopup');

	Route::group(['middleware' => 'admin'], function() {		
		
		Route::get('logout', 'Auth\Admin\LoginController@logout')->name('admin.logout');
		
		/**
		 * Dashboard
		 */
		Route::get('/', 'Admin\DashboardController@index')->name('admin.dashboard');

		/**
		 * Profile
		 */
		Route::group(['prefix' => 'profile'], function(){
			Route::get('/', 'Admin\ProfileController@show')->name('admin.profile.show');
			Route::get('edit', 'Admin\ProfileController@edit')->name('admin.profile.edit');
			Route::post('update', 'Admin\ProfileController@update')->name('admin.profile.update');
			Route::get('password', 'Admin\ProfileController@editPassword')->name('admin.profile.password.edit');
			Route::post('password/update', 'Admin\ProfileController@updatePassword')->name('admin.profile.password.update');
		});		
		
		/**
		 * Admin Users
		 */
		Route::resource('accounts', 'Admin\AdminController', [
			'except' 	=> ['show'], 
			'names' 	=> [
				'index'		=>	'admin.user.admin.index',
				'create'	=>	'admin.user.admin.create',
				'store'		=>	'admin.user.admin.store',
				'edit'		=>	'admin.user.admin.edit',
				'update'	=>	'admin.user.admin.update',
				'destroy'	=>	'admin.user.admin.destroy'
			]
		]);
		Route::group(['prefix' => 'accounts'], function(){
			Route::post('disable', 'Admin\AdminController@disable')->name('admin.user.admin.disable');
			Route::post('activate', 'Admin\AdminController@activate')->name('admin.user.admin.activate');
			Route::post('check-email-exists', 'Admin\AdminController@checkEmailExists')->name('admin.user.admin.checkEmailExist');
		});

		/**
		 * Roles
		 */
		Route::resource('roles', 'Admin\RoleController', [
			'except'	=> ['show'],
			'names' 	=> [
				'index'		=>	'admin.role.index',
				'create'	=>	'admin.role.create',
				'store'		=>	'admin.role.store',
				'edit'		=>	'admin.role.edit',
				'update'	=>	'admin.role.update',
				'destroy'	=>	'admin.role.destroy',
			]
		]);
		Route::post('roles/check-key-exists', 'Admin\RoleController@checkKeyExists')->name('admin.role.checkKeyExist');

		/**
		 * Permissions
		 */
		Route::resource('permissions', 'Admin\PermissionController', [
			'only'	=> ['edit', 'update'],
			'names' 	=> [
				'edit'		=>	'admin.permission.edit',
				'update'	=>	'admin.permission.update'
			]
		]);

		/**
		 * Customer Users
		 */
		Route::resource('customers', 'Admin\CustomerController', [
			'except' 	=> ['create', 'store', 'edit', 'update'], 
			'names' 	=> [
				'index'		=>	'admin.user.customer.index',
				'show'		=>	'admin.user.customer.show',
				'destroy'	=>	'admin.user.customer.destroy'
			]
		]);
		Route::group(['prefix' => 'customers'], function(){
			Route::post('disable', 'Admin\CustomerController@disable')->name('admin.user.customer.disable');
			Route::post('activate', 'Admin\CustomerController@activate')->name('admin.user.customer.activate');
		});

		/**
		 * Products
		 */
		Route::resource('products', 'Admin\ProductController', [
			'except'	=>	['show'],
			'names'		=>	[
				'index'		=>	'admin.product.index',
				'create'	=>	'admin.product.create',
				'store'		=>	'admin.product.store',
				'edit'		=>	'admin.product.edit',
				'update'	=>	'admin.product.update',
				'destroy'	=>	'admin.product.destroy',
			]
		]);
		Route::group(['prefix' => 'products'], function(){
			Route::post('trash', 'Admin\ProductController@trash')->name('admin.product.trash');
			Route::post('restore', 'Admin\ProductController@restore')->name('admin.product.restore');
			Route::post('position', 'Admin\ProductController@position')->name('admin.product.position');
			Route::post('position-in-cat', 'Admin\ProductController@positionInCat')->name('admin.product.positionInCat');
        	/**
        	 * Product info
        	 */
        	Route::get('getinfo/{id}', 'Admin\ProductController@getinfo');
			Route::post('updateinfo/{id}', 'Admin\ProductController@updateinfo');
			Route::post('updatestatus/{id}', 'Admin\ProductController@updatestatus');

			// New api AFR
			Route::post('fecthStock', 'Admin\ProductController@fecthStock');

			/**
			 * Product Categories
			 */
			Route::resource('categories', 'Admin\ProductCategoryController', [
				'except'	=>	['show'],
				'names'		=>	[
					'index'		=>	'admin.product.category.index',
					'create'	=>	'admin.product.category.create',
					'store'		=>	'admin.product.category.store',
					'edit'		=>	'admin.product.category.edit',
					'update'	=>	'admin.product.category.update',
					'destroy'	=>	'admin.product.category.destroy',
				]
			]);
			Route::group(['prefix' => 'categories'], function(){
				Route::post('check-name-exists', 'Admin\ProductCategoryController@checkNameExists')->name('admin.product.category.checkNameExist');
				Route::post('check-slug-exists', 'Admin\ProductCategoryController@checkSlugExists')->name('admin.product.category.checkSlugExist');
			});

			/**
			 * Product Brands
			 */
			Route::resource('brands', 'Admin\ProductBrandController', [
				'except'	=>	['show'],
				'names'		=>	[
					'index'		=>	'admin.product.brand.index',
					'create'	=>	'admin.product.brand.create',
					'store'		=>	'admin.product.brand.store',
					'edit'		=>	'admin.product.brand.edit',
					'update'	=>	'admin.product.brand.update',
					'destroy'	=>	'admin.product.brand.destroy',
				]
			]);
			Route::group(['prefix' => 'brands'], function(){
				Route::post('check-name-exists', 'Admin\ProductBrandController@checkNameExists')->name('admin.product.brand.checkNameExist');
				Route::post('check-slug-exists', 'Admin\ProductBrandController@checkSlugExists')->name('admin.product.brand.checkSlugExist');
			});

			/**
			 * Product Groups
			 */
			Route::resource('groups', 'Admin\ProductGroupController', [
				'except'	=>	['show'],
				'names'		=>	[
					'index'		=>	'admin.product.group.index',
					'create'	=>	'admin.product.group.create',
					'store'		=>	'admin.product.group.store',
					'edit'		=>	'admin.product.group.edit',
					'update'	=>	'admin.product.group.update',
					'destroy'	=>	'admin.product.group.destroy',
				]
			]);
			Route::group(['prefix' => 'groups'], function(){
				Route::post('check-name-exists', 'Admin\ProductGroupController@checkNameExists')->name('admin.product.group.checkNameExist');
				Route::post('check-slug-exists', 'Admin\ProductGroupController@checkSlugExists')->name('admin.product.group.checkSlugExist');
			});
		});

		/**
		 * Product Orders
		 */
		Route::resource('orders', 'Admin\ProductOrderController', [
			'except'	=>	['edit'],
			'names'		=>	[
				'index'		=>	'admin.product.order.index',
				'show'		=>	'admin.product.order.show',
				'create'	=>	'admin.product.order.create',
				'store'		=>	'admin.product.order.store',
				'update'	=>	'admin.product.order.update',
				'destroy'	=>	'admin.product.order.destroy',
			]
		]);
		Route::post('orders/update-status', 'Admin\ProductOrderController@updateStatus')->name('admin.product.order.update_status');
		Route::post('orders/update-admin', 'Admin\ProductOrderController@updateAdmin')->name('admin.product.order.update_admin');
		Route::post('orders/{id}/update-note', 'Admin\ProductOrderController@updateNote')->name('admin.product.order.update_note');

		/**
		 * Transpotations
		 */
		Route::resource('transportations', 'Admin\TransportationController', [
			'only'	=>	['index', 'store', 'update', 'destroy'],
			'names'		=>	[
				'index'		=>	'admin.transportation.index',
				'store'		=>	'admin.transportation.store',
				'update'	=>	'admin.transportation.update',
				'destroy'	=>	'admin.transportation.destroy',
			]
		]);
		Route::post('transportations/setting', 'Admin\TransportationController@setting')->name('admin.transportation.setting');

		/**
		 * Pages
		 */
		Route::resource('pages', 'Admin\PageController', [
			'except' 	=> ['show'], 
			'names' 	=> [
				'index'		=>	'admin.page.index',
				'create'	=>	'admin.page.create',
				'store'		=>	'admin.page.store',
				'edit'		=>	'admin.page.edit',
				'update'	=>	'admin.page.update',
				'destroy'	=>	'admin.page.destroy',
			]
		]);
		Route::group(['prefix' => 'pages'], function(){
			Route::post('trash', 'Admin\PageController@trash')->name('admin.page.trash');
			Route::post('restore', 'Admin\PageController@restore')->name('admin.page.restore');

			/**
			 * Page Groups
			 */
			Route::resource('groups', 'Admin\PageGroupController', [
				'except' 	=> ['show'], 
				'names' 	=> [
					'index'		=>	'admin.page.group.index',
					'create'	=>	'admin.page.group.create',
					'store'		=>	'admin.page.group.store',
					'edit'		=>	'admin.page.group.edit',
					'update'	=>	'admin.page.group.update',
					'destroy'	=>	'admin.page.group.destroy',
				]
			]);
			Route::group(['prefix' => 'groups'], function(){
				Route::post('check-name-exists', 'Admin\PageGroupController@checkNameExists')->name('admin.page.group.checkNameExist');
				Route::post('check-slug-exists', 'Admin\PageGroupController@checkSlugExists')->name('admin.page.group.checkSlugExist');
			});
		});

		/**
		 * Articles
		 */
		Route::resource('articles', 'Admin\ArticleController', [
			'except'	=>	['show'],
			'names'		=>	[
				'index'		=>	'admin.article.index',
				'create'	=>	'admin.article.create',
				'store'		=>	'admin.article.store',
				'edit'		=>	'admin.article.edit',
				'update'	=>	'admin.article.update',
				'destroy'	=>	'admin.article.destroy',
			]
		]);
		Route::group(['prefix' => 'articles'], function(){
			Route::post('trash', 'Admin\ArticleController@trash')->name('admin.article.trash');
			Route::post('restore', 'Admin\ArticleController@restore')->name('admin.article.restore');

			/**
			 * Article Categories
			 */
			Route::resource('categories', 'Admin\ArticleCategoryController', [
				'except'	=>	['show'],
				'names'		=>	[
					'index'		=>	'admin.article.category.index',
					'create'	=>	'admin.article.category.create',
					'store'		=>	'admin.article.category.store',
					'edit'		=>	'admin.article.category.edit',
					'update'	=>	'admin.article.category.update',
					'destroy'	=>	'admin.article.category.destroy',
				]
			]);
			Route::group(['prefix' => 'categories'], function(){
				Route::post('check-name-exists', 'Admin\ArticleCategoryController@checkNameExists')->name('admin.article.category.checkNameExist');
				Route::post('check-slug-exists', 'Admin\ArticleCategoryController@checkSlugExists')->name('admin.article.category.checkSlugExist');
			});
		});

		/**
		 * Configuration
		 */
		Route::group(['prefix' => 'configs'], function(){
			Route::get('/', 'Admin\ConfigurationController@show')->name('admin.configuration.show');
			Route::get('edit', 'Admin\ConfigurationController@edit')->name('admin.configuration.edit');
			Route::post('update', 'Admin\ConfigurationController@update')->name('admin.configuration.update');
		});

		/**
		 * Comments
		 */
		Route::resource('comments', 'Admin\CommentController', [
			'except'	=>	['create', 'store', 'edit', 'update', 'show'],
			'names'		=>	[
				'index'		=>	'admin.comment.index',
				'destroy'	=>	'admin.comment.destroy',
			]
		]);
		Route::group(['prefix' => 'comments'], function(){
			Route::post('trash', 'Admin\CommentController@trash')->name('admin.comment.trash');
			Route::post('restore', 'Admin\CommentController@restore')->name('admin.comment.restore');
		});

		/**
		 * Reviews
		 */
		Route::resource('reviews', 'Admin\ReviewController', [
			'except'	=>	['create', 'store', 'edit', 'update', 'show'],
			'names'		=>	[
				'index'		=>	'admin.review.index',
				'destroy'	=>	'admin.review.destroy',
			]
		]);
		Route::group(['prefix' => 'reviews'], function(){
			Route::post('trash', 'Admin\ReviewController@trash')->name('admin.review.trash');
			Route::post('restore', 'Admin\ReviewController@restore')->name('admin.review.restore');
		});

		/**
		 * Report
		 */
		Route::resource('reports', 'Admin\CommentReportController', [
			'except'	=>	['create', 'store', 'edit', 'update', 'show'],
			'names'		=>	[
				'index'		=>	'admin.report.index',
				'destroy'	=>	'admin.report.destroy',
			]
		]);

		/**
		 * Contact
		 */
		Route::resource('contacts', 'Admin\ContactController', [
			'except'	=>	['create', 'store', 'edit', 'update', 'show'],
			'names'		=>	[
				'index'		=>	'admin.contact.index',
				'destroy'	=>	'admin.contact.destroy',
			]
		]);

		/**
		 * Newsletters
		 */
		Route::resource('newsletters', 'Admin\NewsletterController', [
			'except'	=>	['create', 'store', 'edit', 'update', 'show'],
			'names'		=>	[
				'index'		=>	'admin.newsletter.index',
				'destroy'	=>	'admin.newsletter.destroy',
			]
		]);

		/**
		 * Banners
		 */
		Route::resource('slides', 'Admin\SlideController', [
			'except'	=>	['show', 'create', 'edit'],
			'names'		=>	[
				'index'		=>	'admin.slide.index',
				'store'		=>	'admin.slide.store',
				'update'	=>	'admin.slide.update',
				'destroy'	=>	'admin.slide.destroy',
			]
		]);
		Route::get('banners', 'Admin\SlideController@banner')->name('admin.slide.banner.index');

		/**
		 * Coupons
		 */
		Route::resource('coupons', 'Admin\CouponController', [
			'except'	=>	['show'],
			'names'		=>	[
				'index'		=>	'admin.coupon.index',
				'create'	=>	'admin.coupon.create',
				'store'		=>	'admin.coupon.store',
				'edit'		=>	'admin.coupon.edit',
				'update'	=>	'admin.coupon.update',
				'destroy'	=>	'admin.coupon.destroy',
			]
		]);
		Route::group(['prefix' => 'coupons'], function(){
			Route::post('trash', 'Admin\CouponController@trash')->name('admin.coupon.trash');
			Route::post('restore', 'Admin\CouponController@restore')->name('admin.coupon.restore');
			Route::post('check-code-exists', 'Admin\CouponController@checkCodeExists')->name('admin.coupon.checkCodeExist');
		});

		/**
		 * Hot Deals
		 */
		Route::resource('hot-deals', 'Admin\HotDealController', [
			'except'	=>	['show'],
			'names'		=>	[
				'index'		=>	'admin.hotdeal.index',
				'create'	=>	'admin.hotdeal.create',
				'store'		=>	'admin.hotdeal.store',
				'edit'		=>	'admin.hotdeal.edit',
				'update'	=>	'admin.hotdeal.update',
				'destroy'	=>	'admin.hotdeal.destroy',
			]
		]);

		/**
		 * Free Try
		 */
		Route::resource('freetrial', 'Admin\FreeTrialController', [
			'except'	=>	['show'],
			'names'		=>	[
				'index'		=>	'admin.freetrial.index',
				'create'	=>	'admin.freetrial.create',
				'store'		=>	'admin.freetrial.store',
				'edit'		=>	'admin.freetrial.edit',
				'update'	=>	'admin.freetrial.update',
				'destroy'	=>	'admin.freetrial.destroy',
			]
		]);
		Route::post('freetrial/position', 'Admin\FreeTrialController@position')->name('admin.freetrial.position');

		/**
		 * Combo
		 */
		Route::resource('combo', 'Admin\ComboController', [
			'except'	=>	['show'],
			'names'		=>	[
				'index'		=>	'admin.combo.index',
				'create'	=>	'admin.combo.create',
				'store'		=>	'admin.combo.store',
				'edit'		=>	'admin.combo.edit',
				'update'	=>	'admin.combo.update',
				'destroy'	=>	'admin.combo.destroy',
			]
		]);
    
    	Route::group(['prefix' => 'combo'], function(){
			Route::post('addproduct/{id}', 'Admin\ComboController@addproduct');
			Route::get('getproduct/{id}', 'Admin\ComboController@getproduct');
			Route::get('deleteproduct/{id}', 'Admin\ComboController@deleteproduct');
		});
    
		Route::post('combo/position', 'Admin\ComboController@position')->name('admin.combo.position');
    
    	/*
		* Sale
		*/

		Route::resource('sale', 'Admin\SaleController', [
			'except'	=>	['show'],
			'names'		=>	[
				'index'		=>	'admin.sale.index',
				'create'	=>	'admin.sale.create',
				'store'		=>	'admin.sale.store',
				'edit'		=>	'admin.sale.edit',
				'update'	=>	'admin.sale.update',
				'destroy'	=>	'admin.sale.destroy',
			]
		]);

		Route::group(['prefix' => 'sale'], function(){
			Route::get('delete/{id}', 'Admin\SaleController@delete')->name('admin.sale.delete');
		});
	
		/**
		 * Extra Point
		 */
		Route::group(['prefix' => 'extra-point'], function(){
			Route::get('/', 'Admin\ExtraPointController@show')->name('admin.extra_point.show');
			Route::post('update', 'Admin\ExtraPointController@update')->name('admin.extra_point.update');
		});

		// log api AFR
		Route::resource('log-api', 'Admin\LogAPIController');
		Route::post('log-api/send-again', 'LogAPIController@sendAgain')->name('admin.log-api.send_again');

	});
});

/**
 * Ajax Route
 * Các chức năng sử dụng Ajax
 */
Route::group(['prefix' => 'ajax'], function(){
	Route::post('get-districts', 'AjaxController@getDistrict')->name('ajax.get.district');
	Route::post('get-wards', 'AjaxController@getWard')->name('ajax.get.ward');
	Route::post('deliveryOrder', 'AjaxController@deliveryOrder')->name('ajax.delivery.order');
});
use \App\Exports\CustomExport;
/**
 * Testing Function
 */
Route::group(['prefix' => 'develope'], function(){
	Route::get('export', function(){
		return Excel::download(new CustomExport, 'user.csv');
	});
	Route::get('random{number}', function($number){
		return str_random($number);
	});
	Route::get('cart', function(){
		dd(Cart::content());
	});

	Route::get('mail', function(){
		$user = [
            'name' => 'Thanh',
            'email' => 'mineland405@gmail.com',
        ];

		\Mail::to($user['email'])->send(new \App\Mail\SignUpSuccessMail($user));
	});

	/**
	 * Chức năng import dữ liệu từ website cũ
	 */
	// Route::get('import-database', function(){
	// 	$users = \DB::table('card')->select('fullname', 'email', 'telephone', 'added_date', 'password')->get();
	// 	foreach($users as $user) {
	// 		if(\App\User::where('email', $user->email)->get()->count() == 0) {
	// 			$someone = new \App\User;
	// 			$someone->name = $user->fullname;
	// 			$someone->email = $user->email;
	// 			$someone->phone = $user->telephone;
	// 			$someone->old_password = $user->password;
	// 			$someone->save();
	// 		}				
	// 	}
	// 	return 'Done';
	// });

	// Route::get('pos', function(){
	// 	$products = \App\Product::where('is_combo', false)->get();
	// 	$i = 0;
	// 	foreach($products as $key => $pr) {
	// 		$u = \App\Product::find($pr->id);
	// 		$u->position = $i;
	// 		$u->save();
	// 		$i++;
	// 	}
	// });
	// Route::get('order', function(){
	// 	$orders = \App\ProductOrder::where('status', 5)->update(['status' => 2]);
	// });
	

	Route::get('linkfix', function(){
		$products = \App\Product::all();
		foreach($products as $key => $pr) {
			$linkfix = \App\Product::find($pr->id);
			$linkfix->content = str_replace('http://shopa.azweb.com.vn', 'https://fistore.vn', $linkfix->content);
			$linkfix->save();
		}
	});
});


/**
 * Laravel Auth Route
 * Trình xác thực người dùng mặc định
 * Tạo ra các phương thức Đăng nhập, Đăng ký, Quên mật khẩu, Reset mật khẩu
 */
Auth::routes();

/**
 * Chuyển hướng link cũ sang link mới
 */
Route::group(['prefix' => 'vi-vn'], function(){
	Route::get('/', 'RedirectController@index');
	Route::get('{l1}', 'RedirectController@redirect1Level');
	Route::get('{l1}/{l2}', 'RedirectController@redirect2Level');
	Route::get('{l1}/{l2}/{l3}', 'RedirectController@redirect3Level');
});

Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('redirect', 'Auth\ResetPasswordController@redirect');

# Social Login
Route::get('login/{provider}/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/', 'HomeController@home')->name('home');

Route::group(['prefix' => 'checkout'], function() {
	Route::group(['prefix' => 'cart'], function() {
		/** Giỏ hàng */
		Route::get('/', 'CartController@showCart')->name('cart.show');
		/** Thêm sản phẩm vào giỏ */
		Route::post('add', 'CartController@addToCart')->name('cart.add');
		/** Cập nhật sản phẩm trong giỏ hàng */
		Route::post('update', 'CartController@updateCart')->name('cart.update');
		/** Xóa một sản phẩm trong giỏ hàng */
		Route::post('remove', 'CartController@removeProductFromCart')->name('cart.remove');
		/** Hủy giỏ hàng */
		Route::post('destroy', 'CartController@destroyCart')->name('cart.destroy');
		/** Áp dụng mã khuyến mãi */
		Route::post('coupon', 'CartController@applyCoupon')->name('cart.coupon');
	});
	
	Route::group(['prefix' => 'address'], function() {
		/** Thêm địa chỉ giao hàng mới */
		Route::get('/', 'CheckoutAddressController@getAddress')->name('checkout.address.get');
		/** Lưu địa chỉ */
		Route::post('/', 'CheckoutAddressController@postAddress')->name('checkout.address.post');

		/** Chọn địa chỉ có sẵn */
		Route::get('list', 'CheckoutAddressController@showAddressList')->name('checkout.address.list');
	});

	Route::group(['prefix' => 'payment'], function(){
		/** Thanh toán và Đặt hàng */
		Route::get('/', 'CheckoutPaymentController@getPayment')->name('checkout.payment.get');
		Route::post('/completed', 'CheckoutPaymentController@postPayment')->name('checkout.payment.post');
	});

	/** Đăng ký */
	Route::get('account/register', 'CheckoutRegisterController@register')->name('checkout.account.register');
	Route::post('account/register', 'CheckoutRegisterController@postRegister')->name('checkout.account.register.post');
	/** Đăng nhập */
	Route::get('account/login', 'CheckoutLoginController@login')->name('checkout.account.login');
	Route::post('account/login', 'CheckoutLoginController@postLogin')->name('checkout.account.login.post');
	Route::post('account/login/email-valid', 'CheckoutLoginController@checkEmailValid')->name('checkout.account.login.checkEmailValid');
	Route::post('account/login/password-valid', 'CheckoutLoginController@checkPasswordValid')->name('checkout.account.login.checkPasswordValid');
});

/**
 * Redirect Link | VoTruongPhuc | 10/11/2018
 */
Route::get('son-bam-im-meme-im-tic-toc-cushion-velvet-tint-SP00014.html', function(){
  	return redirect('/son-bam-im-meme-im-tic-toc-cushion-velvet-tint-SP00804.html');
});

/**
 * Sản phẩm
 */
Route::group(['prefix' => '{slug}.html'], function(){
	// if(url()->current() == 'https:/fistore.vn/index.php/son-bam-im-meme-im-tic-toc-cushion-velvet-tint-SP00014.html'){
	// return "123";
	// }
	Route::get('/', 'ProductController@showProductDetail')->name('product.detail');
	/**
	 * Đánh giá
	 */
	Route::group(['prefix' => 'review'], function(){
		Route::post('rate', 'ReviewController@rate')->name('review.rate');
		Route::post('update', 'ReviewController@update')->name('review.update');
		Route::post('thank', 'ReviewController@thank')->name('review.thank');
		Route::post('reply', 'ReviewController@reply')->name('review.reply');
	});
});

/**
 * Thương hiệu sản phẩm
 */
Route::group(['prefix' => 'thuong-hieu'], function(){
	Route::get('/', 'ProductBrandController@showBrandList')->name('brand.list');
	Route::get('{brand_slug}', 'ProductBrandController@showProductListInBrand')->name('brand.detail');
});

/**
 * Phân loại sản phẩm
 */
Route::get('phan-loai/{category_slug}', 'ProductCategoryController@showProductListInCategory')->name('category.detail');
Route::get('san-pham/{group_slug}', 'ProductGroupController@showProductListInGroup')->name('group.detail');

/**
 * Khuyến mãi
 */
Route::group(['prefix' => 'khuyen-mai'], function(){
	Route::get('/', 'SaleOfController@showHotDealProducts');
	Route::get('hotdeal', 'SaleOfController@showHotDealProducts')->name('hotdeal');
	Route::get('freetrial', 'SaleOfController@showFreeTrialProducts')->name('freetrial');
	Route::get('combo', 'SaleOfController@showComboProducts')->name('combo');
	Route::get('combo/{combo_slug}', 'SaleOfController@showComboDetail')->name('combo.detail');

});

/**
 * Bình luận
 */
Route::group(['prefix' => 'comment'], function(){
	Route::post('add', 'CommentController@addComment');
	Route::post('remove', 'CommentController@updateComment');
	Route::post('destroy', 'CommentController@destroyComment');
});

/**
 * Bài viết
 */
Route::group(['prefix' => 'bai-viet-su-kien'], function(){
	Route::get('/', 'ArticleController@showArticleList')->name('article.list');
	Route::get('{article_slug}.html', 'ArticleController@showArticleDetail')->name('article.detail');
	Route::get('{category_slug}', 'ArticleController@showArticleListInCategory')->name('article.category');
});

/**
 * Người dùng
 */
Route::group(['prefix' => 'user', 'middleware' => 'customer'], function(){
	Route::get('/', 'UserProfileController@getProfile');
	Route::get('profile', 'UserProfileController@getProfile')->name('user.profile');
	Route::post('profile', 'UserProfileController@updateProfile')->name('user.profile.update');

	Route::get('orders', 'UserOrderController@getOrder')->name('user.order');
	Route::post('orders', 'UserOrderController@cancelOrder')->name('user.order.cancel');
	Route::get('orders/{code}', 'UserOrderController@orderDetail')->name('user.order.detail');

	Route::get('address-book', 'UserAddressBookController@getAddressBook')->name('user.address_book');
	Route::post('address-book/create', 'UserAddressBookController@createAddress')->name('user.address_book.create');
	Route::post('address-book', 'UserAddressBookController@updateAddress')->name('user.address_book.update');

	Route::get('extra', 'UserExtraPointController@getExtraPoint')->name('user.extra');
	Route::post('extra', 'UserExtraPointController@updateExtraPoint')->name('user.extra.update');
});

/**
 * Liên hệ
 */
Route::get('lien-he', 'PageController@getContact')->name('contact');
Route::post('lien-he', 'PageController@sendContact')->name('contact.send');
Route::post('nhan-tin-khi-co-hang', 'PageController@subscribeInstockCameback')->name('contact.subscribe_instock_cameback');

/**
 * Đăng ký nhận tin
 */
Route::post('newsletter', 'NewsletterController@newsletter')->name('newsletter');

/**
 * Tìm kiếm
 */
Route::get('search', 'SearchController@search')->name('search');

/**
 * Trang
 */
Route::get('page/{page_group_slug}', 'PageController@showPageListInGroup')->name('page.group');
Route::get('{page_slug}', 'PageController@showPageDetail')->name('page.detail');