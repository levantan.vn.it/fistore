<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique()->comment('Mã khuyến mãi');
            $table->tinyInteger('limited')->nullable()->comment('Số lượng giới hạn');

            $table->double('discount')->comment('Giá giảm');
            $table->tinyInteger('unit')->default(0)->comment('Đơn vị: static, percent');
            $table->tinyInteger('target_type')->default(0)->comment('Loại giảm giá: Giảm trên đơn hàng/Giảm trên mỗi sản phẩm/Giảm cho sản phẩm thuộc nhóm, danh mục, thương hiệu...');
            $table->integer('target_id')->nullable()->comment('ID mục được giảm: sản phẩm, danh mục, nhóm, thương hiệu...');
            $table->double('min_value')->default(0)->comment('Áp dụng cho đơn hàng có trị giá tối thiểu bằng bao nhiêu');

            $table->datetime('started_at')->comment('Ngày bắt đầu');
            $table->datetime('expired_at')->comment('Ngày hết hạn');

            $table->boolean('disabled')->default(false);
            $table->boolean('trashed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
