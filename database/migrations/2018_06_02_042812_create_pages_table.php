<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('content')->nullable();
            $table->string('slug')->unique();
            $table->unsignedInteger('author_id')->nullable()->comment('Người đăng');
            $table->foreign('author_id')->references('id')->on('admins')->onDelete('SET NULL');

            $table->unsignedInteger('group_id')->nullable()->comment('Nhóm trang');
            $table->foreign('group_id')->references('id')->on('page_groups')->onDelete('SET NULL');
            
            $table->boolean('hidden')->default(false)->comment('Hiện/Ẩn bài viết');
            $table->boolean('drafted')->default(false)->comment('Lưu nháp');
            $table->boolean('trashed')->default(false)->comment('Chuyển vào thùng rác');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
