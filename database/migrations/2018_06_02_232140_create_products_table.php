<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('code');
            $table->string('slug');
            $table->double('price')->nullable()->comment('Null nếu giá là Liên hệ');
            $table->double('original_price')->default(null)->nullable()->comment('Null nếu không có giá gốc');
            $table->string('thumbnail')->nullable()->comment('Ảnh hiển thị');
            $table->longText('content')->nullable();
            $table->longText('properties')->nullable();
            $table->text('tags')->nullable();
            $table->integer('quantity')->nullable()->comment('Null nếu không giới hạn số lượng');

            $table->string('title')->nullable()->comment('Tiêu đề cho SEO');
            $table->text('description')->nullable()->comment('Mô tả cho SEO');

            $table->unsignedInteger('category_id')->nullable()->comment('Danh mục sản phẩm');
            $table->unsignedInteger('brand_id')->nullable()->comment('Thương hiệu sản phẩm');

            $table->boolean('is_combo')->default(false)->comment('Sản phẩm combo');

            $table->boolean('display_price')->default(true)->comment('Hiển thị giá');
            $table->boolean('closed')->default(false)->comment('Tắt đặt hàng (không kinh doanh)');
            $table->boolean('hidden')->default(false)->comment('Hiện/Ẩn bài viết');
            $table->boolean('drafted')->default(false)->comment('Lưu nháp');
            $table->boolean('trashed')->default(false)->comment('Chuyển vào thùng rác');
            
            $table->datetime('reposted_at')->nullable()->comment('Đăng lên đầu tiên');

            $table->foreign('category_id')->references('id')->on('product_categories')->onDelete('SET NULL');
            $table->foreign('brand_id')->references('id')->on('product_brands')->onDelete('SET NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
