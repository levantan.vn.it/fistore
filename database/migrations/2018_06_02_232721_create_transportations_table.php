<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransportationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('province_id')->nullable()->comment('Null đối với phương thức vận chuyển mặc định');
            $table->string('name');
            $table->double('min_order')->default(0)->comment('Trị giá tối thiểu hóa đơn áp dụng');
            $table->double('max_order')->default(99999999999)->nullable()->comment('Trị giá tối đa hóa đơn áp dụng');
            $table->double('price')->default(0)->comment('Giá vận chuyển');
            $table->boolean('hidden')->default(false)->comment('Hiện/Ẩn');
            
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportations');
    }
}
