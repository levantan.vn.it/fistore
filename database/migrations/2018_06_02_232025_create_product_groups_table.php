<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->unsignedInteger('parent_id')->nullable()->comment('Ánh xạ đến nhóm cha');
            $table->string('title')->nullable()->comment('Tiêu đề cho SEO');
            $table->text('description')->nullable()->comment('Mô tả cho SEO');
            $table->boolean('pinned')->default(false)->comment('Hiển thị ngoài trang chủ');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_groups');
    }
}
