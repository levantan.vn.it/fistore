<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreeTrialTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_trial', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('product_id');
            $table->integer('quantity')->nullable()->comment('Số lượng');
            $table->datetime('started_at')->comment('Ngày bắt đầu');
            $table->datetime('expired_at')->comment('Ngày hết hạn');
            $table->string('target_url')->comment('Đường dẫn trỏ tới');
            $table->boolean('disabled')->default(false);

            $table->foreign('product_id')->references('id')->on('products')->onDelete('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_trial');
    }
}
