<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key')->unique()->comment('sadmns: Quản trị cao cấp; admns: Quản trị viên; mnger: Quản lý; author: Biên tập');
            $table->string('name');
            $table->integer('level')->nullable()->comment('Cấp bậc: Số càng nhỏ thì cấp bậc càng cao. Cao nhất là cấp 1 (Quản trị cao cấp)');
            $table->text('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
