<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_configs', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->comment('Tên site');
            $table->string('title')->comment('Tiêu đề site');
            $table->text('description')->nullable()->comment('Mô tả site');
            $table->text('keywords')->nullable()->comment('Từ khóa site');
            $table->string('domain')->nullable()->comment('Tên miền chính');

            $table->string('author')->nullable()->comment('Tên công ty, doanh nghiệp');
            $table->text('introduce')->nullable()->comment('Mô tả công ty, doanh nghiệp');
            $table->string('contact_email')->nullable()->comment('Email liên hệ, dùng để khách hàng liên hệ tới công ty');
            $table->string('support_email')->nullable()->comment('Email hỗ trợ khách hàng, dùng để liên hệ với khách hàng');
            $table->string('hotline_1')->nullable();
            $table->string('hotline_2')->nullable();
            $table->string('fax')->nullable();

            $table->integer('province_id')->nullable()->comment('Địa chỉ: Tỉnh thành');
            $table->integer('district_id')->nullable()->comment('Địa chỉ: Quận huyện');
            $table->integer('ward_id')->nullable()->comment('Địa chỉ: Phường xã');
            $table->string('address')->nullable()->comment('Địa chỉ: Số nhà, Tên đường, Phường');
            $table->text('map')->nullable()->comment('Mã nhúng bản đồ');

            $table->string('logo')->default('logo.png');
            $table->string('favicon')->default('favicon.png');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_configs');
    }
}
