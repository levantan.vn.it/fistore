<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs_api', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->nullable();
            $table->text('message')->nullable();
            $table->text('data')->nullable();
            $table->string('url',255)->nullable();
            $table->integer('status')->default(1);
            $table->integer('send_again')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs_api');
    }
}
