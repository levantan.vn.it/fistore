<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRealStockQuantityToProductVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_versions', function (Blueprint $table) {
            //
            $table->integer('real_stock_quantity')->nullable()->comment('Số lượng sản phẩm thực trên AFR');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_versions', function (Blueprint $table) {
            $table->dropColumn('real_stock_quantity');
        });
    }
}
