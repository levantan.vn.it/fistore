<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupon_relationships', function (Blueprint $table) {
            $table->unsignedInteger('target_id');
            $table->unsignedInteger('coupon_id');
            $table->string('target_type')->default('product');

            $table->foreign('coupon_id')->references('id')->on('coupons')->onDelete('CASCADE');
            $table->primary(['target_id', 'coupon_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_relationships');
    }
}
