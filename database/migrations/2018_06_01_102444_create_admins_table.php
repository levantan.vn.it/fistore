<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email', 250)->unique();
            $table->string('password')->nullable();
            $table->string('avatar')->default('user.png');

            $table->unsignedInteger('role_id')->default(2)->nullable()->comment('Ánh xạ đến nhóm quyền bảng roles');
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('SET NULL');

            $table->boolean('disabled')->default(false)->comment('True Nếu đã vô hiệu hóa');
            $table->rememberToken();
            $table->timestamps();
        });
        DB::statement('ALTER TABLE admins MODIFY COLUMN remember_token VARCHAR(255)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
