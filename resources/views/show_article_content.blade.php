@extends('layouts.master')

@section('title', $data->title ?? $data->name)
@section('description', $data->description)
@section('keywords', $data->tags)
@section('thumbnail', media_url('articles', $data->thumbnail))

@section('header')
	{{-- Owl Carousel --}}
	<link rel="stylesheet" href="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.css') }}" />
	<script src="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.js') }}"></script>
    <style type="text/css">
    	@media screen and (max-width: 760px){
			iframe {
				width: 100% !important;
				height: 480px !important;
			}
		}
		@media screen and (max-width: 480px){
			iframe {
				width: 100% !important;
				height: 320px !important;
			}
		}
    </style>
@stop

@section('main')
	{!! Breadcrumbs::render('article.detail', $data) !!}

	<div class="row mb-4">
		<div class="col-md-9">
			<div class="block-article-content">
				<div class="article-title">{{ $data->name }}</div>
				<div class="article-content">{!! $data->content !!}</div>
				@if($data->tags != '')
				<div class="article-tags">
					<span>Chủ đề</span>
					@foreach(explode(',', $data->tags) as $tag)
					@if($tag != '')
					<a href="{{ url('tim-kiem?keywords=' . $tag) }}">{{ $tag }}</a>
					@endif
					@endforeach
				</div>
				@endif
				{{-- <div class="facebook-comment"></div> --}}
			</div>

			{{-- <div class="block-article-related mt-4 mb-5">
				<div class="block-header">
					<div class="block-nav"></div>
					<div class="block-title">Bài viết liên quan</div>
				</div>
				<div class="block-body">
					<div class="owl-carousel article-related-slider">

						@foreach($relatedArticles as $item)
						<div class="item">
							<div class="article-item-4">
								<div class="item-thumbnail">
									<img src="{{ media_url('articles', $item->thumbnail) }}" alt="{{ $item->name }}">
								</div>
								<a href="{{ article_link($item) }}" class="item-title">{{ $item->name }}</a>
								<time class="item-time">Ngày {{ date('d/m/Y H:i') }}</time>
							</div>
						</div>
						@endforeach
						
					</div>            				
				</div>
			</div> --}}
		</div>
		<div class="col-md-3">
			@include('particles.aside_mosted_articles')
			@include('particles.aside_newest_articles')
		</div>
	</div>
@stop

@section('footer')
	<script>
		$('.article-related-slider').owlCarousel({
			loop:false,
			margin:20,
			nav:true,
			navText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
			navContainer: ".block-nav",
			dots:false,
			autoplay:false,
			smartSpeed:500,
			responsive:{
				0:{
					items:1
				},
				768:{
					items:3
				},
			},
		})

	</script>
@stop