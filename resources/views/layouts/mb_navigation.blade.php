<nav id="navMobile">
	<div class="left">
		<button type="button" class="btn btn-default" onclick="open_nav()">
			<svg id="i-menu" viewBox="0 0 32 32" width="22" height="22" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
				<path d="M4 8 L28 8 M4 16 L28 16 M4 24 L28 24" />
			</svg>
		</button>
	</div>
	<div class="center">
		<a href="{{ url('/') }}">
			<img src="{{ 'https://cdn.fistore.vn/media/' . $configs->logo }}" alt="{{ $configs->title }}">
		</a>
	</div>
	<div class="right">
		<a href="{{ route('cart.show') }}">
			<img src="{{ asset('images/cart-icon.png') }}">
			<span class="data-cart-count">{{ Cart::count() }}</span>
		</a>
		<span class="dropdown dropdown-hover">
			<div data-toggle="dropdown"><i class="fa fa-user-circle"></i></div>
			<div class="dropdown-menu dropdown-menu-right">
				@auth('customer')
				<a class="dropdown-item" href="{{ route('user.profile') }}">Hồ sơ cá nhân</a>
				<a class="dropdown-item" href="{{ route('user.order') }}">Quản lý đơn hàng</a>
				<a class="dropdown-item" href="{{ route('user.address_book') }}">Sổ địa chỉ</a>
				<a class="dropdown-item" href="{{ route('user.extra') }}">Quản lý điểm</a>
				<div class="dropdown-divider"></div>
				<a class="dropdown-item" href="{{ url('logout') }}">Đăng xuất</a>
				@else
				<a class="dropdown-item" data-toggle="modal" href="#loginModal">Đăng nhập</a>
				<a class="dropdown-item" href="{{ url('register') }}">Đăng ký</a>
				@endauth
			</div>
		</span>
	</div>
</nav>
