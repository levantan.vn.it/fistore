<nav id="navToggle" class="scrollbar">
	<div class="top">
		<img src="{{ 'https://cdn.fistore.vn/media/' . $configs->logo }}" alt="{{ $configs->title }}">
	</div>
	<form class="middle" action="{{ route('search') }}" method="GET">
		<div class="form-group">
			<div class="input-group">
				<input type="search" class="form-control" name="keywords" placeholder="Tìm kiếm" value="{{ request()->keywords }}">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default" onclick="open_nav()">
						<svg id="i-search" viewBox="0 0 32 32" width="22" height="22" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5">
							<circle cx="14" cy="14" r="12" />
							<path d="M23 23 L30 30"  />
						</svg>
					</button>
				</span>
			</div>
		</div>
	</form>
	<div class="bottom">
		<ul class="nav nav-pills nav-justified" role="tablist">
			<li class="nav-item">
				<a class="nav-link active" data-toggle="pill" href="#pillsMenu" role="tab" aria-controls="pillsMenu" aria-selected="true">
					<i class="fa fa-list-ul fa-lg"></i>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" data-toggle="pill" href="#pillsAuth" role="tab" aria-controls="pillsAuth" aria-selected="true">
					<i class="fa fa-user-circle-o fa-lg"></i>
				</a>
			</li>
			<li class="nav-item close-nav">
				<a class="nav-link" onclick="close_nav()">
					<svg id="i-close" viewBox="0 0 32 32" width="15" height="15" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2.5">
						<path d="M2 30 L30 2 M30 30 L2 2" />
					</svg>
				</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane fade show active" id="pillsMenu" role="tabpanel">
				<div class="item single">
					<a href="{{ url('/') }}">
						<i class="fa fa-angle-right"></i>&emsp;Trang chủ
					</a>
				</div>
				<div class="item">
					<a data-toggle="collapse" data-parent="#pillsMenu" href="#menuKhuyenMai" aria-expanded="false" class="collapsed">
						<i class="fa fa-angle-right"></i>&emsp;Khuyến mãi
					</a>
					<ul id="menuKhuyenMai" class="collapse" role="tabpanel">
						@foreach($hotshopa->fk_childs as $key => $item)
							<li><a href="{{ group_link($item->slug) }}">{{ $item->name }}</a></li>
						@endforeach
						<li><a href="{{ route('freetrial') }}">Trải nghiệm 0 đồng</a></li>
						<li><a href="{{ route('combo') }}">Combo</a></li>
						<li><a href="{{ route('hotdeal') }}">Pick Me Now</a></li>
					</ul>
				</div>
				<div class="item">
					<a data-toggle="collapse" data-parent="#pillsMenu" href="#menuCategory{{ $category_1->id }}" aria-expanded="false" class="collapsed">
						<i class="fa fa-angle-right"></i>&emsp;{{ $category_1->name }}
					</a>
					<ul id="menuCategory{{ $category_1->id }}"class="collapse"role="tabpanel">
						@foreach($category_1->fk_childs as $item)
						<li><a href="{{ category_link($item->slug) }}">{{ $item->name }}</a></li>
						@endforeach
						<li><a href="{{ category_link($category_1->slug) }}">Tất cả</a></li>
					</ul>
				</div>
				<div class="item">
					<a data-toggle="collapse" data-parent="#pillsMenu" href="#menuCategory{{ $category_2->id }}" aria-expanded="false" class="collapsed">
						<i class="fa fa-angle-right"></i>&emsp;{{ $category_2->name }}
					</a>
					<ul id="menuCategory{{ $category_2->id }}"class="collapse"role="tabpanel">
						@foreach($category_2->fk_childs as $item)
						<li><a href="{{ category_link($item->slug) }}">{{ $item->name }}</a></li>
						@endforeach
						<li><a href="{{ category_link($category_2->slug) }}">Tất cả</a></li>
					</ul>
				</div>
				<div class="item">
					<a data-toggle="collapse" data-parent="#pillsMenu" href="#menuCategory{{ $category_3->id }}" aria-expanded="false" class="collapsed">
						<i class="fa fa-angle-right"></i>&emsp;{{ $category_3->name }}
					</a>
					<ul id="menuCategory{{ $category_3->id }}"class="collapse"role="tabpanel">
						@foreach($category_3->fk_childs as $item)
						<li><a href="{{ category_link($item->slug) }}">{{ $item->name }}</a></li>
						@endforeach
						<li><a href="{{ category_link($category_3->slug) }}">Tất cả</a></li>
					</ul>
				</div>
				<div class="item">
					<a data-toggle="collapse" data-parent="#pillsMenu" href="#menuThuongHieu" aria-expanded="false" class="collapsed">
						<i class="fa fa-angle-right"></i>&emsp;Thương hiệu
					</a>
					<ul id="menuThuongHieu" class="collapse" role="tabpanel">
						@foreach(get_all_brands() as $item)
						<li><a href="{{ brand_link($item->slug) }}">{{ $item->name }}</a></li>
						@endforeach
					</ul>
				</div>
				<div class="item">
					<a data-toggle="collapse" data-parent="#pillsMenu" href="#menuCategory{{ $category_4->id }}" aria-expanded="false" class="collapsed">
						<i class="fa fa-angle-right"></i>&emsp;{{ $category_4->name }}
					</a>
					<ul id="menuCategory{{ $category_4->id }}" class="collapse" role="tabpanel">
						@foreach($category_4->fk_childs as $item)
						<li><a href="{{ category_link($item->slug) }}">{{ $item->name }}</a></li>
						@endforeach
						<li><a href="{{ category_link($category_4->slug) }}">Tất cả</a></li>
					</ul>
				</div>
				
				<div class="dropdown-divider"></div>

				<div class="item single">
					<a href="{{ url('page/dich-vu') }}">
						<i class="fa fa-angle-right"></i>&emsp;Dịch vụ
					</a>
				</div>

				<div class="item single">
					<a href="{{ url('lien-he') }}">
						<i class="fa fa-angle-right"></i>&emsp;Liên hệ
					</a>
				</div>
			</div>
			<div class="tab-pane fade" id="pillsAuth" role="tabpanel">
				@auth('customer')
				<div class="item single">
					<a href="{{ route('user.profile') }}">
						<i class="fa fa-angle-right"></i>&emsp;Hồ sơ cá nhân
					</a>
				</div>
				<div class="item single">
					<a href="{{ route('user.order') }}">
						<i class="fa fa-angle-right"></i>&emsp;Quản lý đơn hàng
					</a>
				</div>
				<div class="item single">
					<a href="{{ route('user.address_book') }}">
						<i class="fa fa-angle-right"></i>&emsp;Sổ địa chỉ
					</a>
				</div>
				<div class="item single">
					<a href="{{ route('user.extra') }}">
						<i class="fa fa-angle-right"></i>&emsp;Quản lý điểm
					</a>
				</div>
				<div class="dropdown-divider"></div>
				<div class="item single">
					<a href="{{ url('logout') }}">
						<i class="fa fa-angle-right"></i>&emsp;Đăng xuất
					</a>
				</div>
				@else
				<div class="item single">
					<a data-toggle="modal" href="#loginModal">
						<i class="fa fa-angle-right"></i>&emsp;Đăng nhập
					</a>
				</div>
				<div class="item single">
					<a href="{{ route('register') }}">
						<i class="fa fa-angle-right"></i>&emsp;Đăng ký
					</a>
				</div>
				@endauth
			</div>
		</div>
	</div>
</nav>
