<div id="nav-target"></div>
<nav id="navigation">
	<div class="container">
		<ul class="nav-wrap">
			<li class="nav-item">
				<a class="nav-link" href="{{ url('/') }}"><i class="fa fa-home fa-lg"></i></a>
			</li>
			{{-- <li class="nav-item dropdown dropdown-hover">
				<a href="{{ url('khuyen-mai') }}" class="nav-link" data-toggle="dropdown">Hot Shopa</a>
				<div class="dropdown-menu">
					<a class="dropdown-item" href="{{ route('hotdeal') }}">Hot Deal</a>
					<a class="dropdown-item" href="{{ route('freetrial') }}">Trải nghiệm 0 đồng</a>
					<a class="dropdown-item" href="{{ route('combo') }}">Combo sản phẩm</a>
					<div class="cat-thumbnail">
						<img src="{{ asset('images/sale-cat.jpg') }}" alt="Khuyến mãi" width="350">
					</div>
				</div>
			</li> --}}

			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ group_link($hotshopa->slug) }}" class="nav-link" data-toggle="dropdown">{{ $hotshopa->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@if(!get_group(6)->hidden)
						<a class="dropdown-item" href="{{ group_link(get_group(6)->slug) }}">{{ get_group(6)->name }}</a>
						@endif
						<a class="dropdown-item" href="{{ route('combo') }}">Combo sản phẩm</a>
						<a class="dropdown-item" href="{{ route('hotdeal') }}">Pick Me Now</a>
						@if(!get_group(12)->hidden)
						<a class="dropdown-item" href="{{ group_link(get_group(11)->slug) }}">{{ get_group(11)->name }}</a>
						@endif
						@if(!get_group(9)->hidden)
						<a class="dropdown-item" href="{{ group_link(get_group(9)->slug) }}">{{ get_group(9)->name }}</a>
						@endif
						@if(!get_group(8)->hidden)
						<a class="dropdown-item" href="{{ group_link(get_group(8)->slug) }}">{{ get_group(8)->name }}</a>
						@endif
						<a class="dropdown-item" href="{{ route('freetrial') }}">Trải nghiệm 0 đồng</a>
					</div>
					<div class="cat-thumbnail">
						<img src="{{ asset('images/sale-cat.jpg') }}" alt="Khuyến mãi" width="350">
					</div>
				</div>
			</li>

			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ category_link($category_1->slug) }}" class="nav-link" data-toggle="dropdown">{{ $category_1->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach($category_1->fk_childs as $key => $item)
							<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
							@if($key > 14)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						<img src="{{ media_url('product-categories', $category_1->thumbnail) }}" alt="{{ $category_1->name }}" width="350">
					</div>
				</div>
			</li>
			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ category_link($category_2->slug) }}" class="nav-link" data-toggle="dropdown">{{ $category_2->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach($category_2->fk_childs as $key => $item)
							<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
							@if($key > 14)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						<img src="{{ media_url('product-categories', $category_2->thumbnail) }}" alt="{{ $category_2->name }}" width="350">
					</div>
				</div>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="{{ url('/phan-loai/mat-na') }}">Mặt nạ</a>
			</li>
			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ route('brand.list') }}" class="nav-link" data-toggle="dropdown">Thương hiệu</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach(get_all_brands() as $key => $item)
							<a class="dropdown-item" href="{{ brand_link($item->slug) }}">{{ $item->name }}</a>
							@if($key > 26)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						<img src="{{ asset('images/branding.jpg') }}" alt="Thương hiệu" width="350">
					</div>
				</div>
			</li>
			<li class="nav-item dropdown dropdown-hover">
				<a href="{{ category_link($category_4->slug) }}" class="nav-link" data-toggle="dropdown">{{ $category_4->name }}</a>
				<div class="dropdown-menu">
					<div class="cat-list">
						@foreach($category_4->fk_childs as $key => $item)
							<a class="dropdown-item" href="{{ category_link($item->slug) }}">{{ $item->name }}</a>
							@if($key > 14)
								@break
							@endif
						@endforeach
					</div>
					<div class="cat-thumbnail">
						<img src="{{ media_url('product-categories', $category_4->thumbnail) }}" alt="{{ $category_4->name }}" width="350">
					</div>
				</div>
			</li>
		</ul>
	</div>
</nav>
