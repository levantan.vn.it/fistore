<nav id="status">
	<div class="container">
		<div class="row">
			<div class="col-left col-6">
				<span>Mạng xã hội:
					<a href="https://www.facebook.com/fistore.vn/"><img src="{{ asset('images/status-ico/facebook-icon.png') }}" width="15"></a>
					<a href="https://www.instagram.com/fistore.vn/"><img src="{{ asset('images/status-ico/instagram-icon.png') }}" width="15"></a>
					<a href="#"><img src="{{ asset('images/status-ico/zalo-icon.png') }}" width="15"></a>
					<a href="https://shopee.vn/shopavietnam"><img src="{{ asset('images/status-ico/shopee-icon.png') }}" width="15"></a>
					<a href="https://www.youtube.com/channel/UCNKQiOMdCOxX_IbIvs-AiCQ"><img src="{{ asset('images/status-ico/youtube-icon.png') }}" width="15"></a>
				</span>
				<span>Hotline: <a href="tel:{{ $configs->hotline_1 }}">{{ $configs->hotline_1 }}</a></span>
			</div>
			<div class="col-right col-6">
				@auth('customer')
				<span class="dropdown dropdown-hover">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::guard('customer')->user()->name }}</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" href="{{ route('user.profile') }}">Hồ sơ cá nhân</a>
						<a class="dropdown-item" href="{{ route('user.order') }}">Quản lý đơn hàng</a>
						<a class="dropdown-item" href="{{ route('user.address_book') }}">Sổ địa chỉ</a>
						<a class="dropdown-item" href="{{ route('user.extra') }}">Quản lý điểm</a>
						<div class="dropdown-divider"></div>
						<a class="dropdown-item" href="{{ url('logout') }}">Đăng xuất</a>
					</div>
				</span>
				@else
				<span class="dropdown dropdown-hover">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Tài khoản</a>
					<div class="dropdown-menu">
						<a class="dropdown-item" data-toggle="modal" href="#loginModal">Đăng nhập</a>
						<a class="dropdown-item" href="{{ url('register') }}">Đăng ký</a>
					</div>
				</span>
				@endauth
				<span>
					<a href="{{ route('cart.show') }}"><i class="fa fa-shopping-bag fa-fw"></i> Giỏ hàng (<b class="data-cart-count">{{ Cart::count() }}</b>)</a>
				</span>
				<span>
					@auth('customer')
					<a href="{{ route('user.order') }}"><i class="fa fa-file-text fa-fw"></i> Theo dõi đơn hàng</a>
					@else
					<a data-toggle="modal" href="#loginModal"><i class="fa fa-file-text fa-fw"></i> Theo dõi đơn hàng</a>
					@endauth
				</span>
			</div>
		</div>
	</div>
</nav>

<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="header-logo">
					<a href="{{ url('/') }}">
						<img src="{{ 'https://cdn.fistore.vn/media/' . $configs->logo }}" alt="{{ $configs->title }}">
						<h1 class="d-none">{{ $configs->title }}</h1>
					</a>
				</div>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form action="{{ route('search') }}" method="GET" class="search-box">
					<div class="input-group">
						<select class="custom-select" name="category">
							<option value="">Tất cả</option>
							@foreach(get_all_categories() as $cat)
							<option value="{{ $cat->slug }}"{!! request()->category == $cat->slug ? ' selected' : null !!}>{{ $cat->name }}</option>
							@endforeach
						</select>
						<input type="search" name="keywords" class="form-control" placeholder="Tìm sản phẩm, thương hiệu bạn mong muốn..." value="{{ request()->keywords }}">
						<div class="input-group-append">
							<button class="btn btn-primary">
								<i class="fa fa-search"></i>
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</header>
