<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	{{-- Description --}}
	@hasSection('description')
	<meta name="description" content="@yield('description')">
	@else
	<meta name="description" content="{{ $configs->description }}">
	@endif

	{{-- Keywords --}}
	@hasSection('keywords')
	<meta name="keywords" content="@yield('keywords')"/>
	@else
	<meta name="keywords" content="{{ $configs->keywords }}"/>
	@endif

	<meta name="author" content="{{ $configs->author }}"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	{{-- SEO Preview Meta --}}
	<meta property="og:url" content="{{ url()->current() }}" />
	<meta property="og:type" content="website" />
	@hasSection('title')
	<meta property="og:title" content="@yield('title') - {{ $configs->title }}" />
	@else
	<meta property="og:title" content="{{ $configs->title }}" />
	@endif
	@hasSection('description')
	<meta property="og:description" content="@yield('description')" />
	@else
	<meta property="og:description" content="{{ $configs->description }}" />
	@endif
	@hasSection('thumbnail')
	<meta property="og:image" content="@yield('thumbnail')" />
	@else
	<meta property="og:image" content="{{ 'https://cdn.fistore.vn/media/' . $configs->logo }}" />
	@endif

	{{-- Link --}}
	<link rel="icon" href="{{'https://cdn.fistore.vn/media/' . $configs->favicon }}">

	{{-- Title --}}
	@hasSection('title')
		<title>@yield('title') - {{ $configs->title }}</title>
	@else
		<title>{{ $configs->title }}</title>
	@endif

	{{-- JQuery --}}
	<script src="{{ asset('libs/jquery/jquery-3.3.1.min.js') }}"></script>

	{{-- Google Jquery Library --}}
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

	{{-- JQuery UI --}}
	<link rel="stylesheet" href="{{ asset('libs/jquery-ui/1.12.1/jquery-ui.min.css') }}">
	<script src="{{ asset('libs/jquery-ui/1.12.1/jquery-ui.min.js') }}"></script>

	{{-- Bootstrap --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/bootstrap/4-4.0.0-beta/css/bootstrap.min.css') }}"/>
	<script src="{{ asset('libs/popper/1.14.3/popper.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('libs/bootstrap/4-4.0.0-beta/js/bootstrap.min.js') }}"></script>

	{{-- Sweet Alert --}}
	<script src="{{ asset('libs/sweetalert/2v7.0.9/sweetalert.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/sweetalert/2v7.0.9/sweetalert.min.css') }}">

	{{-- Font Awesome --}}
	<link rel="stylesheet" href="{{ asset('libs/font-awesome/4.7.0/css/font-awesome.min.css') }}">

	{{-- Font Valera Round --}}
	<link href="https://fonts.googleapis.com/css?family=Varela+Round&amp;subset=latin-ext,vietnamese" rel="stylesheet">

	{{-- Countdown --}}
	<script src="{{ asset('js/countdown.handler.js') }}"></script>

	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- Header --}}
	@yield('header')

	{{-- StyleSheet --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('css/mobilebar.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

	<script>
		var base_url = "{{ url('/') }}";
	</script>
	
	@php
		$hotshopa = get_group(2);
		$category_1 = get_category(11);
		$category_2 = get_category(8);
		$category_3 = get_category(9);
		$category_4 = get_category(10);
	@endphp

	<!-- Google API -->
	@include('api.google_header')

	<!-- harafunnel -->
	<script src="//harafunnel.com/widget/1882121868702051.js" async="async"></script>
	<style>
		.fb_customer_chat_bubble_animated_no_badge {
		    bottom: 60pt !important;
		    right: 6pt !important;
		}
	</style>

	@include('api.facebook_pixel')
	@include('api.mtag.mtag')
</head>
<body class="scrollbar">
	{{-- Facebook Plugin --}}
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
		  FB.init({
		    appId      : '238904950283826',
		    cookie     : true,
		    xfbml      : true,
		    version    : '3.1'
		  });
		    
		  FB.AppEvents.logPageView();   
		    
		};
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.1&appId=238904950283826&autoLogAppEvents=1';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

	<!-- Google API -->
	@include('api.google_body')
	
	<div class="overlay" onclick="close_nav()"></div>

	{{-- Mobile Navigation --}}
	@include('layouts.mb_navigation')

	{{-- Header --}}
	@include('layouts.header')

	{{-- Navigation --}}
	@include('layouts.navigation')

	{{-- Main --}}
	<div class="container" id="mainContainer">
		@yield('main')

		{{-- Footer --}}
		@include('layouts.footer')
	</div>

	{{-- Mobile Navigation Toggle --}}
	@include('layouts.mb_navigation_toggle')

	@include('particles.fixedbox')

	<script src="{{ asset('js/main.handler.js') }}"></script>
	<script>
		$(document).ready(function(){
			$(window).scroll(function(){
				if($(window).scrollTop() >= $("#nav-target").offset().top - 5)
					$("nav#navigation").addClass("fixed");
				else
					$("nav#navigation").removeClass("fixed");
			});
		});
	</script>
	@yield('footer')
	
	@if(Auth::guard('customer')->guest())
		@include('auth.login_form')
	@endif

	@if(Session::has('alert'))
		<script>
			swal("{{ Session::get('title') }}", "{{ Session::get('message') }}", "{{ Session::get('alert') }}");
		</script>
		{{ Session::forget('alert') }}
	@endif

	@include('particles.popup')

	{{-- Session --}}
	<script src="{{ asset('libs/jquery-session/jquery-session.js') }}"></script>
	@if(!empty(session('product_delivery')) && !empty(session('order')))
	<script type="text/javascript">
		/**
		 * Ajax delivery order to AFR
		 */
		
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		$.ajax({
			url : base_url + '/ajax/deliveryOrder',
			type : 'POST',
			success : function (data) {
				console.log(data);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(thrownError);
			}
		});
	</script>
	@endif
</body>
</html>
