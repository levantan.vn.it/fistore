@extends('layouts.clear')

@section('title', 'Đăng nhập')

@section('header')  
    {{-- Jquery Validation --}}
    <script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

@section('main')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="block-auth">
                <div class="card my-5">
                    <div class="card-header">ĐĂNG NHẬP</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}" id="loginForm">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-sm-4 col-form-label text-md-right">Địa chỉ Email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Mật khẩu</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">&emsp;</label>
                                <div class="col-md-6 offset-md-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Nhớ phiên đăng nhập
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <label class="col-md-4 col-form-label text-md-right">&emsp;</label>
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        ĐĂNG NHẬP
                                    </button>

                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Quên mật khẩu?
                                    </a>
                                </div>
                            </div>
                        </form>
                        <hr class="my-4">
                        <div class="form-group row">
                            <label for="email" class="col-md-4 text-md-right"></label>
                            <div class="col-md-6">
                                <div class="fb-login-button">
                                    <a href="{{ url('login/facebook/redirect') }}"><i class="fa fa-facebook-square fa-fw"></i> Đăng nhập bằng Facebook</a>
                                </div>
                                <div class="gg-login-button">
                                    <a href="{{ url('login/google/redirect') }}"><i class="fa fa-google-plus-square fa-fw"></i> Đăng nhập bằng Google</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        $("#loginForm").validate();
    </script>
@stop