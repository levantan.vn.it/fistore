@extends('layouts.clear')

@section('title', 'Đăng ký')

@section('header')  
    {{-- Jquery Validation --}}
    <script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

@section('main')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="block-auth">
                <div class="card my-5">
                    <div class="card-header">ĐĂNG KÝ</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}" id="registerForm" autocomplete="off">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Họ tên</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Địa chỉ Email</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">Mật khẩu</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Nhập lại Mật khẩu</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <label class="col-md-4 col-form-label text-md-right">&emsp;</label>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        ĐĂNG KÝ
                                    </button>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 text-md-right">
                                </label>
                                <div class="col-md-6">
                                    <hr class="my-5">
                                    <div class="fb-login-button">
                                        <a href="{{ url('login/facebook/redirect') }}"><i class="fa fa-facebook-square fa-fw"></i> Đăng nhập bằng Facebook</a>
                                    </div>
                                    <div class="gg-login-button">
                                        <a href="{{ url('login/google/redirect') }}"><i class="fa fa-google-plus-square fa-fw"></i> Đăng nhập bằng Google</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        $("#registerForm").validate({
            rules: {
                password_confirmation: {
                    equalTo: "#password"
                }
            },
            messages: {
                password_confirmation: "Mật khẩu nhập lại không trùng"
            }
        });
    </script>
@stop