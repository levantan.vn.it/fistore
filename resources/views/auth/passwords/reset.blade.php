@extends('layouts.clear')

@section('title', 'Cập nhật mật khẩu mới')

@section('header')  
  {{-- Jquery Validation --}}
  <script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
  <script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

@section('main')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           <div class="block-auth">
               <div class="card my-5">
                   <div class="card-header">CẬP NHẬT MẬT KHẨU MỚI</div>
                   <div class="card-body">
                       <form method="POST" action="{{ route('password.request') }}" id="resetForm">
                           @csrf

                           <input type="hidden" name="token" value="{{ $token }}">

                           <div class="form-group row">
                               <label for="email" class="col-md-4 col-form-label text-md-right">Địa chỉ Email</label>

                               <div class="col-md-6">
                                   <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                   @if ($errors->has('email'))
                                       <span class="invalid-feedback">
                                           <strong>{{ $errors->first('email') }}</strong>
                                       </span>
                                   @endif
                               </div>
                           </div>

                           <div class="form-group row">
                               <label for="password" class="col-md-4 col-form-label text-md-right">Mật khẩu</label>

                               <div class="col-md-6">
                                   <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                   @if ($errors->has('password'))
                                       <span class="invalid-feedback">
                                           <strong>{{ $errors->first('password') }}</strong>
                                       </span>
                                   @endif
                               </div>
                           </div>

                           <div class="form-group row">
                               <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Nhập lại mật khẩu</label>

                               <div class="col-md-6">
                                   <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                               </div>
                           </div>

                           <div class="form-group row mb-0">
                               <label class="col-md-4 col-form-label text-md-right">&emsp;</label>
                               <div class="col-md-6 offset-md-4">
                                   <button type="submit" class="btn btn-primary">
                                       CẬP NHẬT MẬT KHẨU MỚI
                                   </button>
                               </div>
                           </div>
                       </form>
                   </div>
               </div>
           </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    <script>
        $("#resetForm").validate({
          rules: {
              password_confirmation: {
                  equalTo: "#password"
              }
          },
          messages: {
              password_confirmation: "Mật khẩu nhập lại không trùng"
          }
        });
    </script>
@stop