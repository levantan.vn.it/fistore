<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ media_url('', $configs->favicon) }}">

    <title>Đăng nhập quản trị</title>

    {{-- JQuery --}}
    <script src="{{ asset('libs/jquery/jquery-3.3.1.min.js') }}"></script>

    {{-- Bootstrap --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('libs/bootstrap/4-4.0.0-beta/css/bootstrap.min.css') }}"/>
    <script src="{{ asset('libs/popper/1.14.3/popper.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/4-4.0.0-beta/js/bootstrap.min.js') }}"></script>

    {{-- Jquery Validation --}}
    <script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

    {{-- Font Awesome --}}
    <link rel="stylesheet" href="{{ asset('libs/font-awesome/4.7.0/css/font-awesome.min.css') }}">

    {{-- StyleSheet --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/admin/auth.css') }}">
</head>
<body class="scrollbar login-page">
    <div class="container">
        <form action="{{ route('admin.login') }}" method="POST" autocomplete="off">
            @csrf
            <div class="logo">
                <a href="{{ url('') }}">
                    <img src="{{ media_url('', $configs->logo) }}">
                </a>
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope fa-lg fa-fw"></i></span>
                    <input type="email" name="email" placeholder="Email đăng nhập" class="form-control{{ $errors->has('email') ? ' is-invalid' : null }}" required autocomplete="off" autofocus>
                </div>
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-lock fa-lg fa-fw"></i></span>
                    <input type="password" name="password" placeholder="Mật khẩu" class="form-control{{ $errors->has('password') ? ' is-invalid' : null }}" autocomplete="off" required>
                </div>
                @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group d-flex justify-content-between py-2">
                <label class="custom-control custom-checkbox">
                    <input type="checkbox" name="remember" class="custom-control-input" {{ old('remember') ? 'checked' : '' }}>
                    <span class="custom-control-indicator"></span>
                    <span class="custom-control-description">Nhớ phiên đăng nhập</span>
                </label>
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Đăng nhập</button>
            </div>
        </form>
        <span class="copyright">Copyright © 2017 {{ env('APP_DOMAIN') }}. All Rights Reserved.</span>
    </div>
</body>
