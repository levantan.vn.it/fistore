@extends('layouts.clear')

@section('title', 'Thanh toán & Đặt hàng')
@section('description', null)
@section('keywords', null)

@section('header')
@stop
@section('pixel_event')
fbq('track', 'AddPaymentInfo');
@stop
@section('main')
{{-- Checkout Progress --}}
<div class="checkout-progress">
	<li class="d-none d-md-flex"><span class="step">1</span>Giỏ hàng</li>
	<li class="d-none d-md-flex"><span class="step">2</span>Địa chỉ giao hàng</li>
	<li class="active"><span class="step">3</span>Thanh toán & Đặt hàng</li>
</div>

<div class="row mb-5">
	<div class="col-lg-7 mx-auto mb-3">
		<div class="block-checkout">
			<form action="{{ route('checkout.payment.post') }}" method="POST" id="paymentForm">
				@csrf
				<div class="block-header">
					<div class="block-title">CHỌN HÌNH THỨC THANH TOÁN</div>
				</div>
				<div class="block-body">
					<div class="form-group">
						<label class="custom-control custom-radio">
							<input type="radio" name="payment_method" value="cod" class="custom-control-input" checked required>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Thanh toán khi nhận hàng (COD)</span>
						</label>
					</div>

					{{-- <div class="form-group">
						<label class="custom-control custom-radio">
							<input type="radio" name="payment_method" value="international-card" class="custom-control-input">
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Thanh toán bằng thẻ quốc tế Visa, Master, JCB</span>
						</label>
					</div> --}}
					<div class="form-group" id="internationalWrapper" style="display: none;padding: 1rem;border: 1px solid #dedede;border-radius: 7px;background-color: #fafafa;">
						<div class="row">
							<div class="col-md-7">
								<div class="form-group">
									<label for="card_number">Số thẻ</label>
									<input class="form-control" type="text" name="card_number" placeholder="VD: 1234 4567 7890 0123" autocomplete="off" required disabled>
								</div>
								<div class="form-group">
									<label for="card_name">Tên in trên thẻ</label>
									<input class="form-control" type="text" name="card_name" placeholder="VD: NGUYEN VAN A" autocomplete="off" required disabled>
								</div>
								<div class="form-group">
									<label for="card_expiry">Ngày hết hạn</label>
									<input class="form-control" type="text" name="card_expiry" maxlength="5" placeholder="MM/YY" autocomplete="off" disabled style="max-width: 10rem" required>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-8">
											<label for="card_cvc">Mã bảo mật</label>
											<input class="form-control" type="text" name="card_cvc" maxlength="3" placeholder="VD: 123" autocomplete="off" disabled style="max-width: 10rem" required>
										</div>
										<div class="text-right col-4">
											<label class="d-block" for="card_cvc">&emsp;</label>
											<img src="{{ asset('images/internal/cvc-hint.png') }}" width="70">
										</div>
									</div>												
								</div>
							</div>
							<div class="col-md-5">
								<img src="{{ asset('images/internal/card-guide.png') }}" width="100%">
							</div>
						</div>
					</div>

					{{-- <div class="form-group">
						<label class="custom-control custom-radio">
							<input type="radio" name="payment_method" value="atm-card" class="custom-control-input">
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Thẻ ATM nội địa/Internet Banking</span>
						</label>
					</div> --}}
					<div class="form-group" id="atmWrapper" style="display: none;padding: 1rem;border: 1px solid #dedede;border-radius: 7px;background-color: #fafafa;">
						<div class="row" style="margin-left: -5px;margin-right: -5px;">
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="vietcombank" class="item-input" disabled required>
									<img class="item-img" src="{{ asset('images/payment/vietcombank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="sacombank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/sacombank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="acb" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/acb.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="vietinbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/vietinbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="agribank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/agribank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="bidv" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/bidv.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="abbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/abbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="bacabank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/bacabank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="baoviet" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/baoviet.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="donga" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/donga.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="eximbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/eximbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="gpbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/gpbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="hdbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/hdbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="lienvietpostbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/lienvietpostbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="maritimebank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/maritimebank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="mb" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/mb.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="nama" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/nama.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="ncb" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/ncb.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="ocb" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/ocb.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="oceanbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/oceanbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="pgbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/pgbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="saigonbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/saigonbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="seabank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/seabank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="shb" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/shb.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="techcombank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/techcombank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="tpbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/tpbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="vib" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/vib.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="vietabank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/vietabank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
							<div class="item-ticker-col col-md-2 col-4">
								<label class="item-ticker">
									<input type="radio" name="atm_bank" value="vpbank" class="item-input" disabled>
									<img class="item-img" src="{{ asset('images/payment/vpbank.jpg') }}" width="100%">
									<span class="item-hint"></span>
								</label>
							</div>
						</div>
					</div>
					<label id="atm_bank-error" class="error" for="atm_bank" style="display: none"></label>
					<label id="payment_method-error" class="error" for="payment_method" style="display: none"></label>
				</div>
				<div class="form-group">
					<div class="address-book">
						<div>{{ session('checkout')->name }}</div>
						<div>Điện thoại: {{ session('checkout')->phone }}</div>

						<div>Địa chỉ: {{ session('checkout')->address }}, @if(session()->get('ward_id') && strlen(session()->get('ward_id')) > 0){{ get_ward(session('checkout')->ward_id)->name }} ,@endif {{ get_district(session('checkout')->district_id)->name }}, {{ get_province(session('checkout')->province_id)->name }}</div>
						<div>{{ session('checkout')->address_type ? 'Công ty/Cơ quan' : 'Nhà riêng/Chung cư' }}</div>
					</div>
				</div>
				<div class="form-group">
					<label for="note">Ghi chú</label>
					<textarea name="note" rows="3" class="form-control"></textarea>
					<span class="help-block">Tối đa 200 kí tự</span>
				</div>
				<div class="form-group mt-0 mt-md-5">
					<div class="checkout-button">
						<button class="btn btn-primary btn-block btn-lg" id="btnCheckout">ĐẶT HÀNG</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="col-lg-3 mx-auto mb-4">
		@include('particles.aside_cart')
	</div>
</div>

@stop

@section('footer')
<script>
	$("#paymentForm").validate({
		ignore:'',
		rules: {
			card_number: {
				creditcard: true
			},
			atm_bank: {
				required: true
			}
		},
		messages: {
			card_number: {
				creditcard: "Số thẻ không đúng định dạng"
			},
			atm_bank: {
				required: "Vui lòng chọn một ngân hàng thanh toán."
			}
		}
	});
	$('#paymentForm').submit(function (e) {
		$('#btnCheckout').html('<i class="fa fa-fw fa-pulse fa-spinner"></i> ĐẶT HÀNG')
			.prop('disabled', true);

	})
</script>
<script>
	$("input[name=payment_method]").change(function() {
		if($(this).val() == "international-card"){
			$("#internationalWrapper").slideDown();
			$("#internationalWrapper").find("input").attr("disabled", false);
			$("#atmWrapper").slideUp();
			$("#atmWrapper").find("input").attr("disabled", true);
		}else if($(this).val() == "atm-card"){
			$("#internationalWrapper").slideUp();
			$("#internationalWrapper").find("input").attr("disabled", true);
			$("#atmWrapper").slideDown();
			$("#atmWrapper").find("input").attr("disabled", false);
		}else{
			$("#internationalWrapper").slideUp();
			$("#internationalWrapper").find("input").attr("disabled", true);
			$("#atmWrapper").slideUp();
			$("#atmWrapper").find("input").attr("disabled", true);
		}
	});
</script>
@stop