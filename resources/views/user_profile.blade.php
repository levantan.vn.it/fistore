@extends('layouts.master')

@section('title', 'Thông tin cá nhân')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('user.profile') !!}

	<div class="row">
		<div class="col-md-3">
			@include('particles.aside_user')
		</div>
		<div class="col-md-9">
			<div class="block-user mt-4 mb-5">
				<div class="block-header">
					<div class="block-title">Thông tin tài khoản</div>
				</div>
				<div class="block-body">
					<form action="{{ route('user.profile.update') }}" method="POST" id="profileForm">
						@csrf
						<div class="form-group row mb-4">
							<label for="name" class="col-md-2 align-self-center">Họ tên <span class="required">*</span></label>
							<div class="col-md-7">
								<input type="text" name="name" value="{{ $data->name }}" placeholder="Nhập họ tên" class="form-control" required>
							</div>
						</div>

						<div class="form-group row mb-4">
							<label for="email" class="col-md-2 align-self-center">Email <span class="required">*</span></label>
							<div class="col-md-7">
								<input type="email" name="email" value="{{ $data->email }}" placeholder="Nhập địa chỉ email" class="form-control" required>
							</div>
						</div>

						<div class="form-group row mb-4">
							<label for="phone" class="col-md-2 align-self-center">Số điện thoại</label>
							<div class="col-md-7">
								<input type="text" name="phone" value="{{ $data->phone }}" placeholder="Nhập số điện thoại" class="form-control">
							</div>
						</div>

						<div class="form-group row mb-4">
							<label for="gender" class="col-md-2 align-self-center">Giới tính</label>
							<div class="col-md-7">
								<label class="custom-control custom-radio">
									<input type="radio" name="gender" value="1" class="custom-control-input"{!! !is_null($data->gender) && $data->gender == 1 ? ' checked' : null !!}>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Nam</span>
								</label>
								&emsp;&emsp;&emsp;&emsp;
								<label class="custom-control custom-radio">
									<input type="radio" name="gender" value="0" class="custom-control-input"{!! !is_null($data->gender) && $data->gender == 0 ? ' checked' : null !!}>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Nữ</span>
								</label>
							</div>
						</div>

						<div class="form-group row mb-4">
							<label for="birthday" class="col-md-2">Ngày sinh</label>
							<div class="col-md-7">
								<div class="row row-mx-10">
									<div class="col-md-4">
										<select name="day" id="day" class="custom-select form-control">
											<option value="">Ngày</option>
											@for($i = 1; $i <= 31; $i++)
											<option value="{{ $i }}"{{ !is_null($data->date_of_birth) && date('d', strtotime($data->date_of_birth)) == $i ? ' selected' : null }}>{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-4">
										<select name="month" id="month" class="custom-select form-control">
											<option value="">Tháng</option>
											@for($i = 1; $i <= 12; $i++)
											<option value="{{ $i }}"{{ !is_null($data->date_of_birth) && date('m', strtotime($data->date_of_birth)) == $i ? ' selected' : null }}>{{ $i }}</option>
											@endfor
										</select>
									</div>
									<div class="col-md-4">
										<select name="year" id="year" class="custom-select form-control">
											<option value="">Năm</option>
											@for($i = date('Y'); $i >= 1930; $i--)
											<option value="{{ $i }}"{{ !is_null($data->date_of_birth) && date('Y', strtotime($data->date_of_birth)) == $i ? ' selected' : null }}>{{ $i }}</option>
											@endfor
										</select>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group row mb-4">
							<label for="change_password" class="col-md-2"></label>
							<div class="col-md-7">
								<label class="custom-control custom-checkbox">
									<input type="checkbox" name="change_password" class="custom-control-input" id="changePassword">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Thay đổi mật khẩu</span>
								</label>
							</div>
						</div>

						<div id="changePasswordForm" style="display: none;">
							<div class="form-group row mb-4">
								<label for="old_password" class="col-md-2 align-self-center">Mật khẩu cũ <span class="required">*</span></label>
								<div class="col-md-7">
									<input type="password" name="old_password" placeholder="Nhập mật khẩu cũ" class="form-control" required disabled>
									@if(Session::has('old_password_error'))
									<label id="old_password-error" class="error" for="old_password">{!! Session::get('old_password_error') !!}</label>
									@endif
								</div>
							</div>

							<div class="form-group row mb-4">
								<label for="password" class="col-md-2 align-self-center">Mật khẩu mới <span class="required">*</span></label>
								<div class="col-md-7">
									<input type="password" name="password" placeholder="Mật khẩu từ 8 đế 32 kí tự" class="form-control" id="password" required disabled>
									@if(Session::has('password_error'))
										<label id="password-error" class="error" for="password">{!! Session::get('password_error') !!}</label>
									@endif
								</div>
							</div>

							<div class="form-group row mb-4">
								<label for="password_confirmation" class="col-md-2 align-self-center">Nhập lại <span class="required">*</span></label>
								<div class="col-md-7">
									<input type="password" name="password_confirmation" placeholder="Nhập lại mật khẩu mới" class="form-control" required disabled>
									@if(Session::has('password_confirmation_error'))
										<label id="password_confirmation-error" class="error" for="password_confirmation">{!! Session::get('password_confirmation_error') !!}</label>
									@endif
								</div>
							</div>
						</div>

						<div class="form-group row">
							<label class="col-md-2"></label>
							<div class="col-md-7">
								<button class="btn btn-primary" style="min-width: 200px">CẬP NHẬT</button>
							</div>
						</div>	
					</form>
				</div>
			</div>
		</div>
	</div>

@stop

@section('footer')
	<script>
		$("aside.block-user .profile").addClass("active");
		$("#profileForm").validate({
			rules: {
				old_password: {
					required: true
				},
				password: {
					required: true,
					minlength: 8
				},
				password_confirmation: {
					equalTo: "#password"
				}
			},
			messages: {
				password_confirmation: "Mật khẩu nhập lại không trùng"
			}
		});
		$("#changePassword").change(function(){
			if($(this).is(":checked")) {
				$("#changePasswordForm input").attr("disabled", false);
				$("#changePasswordForm").slideDown();
			} else {
				$("#changePasswordForm input").attr("disabled", true);
				$("#changePasswordForm").slideUp();
			}
		});
	</script>
@stop