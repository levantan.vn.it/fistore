@extends('layouts.clear')

@section('title', 'Địa chỉ có sẵn')
@section('description', null)
@section('keywords', null)

@section('header')
@stop
@section('pixel_event')
	fbq('track', 'AddPaymentInfo');
@stop

@section('main')
	{{-- Checkout Progress --}}
	<div class="checkout-progress">
		<li class="d-none d-md-flex"><span class="step">1</span>Giỏ hàng</li>
		<li class="active"><span class="step">2</span>Địa chỉ giao hàng</li>
		<li class="d-none d-md-flex"><span class="step">3</span>Thanh toán & Đặt hàng</li>
	</div>

	<div class="row">
		<div class="col-md-10 mx-auto mb-5">
			<div class="block-checkout">
				<div class="block-header">
					<div class="block-title">ĐỊA CHỈ GIAO HÀNG</div>
					<div class="block-description">Chọn địa chỉ giao hàng có sẵn bên dưới</div>
				</div>
				<div class="block-body">
					<form action="{{ route('checkout.address.post') }}" method="POST" id="addressForm">
						@csrf
						<div class="row">
							@foreach($address_books as $address_item)
							<div class="col-md-6">
								<div class="address-book">
									<input type="hidden" name="address_id" value="{{ $address_item->id }}">
									<div>{{ $address_item->name }}</div>
									<div>Điện thoại: {{ $address_item->phone }}</div>
									<div>Địa chỉ: {{ $address_item->address }}, {{ $address_item->fk_ward->name }}, {{ $address_item->fk_district->name }}, {{ $address_item->fk_province->name }}</div>
									<div>
										<button class="btn btn-{{ $address_item->defaulted ? 'primary' : 'outline-secondary' }}">GIAO ĐẾN ĐỊA CHỈ NÀY</button>
										<a href="{{ route('checkout.address.get') }}?edit={{ $address_item->id }}" class="btn btn-outline-secondary">SỬA</a>
									</div>
								</div>
							</div>
							@endforeach
						</div>
					</form>
					<span>Bạn muốn giao hàng đến địa chỉ khác? <a href="{{ route('checkout.address.get') }}" class="btn btn-link">Thêm địa chỉ giao hàng mới</a></span>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
@stop