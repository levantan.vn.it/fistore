@extends('layouts.master')

@section('title', 'Bài viết sự kiện')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('article.list') !!}
	
	@if(count($featured) > 0)
	<div class="block-articles mb-3">
		<div class="row row-mx-10">
			<div class="col-md-6">
				<div class="item-article">
					<div class="item-thumbnail" style="padding-bottom: 59%;">
						<a href="{{ article_link($featured[0]->slug) }}">
							<img src="{{ media_url('articles', $featured[0]->thumbnail) }}" alt="{{ $featured[0]->name }}">
						</a>
					</div>
					<div class="item-name">
						<a href="{{ article_link($featured[0]->slug) }}">{{ str_length_limit($featured[0]->name, 60) }}</a>
					</div>
				</div>
			</div>
			<div class="col-md-6 d-none d-md-block">
				<div class="row row-mx-10" style="height: 100%;">
					@for($i = 1; $i <= count($featured) - 1; $i++)
					<div class="col-md-6">

						<div class="item-article">
						  <div class="item-thumbnail">
						    <a href="{{ article_link($featured[$i]->slug) }}">
						      <img src="{{ media_url('articles', $featured[$i]->thumbnail) }}" alt="{{ $featured[$i]->name }}">
						    </a>
						  </div>
						  <div class="item-name">
						    <a href="{{ article_link($featured[$i]->slug) }}">{{ str_length_limit($featured[$i]->name, 30) }}</a>
						  </div>
						</div>

					</div>
					@endfor
				</div>
			</div>
		</div>
	</div>
	@endif
	
	<div class="row">
		<div class="col-md-9">
			<div class="block-articles">
				<div class="block-body">
					@foreach($data as $item)
					<div class="article-item-2">
						<img src="{{ media_url('articles', $item->thumbnail) }}" alt="{{ $item->title ?? $item->name }}" class="item-thumbnail">
						<div class="item-info">
							<a href="{{ article_link($item->slug) }}" class="item-title">{{ $item->name }}</a>
							<time class="item-time">Ngày {{ date('d/m/Y H:i', strtotime($item->created_at)) }}</time>
							<div class="item-description">{{{ str_length_limit($item->description ?? $item->content, 150) }}}</div>
							<a href="{{ article_link($item->slug) }}" class="item-action"><i class="fa fa-long-arrow-right"></i> Xem chi tiết</a>
						</div>
					</div>
					@endforeach
				</div>

				<div class="block-footer">
					@include('particles.pagination')
				</div>
			</div>
		</div>
		<div class="col-md-3">
			@include('particles.aside_mosted_articles')
		</div>
	</div>
@stop

@section('footer')
@stop