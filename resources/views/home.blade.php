@extends('layouts.master')

@section('header')
	{{-- Owl Carousel --}}
	<script src="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.css') }}">
@stop

@section('main')
	@if(Agent::isPhone())
		@include('particles.banner_mobile')
	@else
		@include('particles.banner')
	@endif
	@include('particles.qc_1')
	@include('particles.brands')

	<!-- Hotdeal -->
	<div class="block-primary my-2 my-md-4">
		<div class="block-header">
			<div class="block-title">PICK ME NOW</div>
			<ul class="nav-product hotdeal">
				<li>
					<a id="" data-target="#tabSelling" class="tab-link selected">GO</a>
				</li>
				<li>
					<a id="" data-target="#tabAboutToSell" class="tab-link">READY</a>
				</li>
				<li>
					<a id="" data-target="#tabExpired" class="tab-link">OVER</a>
				</li>
			</ul>
			<div class="view-more" style="margin-top: 1.5rem">
				<a href="{{ route('hotdeal') }}">
					Xem thêm <i class="fa fa-angle-double-right"></i>
				</a>
			</div>
		</div>
		<div class="block-body tab-contain hotdeal">
			<div class="tab-panel" id="tabSelling">
				<div class="owl-carousel product-slider" id="owlHDSelling">
					@foreach($hotDealSelling as $item)
						<div class="item">
							@include('particles.hotdeal_item')
						</div>
					@endforeach
				</div>
			</div>
			<div class="tab-panel" id="tabAboutToSell" style="display: none">
				<div class="owl-carousel product-slider" id="owlHDAboutToSell">
					@foreach($hotDealAboutToSell as $item)
						<div class="item">
							@include('particles.hotdeal_item')
						</div>
					@endforeach
				</div>
			</div>
			<div class="tab-panel" id="tabExpired" style="display: none">
				<div class="owl-carousel product-slider" id="owlHDAboutToSell">
					@foreach($hotDealExpired as $item)
						<div class="item">
							@include('particles.hotdeal_item')
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<script>
		$(".nav-product.hotdeal .tab-link").on("click", function(){
			$(".tab-contain.hotdeal .tab-panel").hide();
			$($(this).data("target")).show();
			$(".nav-product.hotdeal .tab-link").removeClass("selected");
			$(this).addClass("selected");
		});
	</script>
	<!-- End Hotdeal -->

	<!-- Khuyến mãi -->
	<div class="block-primary my-2 my-md-4">
		<div class="block-header">
			<div class="block-title">Khuyến mãi</div>
			<ul class="nav-product saleoff">
				<li>
					<a id="saleOfTab" data-target="#tabSaleOf" class="tab-link selected">Products</a>
				</li>
				<li>
					<a id="comboTab" data-target="#tabCombo" class="tab-link">Combo</a>
				</li>
				<li>
					<a id="campaignSaleTab" data-target="#tabCampaignSale" class="tab-link">Chương trình</a>
				</li>
			</ul>
			<div class="view-more" style="margin-top: 1.5rem">
				<a href="{{ route('combo') }}">
					Xem thêm <i class="fa fa-angle-double-right"></i>
				</a>
			</div>
		</div>
		<div class="block-body tab-contain saleoff">
			<div class="tab-panel" id="tabSaleOf">
				<div class="owl-carousel product-slider" id="owlSaleOf">
					@foreach($SaleOfProducts as $item)
						<div class="item">
							@include('particles.product_item')
						</div>
					@endforeach
				</div>
			</div>
			<div class="tab-panel" id="tabCombo" style="display: none">
				<div class="owl-carousel product-slider" id="owlCombo">
					@foreach($comboProducts as $item)
						<div class="item">
							@include('particles.combo_item')
						</div>
					@endforeach
				</div>
			</div>
			<div class="tab-panel" id="tabCampaignSale" style="display: none">
				<div class="owl-carousel product-slider" id="owlCampaignSale">
					@foreach($campaignSale as $item)
						<div class="item">
							@include('particles.hotdeal_item')
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
	<script>
		$(".nav-product.saleoff .tab-link").on("click", function(){
			$(".tab-contain.saleoff .tab-panel").hide();
			$($(this).data("target")).show();
			$(".nav-product.saleoff .tab-link").removeClass("selected");
			$(this).addClass("selected");
		});
	</script>
	<!-- End Khuyến mãi -->

	@include('particles.qc_2')

	<!-- Hàng bán chạy -->
	<div class="block-primary my-2 my-md-4">
		<div class="block-header">
			<div class="block-title">Hàng bán chạy</div>
			<div class="view-more">
				<a href="{{ group_link('hang-ban-chay') }}">
					Xem thêm <i class="fa fa-angle-double-right"></i>
				</a>
			</div>
		</div>
		<div class="block-body">
			<div class="owl-carousel product-slider">
				@foreach($goodSaleProducts as $item)
					<div class="item">
						@include('particles.product_item')
					</div>
				@endforeach
			</div>
		</div>
	</div>
	<!-- End Hàng bán chạy -->

	@include('particles.qc_3')

	<!-- Hàng mới về -->
	<div class="block-primary my-2 my-md-4">
		<div class="block-header">
			<div class="block-title">Hàng mới về</div>
			<div class="view-more">
				<a href="{{ group_link('hang-moi-ve') }}">
					Xem thêm <i class="fa fa-angle-double-right"></i>
				</a>
			</div>
		</div>
		<div class="block-body">
			<div class="owl-carousel product-slider">
				@foreach($newestProducts as $item)
					<div class="item">
						@include('particles.product_item')
					</div>
				@endforeach
			</div>
		</div>
	</div>
	<!-- End Hàng mới về -->

	<!-- Free Try -->
	@if ($freetrialProducts != null && count($freetrialProducts) > 0)
		<div class="block-primary my-2 my-md-4">
			<div class="block-header">
				<div class="block-title">Trải nghiệm 0 đồng</div>
			</div>
			<div class="block-body">
				<div class="owl-carousel freetrial-slider">
					@foreach($freetrialProducts as $item)
						<div class="item">
							@include('particles.freetrial_item')
						</div>
					@endforeach
				</div>
			</div>
		</div>
	@endif
	<!-- End Free Try -->

	<!-- Bài viết - Sự kiện -->
	@if(count($articles) > 0)
		<div class="block-primary block-articles my-2 my-md-4">
			<div class="block-header">
				<div class="block-title">Bài viết - Sự kiện</div>
				<div class="view-more">
					<a href="{{ route('article.list') }}">
						Xem thêm <i class="fa fa-angle-double-right"></i>
					</a>
				</div>
			</div>
			<div class="block-body">
				<div class="row row-mx-10">
					<div class="col-md-6">

						<div class="item-article">
							<div class="item-thumbnail" style="padding-bottom: 59%;">
								<a href="{{ article_link($articles[0]->slug) }}">
									<img src="{{ media_url('articles', $articles[0]->thumbnail) }}" alt="{{ $articles[0]->name }}">
								</a>
							</div>
							<div class="item-name">
								<a href="{{ article_link($articles[0]->slug) }}">{{ str_length_limit($articles[0]->name, 60) }}</a>
							</div>
						</div>

					</div>
					<div class="col-md-6">
						<div class="row row-mx-10">

							@foreach($articles as $key => $item)
								@if($key > 0)
									<div class="col-md-6{{ $key > 1 ? ' d-none d-md-block' : null }}">
										@include('particles.article_item')
									</div>
								@endif
							@endforeach

						</div>
					</div>
				</div>
			</div>
		</div>
	@endif
	<!-- End Bài viết - Sự kiện -->
@stop

@section('footer')
	{{-- Product Slider --}}
	<script src="{{ asset('js/product-slider.handler.js') }}"></script>
	{{-- Sub Banner Slider --}}
	<script>
		$('.sub-banner-slider').owlCarousel({
			loop:false,
			margin:20,
			nav:true,
			navText:['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
			dots:false,
			autoplay:false,
			smartSpeed:700,
			responsive:{
				0:{
					items:1
				},
				768:{
					items:3
				},
			},
		});
	</script>
@stop
