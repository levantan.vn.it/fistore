<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Mail Template</title>
    <style>
        @import url('https://fonts.googleapis.com/css?family=Varela+Round&subset=latin-ext,vietnamese');
    </style>
</head>
<body>
    <div class="body" ]
         style="
            font-family: 'Varela Round', sans-serif;
            font-size:12px;
            width: 800px;
            margin: auto;
            padding: 50px;
            padding-top: 0px;
            background-size: contain;
            /*background-image: url({{ asset('images/mails/background.jpg') }});*/
            background-repeat: no-repeat;">
        <div class="box"
            style="
                height: 100%;
                padding: 90px 85px;
                padding-top: 0px;">
            <div class="box-header"
                style="
                    border-bottom: 1px dashed;
                    padding-bottom: 25px;
                    margin-top: 25px;">
                <span class="logo">
                    <a href="{{ url('/') }}"><img src="{{ asset('images/mails/logo.png') }}" alt="Shopa"></a>
                </span>

                <span class="social"
                    style="
                            float: right;">
                    <a href="https://www.facebook.com/fistore.vn/"
                    style="
                            margin-right:10px;
                            text-decoration: none;" class="fb">
                        <img src="{{ asset('images/mails/fb.png') }}" alt="">
                    </a>
                    <a href="https://www.instagram.com/fistore.vn/"
                    style="
                        margin-right:10px;
                        text-decoration: none;" class="instagram">
                        <img src="{{ asset('images/mails/instagram.png') }}" alt="">
                    </a>
                    <a href="https://www.youtube.com/channel/UCNKQiOMdCOxX_IbIvs-AiCQ"
                    style="
                            margin-right:10px;
                            text-decoration: none;" class="fb">
                        <img src="{{ asset('images/mails/youtube.png') }}" alt="">
                    </a>
                    <a href="https://shopee.vn/shopavietnam"
                    style="
                            text-decoration: none;" class="fb">
                        <img src="{{ asset('images/mails/shoppe.png') }}" alt="">
                    </a>
                </span>
            </div>

            <div class="box-body"
                style="margin-top: 50px">
                <div class="box-title">
                    <div class="thank-customer">
                        <h3 style="padding: 0;margin: 0;">Cảm ơn quý khách {{ $order->name }} đã đặt hàng tại Shopa</h3>
                        <p style="padding: 0;margin: 0;padding-top: 15px;">
                            Shopa rất vui thông báo đơn hàng {{ $order->code }} của quý khách đã được tiếp nhận và đang trong
                            quá trình xử lý. Shopa sẽ thông báo đến quý khách ngay khi hàng chuẩn bị được giao.
                        </p>
                    </div>
                </div>

                <div class="box-information" style="margin-top:40px;">
                    <div class="box-title" style="font-weight:700;">
                        <h2 style="
                                margin:0;
                                padding-bottom:5px;
                                padding-right: 5px;
                                display: inline-block;">THÔNG TIN ĐƠN HÀNG</h2>
                        <span class="bill">{{ $order->code }}</span>
                        <span class="datetime">( Ngày {{ $time->format('d') }} Tháng {{ $time->format('m') }} Năm {{ $time->format('Y') }} )</span>
                        <span>
                            <img src="{{ asset('images/mails/star.png') }}" alt="">
                        </span>
                    </div>

                    <div class="bar" style="background-image: url({{ asset('images/mails/bar.jpg') }});height: 13px;margin-bottom: 15px;"></div>

                    <div class="bill-information">
                        <div class="table">
                            <div class="row" style="display:flex;">
                                <div style="
                                        width:50%;
                                        text-align:center;
                                        font-weight: 700;
                                        border-right:1px solid;
                                        border-bottom:1px dotted;
                                        padding-bottom:10px;">
                                    Thông tin thanh toán
                                </div>
                                <div style="width:50%;
                                            text-align:center;
                                            font-weight: 700;
                                            border-bottom:1px dotted;
                                            padding-bottom:10px;">
                                    Địa chỉ giao hàng
                                </div>
                            </div>

                            <div class="row" style="display:flex;">
                                    <div style="
                                            width:50%;
                                            border-right:1px solid;
                                            padding-top:20px;
                                            padding-bottom:10px;">
                                        <div class="name" style="margin-bottom: 15px;">Họ tên: {{ $order->name }}</div>
                                        <div class="email" style="margin-bottom: 15px;">EmaiI: {{ $order->email }}</div>
                                        <div class="phone" style="margin-bottom: 15px;">Điện thoại: {{ $order->phone }}</div>
                                    </div>
                                    <div style="width:50%;
                                                padding-top:20px;
                                                padding-bottom:10px;">
                                        {{-- <div class="name" style="margin-bottom: 25px;margin-left:15px;">Tỉnh thành: {{ \App\Province::find($order->province_id)->name }}</div>
                                        <div class="email" style="margin-bottom: 25px;margin-left:15px;">Quận huyện: {{ \App\District::find($order->district_id)->name }}</div>
                                        <div class="phone" style="margin-bottom: 25px;margin-left:15px;">Phường xã: {{ $order->ward_id != 0 ? \App\Ward::find($order->ward_id)->name : '' }}</div>
                                        <div class="phone" style="margin-bottom: 25px;margin-left:15px;">Địa chỉ: {{ $order->address }}</div> --}}
                                        <div class="name" style="margin-bottom: 15px;margin-left:15px;">{{ $order->address }}, {{ $order->ward_id != 0 ? \App\Ward::find($order->ward_id)->name : '' }}, {{ \App\District::find($order->district_id)->name }}, {{ \App\Province::find($order->province_id)->name }}</div>
                                    </div>
                            </div>
                        </div>

                        <div class="method" style="margin-top: 45px;">
                            <div style="margin-bottom:2px;"><b>Phương thức thanh toán :</b> Thanh toán tiền mặt khi nhận hàng</div>
                            {{--<div style="margin-bottom:2px;"><b>Thời gian giao hàng dự kiến :</b> ...</div>--}}
                            <div style="margin-bottom:2px;"><b>Hình thức đóng gói :</b> Hộp carton và túi zip</div>
                        </div>

                        <div class="box-title" style="font-weight:700;margin-top: 35px;">
                            <h2 style="
                                    margin:0;
                                    padding-bottom:5px;
                                    padding-right: 5px;
                                    display: inline-block;">CHI TIẾT ĐƠN HÀNG</h2>
                            <span>
                                <img src="{{ asset('images/mails/moi.png') }}" alt="">
                            </span>
                        </div>

                        <div class="bar" style="background-image: url({{ asset('images/mails/bar.jpg') }});height: 13px;margin-bottom: 15px;"></div>
                        <br>
                        <div class="table">
                            <div class="table-header" style="display:flex;background-image: url({{ asset('images/mails/background_table.jpg') }});color: white">
                                <div style="width: 30%;    padding: 10px;">
                                    Sản phẩm
                                </div>

                                <div style="width: 20%;    padding: 10px;">
                                    Đơn giá
                                </div>

                                <div style="width: 15%;    padding: 10px;">
                                    Số lượng
                                </div>

                                <div style="width: 15%;    padding: 10px;text-align:right;">
                                    Giảm giá
                                </div>

                                <div style="width: 20%;    padding: 10px;text-align:right;">
                                    Tạm tính
                                </div>
                            </div>

                            @foreach($orderDetail as $item)
                            <div class="table-header" style="display:flex;background-color: #dedede;">
                                <div style="width: 30%;padding: 10px;">
                                    {{ $item->name }}
                                </div>

                                <div style="width: 20%;padding: 10px;">
                                    {{ number_format($item->original_price ?: $item->price, 0, ',', '.')  }} đ
                                </div>

                                <div style="width: 15%;padding: 10px;">
                                    {{ $item->qty }}
                                </div>

                                <div style="width: 15%;padding: 10px;text-align:right;">
                                    {{ number_format($item->original_price ? ($item->original_price - $item->price) : 0, 0, ',', '.') }} đ
                                </div>

                                <div style="width: 20%;padding: 10px;text-align:right;">
                                    {{ number_format($item->subtotal, 0, ',', '.') }} đ
                                </div>
                            </div>
                            @endforeach

                            <div class="table-footer">
                                {{-- <div class="row" style="display:flex;background-color: #ededed;">
                                    <div style="width: 79%;padding: 10px 0;text-align:right;">
                                        Tổng tạm tính
                                    </div>

                                    <div style="width: 18%;padding: 10px;text-align:right;">
                                        {{ number_format($order->subtotal, 0, ',', '.') }} đ
                                    </div>
                                </div> --}}

                                <div class="row" style="display:flex;background-color: #ededed;">
                                    <div style="width: 79%;padding: 10px 0;text-align:right;">
                                        Phí vận chuyển
                                    </div>

                                    <div style="width: 18%;padding: 10px;text-align:right;">
                                        {{ number_format($order->transport_price,0,',','.') }} đ
                                    </div>
                                </div>

                                <div class="row" style="padding: 5px;padding-bottom: 0px;display:flex;background-color: #ededed;">
                                    <div style="width: 79%;text-align:right;">
                                        <h3 style="margin:0; padding:0;"><b>Thành tiền</b></h3>
                                    </div>

                                    <div style="width: 20%;text-align:right;">
                                        <b>{{ number_format($order->total, 0, ',', '.') }} đ</b>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                    </div>

                    <div class="case" style="padding:40px 0;">
                        Trường hợp quý khách có những băn khoăn về đơn hàng, có thể xem thêm mục các câu hỏi thường gặp.
                    </div>

                    <div class="support" style="background-image: url({{ asset('images/mails/background_support.jpg') }});padding:12px;line-height:1.5;color:white;">
                        Bạn cần được hỗ trợ ngay? Chỉ cần email <b>info.fistore@dmnc.vn</b> , hoặc gọi số điện thoại
                        <b>0902 322 350</b> (9 - 18h trừ T7,CN). Đội ngũ Shopa luôn sẵn sàng hỗ trợ bạn bất kì lúc nào.
                        <b>Văn phòng Shopa</b>: <i>194 Golden Building, 473 Điện Biên Phủ, Phường 25, Quận Bình Thạnh,
                        Thành phố Hồ Chí Minh, Việt Nam</i>
                    </div>

                    <div class="thanks-customer" style="text-align:right;">
                        <div><h2>Một lần nữa Shopa cảm ơn quý khách.</h2></div>
                        <div><h2>SHOPA</h2></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
