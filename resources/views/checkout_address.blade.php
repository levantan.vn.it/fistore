@extends('layouts.clear')

@section('title', 'Địa chỉ giao hàng')
@section('description', null)
@section('keywords', null)

@section('header')
	<style type="text/css">
		.sortmobile{display:none;}
		@media screen and (max-width: 760px) {
			.sortmobile{display:block;}
			.sortdesktop{display:none;}
		}
	</style>
@stop
@section('pixel_event')
	fbq('track', 'AddPaymentInfo');
@stop

@section('main')
	{{-- Checkout Progress --}}
	<div class="checkout-progress">
		<li class="d-none d-md-flex"><span class="step">1</span>Giỏ hàng</li>
		<li class="active"><span class="step">2</span>Địa chỉ giao hàng</li>
		<li class="d-none d-md-flex"><span class="step">3</span>Thanh toán & Đặt hàng</li>
	</div>

	<div class="row">
		<div class="col-md-10 mx-auto mb-5">
			<div class="block-checkout">
				@guest('customer')
				<div class="block-header">
					<div class="block-title"><a href="#loginModal" data-toggle="modal" class="btn btn-link font-weight-bold fz-up-4">ĐĂNG NHẬP</a> <span style="color: #676767;">hoặc</span> <a href="{{ route('checkout.account.register') }}" class="btn btn-link font-weight-bold fz-up-4">ĐĂNG KÝ</a></div>
					<div class="block-description">Để tích lũy điểm và nhận các chương trình ưu đãi từ <strong>Shopa</strong></div>
					<div class="form-group row row-mx-5 my-3">
						<div class="col-md-4">
							<div class="fb-login-button">
								<a href="{{ url('login/facebook/redirect') }}"><i class="fa fa-facebook-square fa-fw"></i> Đăng nhập bằng Facebook</a>
							</div>
						</div>
						<div class="col-md-4">
							<div class="gg-login-button">
								<a href="{{ url('login/google/redirect') }}"><i class="fa fa-google-plus-square fa-fw"></i> Đăng nhập bằng Google</a>
							</div>
						</div>
					</div>
				</div>
				<hr>
				@endguest
				<div class="block-header">
					<div class="block-title">MUA HÀNG KHÔNG CẦN ĐĂNG NHẬP</div>
					<div class="block-description">Vui lòng điền thông tin bên dưới</div>
				</div>
				<div class="block-body">
					<form action="{{ route('checkout.address.post') }}" method="POST" id="addressForm">
						@csrf
						<input type="hidden" name="address_id" value="{{ $userAddress->id ?? null }}">
						<div class="row row-mx-10">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name">Họ tên <span class="required">*</span></label>
									<input type="text" name="name" class="form-control" placeholder="Nhập họ tên" value="{{ $userAddress->name ?? null }}" required>
								</div>
								<div class="form-group">
									<label for="phone">Điện thoại liên hệ <span class="required">*</span></label>
									<input type="text" name="phone" class="form-control" placeholder="Nhập số điện thoại" value="{{ $userAddress->phone ?? null }}" required>
								</div>
								@guest('customer')
								<div class="form-group">
									<label for="email">Địa chỉ email <span class="required">*</span></label>
									<input type="email" name="email" class="form-control" placeholder="Nhập địa chỉ email" required>
								</div>
								@endguest
								<div class="form-group sortdesktop">
									<label class="mb-3">Để nhận hàng thuận tiện hơn, bạn vui lòng cho <strong>Shopa</strong> biết nơi nhận hàng cụ thể:</label>
									<div class="form-group">
										<label class="custom-control custom-radio">
											<input type="radio" name="address_type" value="0" class="custom-control-input"{!! isset($userAddress) && $userAddress->address_type == 0 ? ' checked' : null !!} required>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Nhà riêng/Chung cư</span>
										</label>
									</div>
									<div class="form-group">
										<label class="custom-control custom-radio">
											<input type="radio" name="address_type" value="1" class="custom-control-input"{!! isset($userAddress) && $userAddress->address_type == 1 ? ' checked' : null !!}>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Cơ quan/Công ty</span>
										</label>
									</div>
									<label id="address_type-error" class="error" for="address_type" style="display: none;"></label>

									@auth('customer')
									<div class="form-group">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" name="default" class="custom-control-input"{!! isset($userAddress) && $userAddress->defaulted ? ' checked' : null !!}>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Sử dụng làm địa chỉ mặc định</span>
										</label>
									</div>
									@endauth
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="province">Tỉnh/Thành phố <span class="required">*</span></label>
									<select name="province" class="custom-select form-control" onchange="get_districts(event)" required>
										<option value="">Chọn Tỉnh/Thành phố</option>
										@foreach(get_provinces() as $province)
											<option value="{{ $province->id }}"{!! isset($userAddress) && $userAddress->province_id == $province->id ? ' selected' : null !!}>{{ $province->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group">
									<label for="district">Quận/Huyện <span class="required">*</span></label>
									<select name="district" class="custom-select form-control" onchange="get_wards(event)" required>
										<option value="">Chọn Quận/Huyện</option>
										@isset($userAddress)
											@foreach(get_districts_by_province($userAddress->province_id) as $district)
												<option value="{{ $district->id }}"{!! isset($userAddress) && $userAddress->district_id == $district->id ? ' selected' : null !!}>{{ $district->name }}</option>
											@endforeach
										@endisset
									</select>
								</div>
								<div class="form-group">
									<label for="ward">Phường/Xã </label>
									<select name="ward" class="custom-select form-control">
										<option value="">Chọn Phường/Xã</option>
										@isset($userAddress)
											@foreach(get_wards_by_district($userAddress->district_id) as $ward)
												<option value="{{ $ward->id }}"{!! isset($userAddress) && $userAddress->ward_id == $ward->id ? ' selected' : null !!}>{{ $ward->name }}</option>
											@endforeach
										@endisset
									</select>
								</div>
								<div class="form-group">
									<label for="address">Địa chỉ <span class="required">*</span></label>
									<textarea name="address" rows="3" class="form-control" placeholder="Vd: 45, điện biên phủ, p10, q1, hcm...." required>{!! $userAddress->address ?? null !!}</textarea>
								</div>
							</div>
						</div>
                       	<div class="col-xs-12 sortmobile">
                            <div class="form-group">
									<label class="mb-3">Để nhận hàng thuận tiện hơn, bạn vui lòng cho <strong>Shopa</strong> biết nơi nhận hàng cụ thể:</label>
									<div class="form-group">
										<label class="custom-control custom-radio">
											<input type="radio" name="address_type" value="0" class="custom-control-input"{!! isset($userAddress) && $userAddress->address_type == 0 ? ' checked' : null !!} required>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Nhà riêng/Chung cư</span>
										</label>
									</div>
									<div class="form-group">
										<label class="custom-control custom-radio">
											<input type="radio" name="address_type" value="1" class="custom-control-input"{!! isset($userAddress) && $userAddress->address_type == 1 ? ' checked' : null !!}>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Cơ quan/Công ty</span>
										</label>
									</div>
									<label id="address_type-error" class="error" for="address_type" style="display: none;"></label>

									@auth('customer')
									<div class="form-group">
										<label class="custom-control custom-checkbox">
											<input type="checkbox" name="default" class="custom-control-input"{!! isset($userAddress) && $userAddress->defaulted ? ' checked' : null !!}>
											<span class="custom-control-indicator"></span>
											<span class="custom-control-description">Sử dụng làm địa chỉ mặc định</span>
										</label>
									</div>
									@endauth
								</div>
							</div>      
                        </div>
						<div class="form-group mt-5">
							<div class="checkout-button">
								<button class="btn btn-primary">GIAO ĐẾN ĐỊA CHỈ NÀY</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	<script src="{{ asset('js/areas.handler.js') }}"></script>
	<script>
		$("#addressForm").validate({
			rules: {
				phone: {
					minlength: 10,
					maxlength: 13,
					digits: true
				}
			},
			messages: {
				phone: "Số điện thoại không đúng định dạng",
			}
		});
	</script>
@stop