<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Truy cập bị từ chối</title>
</head>
<body>
	<h2>Bạn không có quyền truy cập trang này</h2>
	<a href="{{ url()->previous() }}">Trở về trang trước</a>
</body>
</html>