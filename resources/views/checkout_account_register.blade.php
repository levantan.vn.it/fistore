@extends('layouts.clear')

@section('title', 'Đăng ký tài khoản')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{{-- Checkout Progress --}}
	<div class="checkout-progress">
		<li class="d-none d-md-flex"><span class="step">1</span>Giỏ hàng</li>
		<li class="active"><span class="step">2</span>Địa chỉ giao hàng</li>
		<li class="d-none d-md-flex"><span class="step">3</span>Thanh toán & Đặt hàng</li>
	</div>

	<div class="row justify-content-center">
		<div class="col-md-5 mb-5">
			<div class="block-checkout">
				<div class="block-header">
					<div class="block-title">ĐĂNG KÝ TÀI KHOẢN</div>
					<div class="block-description">Vui lòng điền các thông tin bên dưới</div>
				</div>
				<div class="block-body">
					<form action="{{ route('checkout.account.register.post') }}" method="POST" id="registerForm">
						@csrf
						<div class="form-group">
							<label for="name">Họ tên <span class="required">*</span></label>
							<input type="text" name="name" value="{{ old('name') }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Nhập họ tên" required>
							@if ($errors->has('name'))
							    <span class="invalid-feedback">
							        <strong>{{ $errors->first('name') }}</strong>
							    </span>
							@endif
						</div>

						<div class="form-group">
							<label for="phone">Số điện thoại</label>
							<input type="text" name="phone" class="form-control" placeholder="Nhập số điện thoại" minlength="10" maxlength="13">
						</div>

						<div class="form-group">
							<label for="email">Email <span class="required">*</span></label>
							<input type="email" name="email" value="{{ old('email') }}" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Nhập địa chỉ email" required>
							@if ($errors->has('email'))
							    <span class="invalid-feedback">
							        <strong>{{ $errors->first('email') }}</strong>
							    </span>
							@endif
						</div>

						<div class="form-group">
							<label for="password">Mật khẩu <span class="required">*</span></label>
							<input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" placeholder="Tối thiểu 8 kí tự" minlength="8" required>
							@if ($errors->has('password'))
							    <span class="invalid-feedback">
							        <strong>{{ $errors->first('password') }}</strong>
							    </span>
							@endif
						</div>

						<div class="form-group">
							<label for="password">Nhập lại mật khẩu <span class="required">*</span></label>
							<input type="password" name="password_confirmation" class="form-control" placeholder="Nhập lại mật khẩu" required>
						</div>

						<div class="form-group">
							<span class="help-block">Khi chọn <strong>ĐĂNG KÝ</strong> là bạn đã đồng ý với <a href="#">Điều khoản sử dụng</a> của <strong>Shopa</strong></span>
						</div>

						<div class="form-group mt-5">
							<div class="checkout-button">
								<button class="btn btn-primary btn-block">ĐĂNG KÝ</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-4 mb-5">
			<img src="{{ asset('images/checkout-img-01.png') }}" width="100%" style="border-radius: 7px;">
		</div>
	</div>
@stop

@section('footer')
	<script>
		$("#registerForm").validate({
			rules: {
				phone: {
					minlength: 10,
					maxlength: 13,
					digits: true
				},
				password_confirmation: {
					equalTo: "#password"
				}
			},
			messages: {
				phone: "Số điện thoại không đúng định dạng",
				password_confirmation: "Mật khẩu nhập lại không trùng"
			}
		});
	</script>
@stop