@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Đánh giá')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.review'))

{{-- Navs --}}
@section('nav')
	@if(request('trash'))
		<a href="{{ current_url() }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
	@else
		<a href="{{ current_url('?trash=1') }}" class="btn btn-default"><i class="fa fa-trash fa-fw"></i> Thùng rác</a>
	@endif
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th width="200">Người dùng</th>
				<th width="500">Nội dung đánh giá</th>
				<th>Đánh giá</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>
						<div class="font-weight-bold">{{ $item->fk_user->name }}</div>
						<div class="font-italic"><a href="mailto:{{ $item->fk_user->email }}">{{ $item->fk_user->email }}</a></div>
						@if($item->bought)
						<div class="text-success">Khách hàng đã mua sản phẩm <i class="fa fa-check-circle fa-lg"></i></div>
						@endif
					</td>
					<td class="white-space-unset">
						<div class="text-muted">Đánh giá vào {{ date('d/m/Y', strtotime($item->created_at)) }} lúc {{ date('H:s', strtotime($item->created_at)) }}</div>
						<div>{!! str_limit($item->content, 200) !!}</div>
					</td>
					<td class="white-space-unset">
						<div class="font-weight-bold"><a href="{{ product_link($item->fk_product->slug) }}#reviewId{{ $item->id }}" target="_blank">{{ $item->fk_product->name }}</a></div>
						@if(is_null($item->parent_id))
						<div>
							<i class="fa fa-star fa-fw fa-lg" style="color:#FFF000"></i>
							@if($item->rating >= 2)
								<i class="fa fa-star fa-fw fa-lg" style="color:#FFF000"></i>
							@endif
							@if($item->rating >= 3)
								<i class="fa fa-star fa-fw fa-lg" style="color:#FFF000"></i>
							@endif
							@if($item->rating >= 4)
								<i class="fa fa-star fa-fw fa-lg" style="color:#FFF000"></i>
							@endif
							@if($item->rating == 5)
								<i class="fa fa-star fa-fw fa-lg" style="color:#FFF000"></i>
							@endif
						</div>
						@else
						<div class="text-muted">Trả lời bình luận</div>
						@endif
					</td>
					<td>
						<ul class="table-options">
							@if(request('trash'))
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Khôi phục" onclick="restoreItem(event, {{ $item->id }}, '{{ route('admin.review.restore') }}')"><i class="fa fa-undo"></i></button>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.review.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
								</li>
							@else
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Chuyển vào thùng rác" onclick="trashItem(event, {{ $item->id }}, '{{ route('admin.review.trash') }}')"><i class="fa fa-trash"></i></button>
								</li>
							@endif
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navFeedback").addClass("active").find("ul").addClass("show").find(".review").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop