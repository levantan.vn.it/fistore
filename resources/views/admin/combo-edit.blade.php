@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- CKEditor --}}
	<script src="{{ asset('libs/ckeditor/4.10.0/ckeditor.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">

	{{-- Wheel Color Picker --}}
	<link rel="stylesheet" href="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.css') }}">
	<script src="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.min.js') }}"></script>
    
    <style type="text/css">
		fieldset.scheduler-border {
	        border: 1px dotted #dfdfdf !important;
	        padding: 0 1.4em 1.4em 1.4em !important;
	        margin: 0 0 1.5em 0 !important;
	        -webkit-box-shadow:  0px 0px 0px 0px #fafafa;
	                box-shadow:  0px 0px 0px 0px #fafafa;
	    }

        legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border-bottom:none;
        }
	</style>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.combo.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Cập nhật</button>
	<button class="btn btn-primary" name="repost" value="1" form="mainForm"><i class="fa fa-level-up fa-fw"></i> Đăng lên trang đầu</button>
	<a href="{{ route('admin.combo.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Hủy bỏ</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.combo.update', $data->id) }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@method("PUT")
		@csrf
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tên gói khuyến mãi <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" value="{{ $data->name }}" onkeyup="slug_make();" required>
						</div>
						<div class="form-group">
							<label for="content">Thông tin gói khuyến mãi</label>
							<textarea name="content" id="content">{!! $data->content !!}</textarea>
							<script>
								CKEDITOR.replace('content');
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="products">Chọn sản phẩm</label>
							<div class="control-group">
								<select id="products" name="products[]" multiple placeholder="Tên sản phẩm, Mã sản phẩm" required>
									@foreach($products as $product)
									<option value="{{ $product->id }}" {{ $data->fk_products->contains($product) ? 'selected' : null }}>{{ $product->name }}</option>
									@endforeach
								</select>
							</div>
							<script>
								$('#products').selectize({
									plugins: ['remove_button'],
									delimiter: ',',
									persist: false,
									create: false,
									maxItems: null,
								});
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="code">Mã sản phẩm (SKU) <span class="required">*</span></label>
							<input type="text" name="code" value="{{ $data->code }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="group">Số lượng</label>
							<input type="number" name="quantity" min="0" value="{{ $data->quantity }}" class="form-control">
							<span class="help-block">Để trống nếu không giới hạn số lượng</span>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="group">Giá bán <span class="required">*</span></label>
									<div class="input-group">
										<span class="input-group-addon addon-info">đ</span>
										<input type="text" name="price" class="form-control" value="{{ number_format($data->price) }}" onkeyup="make_money_format(event)" required>
									</div>
									<label id="price-error" class="error" for="price" style="display: none;"></label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="group">Giá thị trường</label>
									<div class="input-group">
										<span class="input-group-addon addon-info">đ</span>
										<input type="text" name="original_price" class="form-control" value="{{ !empty($data->original_price) ? number_format($data->original_price) : null }}" onkeyup="make_money_format(event)">
									</div>
									<label id="original_price-error" class="error" for="original_price" style="display: none;"></label>
								</div>
							</div>
						</div>
					</div>
				</div>
                                    
                <div class="card">
					<div class="card-body">
						<div class="form-group">
							<fieldset class="scheduler-border">
							    <legend class="scheduler-border">
							        <h3 style="font-weight:300;font-size:20px;text-transform:none;">
							            Chọn sản phẩm đi kèm
                                    </h3>
							    </legend>
							    <form id="themsanphamdikem">
							    	<div class="form-group">
										<label for="productss">Tên sản phẩm</label>
										<select id="productss" name="productss" class="custom-select form-control" placeholder="Tên sản phẩm, Mã sản phẩm">
											@foreach($products as $product)
												<option value="{{ $product->id }}">{{ $product->name }}</option>
											@endforeach
										</select>
									</div>
									<div class="form-group">
										<label for="pricee">Giá đi chung</label>
										<input type="text" id="pricee" value="" class="form-control" name="pricee" onkeyup="make_money_format(event)">
									</div>
									<div class="form-group">
										<center>
											<button type="button" class="btn btn-primary" onclick="addproduct({{ $data->id }});">Thêm</button>
										</center>
									</div>
							    </form>
								<table class="table">
									<thead>
										<th>Sản phẩm đi kèm</th>
										<th>Giá</th>
										<th>Xoá</th>
									</thead>
									<tbody id="sanphamdikem">
										
									</tbody>
								</table>
							</fieldset>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="float-right">
							<button type="button" class="btn btn-link" data-toggle="collapse" href="#seoOption">Tùy chỉnh nội dung SEO</button>
						</div>
						<label>Xem trước kết quả tìm kiếm</label>

						<div class="seo-container">
							<p class="seo-title">{{ $data->title ?? $data->name }}</p>
							<p class="seo-url">{{ url($data->slug) }}.html</p>
							<p class="seo-description">{{ $data->description ?? 'Mô tả sản phẩm hoặc trang web được rút gọn từ bài viết thông tin sản phẩm' }}</p>

							<div class="collapse" id="seoOption">
								<div class="card">
									<hr/>
									<div class="form-group">
										<label>Tiêu đề</label>
										<input type="text" name="title" class="form-control" value="{{ $data->title }}" onkeyup="title_make();">
									</div>

									<div class="form-group">
										<label>Mô tả</label>
										<textarea rows="3" name="description" class="form-control" onkeyup="description_make();">{!! $data->description !!}</textarea>
									</div>

									<div class="form-group">
										<label>Đường dẫn <span class="required">*</span></label>
										<div class="input-group">
											<span class="input-group-addon addon-info">{{ url('') }}</span>
											<input type="text" name="slug" class="form-control" value="{{ $data->slug }}" placeholder="ten-san-pham-123456" required>
											<span class="input-group-btn">
												<button class="btn btn-info slug-refresh" type="button" onclick="slug_make();"><i class="fa fa-refresh"></i></button>
											</span>
										</div>
										<label id="slug-error" class="error" for="slug" style="display: none;"></label>
									</div>
									<script>
										$(".slug-refresh").click(function() {
											var event = $(this);
											$(this).find("i").addClass("fa-spin");
											setTimeout(function() {
												$(event).find("i").removeClass("fa-spin");
											}, 1000);
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Ảnh đại diện sản phẩm</label>
							<div class="custom-image-upload custom-iu-md image-preview m-0">
								<img class="img-preview" src="{{ media_url('products', $data->thumbnail) }}">
								<label class="custom-label">
									<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*">
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label>Ảnh mô tả sản phẩm</label>
							<div id="multiImagePanel">
								<div class="row">
									<input type="hidden" id="numimage" value="{{ count($data->fk_images) }}" />
									@foreach($data->fk_images as $key => $image)
									<div class="img-wrap col-md-3">
										<div class="image_{{ $key+1 }}" id="image">
											<label>{{--  for="image_{{ $key+1 }}" --}}
												<img id="image_preview_{{ $key+1 }}" src="{{ media_url('products', $image->path) }}" height="130px">
												<input type="text" id="image_name_{{ $key+1 }}" class="d-block form-control" name="image_name{{ $image->id }}" placeholder="Tiêu đề" value="{{ $image->name }}">
												<input type="number" id="image_index_{{ $key+1 }}" class="d-block form-control" name="image_index{{ $image->id }}" placeholder="Thứ tự" value="{{ $image->index }}">
											</label>
											<input type="file" class="photo" name="images[]" id="image_{{ $key+1 }}" onchange="makeImageArr(event, {{ $key+1 }})" accept="image/*">
											<input type="hidden" name="old_images[]" value="{{ $image->id }}" id="old_images_{{ $key+1 }}">
											<svg id="i-close" onclick="this.parentElement.parentElement.remove();" class="remove button_{{ $key+1 }}" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
												<path d="M2 30 L30 2 M30 30 L2 2"></path>
											</svg>
										</div>
									</div>
									@endforeach
								</div>
							</div>
						</div>

						<div class="form-group">
							<button class="btn btn-info" type="button" id="add_image"><i class="fa fa-plus"></i>&emsp;Thêm ảnh</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg 4">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<div class="flex-row">
								<label for="show">Hiển thị công khai</label>
								<label class="custom-toggle toggle-info mb-0">
									<input type="checkbox" name="show" class="toggle-checkbox"{!! $data->hidden ? null : ' checked' !!}>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Thêm từ khóa liên quan (Tags)</label>
							<div class="control-group">
								<select id="tags" name="tags[]" multiple placeholder="Chọn trong danh sách hoặc Enter để thêm từ khóa">
									@foreach($tags as $tag)
										<option value="{{ $tag->keyword }}"{!! strpos($data->tags, $tag->keyword) !== false ? ' selected' : null !!}>{{ $tag->keyword }}</option>
									@endforeach
								</select>
							</div>
							<script>
								$('#tags').selectize({
									plugins: ['remove_button'],
									delimiter: ',',
									persist: false,
									createOnBlur: true,
									create: true
								});
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navSaleOf").addClass("active").find("ul").addClass("show").find(".combo").addClass("active");

		$("#mainForm").validate();

		// Multi-Image Handle
		var product_id = "{{ $data->id }}";
		var i = {{ count($data->fk_images) }} + 1;

		getproduct(product_id);

		function addproduct(id){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			var productss = $("#productss").val();
			var pricee = $("#pricee").val();

			$.ajax({
				url : base_url + 'admin/combo/addproduct/'+id,
				data : {
					productss: productss,
					pricee: pricee
				},
				type : 'POST',
				success : function (data) {
					if(data == 1){
						swal('Thành công !', 'success');
						$('#themsanphamdikem').trigger("reset");
						getproduct(id);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					swal('Lỗi!', xhr.responseText, 'error');
				}
			});
		}

		function getproduct(id){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				url : base_url + 'admin/combo/getproduct/'+id,
				type : 'GET',
				success : function (data) {
					if(data){
						$("#sanphamdikem").empty();
						$("#sanphamdikem").append(data);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					swal('Lỗi!', xhr.responseText, 'error');
				}
			});
		}

		function deletecomboproduct(id){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});

			$.ajax({
				url : base_url + 'admin/combo/deleteproduct/'+id,
				type : 'GET',
				success : function (data) {
					if(data){
						swal('Thành công !', 'success');
						getproduct(product_id);
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					swal('Lỗi!', xhr.responseText, 'error');
				}
			});
		}
	</script>
	<script src="{{ asset('js/admin/multi-image.handler.js') }}"></script>
	<script src="{{ asset('js/admin/seo-preview.handler.js') }}"></script>
	<script src="{{ asset('js/admin/product.handler.js') }}"></script>
	<style>
		.selectize-item{
			display: block;
		}
		.selectize-item img{
			float: left;
			width: 70px;
			margin-right: 10px;
			height: 4.5rem;
			object-fit: contain;
			padding: 2px;
			background-color: #fafafa;
			border-radius: 3px;
			border: 1px solid #DCDCDC;
		}
		.selectize-item .item-name{
			font-weight: 500;
		}
		.selectize-item .item-price strong{
			font-weight: 500;
			color: #ED0000;
		}
	</style>
@stop
