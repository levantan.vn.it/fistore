@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Trang nội dung')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">

	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.page'))

{{-- Navs --}}
@section('nav')
	@if(request('trash'))
	<a href="{{ current_url() }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
	@else
	<a href="{{ route('admin.page.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
	<a href="{{ current_url('?trash=1') }}" class="btn btn-default"><i class="fa fa-trash fa-fw"></i> Thùng rác</a>
	@endif
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th width="50" class="no-sort"></th>
				<th>Tiêu đề</th>
				<th>Chỉnh sửa lần cuối</th>
				<th>Chuyên mục</th>
				<th class="text-center">Trạng thái</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>
						<a data-fancybox="gallery-{{ $item->id }}" href="{{ media_url('pages', $item->thumbnail) }}">
							<img src="{{ media_url('pages', $item->thumbnail) }}" width="60">
						</a>
					</td>
					<td class="white-space-unset">
						<div>
							<a href="{{ page_link($item->slug) }}" target="_blank">{{ $item->name }}</a>&nbsp;
							{!! $item->draft ? '<i class="fa fa-paste fa-lg" data-toggle="tooltip" data-placement="top" title="Bản nháp"></i>' : null !!}
						</div>
						<div>Đăng bởi: {{ optional($item->fk_author)->name }}</div>
					</td>
					<td>
						{{ date('d/m/Y H:i', strtotime($item->updated_at)) }}
					</td>
					<td>{{ optional($item->fk_group)->name }}</td>
					<td>
						@if($item->hidden === 0)
						<mark class="mark mark-success mark-block">Hiển thị</mark>
						@else
						<mark class="mark mark-warning mark-block">Đã ẩn</mark>
						@endif
					</td>
					<td>
						<ul class="table-options">
							@if(request('trash'))
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Khôi phục" onclick="restoreItem(event, {{ $item->id }}, '{{ route('admin.page.restore') }}')"><i class="fa fa-undo"></i></button>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.page.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
								</li>
							@else
								<li>
									<a href="{{ route('admin.page.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Chuyển vào thùng rác" onclick="trashItem(event, {{ $item->id }}, '{{ route('admin.page.trash') }}')"><i class="fa fa-trash"></i></button>
								</li>
							@endif
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navPage").addClass("active").find("ul").addClass("show").find(".page").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop