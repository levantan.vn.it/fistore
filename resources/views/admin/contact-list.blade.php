@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Danh sách liên hệ')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.contact'))

{{-- Navs --}}
@section('nav')
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th>Tên</th>
				<th>Địa chỉ email</th>
				<th>Số điện thoại</th>
				<th class="white-space-unset">Nội dung</th>
				<th>Thời gian</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>{{ $item->name }}</td>
					<td>{{ $item->email }}</td>
					<td>{{ $item->phone }}</td>
					<td class="white-space-unset">
						<div><b>{{ $item->subject }}</b></div>
						<div>{!! $item->content !!}</div>
					</td>
					<td>{{ date('d/m/Y H:i', strtotime($item->created_at)) }}</td>
					<td>
						<ul class="table-options">
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.contact.destroy', $item->id) }}')"><i class="fa fa-close"></i></button>
							</li>
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navContact").addClass("active").find("ul").addClass("show").find(".contact").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop