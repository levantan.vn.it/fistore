@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Bảng điều khiển')

{{-- Import CSS, JS --}}
@section('header', null)

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.dashboard'))

{{-- Navs --}}
@section('nav')
	<a href="{{ url('') }}" class="btn btn-success"><i class="fa fa-home fa-fw"></i>Trang chủ</a>
@stop

{{-- Main --}}
@section('main')
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navDashboard").addClass("active");
	</script>
@stop