@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Log API')

{{-- Import CSS, JS --}}
@section('header')
	{{-- DataTable --}}
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">

	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
    
    <style type="text/css">
		fieldset.scheduler-border {
	        border: 1px groove #dfdfdf !important;
	        padding: 0 1.4em 1.4em 1.4em !important;
	        margin: 0 0 1.5em 0 !important;
	        -webkit-box-shadow:  0px 0px 0px 0px #fafafa;
	                box-shadow:  0px 0px 0px 0px #fafafa;
	    }

        legend.scheduler-border {
            font-size: 1.2em !important;
            font-weight: bold !important;
            text-align: left !important;
            width:auto;
            padding:0 10px;
            border-bottom:none;
        }
	</style>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.log-api'))


{{-- Main --}}
@section('main')
	<div class="card card-primary mb-4">
		<div class="card-header" data-toggle="collapse" href="#filterCollapse" aria-expanded="true">
			Tìm kiếm
		</div>
		<div class="card-body collapse show p-0" id="filterCollapse">
			<form action="" method="GET" class="m-4" id="filterForm" autocomplete="off">
				<div class="form-row form-group">
					<div class="col-lg-4">
						<div class="form-group">
							<label for="name">Tiêu đề</label>
							<input type="text" name="name" value="{{ request()->name }}" class="form-control">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="message">Nội dụng</label>
							<input type="text" name="message" value="{{ request()->message }}" class="form-control">
						</div>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label for="status">Trạng thái</label>
							<select name="status" class="custom-select form-control">
								<option value="">Tất cả</option>
								<option value="success"{!! request()->status == 'success' ? ' selected' : null !!}>Thành công</option>
								<option value="error"{!! request()->status == 'error' ? ' selected' : null !!}>Lỗi</option>
								
							</select>
						</div>
					</div>
				</div>
				<div class="form-group text-center">
					<button class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
					<a href="{{ url()->current() }}" class="btn btn-default"><i class="fa fa-refresh"></i> Xem tất cả</a>
				</div>
			</form>
		</div>
	</div>
	<div class="table-filter py-2">
		<div class="alert alert-success" role="alert">
			Có <strong>{{ $data->total() }}</strong> logs được tìm thấy
		</div>
	</div>

	<table class="table table-primary table-hover table-striped table-responsive">
		<thead>
			<tr>
				<th>Tiêu đề</th>
				<th>Nội dung</th>
				<th width="100">Trạng thái</th>
				<th width="100">Thời gian</th>
				<th class="no-sort" width="100"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>{{$item->name}}</td>
					<td>{{$item->message}}</td>
					<td class="{{$item->status == 2?'text-danger':'text-success'}}">{{$item->status == 2?"Error":"Success"}}</td>
					<td>
						{{ date('Y-m-d H:i',strtotime($item->created_at)) }}
					</td>
					<td>
						<ul class="table-options">
							@if(!empty($item->send_again))
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Send Again" onclick="sendAgain(event, {{ $item->id }}, '{{ route('admin.log-api.send_again') }}')">Send Again</button>
								</li>
							@endif
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	@include('particles.pagination')
@stop

{{-- Footer --}}
@section('footer')
	<script>

		function sendAgain(event,id,url){
			console.log(id);
			console.log(url);
			swal({
				title: 'Bạn có chắc',
				text: "Muốn gửi lại nội dụng API này ?",
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Gửi lại',
				cancelButtonText: 'Bỏ qua'
			}).then(function(isConfirm) {
				console.log('isConfirm',isConfirm);
				if(isConfirm.value){
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
				    $.ajax({
						url : url,
						data : {
							id : id
						},
						type : 'POST',
						success : function (data) {
							swal('Thành công !', 'success');
						},
						error: function (xhr, ajaxOptions, thrownError) {
							swal('Lỗi!', xhr.responseText, 'error');
						}
					});
				}
				
				
			});
		}
	</script>
@stop