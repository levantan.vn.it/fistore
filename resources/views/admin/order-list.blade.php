@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Đơn hàng')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.order'))

{{-- Navs --}}
@section('nav')
	{{-- <a href="{{ route('admin.product.order.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo đơn hàng mới</a> --}}
@stop

{{-- Main --}}
@section('main')
	<form method="GET" action="" class="mb-3">
	  <div class="nav nav-pills" role="tablist">
	    <label class="nav-item nav-link{!! !request()->filled('tab') ? ' active' : null !!}">Tất cả
			<input type="radio" name="tab" value="" class="d-none" onclick="this.form.submit()">
	    </label>
	    @foreach($status as $sts)
	    <label class="nav-item nav-link{!! request()->filled('tab') && request()->tab == $sts['value'] ? ' active' : null !!}">{{ $sts['name'] }}
			<input type="radio" name="tab" value="{{ $sts['value'] }}" class="d-none" onclick="this.form.submit()">
	    </label>
	    @endforeach
	    <label class="nav-item nav-link{!! request()->tab == 'cancelled' ? ' active' : null !!}">Đã hủy
			<input type="radio" name="tab" value="cancelled" class="d-none" onclick="this.form.submit()">
	    </label>
	    <label class="nav-item nav-link{!! request()->tab == 'contacted' ? ' active' : null !!}">Liên hệ
			<input type="radio" name="tab" value="contacted" class="d-none" onclick="this.form.submit()">
	    </label>
	    <label class="nav-item nav-link{!! request()->tab == 'delayed' ? ' active' : null !!}">Trì hoãn
			<input type="radio" name="tab" value="delayed" class="d-none" onclick="this.form.submit()">
	    </label>
	  </div>
	</form>

	<div class="card card-primary mb-4">
		<div class="card-header" data-toggle="collapse" href="#filterCollapse" aria-expanded="true">
			Tìm kiếm
		</div>
		<div class="card-body collapse show p-0" id="filterCollapse">
			<form action="" method="GET" class="m-4" id="filterForm" autocomplete="off">
				<div class="form-row form-group">
					<div class="col-lg-2">
						<label for="code">Số hóa đơn</label>
						<input type="text" name="code" value="{{ request()->code }}" class="form-control">
					</div>
					<div class="col-lg-3">
						<label for="tab">Trạng thái</label>
						<select name="tab" class="custom-select form-control">
							<option value=""></option>
							@foreach($status as $sts)
							<option value="{{ $sts['value'] }}" {!! request()->filled('tab') && request()->tab == $sts['value'] ? ' selected' : null !!}>{{ $sts['name'] }}</option>
							@endforeach
							<option value="cancelled"{!! request()->tab == 'cancelled' ? ' selected' : null !!}>Đã hủy</option>
							<option value="contacted"{!! request()->tab == 'contacted' ? ' selected' : null !!}>Liên hệ</option>
							<option value="delayed"{!! request()->tab == 'delayed' ? ' selected' : null !!}>Trì hoãn</option>
						</select>
					</div>
					<div class="col-lg-3">
						<label for="admin_id">Nhân viên chịu trách nhiệm</label>
						<select name="admin_id" class="custom-select form-control">
							<option value=""></option>
							@foreach($admins as $admin)
								<option value="{{ $admin->id }}"{!! request()->admin_id == $admin->id ? ' selected' : null !!}>{{ $admin->name }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-2">
						<label for="created_at_from">Ngày tạo</label>
						<div class="custom-datetimepicker">
							<div class="form-inline">
								<input type="text" name="created_at_from" value="{{ request()->created_at_from }}" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}">
							</div>
						</div>
					</div>
					<div class="col-lg-2">
						<label for="created_at_to">Đến</label>
						<div class="custom-datetimepicker">
							<div class="form-inline">
								<input type="text" name="created_at_to" value="{{ request()->created_at_to }}" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}">
							</div>
						</div>
					</div>
				</div>
				<div class="form-row form-group">
					<div class="col-lg-4">
						<label for="name">Tên khách hàng</label>
						<input type="text" name="name" value="{{ request()->name }}" class="form-control">
					</div>
					<div class="col-lg-4">
						<label for="email">Email khách hàng</label>
						<input type="email" name="email" value="{{ request()->email }}" class="form-control">
					</div>
					<div class="col-lg-4">
						<label for="phone">Số điện thoại LH</label>
						<input type="text" name="phone" value="{{ request()->phone }}" class="form-control">
					</div>
				</div>
				<div class="form-group justify-content-between d-flex">
					<div>
						<button class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
						<a href="{{ url()->current() }}" class="btn btn-default"><i class="fa fa-refresh"></i> Xem tất cả</a>
					</div>
					<div>
						<div class="input-group">
							<input type="number" class="form-control" placeholder="Số lượng đơn hàng" name="ex_num" min="0" step="100" value="{{ request()->ex_num }}" style="padding-top: .5rem;padding-bottom: .5rem;">
							<div class="input-group-append">
								<button class="btn btn-primary" name="export" value="1" form="filterForm" onclick="$('#filterForm').attr('target', '');">
									<i class="fa fa-file-excel-o"></i> Export Excel
								</button>
							</div>
						</div>				
					</div>
				</div>
			</form>
		</div>
	</div>
	
	@if(!empty(request()->all()))
	<div class="alert alert-success alert-dismissible fade show" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">×</span>
		</button>
		Có <strong>{{ $data->total() }}</strong> đơn hàng phù hợp với điều kiện tìm kiếm.
	</div>
	@endif

	<div class="table-filter py-2">
		<div class="flex-row">
			<select name="show" class="custom-select mr-1" form="filterForm" onchange="this.form.submit()">
				<option value="25"{!! request()->show == '25' ? ' selected' : null !!}>Hiển thị 25 kết quả</option>
				<option value="50"{!! request()->show == '50' ? ' selected' : null !!}>Hiển thị 50 kết quả</option>
				<option value="100"{!! request()->show == '100' ? ' selected' : null !!}>Hiển thị 100 kết quả</option>
				<option value="200"{!! request()->show == '200' ? ' selected' : null !!}>Hiển thị 200 kết quả</option>
				<option value="500"{!! request()->show == '500' ? ' selected' : null !!}>Hiển thị 500 kết quả</option>
				<option value="99999999"{!! request()->show == '99999999' ? ' selected' : null !!}>Hiển thị tất cả</option>
			</select>
			<input type="hidden" name="page" value="{{ request()->page }}" form="filterForm">
			
		</div>
	</div>
	<table class="table table-primary table-responsive">
		<thead>
			<tr>
				<th class="text-center" width="50">Mã đơn hàng</th>
				<th>Sản phẩm</th>
				<th>Thông tin Khách hàng</th>
				<th class="no-sort">Trị giá ĐH</th>
				<th>Ngày đặt hàng</th>
				<th class="text-center">Trạng thái</th>
				@can('order-ascribe')
				<th class="text-center">Chịu trách nhiệm</th>
				@endcan
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr
					@if($item->cancelled)
						bgcolor="#ED6A6A"
					@elseif($item->contacted)
						bgcolor="#40BCB4"
					@elseif($item->delayed)
						bgcolor="#EE82EE"
					@else
						@switch($item->status)
							@case(0)
								bgcolor="#FFFFFF"
								@break
							@case(1)
								bgcolor="#FCF8E3"
								@break
							@case(2)
								bgcolor="#FCFCA1"
								@break
							@case(3)
								bgcolor="#F2DEDE"
								@break
							@case(4)
								bgcolor="#D9EDF7"
								@break
						@endswitch
					@endif
				>
					<td class="font-weight-bold"><a class="c-red" href="{{ route('admin.product.order.show', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Xem thông tin đơn hàng">{{ $item->code }}</a></td>
					<td width="400">
						@foreach($item->fk_details as $productItem)
							@if(is_null($productItem->product_id))
								<div>
									<strike class="text-muted">{{ $productItem->name }}</strike>
								</div>
							@else
								<div>
									<a href="{{ url($productItem->fk_product->slug) }}.html" target="_blank">{{ $productItem->name }} <span class="text-muted">{!! $productItem->fk_product->is_combo ? '(Combo)' : null !!}</span></a>
								</div>
								@foreach($productItem->fk_comboItems as $comboItem)
									@if(is_null($comboItem->product_id))
										<div>&emsp;<i class="fa fa-level-up fa-rotate-90"></i>&emsp;<strike class="text-muted">{{ $comboItem->name }}</strike></div>
									@else
										<div>&emsp;<i class="fa fa-level-up fa-rotate-90"></i>&emsp;<a href="{{ url($comboItem->fk_product->slug) }}.html" target="_blank">{{ $comboItem->name }}</a></div>
									@endif
								@endforeach
							@endif
						@endforeach
					</td>
					<td>
						<div>{{ $item->name }}</div>
						<div>{{ $item->phone }}</div>
					</td>
					<td>
						<div class="flex-row">Tạm tính: <b>{{ number_format($item->subtotal, 0, ',', '.') }}</b></div>
						<div class="flex-row">Giảm giá: <b>{{ number_format($item->discounted, 0, ',', '.') }}</b></div>
						<div class="flex-row">Phí vận chuyển: <b>{{ number_format($item->transport_price, 0, ',', '.') }}</b></div>
						<div class="flex-row">Thành tiền: <b class="text-danger">{{ number_format($item->total, 0, ',', '.') }}</b></div>
					</td>
					<td>
						<div>Đặt hàng: {{ date('d/m/Y H:i', strtotime($item->created_at)) }}</div>
						<div>Cập nhật: {{ date('d/m/Y H:i', strtotime($item->updated_at)) }}</div>
					</td>
					<td class="text-center">
						<select name="status" class="custom-select" onchange="updateStatus(event, {{ $item->id }});">
							@foreach($status as $sts)
							<option value="{{ $sts['value'] }}"{!! $item->status == $sts['value'] ? ' selected' : null !!}>{{ $sts['name'] }}</option>
							@endforeach
							<option value="cancel"{!! $item->cancelled ? ' selected' : null !!}>Hủy</option>
							<option value="contact"{!! $item->contacted ? ' selected' : null !!}>Liên hệ</option>
							<option value="delay"{!! $item->delayed ? ' selected' : null !!}>Trì hoãn</option>
						</select>
					</td>
					@can('order-ascribe')
					<td>
						<select name="admin_id" class="custom-select" onchange="updateAdmin(event, {{ $item->id }});">
							<option value="">Chưa điều phối</option>
							@foreach($admins as $admin)
								<option value="{{ $admin->id }}"{!! $item->admin_id == $admin->id ? ' selected' : null !!}>{{ $admin->name }}</option>
							@endforeach
						</select>
					</td>
					@endcan
					<td class="text-right">
						<ul class="table-options justify-content-start">
							<li>
								<a href="{{ route('admin.product.order.show', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Xem thông tin đơn hàng"><i class="fa fa-external-link-square"></i></a>
							</li>
							@if($item->cancelled)
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa bản ghi" onclick="destroyItem(event, '{{ route('admin.product.order.destroy', $item->id) }}')"><i class="fa fa-close"></i></button>
								</li>
							@endif
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
	@include('particles.pagination')
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navOrder").addClass("active").find("ul").addClass("show").find(".order").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
	<script>
		function updateStatus(event, id){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : "{{ route('admin.product.order.update_status') }}",
				data : {
					id : id,
					status : $(event.target).val()
				},
				type : 'POST',
				success : function (data) {
					toastr[data.alert]("<b>" + data.title + "</b><br/>" + data.msg);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					toastr.error(xhr.responseText);
				}
			});
		}

		function updateAdmin(event, id){
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			$.ajax({
				url : "{{ route('admin.product.order.update_admin') }}",
				data : {
					order_id : id,
					admin_id : $(event.target).val()
				},
				type : 'POST',
				success : function (data) {
					toastr[data.alert]("<b>" + data.title + "</b><br/>" + data.msg);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					toastr.error(xhr.responseText);
				}
			});
		}
		// Datetime Picker
		$(function() {
			$( ".datepicker" ).datepicker({
				dateFormat : "dd/mm/yy"
			});
		});
	</script>
@stop