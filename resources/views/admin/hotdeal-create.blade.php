@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Tạo khuyến mãi')

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">

	{{-- Wickedpicker --}}
	<script type="text/javascript" src="{{ asset('libs/wickedpicker/0.4.1/wickedpicker.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/wickedpicker/0.4.1/wickedpicker.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.hotdeal.create'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
	<a href="{{ route('admin.hotdeal.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Hủy bỏ</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.hotdeal.store') }}" method="POST" id="mainForm" autocomplete="off">
		@csrf
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="product">Sản phẩm <span class="required">*</span></label>
							<div class="control-group">
								<select id="product" name="product" placeholder="Tên sản phẩm, Mã sản phẩm" autocomplete="off" required></select>
							</div>
							<script>
								$('#product').selectize({
									maxItems: 1,
									create: false,
									persist: false,
									valueField: 'id',
									labelField: 'name',
									searchField: ['name', 'code'],
									options: [
										@foreach($products as $product)
											{
												id: {{ $product->id }}, 
												name: '{{ $product->name }}', 
												code: '{{ $product->code }}', 
												thumbnail: '{{ $product->thumbnail }}',
												price: '{{ $product->price }}',
											},										
										@endforeach
									],
									render: {
								        item: function(item, escape) {
								        	var hitem = '<div class="selectize-item">';
								        		hitem +=	'<img src="https://cdn.fistore.vn/media/products/' + escape(item.thumbnail) + '">';
								        		hitem +=	'<div class="item-name">' + escape(item.name) + '</div>';
								        		hitem +=	'<div class="item-code">Mã sản phẩm: <strong>' + escape(item.code) + '</strong></div>';
								        		hitem +=	'<div class="item-price">Giá bán: <strong>' + number_format(escape(item.price)) + '</strong>đ</div>';
								            return hitem;						            
								        },
								        option: function(item, escape) {
							            	var hitem = '<div class="selectize-item">';
							            		hitem +=	'<img src="https://cdn.fistore.vn/media/products/' + escape(item.thumbnail) + '">';
							            		hitem +=	'<div class="item-name">' + escape(item.name) + '</div>';
							            		hitem +=	'<div class="item-code">Mã sản phẩm: <strong>' + escape(item.code) + '</strong></div>';
							            		hitem +=	'<div class="item-price">Giá bán: <strong>' + number_format(escape(item.price)) + '</strong>đ</div>';
							                return hitem;
								        }
								    },
								});
							</script>
						</div>
						<div class="row">
							<div class="col-lg-8">
								<div class="form-group">
									<label for="price">Giá bán <span class="required">*</span></label>
									<input type="text" name="price" class="form-control" onkeyup="make_money_format(event);" autocomplete="off" required>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="quantity">Số lượng</label>
									<input type="number" name="quantity" class="form-control" autocomplete="off" min="0">
									<span class="help-block">Tổng số sản phẩm bán ra trong đợt Sale.<br/>Để trống nếu không giới hạn</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="row row-mx-5">
									<div class="col-8">
										<div class="form-group">
											<label for="started_at">Ngày bắt đầu <span class="required">*</span></label>
											<div class="custom-datetimepicker">
												<div class="form-inline">
													<input type="text" name="started_at" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" autocomplete="off" required>
												</div>
											</div>
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>&emsp;</label>
											<input type="text" name="started_at_time" placeholder="00:00" class="form-control timepicker" autocomplete="off" required/>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="row row-mx-5">
									<div class="col-8">
										<div class="form-group">
											<label for="expired_at">Ngày hết hạn <span class="required">*</span></label>
											<div class="custom-datetimepicker">
												<div class="form-inline">
													<input type="text" name="expired_at" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" autocomplete="off" required>
												</div>
											</div>
										</div>
									</div>
									<div class="col-4">
										<div class="form-group">
											<label>&emsp;</label>
											<input type="text" name="expired_at_time" placeholder="00:00" class="form-control timepicker" autocomplete="off" required/>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group mt-2">
							<label class="custom-toggle toggle-danger">
								<input type="checkbox" name="disabled" class="toggle-checkbox">
								<div class="toggle-inner">
									<span class="toggle-button"></span>
								</div>
								<span class="toggle-text">Tạm ngưng cung cấp</span>
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navSaleOf").addClass("active").find("ul").addClass("show").find(".hotdeal").addClass("active");

		$("#mainForm").validate();

		// Datetime Picker
		$(function() {
			$( ".datepicker" ).datepicker({
				dateFormat : "dd/mm/yy"
			});
		});
		// Time Picker
		$('.timepicker').wickedpicker({
			now: "00:00",
			twentyFour: true,
			title: 'Thời gian',
			showSeconds: false,
		});
	</script>
	<style>
		.selectize-item{
			display: block;
		}
		.selectize-item img{
			float: left;
			width: 70px;
			margin-right: 10px;
			height: 4.5rem;
			object-fit: contain;
			padding: 2px;
			background-color: #fafafa;
			border-radius: 3px;
			border: 1px solid #DCDCDC;
		}
		.selectize-item .item-name{
			font-weight: 500;
		}
		.selectize-item .item-price strong{
			font-weight: 500;
			color: #ED0000;
		}
	</style>
@stop