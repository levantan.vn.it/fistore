@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Khách hàng')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.customer'))

{{-- Navs --}}
@section('nav')
	{{-- <a href="{{ route('admin.user.customer.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a> --}}
@stop

{{-- Main --}}
@section('main')
	<div class="card card-primary mb-4">
		<div class="card-header" data-toggle="collapse" href="#filterCollapse" aria-expanded="true">
			Tìm kiếm
		</div>
		<div class="card-body collapse show p-0" id="filterCollapse">
			<form action="" method="GET" class="m-4" id="filterForm" autocomplete="off">
				<div class="form-row form-group">
					<div class="col-lg-4">
						<label for="email">Email</label>
						<input type="email" name="email" value="{{ request()->email }}" class="form-control">
					</div>
					<div class="col-lg-4">
						<label for="name">Họ tên</label>
						<input type="text" name="name" value="{{ request()->name }}" class="form-control">
					</div>
					<div class="col-lg-4">
						<label for="provider">Loại tài khoản</label>
						<select name="provider" class="custom-select form-control">
							<option value=""></option>
							<option value="null"{!! request()->provider == 'null' ? ' selected' : null !!}>Tài khoản thường</option>
							<option value="facebook"{!! request()->provider == 'facebook' ? ' selected' : null !!}>Facebook</option>
							<option value="google"{!! request()->provider == 'google' ? ' selected' : null !!}>Google</option>
						</select>
					</div>
					<div class="col-lg-6">
						<label for="created_at_from">Ngày đăng ký</label>
						<div class="custom-datetimepicker">
							<div class="form-inline">
								<input type="text" name="created_at_from" value="{{ request()->created_at_from }}" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}">
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<label for="created_at_to">Đến ngày</label>
						<div class="custom-datetimepicker">
							<div class="form-inline">
								<input type="text" name="created_at_to" value="{{ request()->created_at_to }}" class="form-control datepicker" placeholder="dd/mm/yyyy" style="min-width: 100%;" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}">
							</div>
						</div>
					</div>
				</div>
				<div class="form-group text-center">
					<button class="btn btn-primary"><i class="fa fa-search"></i> Tìm kiếm</button>
					<a href="{{ url()->current() }}" class="btn btn-default"><i class="fa fa-refresh"></i> Xem tất cả</a>
				</div>
			</form>
		</div>
	</div>
	<div class="table-filter py-2">
		<div class="flex-row">
			<select name="show" class="custom-select mr-1" form="filterForm" onchange="this.form.submit()">
				<option value="25"{!! request()->show == '25' ? ' selected' : null !!}>Hiển thị 25 kết quả</option>
				<option value="50"{!! request()->show == '50' ? ' selected' : null !!}>Hiển thị 50 kết quả</option>
				<option value="100"{!! request()->show == '100' ? ' selected' : null !!}>Hiển thị 100 kết quả</option>
				<option value="200"{!! request()->show == '200' ? ' selected' : null !!}>Hiển thị 200 kết quả</option>
				<option value="500"{!! request()->show == '500' ? ' selected' : null !!}>Hiển thị 500 kết quả</option>
				<option value="99999999"{!! request()->show == '99999999' ? ' selected' : null !!}>Hiển thị tất cả</option>
			</select>
			<button class="btn btn-primary mr-1" name="export" value="excel" form="filterForm"><i class="fa fa-file-excel-o"></i> Export Excel</button>
		</div>
	</div>

	<table class="table table-primary table-striped table-responsive">
		<thead>
			<tr>
				<th class="no-sort" width="50"></th>
				<th width="300">Khách hàng</th>
				<th>Loại tài khoản</th>
				<th class="text-center">Điểm tích lũy</th>
				<th class="no-sort text-center" width="150">Tình trạng</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>
						<img src="{{ media_url('users', 'user.png') }}" width="60">
					</td>
					<td>
						<div>{{ $item->name }}</div>
						<div>{{ $item->email }}</div>
					</td>
					<td>{{ !is_null($item->provider) ? title_case($item->provider) : 'Tài khoản thường' }}</td>
					<td class="text-center">
						@if($item->extra_point > 0 && $item->extra_point < 100)
							<b class="fz-up-1">{{ $item->extra_point }}</b>
						@elseif($item->extra_point >= 100)
							<b class="fz-up-3">{{ $item->extra_point }}</b>
						@else
							{{ $item->extra_point }}
						@endif
					</td>
					<td>
						@if(!$item->disabled)
						<mark class="mark mark-success mark-block">Hợp lệ</mark>
						@else
						<mark class="mark mark-danger mark-block">Vô hiệu hóa</mark>
						@endif
					</td>
					<td>
						<ul class="table-options">
							<li>
								<a href="{{ route('admin.user.customer.show', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Xem thông tin"><i class="fa fa-external-link-square"></i></a>
							</li>
							@if(!$item->disabled)
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Vô hiệu hóa" onclick="disableItem(event, {{ $item->id }}, '{{ route('admin.user.customer.disable') }}')"><i class="fa fa-lock"></i></button>
								</li>
							@else
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Mở khóa" onclick="activateItem(event, {{ $item->id }}, '{{ route('admin.user.customer.activate') }}')"><i class="fa fa-unlock"></i></button>
								</li>
							@endif
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.user.customer.destroy', $item->id) }}')"><i class="fa fa-close"></i></button>
							</li>
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>

	@include('particles.pagination')
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navCustomer").addClass("active");

		// Datetime Picker
		$(function() {
			$( ".datepicker" ).datepicker({
				dateFormat : "dd/mm/yy"
			});
		});
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop