@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Chuyên mục bài viết')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.article.category'))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.article.category.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th>Tên chuyên mục</th>
				<th>Đường dẫn</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)			
				<tr>
					<td>
						{{ $item->name }}&emsp;{!! $item->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
					</td>
					<td>{{ $item->slug }}</td>
					<td>
						<ul class="table-options">
							<li>
								<a href="{{ route('admin.article.category.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
							</li>
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.article.category.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
							</li>
						</ul>
					</td>
				</tr>

				@foreach($item->fk_childs as $child)
					<tr>
						<td width="20">{{ $child->id }}</td>
						<td>
							— {{ $child->name }}&emsp;{!! $child->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
						</td>
						<td>{{ $child->slug }}</td>
						<td>{{ date('d/m/Y H:i', strtotime($child->created_at)) }}</td>
						<td>
							<ul class="table-options">
								<li>
									<a href="{{ route('admin.article.category.edit', $child->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.article.category.destroy', $child->id) }}')"><i class="fa fa-remove"></i></button>
								</li>
							</ul>
						</td>
					</tr>

					@foreach($child->fk_childs as $grandchild)
						<tr>
							<td width="20">{{ $grandchild->id }}</td>
							<td>
								——— {{ $grandchild->name }}&emsp;{!! $grandchild->pinned ? '<i class="fa fa-thumb-tack" data-toggle="tooltip" data-placement="top" title="Đã ghim lên trang chủ"></i>' : null !!}
							</td>
							<td>{{ $grandchild->slug }}</td>
							<td>{{ date('d/m/Y H:i', strtotime($grandchild->created_at)) }}</td>
							<td>
								<ul class="table-options">
									<li>
										<a href="{{ route('admin.article.category.edit', $grandchild->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
									</li>
									<li>
										<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.article.category.destroy', $grandchild->id) }}')"><i class="fa fa-remove"></i></button>
									</li>
								</ul>
							</td>
						</tr>
					@endforeach
				@endforeach
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navArticle").addClass("active").find("ul").addClass("show").find(".category").addClass("active");
	</script>

	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop