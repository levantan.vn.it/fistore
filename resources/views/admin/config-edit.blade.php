@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Cấu hình')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.configuration.edit'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu thay đổi</button>
	<a href="{{ asset('admin/configs') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.configuration.update') }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@csrf
		<div class="row">
			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tên website <span class="required">*</span></label>
							<input type="text" name="name" value="{{ $data->name }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="title">Tiêu đề <span class="required">*</span></label>
							<input type="text" name="title" value="{{ $data->title }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="description">Mô tả</label>
							<textarea name="description" rows="5" class="form-control">{!! $data->description !!}</textarea>
						</div>
						<div class="form-group">
							<label for="domain">Địa chỉ tên miền <span class="required">*</span></label>
							<div class="input-group">
								<span class="input-group-addon addon-info">https://</span>
								<input type="text" name="domain" value="{{ $data->domain }}" placeholder="yourdomain.com" class="form-control" readonly required>
							</div>
						</div>
						<div class="form-group">
							<label for="keywords">Từ khóa tìm kiếm</label>
							<textarea name="keywords" rows="5" class="form-control">{!! $data->keywords !!}</textarea>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label for="logo">Ảnh logo</label>
									<div class="custom-image-upload custom-iu-sm image-preview m-0">
										<img class="img-preview" src="{{ media_url(null, $data->logo) }}">
										<label class="custom-label">
											<input type="file" name="logo" onchange="upload_img_preview(event);" accept="image/*">
										</label>
										<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
											<path d="M2 30 L30 2 M30 30 L2 2"></path>
										</svg>
									</div>
								</div>
							</div>

							<div class="col-lg-6">
								<div class="form-group">
									<label for="favicon">Favicon</label>
									<div class="custom-image-upload custom-iu-sm image-preview m-0">
										<img class="img-preview" src="{{ media_url(null, $data->favicon) }}">
										<label class="custom-label">
											<input type="file" name="favicon" onchange="upload_img_preview(event);" accept="image/*">
										</label>
										<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
											<path d="M2 30 L30 2 M30 30 L2 2"></path>
										</svg>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-6">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="author">Tên Doanh nghiệp/Cá nhân</label>
							<input type="text" name="author" value="{{ $data->author }}" class="form-control">
						</div>
						<div class="form-group">
							<label for="introduce">Mô tả Doanh nghiệp/Cá nhân</label>
							<textarea name="introduce" rows="5" class="form-control">{!! $data->introduce !!}</textarea>
						</div>
						<div class="form-group">
							<label for="contact_email">Email liên hệ <span class="required">*</span></label>
							<input type="email" name="contact_email" value="{{ $data->contact_email }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="support_email">Email hỗ trợ <span class="required">*</span></label>
							<input type="email" name="support_email" value="{{ $data->support_email }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="hotline_1">Hotline 1 <span class="required">*</span></label>
							<input type="text" name="hotline_1" value="{{ $data->hotline_1 }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="hotline_2">Hotline 2</label>
							<input type="text" name="hotline_2" value="{{ $data->hotline_2 }}" class="form-control">
						</div>
						<div class="form-group">
							<label for="fax">Fax</label>
							<input type="text" name="fax" value="{{ $data->fax }}" class="form-control">
						</div>
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label for="province">Tỉnh thành <span class="required">*</span></label>
									<select name="province" class="custom-select form-control" onchange="get_districts(event)" required>
										<option value=""></option>
										@foreach($provinces as $province)
											<option value="{{ $province->id }}"{!! $data->province_id === $province->id ? ' selected' : null !!}>{{ $province->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="district">Quận huyện <span class="required">*</span></label>
									<select name="district" class="custom-select form-control" onchange="get_wards(event)" required>
										<option value=""></option>
										@foreach($districts as $district)
											<option value="{{ $district->id }}"{!! $data->district_id === $district->id ? ' selected' : null !!}>{{ $district->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label for="district">Phường xã <span class="required">*</span></label>
									<select name="ward" class="custom-select form-control" required>
										<option value=""></option>
										@foreach($wards as $ward)
											<option value="{{ $ward->id }}"{!! $data->ward_id === $ward->id ? ' selected' : null !!}>{{ $ward->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="address">Địa chỉ <span class="required">*</span></label>
							<input type="text" name="address" value="{{ $data->address }}" placeholder="Số nhà, Tên đường" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="map">Nhúng bản đồ</label>
							<textarea name="map" rows="3" class="form-control">{{ $data->map }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navConfig").addClass("active");

		$("#mainForm").validate();
	</script>
	<script src="{{ asset('js/areas.handler.js') }}"></script>
@stop