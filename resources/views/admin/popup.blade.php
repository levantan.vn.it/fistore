@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Cập nhật popup')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('public/libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('public/libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.popup'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.popup.savepopup') }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@csrf
		<div class="row">
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-6">
						<div class="card card-primary">
							<div class="card-header justify-content-lg-between d-flex">
								<div>POPUP cho Máy tính</div>
								<label class="custom-toggle toggle-info mb-0">
									<label class="mr-3">Bật/Tắt</label>
									<input type="checkbox" name="popup_lg_sts" value="1" class="toggle-checkbox" {!! ($popupLG->disabled ?? true) ? '' : 'checked' !!}>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
							<div class="card-body">
								<div class="form-group text-center">
									<div class="custom-image-upload custom-iu-md image-preview">
										<img class="img-preview" src="{{ media_url('popups', $popupLG->value ?? null) }}">
										<label class="custom-label">
											<input type="file" name="popup_lg" onchange="upload_img_preview(event);" accept="image/*">
										</label>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="card card-primary">
							<div class="card-header justify-content-lg-between d-flex">
								<div>POPUP cho Mobile</div>
								<label class="custom-toggle toggle-info mb-0">
									<label class="mr-3">Bật/Tắt</label>
									<input type="checkbox" name="popup_sm_sts" value="1" class="toggle-checkbox" {!! ($popupSM->disabled ?? true) ? '' : 'checked' !!}>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
							<div class="card-body">
								<div class="form-group text-center">
									<div class="custom-image-upload custom-iu-md image-preview">
										<img class="img-preview" src="{{ media_url('popups', $popupSM->value ?? null) }}">
										<label class="custom-label">
											<input type="file" name="popup_sm" onchange="upload_img_preview(event);" accept="image/*">
										</label>
									</div>
								</div>
								
							</div>
						</div>
					</div>

				</div>
				<div class="card card-body">
					<div class="form-group">
						<label for="name">Đường dẫn POPUP <span class="required">*</span></label>
						<input type="text" name="popup_url" value="{{ $popupURL->value ?? null }}" class="form-control" required>
					</div>
				</div>
			</div>

			<div class="col-lg-4">
				<div class="card card-primary">
					<div class="card-header justify-content-lg-between d-flex">
						<div>Khung sản phẩm</div>
						<label class="custom-toggle toggle-info mb-0">
							<label class="mr-3">Bật/Tắt</label>
							<input type="checkbox" name="product_border_sts" value="1" class="toggle-checkbox" {!! ($productBorder->disabled ?? true) ? '' : 'checked' !!}>
							<div class="toggle-inner mr-0">
								<span class="toggle-button"></span>
							</div>
						</label>
					</div>
					<div class="card-body">
						<div class="form-group">
							<div class="custom-image-upload custom-iu-md image-preview">
								<img class="img-preview" src="{{ media_url('popups', $productBorder->value ?? null) }}">
								<label class="custom-label">
									<input type="file" name="product_border" onchange="upload_img_preview(event);" accept="image/*">
								</label>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	
@stop