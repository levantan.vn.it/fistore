@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Nhóm quyền')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.role'))

{{-- Navs --}}
@section('nav')
	<a href="{{ current_url('/create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th class="text-center" width="80">Key</th>
				<th>Tên nhóm quyền</th>
				<th class="text-center sort-asc" width="80">Cấp bậc</th>
				<th class="no-sort" width="500">Ghi chú</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td width="80"><mark class="mark mark-info mark-block">{{ $item->key }}</mark></td>
					<td>{{ $item->name }}<br/>{{ $item->email }}</td>
					<td width="80" class="text-center">{{ $item->level ?? 'Không phân cấp' }}</td>
					<td>{!! $item->comment !!}</td>
					<td>
						@if(Auth::guard('admin')->user()->fk_role->level < $item->level)
						<ul class="table-options">
							<li>
								<a href="{{ route('admin.permission.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Phân uyền"><i class="fa fa-shield"></i></a>
							</li>
							<li>
								<a href="{{ route('admin.role.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
							</li>
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.role.destroy', $item->id) }}')"><i class="fa fa-close"></i></button>
							</li>
						</ul>
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navAccount").addClass("active").find("ul").addClass("show").find(".role").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop