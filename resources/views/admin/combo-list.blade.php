@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Combo')

{{-- Import CSS, JS --}}
@section('header')
	{{-- DataTable --}}
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">

	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.combo'))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.combo.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-hover table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th class="no-sort" width="50"></th>
				<th>Tên combo</th>
				<th>Thứ tự</th>
				<th>Giá bán</th>
				<th class="text-center">Tồn kho</th>
				<th>Sản phẩm</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>
						<a data-fancybox="gallery-{{ $item->id }}" href="{{ media_url('products', $item->thumbnail) }}">
							<img src="{{ media_url('products', $item->thumbnail) }}" width="60">
						</a>
					</td>
					<td class="white-space-unset">
						<div>
							<a href="{{ combo_link($item->slug) }}" target="_blank">{{ $item->name }}</a>&nbsp;
						</div>
					</td>
					<td>
						<select name="position" class="custom-select" onchange="updatePosition(event, {{ $item->id }}, '{{ route('admin.combo.position') }}')">
							@for($i = 0; $i <= count($data); $i++)
							<option value="{{ $i }}"{!! $item->position == $i ? ' selected' : null !!}>{{ $i }}</option>
							@endfor
						</select>
					</td>
					<td>
						<div>{{ number_format($item->price, 0, ',', '.') }}</div>
					</td>
					<td class="text-center">{{ $item->quantity ?? 'Không giới hạn' }}</td>
					<td>
						@foreach($item->fk_products as $product)
						<div>{{ $product->name }}</div>
						@endforeach
					</td>
					<td>
						<ul class="table-options">
							<li>
								<a href="{{ route('admin.combo.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
							</li>
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.combo.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
							</li>
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navSaleOf").addClass("active").find("ul").addClass("show").find(".combo").addClass("active");
	</script>

	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop