@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Tạo sản phẩm mới')

{{-- Import CSS, JS --}}
@section('header')
	{{-- Jquery Validation --}}
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>

	{{-- CKEditor --}}
	<script src="{{ asset('libs/ckeditor/4.10.0/ckeditor.js') }}"></script>

	{{-- Selectize --}}
	<script type="text/javascript" src="{{ asset('libs/selectize/0.12.4/js/standalone/selectize.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/selectize/0.12.4/css/selectize.css') }}">

	{{-- Wheel Color Picker --}}
	<link rel="stylesheet" href="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.css') }}">
	<script src="{{ asset('libs/wheelcolorpicker/3.0.3/wheelcolorpicker.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.product.create'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Đăng sản phẩm</button>
	<button class="btn btn-info" name="draft" value="1" form="mainForm"><i class="fa fa-clipboard fa-fw"></i> Lưu bản nháp</button>
	<a href="{{ route('admin.product.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Hủy bỏ</a>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.product.store') }}" method="POST" enctype="multipart/form-data" id="mainForm">
		@csrf
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Tên sản phẩm <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" onkeyup="slug_make();" required>
						</div>
						<div class="form-group">
							<label for="content">Thông tin sản phẩm</label>
							<textarea name="content" id="content"></textarea>
							<script>
								CKEDITOR.replace('content');
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="content">Thông số kỹ thuật</label>
							<textarea name="properties" id="property"></textarea>
							<script>
								CKEDITOR.replace('property');
							</script>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="code">Mã sản phẩm (SKU) <span class="required">*</span></label>
							<input type="text" name="code" value="{{ $code }}" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="external_id">AFR product code</label>
							<input type="text" name="external_id" class="form-control">
						</div>
						<div class="form-group">
							<label for="group">Số lượng</label>
							<input type="number" name="quantity" min="0" class="form-control">
							<span class="help-block">Để trống nếu không giới hạn số lượng</span>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label for="group">Giá bán hiện tại <span class="required">*</span></label>
									<div class="input-group">
										<span class="input-group-addon addon-info">đ</span>
										<input type="text" name="price" class="form-control" onkeyup="make_money_format(event)" required>
									</div>
									<label id="price-error" class="error" for="price" style="display: none;"></label>
								</div>
								<div class="form-group">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" name="display_price" class="custom-control-input" checked>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Hiển thị giá bán</span>
									</label>
									<span class="help-block">Bỏ chọn nếu muốn hiển thị giá là <b>"Liên hệ"</b></span>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label for="group">Giá thị trường</label>
									<div class="input-group">
										<span class="input-group-addon addon-info">đ</span>
										<input type="text" name="original_price" class="form-control" onkeyup="make_money_format(event)">
									</div>
									<span class="help-block">Giá thị trường luôn cao hơn giá hiện tại. Không bắt buộc nhập</span>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<div class="float-right">
								<button type="button" class="btn btn-info" id="addVersionBtn" onclick="addVersion()">Thêm phiên bản</button>
							</div>
							<label>Sản phẩm có nhiều phiên bản</label>
							<hr>
							<table width="100%" cellpadding="3" class="mb-3" id="versionTable" style="display: none;">
								<thead>
									<tr>
										<th width="20%">Tên mẫu <span class="required">*</span></th>
										<th width="20%">Mã sản phẩm (SKU) <span class="required">*</span></th>
										<th width="15%">Màu sắc</th>
										<th width="15%">AFR product code</th>
										<th width="20%">Giá bán</th>
										<th width="80">Số lượng</th>
										<th width="20"></th>
									</tr>
								</thead>
								@for($i = 0;$i < 100; $i++)
								<tbody style="display: none;" {!! $i % 2 == 0 ? 'bgcolor="#EDEDED"' : 'bgcolor="#D9D9D9"' !!}>
									<tr>
										<td>
											<input type="text" name="version_name[{{ $i }}]" class="form-control table-element" required disabled>
										</td>
										<td>
											<input type="text" name="version_code[{{ $i }}]" class="form-control table-element" required disabled>
										</td>
										<td>
											<input type="text" name="version_external_id[{{ $i }}]" class="form-control table-element" required disabled>
										</td>
										<td>
											<div class="input-group">
												<input type="text" name="version_color[{{ $i }}]" class="form-control table-element" placeholder="000000" data-wheelcolorpicker="" onchange="colorPreview(event)"{!! isset($versions[$i]) ? null : ' disabled' !!}>
												<span class="input-group-addon">&emsp;</span>
											</div>
										</td>
										<td>
											<input type="text" name="version_price[{{ $i }}]" class="form-control table-element" onkeyup="make_money_format(event)" disabled>
										</td>
										<td>
											<input type="number" name="version_quantity[{{ $i }}]" class="form-control table-element" min="0" disabled>
										</td>
										<td style="white-space:nowrap;text-align: center;">
											<button type="button" class="btn btn-link text-muted px-1" data-toggle="tooltip" data-placement="top" title="Thêm ảnh" onclick="addVersionImage({{ $i }})"><i class="fa fa-picture-o fa-lg"></i></button>
											<button type="button" class="btn btn-link text-muted px-1" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="removeVersion()"><i class="fa fa-trash fa-lg"></i></button>
										</td>
									</tr>
									<tr>
										<td colspan="5" class="py-0">
											<div class="selected-images row row-mx-5">

											</div>
										</td>
										<td class="py-0"></td>
									</tr>
									
								</tbody>
								@endfor
							</table>
							<script>
								function colorPreview(event){
									$(event.target).parents(".input-group").find(".input-group-addon").css("background", "#" + $(event.target).val());
								}
							</script>
						</div>
					</div>
					<div class="card-footer">
						Bạn đã thêm <b id="VersionCount">0</b> phiên bản.<!-- Tối đa <b>20</b> phiên bản.-->
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="float-right">
							<button type="button" class="btn btn-link" data-toggle="collapse" href="#seoOption">Tùy chỉnh nội dung SEO</button>
						</div>
						<label>Xem trước kết quả tìm kiếm</label>

						<div class="seo-container">
							<p class="seo-title">Tên sản phẩm</p>
							<p class="seo-url">{{ url('ten-san-pham-123456.htm') }}</p>
							<p class="seo-description">Mô tả sản phẩm hoặc trang web được rút gọn từ bài viết thông tin sản phẩm</p>

							<div class="collapse" id="seoOption">
								<div class="card">
									<hr/>
									<div class="form-group">
										<label>Tiêu đề</label>
										<input type="text" name="title" class="form-control" onkeyup="title_make();">
									</div>

									<div class="form-group">
										<label>Mô tả</label>
										<textarea rows="3" name="description" class="form-control" onkeyup="description_make();"></textarea>
									</div>

									<div class="form-group">
										<label>Keywords</label>
										<textarea rows="2" name="keywords" class="form-control"></textarea>
									</div>

									<div class="form-group">
										<label>Đường dẫn <span class="required">*</span></label>
										<div class="input-group">
											<span class="input-group-addon addon-info">{{ url('') }}/</span>
											<input type="text" name="slug" class="form-control" placeholder="ten-san-pham-123456" required>
											<span class="input-group-btn">
												<button class="btn btn-info slug-refresh" type="button" onclick="slug_make();"><i class="fa fa-refresh"></i></button>
											</span>
										</div>
										<label id="slug-error" class="error" for="slug" style="display: none;"></label>
									</div>
									<script>
										$(".slug-refresh").click(function() {
											var event = $(this);
											$(this).find("i").addClass("fa-spin");
											setTimeout(function() {
												$(event).find("i").removeClass("fa-spin");
											}, 1000);
										});
									</script>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Ảnh đại diện sản phẩm</label>
							<div class="custom-image-upload custom-iu-md image-preview m-0">
								<img class="img-preview">
								<label class="custom-label">
									<input type="file" name="thumbnail" onchange="upload_img_preview(event);" accept="image/*">
								</label>
								<svg id="i-close" onclick="remove_file(event);" class="remove-preview" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
									<path d="M2 30 L30 2 M30 30 L2 2"></path>
								</svg>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<label>Ảnh mô tả sản phẩm</label>
							<div id="multiImagePanel">
								<div class="row">
                                    <input type="hidden" id="numimage" value="1">
									<div class="img-wrap col-md-3">
										<div class="image_1" id="image">
											<label for="image_1">
												<img id="image_preview_1" height="130px">
												<input type="text" class="d-block form-control" id="image_name_1" placeholder="Tiêu đề" name="names[]">
												<input type="number" class="d-block form-control" id="image_index_1" placeholder="Thứ tự" name="indexes[]">
											</label>
											<input type="file" class="photo" name="images[]" id="image_1" onchange="makeImageArr(event, 1)" accept="image/*">
											<svg id="i-close" onclick="this.parentElement.parentElement.remove();" class="remove button_1" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
												<path d="M2 30 L30 2 M30 30 L2 2"></path>
											</svg>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group">
							<button class="btn btn-info" type="button" id="add_image"><i class="fa fa-plus"></i>&emsp;Thêm ảnh</button>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg 4">
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<div class="flex-row">
								<label for="show">Hiển thị công khai</label>
								<label class="custom-toggle toggle-info mb-0">
									<input type="checkbox" name="show" class="toggle-checkbox" checked>
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
						</div>

						<div class="form-group">
							<div class="flex-row">
								<label for="closed">Tắt chức năng đặt hàng</label>
								<label class="custom-toggle toggle-danger mb-0">
									<input type="checkbox" name="close" class="toggle-checkbox">
									<div class="toggle-inner mr-0">
										<span class="toggle-button"></span>
									</div>
								</label>
							</div>
							<span class="help-block">Chỉ hiển thị thông tin sản phẩm, không kinh doanh sản phẩm này</span>
						</div>
						
						<div class="form-group">
							<label for="group">Trạng thái sản phẩm</label>
							<select name="status" class="custom-select form-control" >
								@foreach ($status as $key => $value)
									<option value="{{ $key }}">{{ $value }}</option>
								@endforeach 
							</select>
						</div>
	
					</div>
				</div>
				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="group">Nhóm sản phẩm</label>
							<div class="card px-4 py-3" style="background: #F8F8F8">
								{!! get_checkbox_product_groups_control() !!}
							</div>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label for="group">Loại sản phẩm</label>
							{!! get_selectize_product_categories_control() !!}
							<label id="categories-selectized-error" class="error" for="categories-selectized" style="display: none"></label>
						</div>
						<div class="form-group">
							<label for="group">Thương hiệu <span class="required">*</span></label>
							{!! get_selectize_product_brands_control() !!}
							<label id="brands-selectized-error" class="error" for="brands-selectized" style="display: none"></label>
						</div>
					</div>
				</div>

				<div class="card">
					<div class="card-body">
						<div class="form-group">
							<label>Thêm từ khóa liên quan (Tags)</label>
							<div class="control-group">
								<select id="tags" name="tags[]" multiple placeholder="Chọn trong danh sách hoặc Enter để thêm từ khóa">
									@foreach($tags as $tag)
										<option value="{{ $tag->keyword }}">{{ $tag->keyword }}</option>
									@endforeach
								</select>
							</div>
							<script>
								$('#tags').selectize({
									plugins: ['remove_button'],
									delimiter: ',',
									persist: false,
									createOnBlur: true,
									create: true
								});
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navProduct").addClass("active").find("ul").addClass("show").find(".product").addClass("active");

		$("#mainForm").validate();

		// TODO: Check nếu form submit thì disabled các nút
		$('#mainForm').submit(function (e) {
			$('.btn-success[form="mainForm"]').prop('disabled', true);
			$('.btn-info[form="mainForm"]').prop('disabled', true);

			if (handle == 'draft') {
				$('.btn-info[form="mainForm"] i').attr('class', 'fa fa-fw fa-pulse fa-spinner');
			}else{
				$('.btn-success[form="mainForm"] i').attr('class', 'fa fa-fw fa-pulse fa-spinner');
			}
		})

		var handle = '';
		$('.btn-success[form="mainForm"]').click(function (e) {
			handle = 'save';
		})
		$('.btn-info[form="mainForm"]').click(function (e) {
			handle = 'draft';
		})
		// Multi-Image Handle
		var product_id = "";
		var i = 2;
	</script>
	<script src="{{ asset('js/admin/multi-image.handler.js') }}"></script>
	<script src="{{ asset('js/admin/seo-preview.handler.js') }}"></script>
	<script src="{{ asset('js/admin/product.handler.js') }}"></script>
	<script type="text/javascript">
		if (typeof CKEDITOR != "undefined") {
			for (var i in CKEDITOR.instances) {
				CKEDITOR.instances[i].on('key', function(e) {
					$(window).bind('beforeunload', function() {
						return 'Bạn có chắc chắn chuyển sang trang khác, Nếu thực hiện dữ liệu sẽ không được lưu';
					});
				});
			}
		}
	</script>
@stop