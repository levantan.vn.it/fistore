@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Bình luận')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.comment'))

{{-- Navs --}}
@section('nav')
	@if(request('trash'))
		<a href="{{ current_url() }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
	@else
		<a href="{{ current_url('?trash=1') }}" class="btn btn-default"><i class="fa fa-trash fa-fw"></i> Thùng rác</a>
	@endif
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th width="200">Người dùng</th>
				<th width="500">Nội dung bình luận</th>
				<th>Bình luận trong</th>
				<th width="80" class="text-center">Báo xấu</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				<tr>
					<td>
						<div class="font-weight-bold">{{ $item->fk_author->name }}</div>
						<div class="font-italic"><a href="mailto:{{ $item->fk_author->email }}">{{ $item->fk_author->email }}</a></div>
					</td>
					<td class="white-space-unset">
						<div class="text-muted">Bình luận vào {{ date('d/m/Y', strtotime($item->created_at)) }} lúc {{ date('H:s', strtotime($item->created_at)) }}</div>
						<div>{!! str_limit($item->content, 200) !!}</div>
					</td>
					<td class="white-space-unset">
						<div class="font-weight-bold"><a href="{{ product_link($item->fk_product) }}" target="_blank">{{ $item->fk_product->name }}</a></div>
					</td>
					<td class="text-center">
						<div class="badge badge-danger py-2 px-3" style="font-size: 1rem">{{ $item->fk_reports->count() }}</div>
					</td>
					<td>
						<ul class="table-options">
							@if(request('trash'))
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Khôi phục" onclick="restoreItem(event, {{ $item->id }}, '{{ route('admin.comment.restore') }}')"><i class="fa fa-undo"></i></button>
								</li>
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.comment.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
								</li>
							@else
								<li>
									<button type="button" data-toggle="tooltip" data-placement="top" title="Chuyển vào thùng rác" onclick="trashItem(event, {{ $item->id }}, '{{ route('admin.comment.trash') }}')"><i class="fa fa-trash"></i></button>
								</li>
							@endif
						</ul>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navFeedback").addClass("active").find("ul").addClass("show").find(".comment").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop