<!DOCTYPE html>
<html>
<head>
	{{-- Meta --}}
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	{{-- Link --}}
	<link rel="icon" href="{{ media_url('', $configs->favicon) }}">

	<title>@yield('title')</title>

	{{-- JQuery --}}
	<script src="{{ asset('libs/jquery/jquery-3.2.1.min.js') }}"></script>
	
	{{-- Google Jquery Library --}}
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	{{-- JQuery UI --}}
	<link rel="stylesheet" href="{{ asset('libs/jquery-ui/1.12.1/jquery-ui.min.css') }}">
	<script src="{{ asset('libs/jquery-ui/1.12.1/jquery-ui.min.js') }}"></script>

	{{-- Bootstrap --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/bootstrap/4-4.0.0-beta/css/bootstrap.min.css') }}"/>	
	<script src="{{ asset('libs/popper/1.14.3/popper.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('libs/bootstrap/4-4.0.0-beta/js/bootstrap.min.js') }}"></script>
	
	{{-- Font Awesome --}}
	<link rel="stylesheet" href="{{ asset('libs/font-awesome/4.7.0/css/font-awesome.min.css') }}">
	
	{{-- Toastr Notification JS --}}
	<script type="text/javascript" src="{{ asset('libs/toastr/2.1.3/toastr.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('libs/toastr/2.1.3/toastr.config.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/toastr/2.1.3/toastr.min.css') }}">

	{{-- Sweet Alert --}}
	<script src="{{ asset('libs/sweetalert/2v7.0.9/sweetalert.min.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('libs/sweetalert/2v7.0.9/sweetalert.min.css') }}">

	<!-- Import -->
	@yield('header')

	{{-- StyleSheet --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('css/admin/admin.css') }}">

	<script>
		$(window).on('load', function() {
			$(".background").fadeOut(1000);
		});
		$(document)
		.ajaxStart(function () {
			$(".background").fadeIn("faster");
		})
		.ajaxStop(function () {
			$(".background").fadeOut(1000);
		});

		// Base URL
		var base_url = "{!! url('') !!}/";
	</script>
</head>
<body class="scrollbar">
	<div class="background">
		<div class="loader"></div>
	</div>

	<button id="sideRight" onclick="sideRight()">Mở rộng &nbsp;<i class="fa fa-chevron-circle-right fa-lg"></i></button>
	@include('admin.layouts.sidebar')

	<main class="viewport">
		@include('admin.layouts.navigation')
		<main>
			@yield('main')
		</main>
	</main>
	
	@yield('footer')
	<!-- JS -->
	<script src="{{ asset('js/main.handler.js') }}"></script>
	<script>
		// Đóng Sidebar
		function sideLeft(){
			$(".sidebar").addClass("slide-left");
			$(".viewport").addClass("slide-left");
			$(".navbar").addClass("slide-left");
		};
		// Mở sidebar
		function sideRight(){
			$(".sidebar").removeClass("slide-left");
			$(".viewport").removeClass("slide-left");
			$(".navbar").removeClass("slide-left");
		};
		// Tự ẩn sidebar
		$(document).ready(function(){
			if($(window).width() < 992) sideLeft();
		});
	</script>
	@if(Session::has('alert'))
		<script>
			toastr.{!! Session::get('alert') !!}("<b>{!! Session::get('title') !!}</b><br/>{!! Session::get('msg') !!}{!! Session::get('message') !!}");
		</script>
		{{ Session::forget('alert') }}
	@endif
</body>
</html>
