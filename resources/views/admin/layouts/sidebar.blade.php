<aside class="sidebar">
	<header class="authen">
		<div class="account-info">
			<div class="media">
				<img class="avatar align-self-center mr-3" src="{{ media_url('users', Auth::guard('admin')->user()->avatar) }}">
				<div class="media-body">
					<h5 class="full-name">{{ Auth::guard('admin')->user()->name }}</h5>
					<span class="security">{{ Auth::guard('admin')->user()->fk_role->name }}</span>
				</div>
			</div>
		</div>
	</header>
	<div class="sidebar-list">
		<div id="sidebarList" class="scrollbar" data-children=".item">
			<div class="shortcuts">
				<div class="shortcut-contain row">
					<div class="shortcut col-3">
						<a id="sideLeft" onclick="sideLeft()" data-toggle="tooltip" data-placement="top" title="Thu gọn">
							<i class="fa fa-chevron-circle-left fa-lg"></i>
						</a>
					</div>
					<div class="shortcut col-3">
						<a href="{{ url('') }}" target="_blank" data-toggle="tooltip" data-placement="top" title="Trang chủ">
							<i class="fa fa-home fa-lg"></i>
						</a>
					</div>
					<div class="shortcut col-3">
						<a href="{{ route('admin.profile.show') }}" data-toggle="tooltip" data-placement="top" title="Hồ sơ cá nhân">
							<i class="fa fa-user fa-lg"></i>
						</a>
					</div>
					<div class="shortcut col-3">
						<a href="{{ route('admin.logout') }}" data-toggle="tooltip" data-placement="top" title="Đăng xuất">
							<i class="fa fa-sign-out fa-lg"></i>
						</a>
					</div>
				</div>
			</div>

			<div class="item single" id="navDashboard">
				<a href="{{ route('admin.dashboard') }}">
					<i class="fa fa-dashboard fa-fw fa-lg"></i>&nbsp; Bảng điều khiển
				</a>
			</div>

			@can('product-access')
			<div class="item" id="navProduct">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemProduct">
					<i class="fa fa-product-hunt fa-fw fa-lg"></i>&nbsp; Sản phẩm
				</a>
				<ul id="itemProduct" class="collapse">
					<li class="product"><a href="{{ route('admin.product.index') }}">Sản phẩm</a></li>
					@can('product-cat-access')
					<li class="category"><a href="{{ route('admin.product.category.index') }}">Loại sản phẩm</a></li>
					@endcan
					@can('brand-access')
					<li class="brand"><a href="{{ route('admin.product.brand.index') }}">Thương hiệu</a></li>
					@endcan
					@can('product-group-access')
					<li class="group"><a href="{{ route('admin.product.group.index') }}">Nhóm sản phẩm</a></li>
					@endcan
				</ul>
			</div>
			@endcan
                    
            @can('product-cat-access')
			<div class="item" id="navProductCategory">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemProductCategory">
					<i class="fa fa-list-ul fa-fw fa-lg"></i>&nbsp;&nbsp;Chuyên mục sản phẩm
				</a>
				<ul id="itemProductCategory" class="collapse">
					@if(isset($listcategory))
						@foreach($listcategory as $lp)
					<li class="category">
						<a href="{{ route('admin.product.index') }}?category={{ $lp['id'] }}">
							{{ $lp['name'] }}
						</a>
					</li>
					@if(isset($lp['listchild']))
						@php 
							$listchild = $lp['listchild'];
						@endphp
						@foreach($listchild as $lpp)
					<li class="subcategory">
						<a href="{{ route('admin.product.index') }}?category={{ $lpp['id'] }}">
							{{ $lpp['name'] }}
						</a>
					</li>
						@endforeach
					@endif
						@endforeach
					@endif
					
				</ul>
			</div>
			@endcan

			@can('brand-access')
			<div class="item" id="navProductBrand">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemBrand">
					<i class="fa fa-map-signs fa-fw fa-lg"></i>&nbsp; Thương hiệu
				</a>
				<ul id="itemBrand" class="collapse">
					@if(isset($listbrand))
						@foreach($listbrand as $lp)
					<li class="brand">
						<a href="{{ route('admin.product.index') }}?brand={{ $lp['id'] }}">
							{{ $lp['name'] }}
						</a>
					</li>
						@endforeach
					@endif
				</ul>
			</div>
			@endcan
			
			@canany(['hotdeal-access', 'freetrial-access', 'combo-access','sale-access'])
			<div class="item" id="navSaleOf">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemSaleOf">
					<i class="fa fa-gift fa-fw fa-lg"></i>&nbsp; Khuyến mãi
				</a>
				<ul id="itemSaleOf" class="collapse">
					@can('hotdeal-access')
					<li class="hotdeal"><a href="{{ route('admin.hotdeal.index') }}">Hot Deal</a></li>
					@endcan
					@can('freetrial-access')
					<li class="freetrial"><a href="{{ route('admin.freetrial.index') }}">Trải nghiệm sản phẩm</a></li>
					@endcan
					@can('combo-access')
					<li class="combo"><a href="{{ route('admin.combo.index') }}">Combo</a></li>
					@endcan
					@can('sale-access')
					<li class="sales"><a href="{{ route('admin.sale.index') }}">Chương trình khuyến mãi</a></li>
					@endcan
				</ul>
			</div>
			@endcanany
			
			@canany(['order-access', 'transportation-access', 'coupon-access'])
			<div class="item" id="navOrder">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemOrder">
					<i class="fa fa-shopping-basket fa-fw fa-lg"></i>&nbsp; Bán hàng
				</a>
				<ul id="itemOrder" class="collapse">
					@can('order-access')
					<li class="order"><a href="{{ route('admin.product.order.index') }}">Đơn hàng</a></li>
					@endcan
					@can('transportation-access')
					<li class="transportation"><a href="{{ route('admin.transportation.index') }}">Vận chuyển</a></li>
					@endcan
					@can('coupon-access')
					<li class="coupon"><a href="{{ route('admin.coupon.index') }}">Mã khuyến mãi</a></li>
					@endcan
					@can('extra-point-access')
					<li class="extra"><a href="{{ route('admin.extra_point.show') }}">Điểm tích lũy</a></li>
					@endcan
				</ul>
			</div>
			@endcanany
			
			@can('customer-access')
			<div class="item single" id="navCustomer">
				<a href="{{ route('admin.user.customer.index') }}">
					<i class="fa fa-handshake-o fa-fw fa-lg"></i>&nbsp; Khách hàng
				</a>
			</div>
			@endcan

			@can('admin-access')
			<div class="item" id="navAccount">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemAccount">
					<i class="fa fa-users fa-fw fa-lg"></i>&nbsp; Tài khoản
				</a>
				<ul id="itemAccount" class="collapse">
					<li class="user"><a href="{{ route('admin.user.admin.index') }}">Quản trị</a></li>
					@can('permission-access')
					<li class="role"><a href="{{ route('admin.role.index') }}">Nhóm quyền</a></li>
					@endcan
				</ul>
			</div>
			@endcan
			
			@can('page-access')
			<div class="item" id="navPage">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemPage">
					<i class="fa fa-leanpub fa-fw fa-lg"></i>&nbsp; Trang nội dung
				</a>
				<ul id="itemPage" class="collapse">
					<li class="page"><a href="{{ route('admin.page.index') }}">Trang nội dung</a></li>
					@can('page-cat-access')
					<li class="group"><a href="{{ route('admin.page.group.index') }}">Chuyên mục</a></li>
					@endcan
				</ul>
			</div>
			@endcan
			
			@can('article-access')
			<div class="item" id="navArticle">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemArticle">
					<i class="fa fa-feed fa-fw fa-lg"></i>&nbsp; Bài viết
				</a>
				<ul id="itemArticle" class="collapse">
					<li class="article"><a href="{{ route('admin.article.index') }}">Bài viết</a></li>
					@can('article-cat-access')
					<li class="category"><a href="{{ route('admin.article.category.index') }}">Chuyên mục</a></li>
					@endcan
				</ul>
			</div>
			@endcan

			@canany(['review-access', 'report-access'])
			<div class="item" id="navFeedback">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemFeedback">
					<i class="fa fa-comments-o fa-fw fa-lg"></i>&nbsp; Phản hồi
				</a>
				<ul id="itemFeedback" class="collapse">
					@can('review-access')
					<li class="review"><a href="{{ route('admin.review.index') }}">Đánh giá</a></li>
					@endcan
					@can('report-access')
					<li class="report"><a href="{{ route('admin.report.index') }}">Báo xấu</a></li>
					@endcan
				</ul>
			</div>
			@endcanany

			@canany(['contact-access', 'newsletter-access'])
			<div class="item" id="navContact">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemContact">
					<i class="fa fa-address-book-o fa-fw fa-lg"></i>&nbsp; Liên hệ
				</a>
				<ul id="itemContact" class="collapse">
					@can('contact-access')
					<li class="contact"><a href="{{ route('admin.contact.index') }}">Danh sách liên hệ</a></li>
					@endcan
					@can('newsletter-access')
					<li class="newsletter"><a href="{{ route('admin.newsletter.index') }}">Đăng ký nhận tin</a></li>
					@endcan
				</ul>
			</div>
			@endcanany
			
			@can('slide-access')
			<div class="item" id="navInterface">
				<a data-toggle="collapse" data-parent="#sidebarList" href="#itemInterface">
					<i class="fa fa-asterisk fa-fw fa-lg"></i>&nbsp; Giao diện
				</a>
				<ul id="itemInterface" class="collapse">
					<li class="slide"><a href="{{ route('admin.slide.index') }}">Slide ảnh</a></li>
					<li class="banner"><a href="{{ route('admin.slide.banner.index') }}">Banner</a></li>
				</ul>
			</div>
			@endcan
                    
           	@can('popup-access')
			<div class="item single" id="navPopup">
				<a href="{{ route('admin.popup') }}">
					<i class="fa fa-image fa-fw fa-lg"></i>&nbsp; Popup
				</a>
			</div>
			@endcan

			@can('config-access')
			<div class="item single" id="navConfig">
				<a href="{{ route('admin.configuration.show') }}">
					<i class="fa fa-cogs fa-fw fa-lg"></i>&nbsp; Cấu hình
				</a>
			</div>
			@endcan
		</div>
	</div>
</aside>