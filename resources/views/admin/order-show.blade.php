@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Đơn hàng #'.$data->code)

{{-- Import CSS, JS --}}
@section('header')
	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.order.show', $data))

{{-- Navs --}}
@section('nav')
	<select name="status" class="custom-select" form="mainForm" onchange="updateItem(event)">
		@for($i = 0;$i <= count($status) - 1;$i++)
		<option value="{{ $i }}"{!! $data->status == $i ? ' selected' : null !!}>{{ $status[$i] }}</option>
		@endfor
		<option value="cancel"{!! $data->cancelled ? ' selected' : null !!}>Hủy</option>
		<option value="contact"{!! $data->contacted ? ' selected' : null !!}>Liên hệ</option>
		<option value="delay"{!! $data->delayed ? ' selected' : null !!}>Trì hoãn</option>
	</select>
@stop

{{-- Main --}}
@section('main')
	<form action="{{ route('admin.product.order.update', $data->id) }}" method="POST" id="mainForm">
		@method('PUT')
		@csrf
		<input type="hidden" name="status">
	</form>
	<div class="row">
		<div class="col-lg-4">
			<div class="card card-primary fz-up-1">
				<div class="card-header">ĐƠN HÀNG <b>#{{ $data->code }}</b></div>
				<div class="card-body">
					<div class="form-group">
						<label>Ngày đặt hàng:</label> {{ date('d/m/Y H:i', strtotime($data->created_at)) }}
					</div>
					<div class="form-group">
						<label>Ngày cập nhật:</label> {{ date('d/m/Y H:i', strtotime($data->updated_at)) }}
					</div>
					@if(!is_null($data->user_id))
						<hr>
						<p class="font-weight-bold">TÀI KHOẢN ĐẶT HÀNG</p>
						<div class="form-group">
							<label>Tên tài khoản:</label> {{ $data->fk_user->name }}
						</div>
						<div class="form-group">
							<label>Email:</label> {{ $data->fk_user->email }}
						</div>
						<div class="form-group">
							<label>Điện thoại liên hệ:</label> {{ $data->fk_user->phone }}
						</div>
					@endif
				</div>
			</div>
		</div>

		<div class="col-lg-4">
			<div class="card card-primary fz-up-1">
				<div class="card-header">THÔNG TIN GIAO HÀNG</div>
				<div class="card-body">
					<div class="form-group">
						<label>Tên:</label> {{ $data->name }}
					</div>
					<div class="form-group">
						<label>Số điện thoại:</label> {{ $data->phone }}
					</div>
                    @if($data->ward_id != 0)
					<div class="form-group">
						<label>Địa chỉ:</label> {{ $data->address }}, {{ $data->fk_ward->name }}, {{ $data->fk_district->name }}, {{ $data->fk_province->name }}
					</div>
                    @else
                    <div class="form-group">
						<label>Địa chỉ:</label> {{ $data->address }}, {{ $data->fk_district->name }}, {{ $data->fk_province->name }}
					</div>
                    @endif
					<div class="form-group">
						<label>Ghi chú đơn hàng:</label> {!! $data->note !!}
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-4">
			<div class="card card-primary fz-up-1">
				<div class="card-header">Trị giá</div>
				<div class="card-body">
					<div class="form-group">
						<label>Tạm tính:</label> {{ number_format($data->subtotal, 0, ',', '.') }} <span class="sb sb-vnd"></span>
					</div>
					<div class="form-group">
						<label>Giảm giá:</label> {{ number_format($data->discounted, 0, ',', '.') }} <span class="sb sb-vnd"></span>
					</div>
					<div class="form-group">
						<label>Phí vận chuyển:</label> {{ number_format($data->transport_price, 0, ',', '.') }} <span class="sb sb-vnd"></span>
					</div>
					<div class="form-group">
						<label>Thành tiền:</label> <b class="fz-up-5 c-red">{{ number_format($data->total, 0, ',', '.') }}</b> <span class="sb sb-vnd"></span>
					</div>
				</div>
			</div>
		</div>
	</div>

	<hr>

	<table class="table table-primary table-striped table-bordered fz-up-1">
		<thead>
			<tr>
				<th class="text-center" width="100">Ảnh sản phẩm</th>
				<th class="text-center">Mã sản phẩm</th>
				<th>Tên sản phẩm</th>
				<th width="200">Đơn giá</th>
				<th class="text-center" width="150">Số lượng</th>
				<th width="200" class="text-center">Tổng giá trị</th>
			</tr>
		</thead>
		<tbody>
			@foreach($data->fk_details as $item)
			<tr>
				<td class="text-center" >
					@if(is_null($item->product_id))
						<img src="{{ asset('medias/default.jpg') }}" width="70">
					@else
						<a data-fancybox="gallery-{{ $item->id }}" href="{{ media_url('products', $item->fk_product->thumbnail) }}">
							<img src="{{ media_url('products', $item->fk_product->thumbnail) }}" width="70">
						</a>
					@endif
				</td>
				<td class="text-center">
					@if(is_null($item->product_id))
						<strike class="text-muted">{{ $item->code }}</strike>
					@else
						{{ $item->code }}
					@endif
				</td>
				<td>
					@if(is_null($item->product_id))
						<strike class="text-muted">{{ $item->name }}</strike>
					@else
						<a href="{{ url($item->fk_product->slug) }}.html" target="_blank">{{ $item->name }} <span class="text-muted">{!! $item->fk_product->is_combo ? '(Combo)' : null !!}</span></a>
					@endif
				</td>
				<td>
					<div class="flex-row">Giá mua: <b>{{ number_format($item->price, 0, ',', '.') }} <span class="sb sb-vnd"></span></b></div>
					@if($item->original_price > 0)
					<div class="flex-row">Giá gốc: <b>{{ number_format($item->original_price, 0, ',', '.') }} <span class="sb sb-vnd"></span></b></div>
					<div class="flex-row">Giảm giá: <b>{{ number_format($item->original_price - $item->price, 0, ',', '.') }} <span class="sb sb-vnd"></span></b></div>
					@endif
				</td>
				<td class="text-center font-weight-bold">{{ $item->quantity }}</td>
				<td class="fz-up-1 c-red text-center" rowspan="{{ count($item->fk_comboItems) + 1 }}"><b>{{ number_format($item->subtotal, 0, ',', '.') }}</b> <span class="sb sb-vnd"></span></td>
			</tr>
				@if(!is_null($item->product_id))
					@foreach($item->fk_comboItems as $comboItem)
						<tr>
							<td class="text-center" >
								<i class="fa fa-level-up fa-2x fa-rotate-90"></i>
							</td>
							<td class="text-center">
								@if(is_null($comboItem->product_id))
									<strike class="text-muted">{{ $comboItem->code }}</strike>
								@else
									{{ $comboItem->code }}
								@endif
							</td>
							<td>
								@if(is_null($comboItem->product_id))
									<strike class="text-muted">{{ $comboItem->name }}</strike>
								@else
									<a href="{{ url($comboItem->fk_product->slug) }}.html" target="_blank">{{ $comboItem->name }}</a>
								@endif
							</td>
							<td>
								<div class="flex-row">Đơn giá: <i>{{ number_format($comboItem->price, 0, ',', '.') }} <span class="sb sb-vnd"></span></i></div>
							</td>
							<td class="text-center">{{ $comboItem->quantity}} <span class="text-danger">(x{{ $item->quantity }})</span></td>
							{{-- <td class="fz-up-1 c-red"><b>{{ number_format($comboItem->subtotal, 0, ',', '.') }}</b> <span class="sb sb-vnd"></span></td> --}}
						</tr>
					@endforeach
				@endif
			@endforeach
		</tbody>
	</table>

	<hr>

	<div class="card fz-up-1">
		<form action="{{ route('admin.product.order.update_note', $data->id) }}" method="POST" class="card-body">
			@csrf
			<div class="form-group">
				<label for="todo">GHI CHÚ</label>
				<textarea name="todo" rows="8" class="form-control fz-up-1">{!! $data->todo !!}</textarea>
			</div>
			<div class="form-group text-right">
				<button class="btn btn-success">Lưu ghi chú</button>
			</div>
		</form>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navOrder").addClass("active").find("ul").addClass("show").find(".order").addClass("active");

		function updateItem(event){
			swal({
				title: 'Bạn có chắc',
				text: "Thực hiện thao tác này?",
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: 'Thực hiện',
				cancelButtonText: 'Bỏ qua'
			}).then(function(isConfirm) {
				if (isConfirm.value) {
					$("#mainForm").find("input[name=status]").val($(event.target).val());
					$("#mainForm").submit();
				}
			})
		}
	</script>
@stop