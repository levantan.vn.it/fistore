@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Vận chuyển')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/jquery-validation/1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.transportation'))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-info" data-toggle="modal" data-target="#createAreaModal"><i class="fa fa-plus fa-fw"></i>Thêm khu vực</button>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-4">
			<div class="card">
				<h5 class="card-header pb-0">Phí vận chuyển mặc định</h5>
				<div class="card-body">
					<form action="{{ route('admin.transportation.setting') }}" method="POST" id="settingForm">
						@csrf
						<div class="form-group">
							<div class="input-group">
								<input type="text" name="default_shipping_price" value="{{ number_format(get_setting('default_shipping_price')->value) }}" class="form-control" onkeyup="make_money_format(event)"{!! get_setting('default_shipping_price')->disabled ? ' disabled' : null !!} required>
								<span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
							</div>
						</div>
						<div class="form-group">
							<label class="custom-control custom-checkbox">
								<input type="checkbox" name="disabled" class="custom-control-input" id="disabledDefaultShipping"{!! get_setting('default_shipping_price')->disabled ? ' checked' : null !!}>
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description">Từ chối vận chuyển</span>
							</label>
						</div>
					</form>
					<script>
						$("#disabledDefaultShipping").change(function(){
							if($(this).is(":checked")){
								$("input[name=default_shipping_price]").attr("disabled", true);
							}else{
								$("input[name=default_shipping_price]").attr("disabled", false);
							}
						});
					</script>
					<p>Các khu vực không được cài đặt sẵn sẽ áp dụng mức phí vận chuyển mặc định.</p>
					<p>Nếu tùy chọn <strong>Từ chối vận chuyển</strong> được kích hoạt, các khu vực không được cài đặt sẵn sẽ không được áp dụng vận chuyển.</p>
				</div>
				<div class="card-footer">
					<button class="btn btn-success" form="settingForm">Lưu cài đặt</button>
				</div>
			</div>
			<div class="card">
				<h5 class="card-header pb-0">Hướng dẫn</h5>
				<div class="card-body">
					<p>Bạn có thể tùy chọn mức phí vận chuyển theo từng Tỉnh thành và Quận huyện cụ thể</p>
				</div>
				<div class="card-footer">
					{{-- <a href="#" class="btn btn-info">Xem hướng dẫn</a> --}}
				</div>
			</div>
		</div>

		<div class="col-lg-8">
			@foreach($data as $item)
				<table class="table table-responsive">
					<thead>
						<tr>
							<th colspan="3">
								{{ $item->fk_province->name }} - <a href="#" onclick="destroyItem(event, '{{ route('admin.transportation.destroy', $item->id) }}');">Xóa khu vực</a>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="50%" class="py-3">
								<a data-toggle="collapse" href="#collapse-{{ $item->id }}">{{ $item->name }}</a>
							</td>
							<td width="30%" class="py-3">
								@if($item->max_order == 99999999999)
									{{ number_format($item->min_order, 0, ',', '.') }} <span class="sb sb-vnd"></span> trở lên
								@elseif($item->min_order == 0)
									Dưới {{ number_format($item->max_order, 0, ',', '.') }} <span class="sb sb-vnd"></span>
								@else
									{{ number_format($item->min_order, 0, ',', '.') }} <span class="sb sb-vnd"></span> đến {{ number_format($item->max_order, 0, ',', '.') }} <span class="sb sb-vnd"></span>
								@endif
							</td>
							<td width="20%" class="py-3">
								@if($item->price > 0)
									{{ number_format($item->price, 0, ',', '.')}} <span class="sb sb-vnd"></span>
								@else
									Miễn phí
								@endif
							</td>
						</tr>
					</tbody>
				</table>
				<div class="collapse" id="collapse-{{ $item->id }}">
					<form action="{{ route('admin.transportation.update', $item->id) }}" method="POST">
						@method('PUT')
						@csrf
						<div class="card">
							<div class="card-body">
								<div class="form-group">
									<label>Tên phương thức vận chuyển <span class="required">*</span></label>
									<input type="text" name="name" value="{{ $item->name }}" class="form-control" required>
								</div>

								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<label>Áp dụng cho đơn hàng từ <span class="required">*</span></label>
											<div class="input-group">
												<input type="text" name="min_order" value="{{ number_format($item->min_order) }}" class="form-control" onkeyup="make_money_format(event);"{{ $item->id > 1 ? null : ' readonly' }} required>
												<span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
											</div>
										</div>
									</div>

									<div class="col-lg-6">
										<div class="form-group">
											<label>Đến</label>
											<div class="input-group">
												<input type="text" name="max_order" value="{{ $item->max_order == 99999999999 ? null : number_format($item->max_order) }}" class="form-control" onkeyup="make_money_format(event);"{{ $item->id > 1 ? null : ' readonly' }}>
												<span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
											</div>
											<span class="help-block">Để trống nếu nếu không giới hạn.</span>
										</div>
									</div>

									<div class="col-lg-6">
										<div class="form-group">
											<label>Mức phí chung <span class="required">*</span></label>
											<div class="input-group">
												<input type="text" name="price" value="{{ number_format($item->price) }}" class="form-control" onkeyup="make_money_format(event);" required>
												<span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
											</div>
											<span class="help-block">Áp dụng cho tất cả các Quận/Huyện trong khu vực.</span>
										</div>
									</div>
								</div>
								
								<table class="table table-striped table-responsive">
									<thead>
										<tr>
											<th>Quận/Huyện</th>
											<th width="200">Mức phí cụ thể</th>
											<th width="200"></th>
										</tr>
									</thead>
									<tbody>
										@foreach($item->fk_transportation_areas as $area)
										<tr{!! $area->denied ? ' class="text-danger"' : null !!}>
											<td>
												<input type="hidden" name="district_id[{{ $area->id }}]" value="{{ $area->district_id }}">
												{{ $area->fk_district->name }}
											</td>
											<td>
												<div class="input-group">
													<input type="text" name="district_price[{{ $area->id }}]" value="{{ number_format($area->price) }}" class="form-control" onkeyup="make_money_format(event);"{!! $area->denied ? ' readonly' : null !!}>
													<span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
												</div>
											</td>
											<td class="text-right">
												<label class="custom-control custom-checkbox pl-4">
													<input type="checkbox" name="district_deny[{{ $area->id }}]" class="custom-control-input" onchange="is_deny(event)"{!! $area->denied ? ' checked' : null !!}>
													<span class="custom-control-indicator"></span>
													<span class="custom-control-description">Từ chối vận chuyển</span>
												</label>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>

								<button class="btn btn-success">Lưu</button>
								<button class="btn btn-light" type="button" data-toggle="collapse" href="#collapse-{{ $item->id }}">Hủy</button>
							</div>
						</div>
					</form>
				</div>
			@endforeach
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<div class="modal fade" id="createAreaModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Thêm khu vực</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="{{ route('admin.transportation.store') }}" method="POST" id="createAreaForm">
						@csrf
						<div class="form-group">
							<label>Khu vực <span class="required">*</span></label>
							<select class="custom-select form-control" name="province_id" required>
								<option value="">— Chọn khu vực —</option>
								@foreach($provinces as $province)
									<option value="{{ $province->id }}">{{ $province->name }}</option>
								@endforeach
							</select>
						</div>
						<hr>
						<div class="form-group">
							<label>Tên phương thức vận chuyển <span class="required">*</span></label>
							<input type="text" name="name" class="form-control" required>
						</div>

						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Áp dụng cho đơn hàng từ <span class="required">*</span></label>
									<div class="input-group">
										<input type="text" name="min_order" value="0" class="form-control" onkeyup="make_money_format(event);" required>
										<span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
									</div>
									<label id="min_order-error" class="error" for="min_order" style="display: none;"></label>
								</div>
							</div>

							<div class="col-lg-6">
								<div class="form-group">
									<label>Đến</label>
									<div class="input-group">
										<input type="text" name="max_order" class="form-control" onkeyup="make_money_format(event);">
										<span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
									</div>
									<span class="help-block">Để trống nếu không giới hạn.</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label>Mức phí chung <span class="required">*</span></label>
							<div class="input-group">
								<input type="text" name="price" value="0" class="form-control" onkeyup="make_money_format(event);" required>
								<span class="input-group-addon addon-info"><span class="sb sb-vnd"></span></span>
							</div>
							<label id="price-error" class="error" for="price" style="display: none;"></label>
							<span class="help-block">Áp dụng cho tất cả các Quận/Huyện trong khu vực.</span>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button class="btn btn-success" form="createAreaForm">Lưu</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		$("#navOrder").addClass("active").find("ul").addClass("show").find(".transportation").addClass("active");

		$("#createAreaForm").validate();
		function is_deny(event){
			if($(event.target).is(":checked")){
				$(event.target).parents("tr").find(".form-control").attr("readonly", true);
				$(event.target).parents("tr").addClass("text-danger");
			}else{
				$(event.target).parents("tr").find(".form-control").attr("readonly", false);
				$(event.target).parents("tr").removeClass("text-danger");
			}
		}
	</script>
	<script>
		function destroyItem(event, url){
			swal({
				title: 'Bạn có chắc',
				text: "Thực hiện thao tác xóa mục này?",
				type: 'question',
				showCancelButton: true,
				confirmButtonColor: '#d33',
				cancelButtonColor: '#3085d6',
				confirmButtonText: 'Xóa',
				cancelButtonText: 'Bỏ qua'
			}).then(function(isConfirm) {
				if (isConfirm.value) {
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						url : url,
						type : 'DELETE',
						success : function (data) {
							$(event.target).parents("table").remove();
							toastr[data.alert]("<b>" + data.title + "</b><br/>" + data.msg);
						},
						error: function (xhr, ajaxOptions, thrownError) {
							toastr.error(xhr.responseText);
						}
					});
				}
			})
		}
	</script>
@stop