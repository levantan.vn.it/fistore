@extends('admin.layouts.master')

{{-- Title --}}
@section('title', 'Trải nghiệm sản phẩm')

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/datatable/1.10.16/datatables.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/datatables.config.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/dataTables.buttons.min.js') }}"></script>
	<script src="{{ asset('libs/datatable/1.10.16/Buttons-1.4.2/js/buttons.bootstrap4.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/datatable/1.10.16/datatables.min.css') }}">

	{{-- Fancybox --}}
	<link rel="stylesheet" href="{{ asset('libs/fancybox/3/jquery.fancybox.min.css') }}" />
	<script src="{{ asset('libs/fancybox/3/jquery.fancybox.min.js') }}"></script>
	
	{{-- Countdown --}}
	<script src="{{ asset('js/countdown.handler.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.freetry'))

{{-- Navs --}}
@section('nav')
	<a href="{{ route('admin.freetry.create') }}" class="btn btn-info"><i class="fa fa-plus fa-fw"></i> Tạo mới</a>
@stop

{{-- Main --}}
@section('main')
	<table class="table table-primary table-striped table-responsive" id="dataTable">
		<thead>
			<tr>
				<th class="no-sort" width="50"></th>
				<th>Sản phẩm</th>
				<th>Thời hạn</th>
				<th>Số lượng</th>
				<th width="120" class="text-center">Trạng thái</th>
				<th class="no-sort" width="50"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($data as $item)
				@if($item->fk_product)
				<tr>
					<td>
						<a data-fancybox="product-{{ $item->id }}" href="{{ media_url('products', $item->fk_product->thumbnail) }}">
							<img src="{{ media_url('products', $item->fk_product->thumbnail) }}" width="60">
						</a>
					</td>
					<td class="white-space-unset">
						<div>
							<a href="{{ product_link($item->fk_product) }}" target="_blank">{{ $item->fk_product->name }}</a>
						</div>
						<div>Mã sản phẩm: <b>{{ $item->fk_product->code }}</b></div>
					</td>
					<td>
						<div>Bắt đầu: {{ date('d/m/Y H:i', strtotime($item->started_at)) }}</div>
						<div>Kết thúc: {{ date('d/m/Y H:i', strtotime($item->expired_at)) }}</div>
						<div>Kết thúc sau: <strong class="countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->expired_at)) }}" onload="countdown(event);"></strong></div>
					</td>
					<td>
						{!! $item->quantity ?? 'Không giới hạn' !!}
					</td>
					<td>
						@if($item->disabled)
							<mark class="mark mark-warning mark-block">Tạm ngưng</mark>
						@elseif(date('U', strtotime($item->started_at)) > date('U'))
							<mark class="mark mark-info mark-block">Chưa bắt đầu</mark>
						@elseif(date('U', strtotime($item->expired_at)) < date('U'))
							<mark class="mark mark-danger mark-block">Hết hạn</mark>
						@else
							<mark class="mark mark-success mark-block">Hoạt động</mark>
						@endif
					</td>
					<td>
						<ul class="table-options">
							<li>
								<a href="{{ route('admin.freetry.edit', $item->id) }}" data-toggle="tooltip" data-placement="top" title="Sửa"><i class="fa fa-pencil"></i></a>
							</li>
							<li>
								<button type="button" data-toggle="tooltip" data-placement="top" title="Xóa" onclick="destroyItem(event, '{{ route('admin.freetry.destroy', $item->id) }}')"><i class="fa fa-remove"></i></button>
							</li>
						</ul>
					</td>
				</tr>
				@endif
			@endforeach
		</tbody>
	</table>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navSaleOf").addClass("active").find("ul").addClass("show").find(".freetry").addClass("active");
	</script>
	<script src="{{ asset('js/admin.resource.handler.js') }}"></script>
@stop