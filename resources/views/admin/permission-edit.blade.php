@extends('admin.layouts.master')

{{-- Title --}}
@section('title', $data->name)

{{-- Import CSS, JS --}}
@section('header')
	<script src="{{ asset('libs/jquery-validation/jquery-validation-1.17.0/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('libs/jquery-validation/jquery-validation-1.17.0/localization/messages_vi.js') }}"></script>
@stop

{{-- Breadcrumbs --}}
@section('breadcrumbs', Breadcrumbs::render('admin.permission.edit', $data))

{{-- Navs --}}
@section('nav')
	<button class="btn btn-success" form="mainForm"><i class="fa fa-save fa-fw"></i> Lưu</button>
	<a href="{{ route('admin.role.index') }}" class="btn btn-default"><i class="fa fa-reply fa-fw"></i> Trở về</a>
@stop

{{-- Main --}}
@section('main')
	<div class="row">
		<div class="col-lg-4">
			<div class="card">
				<h5 class="card-header pb-0">Hướng dẫn</h5>
				<div class="card-body">
					<p><strong>Chức năng</strong> Thông tin hướng dẫn viết ngắn gọn giúp người dùng dễ đọc, hiểu và tiếp cận.</p>
				</div>
			</div>
		</div>
		<div class="col-lg-8">
			<form action="{{ route('admin.permission.update', $data->id) }}" method="POST" id="mainForm">
				@method("PUT")
				@csrf
				@foreach($modules as $module)
				<table class="table table-primary table-striped table-hover">
					<thead>
						<tr>
							<th colspan="3">Chức năng {{ $module->name }}</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th width="30%">Tên quyền</th>
							<th>Nội dung</th>
							<th width="100" class="text-center">Cho phép truy cập</th>
						</tr>
						@foreach($module->fk_permissions as $item)
						<tr>
							<td>{{ $item->name }}</td>
							<td>{!! $item->comment !!}</td>
							<td class="text-center">
								<label class="custom-toggle toggle-success">
									<input type="checkbox" name="allow[{{ $item->id }}]" value="{{ $item->id }}" class="toggle-checkbox"{{ $data->fk_permissions->contains($item->id) ? ' checked' : null }}>
									<div class="toggle-inner">
										<span class="toggle-button"></span>
									</div>
								</label>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				@endforeach
			</form>
		</div>
	</div>
@stop

{{-- Footer --}}
@section('footer')
	<script>
		$("#navAccount").addClass("active").find("ul").addClass("show").find(".role").addClass("active");
	</script>
	<script>
		$("#mainForm").validate();
	</script>
@stop