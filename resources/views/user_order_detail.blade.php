@extends('layouts.master')

@section('title', 'Quản lý đơn hàng')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('user.order') !!}

	<div class="row">
		<div class="col-md-3">
			@include('particles.aside_user')
		</div>
		<div class="col-md-9">	
			<div class="card mb-3">
				<div class="card-body">
					<div class="font-weight-bold">Mã đơn hàng: {{ $order->code }}</div>
					<div>Ngày đặt hàng: {{ date('d/m/Y \l\ú\c H:i', strtotime($order->created_at)) }}</div>
					<div>Trạng thái giao hàng:
						@if($order->cancelled)
						Đã hủy
						@elseif($order->delayed)
						Trì hoãn
						@elseif($order->contacted)
						Liên hệ
						@else
							{{ $status[$order->status] }}
							@if($order->status == 0)
							<form action="{{ route('user.order.cancel') }}" method="POST">
								@csrf
								(<button class="btn btn-cancel" name="id" value="{{ $order->id }}">Hủy đơn hàng</button>)
							</form>
							@endif
						@endif
					</div>
				</div>
			</div>

			<div class="block block-user mb-3">
				<div class="block-header">
					<div class="block-title">ĐỊA CHỈ NHẬN HÀNG</div>
				</div>
				<div class="block-body">
					<div class="font-weight-bold">{{ $order->fk_user->name }}</div>
					<div>{{ $order->fk_user->phone }}</div>
					<div>{{ $order->address }}, {{ $order->fk_ward->name }}, {{ $order->fk_district->name }}, {{ $order->fk_province->name }}</div>
				</div>
			</div>

			<div class="block block-user">
				<div class="block-header">
					<div class="block-title">THÔNG TIN ĐƠN HÀNG</div>
				</div>
				<div class="block-body p-0">
					<table class="table table-bordered mb-0">
						<tbody>
							@foreach($order->fk_details as $product)
								@if(is_null($product->product_id))
									<tr>
										<td><img src="{{ asset('media/default.jpg') }}" width="80"></td>
										<td>
											<div class="text-muted"><strike>{{ $product->name }} <span class="text-muted"></span></strike></div>
											<div><b class="c-red">{{ number_format($product->price,0,',','.') }}</b> x {{ $product->quantity }}</div>
										</td>
									</tr>
								@else
									<tr>
										<td><img src="{{ media_url('products', $product->fk_product->thumbnail) }}" width="80"></td>
										<td>
											<div><a href="{{ url($product->fk_product->slug) }}.html" target="_blank">{{ $product->name }}</a></div>
											<div><b class="c-red">{{ number_format($product->price,0,',','.') }}</b> x {{ $product->quantity }}</div>
										</td>
									</tr>
								@endif							
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

@stop

@section('footer')
	<script>
		$("aside.block-user .order").addClass("active");
	</script>
@stop