@extends('layouts.clear')

@section('title', 'Giỏ hàng')
@section('description', null)
@section('keywords', null)

@section('header')
@stop
@section('pixel_event')
	fbq('track', 'AddToCart');
@stop

@section('main')
	{{-- Checkout Progress --}}
	<div class="checkout-progress">
		<li class="active"><span class="step">1</span>Giỏ hàng</li>
		<li class="d-none d-md-flex"><span class="step">2</span>Địa chỉ giao hàng</li>
		<li class="d-none d-md-flex"><span class="step">3</span>Thanh toán & Đặt hàng</li>
	</div>
	
	@if(Cart::count() > 0)
	<div class="block-cart" id="cartNotEmpty">
		<div class="row">
			<div class="col-lg-9 mb-4">
				@if(Agent::isPhone())
					@include('particles.mobile_cart')
				@else
					@include('particles.desktop_cart')
				@endif
				
				<hr>
				<a href="{{ url('/') }}" class="btn btn-link">Tiếp tục mua hàng <i class="fa fa-angle-double-right"></i></a>
			</div>
			<div class="col-lg-3 mb-4">
				@include('particles.aside_cart')
				
				<aside class="aside-cart mb-4">
					<div class="aside-header">Mã giảm giá</div>
					@auth('customer')
					<form action="{{ route('cart.coupon') }}" method="POST" class="aside-body p-3">
						@csrf
						<div class="form-group">
							
							<div class="input-group">
								<input type="text" name="coupon" value="{{ session()->has('coupon') ? session('coupon')->code : null }}" class="form-control" placeholder="Nhập mã giảm giá" required>
								<span class="input-group-btn">
									<button class="btn btn-secondary btn-block">Áp dụng</button>
								</span>
							</div>
							@if($errors->has('coupon'))
							    <span class="invalid-feedback">
							        <strong>{{ $errors->first('coupon') }}</strong>
							    </span>
							@endif
							{{-- Nếu có mã khuyến mãi và áp dụng cho phí ship --}}
							@if (session()->has('coupon') && session('coupon')->type == 2)
								<small>Chỉ áp dụng trên tổng tiền vận chuyển</small>
							@endif
						</div>
						
					</form>
					@else
					<div class="aside-body p-3"><a class="text-primary" data-toggle="modal" href="#loginModal">Đăng nhập</a> để sử dụng mã giảm giá</div>
					@endauth
				</aside>

				<div class="checkout-button">
					<a href="{{ route('checkout.address.list') }}" class="btn btn-primary btn-block">TIẾN HÀNH THANH TOÁN</a>
				</div>

			</div>
		</div>
	</div>
	@endif
	<div class="block-cart" id="cartEmpty" {!! Cart::count() > 0 ? 'style="display: none;"' : null !!}>
		<img src="{{ asset('images/empty-cart.png') }}" width="200">
		<div class="cart-empty-title">Chưa có sản phẩm nào trong giỏ hàng!</div>
		<div class="cart-empty-description">
			Trước khi thực hiện mua hàng, bạn cần thêm vài sản phẩm vào giỏ trước.
		</div>
		<a href="{{ url('/') }}" class="btn btn-link">Tiếp tục mua hàng <i class="fa fa-angle-double-right"></i></a>
	</div>
@stop

@section('footer')
	<script src="{{ asset('js/cart.handler.js') }}"></script>
@stop