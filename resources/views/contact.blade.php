@extends('layouts.master')

@section('title', 'Liên hệ')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('contact') !!}

	<div class="row">
		<div class="col-md-10 mx-auto">
			<div class="block-contact my-5">
				<div class="row">
					<div class="col-md-7">
						<div class="block-header">
							<div class="block-title">Liên hệ với chúng tôi</div>
							<div class="block-description">Vui lòng điền thông tin bên dưới</div>
						</div>
						<div class="block-body">
							<form action="{{ route('contact.send') }}" method="POST" id="contactForm">
								@csrf
								<div class="form-group row row-mx-5">
									<label for="title" class="col-md-3">Tiêu đề <span class="required">*</span></label>
									<div class="col-md-7">
										<input type="text" name="subject" class="form-control" required>
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label for="name" class="col-md-3">Họ tên <span class="required">*</span></label>
									<div class="col-md-7">
										<input type="text" name="name" class="form-control" required>
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label for="email" class="col-md-3">Email <span class="required">*</span></label>
									<div class="col-md-7">
										<input type="text" name="email" class="form-control" required>
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label for="phone" class="col-md-3">Điện thoại</label>
									<div class="col-md-7">
										<input type="text" name="phone" class="form-control">
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label for="title" class="col-md-3">Nội dung <span class="required">*</span></label>
									<div class="col-md-7">
										<textarea name="content" rows="4" class="form-control" required></textarea>
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label class="col-md-3"></label>
									<div class="col-md-7">
										<button class="btn btn-primary" style="min-width: 200px">GỬI</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-5">
						<div class="block-header">
							<div class="block-title">Địa chỉ cửa hàng</div>
							<div class="block-description">Văn phòng đại diện</div>
						</div>
						<div class="block-body">
							<div><span class="text-primary">Địa chỉ:</span> {{ $data->address }}, {{ $data->fk_ward->name }}, {{ $data->fk_district->name }}, {{ $data->fk_province->name }}</div>
							<div><span class="text-primary">Điện thoại:</span> {{ $data->hotline_1 }}</div>
							<div><span class="text-primary">Email:</span> {{ $data->contact_email }}</div>
						</div>
					</div>
				</div>
				
				{{-- <div class="block-header">
					<div class="block-title">Liên hệ với chúng tôi</div>
					<div class="block-description">Vui lòng điền thông tin bên dưới</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-7">
							<form action="{{ route('contact.send') }}" method="POST" id="contactForm">
								@csrf
								<div class="form-group row row-mx-5">
									<label for="title" class="col-md-3">Tiêu đề <span class="required">*</span></label>
									<div class="col-md-7">
										<input type="text" name="subject" class="form-control" required>
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label for="name" class="col-md-3">Họ tên <span class="required">*</span></label>
									<div class="col-md-7">
										<input type="text" name="name" class="form-control" required>
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label for="email" class="col-md-3">Email <span class="required">*</span></label>
									<div class="col-md-7">
										<input type="text" name="email" class="form-control" required>
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label for="phone" class="col-md-3">Điện thoại</label>
									<div class="col-md-7">
										<input type="text" name="phone" class="form-control">
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label for="title" class="col-md-3">Nội dung <span class="required">*</span></label>
									<div class="col-md-7">
										<textarea name="content" rows="4" class="form-control" required></textarea>
									</div>
								</div>
								<div class="form-group row row-mx-5">
									<label class="col-md-3"></label>
									<div class="col-md-7">
										<button class="btn btn-primary" style="min-width: 200px">GỬI</button>
									</div>
								</div>
							</form>
						</div>

						<div class="col-md-5">
							<div class="block-header">
								<div class="block-title">Địa chỉ cửa hàng</div>
								<div class="block-description">Văn phòng đại diện</div>
							</div>
							<div class="block-body">
								<div><span class="text-primary">Địa chỉ:</span> {{ $data->address }}, {{ $data->fk_ward->name }}, {{ $data->fk_district->name }}, {{ $data->fk_province->name }}</div>
								<div><span class="text-primary">Điện thoại:</span> {{ $data->phone }}</div>
								<div><span class="text-primary">Email:</span> {{ $data->contact_email }}</div>
							</div>
						</div>
					</div>
				</div> --}}
			</div>
			@if(!is_null($data->map))
			<div class="block-map my-5">
				<iframe src="{!! explode('"', $data->map)[1] !!}" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
			@endif
		</div>
	</div>

@stop

@section('footer')
	<script>
		$("#contactForm").validate();
	</script>
@stop