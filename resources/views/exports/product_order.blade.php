<table>
	<thead>
		<tr>
			<th>MÃ ĐƠN HÀNG</th>
			<th>NGÀY ĐẶT HÀNG</th>
			<th>SẢN PHẨM</th>
			<th>MÃ SẢN PHẨM</th>
			<th>SỐ LƯỢNG</th>
			<th>TÊN KHÁCH HÀNG</th>
			<th>SỐ ĐIỆN THOẠI</th>
			<th>THÀNH TIỀN</th>
			<th>TRẠNG THÁI</th>
			<th>LƯU Ý KHÁCH HÀNG</th>
			<th>GHI CHÚ ĐƠN HÀNG</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $key => $row)
			@php 
				$count = 1;
			@endphp
			@for($i = $key + 1; $i <= count($data) - 1; $i++)
				@if($data[$i]->code === $row->code)
					@php $count++; @endphp
				@else
					@break
				@endif
			@endfor
			@if($data[$key]->code != ($data[$key - 1]->code ?? null))
			<tr>
				<td rowspan="{{ $count }}">{{ $row->code }}</td>
				<td rowspan="{{ $count }}">{{ date('d/m/Y H:i', strtotime($row->created_at)) }}</td>
				<td>{{ $row->productName }} {{ $row->productIsCombo ? '(Combo)' : '' }}</td>
				<td>{{ $row->productCode }}</td>
				<td>{{ $row->productQuantity }}</td>
				<td rowspan="{{ $count }}">{{ $row->name }}</td>
				<td rowspan="{{ $count }}">{{ $row->phone }}</td>
				<td rowspan="{{ $count }}">{{ $row->total }}</td>
				<td rowspan="{{ $count }}">
					@if($row->cancelled)
					Đã hủy
					@elseif($row->contacted)
					Liên hệ
					@elseif($row->delayed)
					Trì hoãn
					@else
						@switch($row->status)
							@case(0)
								Chờ duyệt
								@break
							@case(1)
								Xác nhận
								@break
							@case(2)
								Đóng gói
								@break
							@case(3)
								Hoàn thành
								@break
						@endswitch
					@endif
				</td>
				<td rowspan="{{ $count }}">{{ $row->note }}</td>
				<td rowspan="{{ $count }}">{{ $row->todo }}</td>
			</tr>
				{{-- @if($row->productIsCombo)
					@foreach(get_combo_details($row->productId)->fk_products as $comboItem)
						<tr>
							<td>&#8594; {{ $comboItem->name }}</td>
							<td>{{ $comboItem->code }}</td>
						</tr>
					@endforeach
				@endif --}}
			@else
			<tr>
				<td>{{ $row->productName }} {{ $row->productIsCombo ? '(Combo)' : '' }}</td>
				<td>{{ $row->productCode }}</td>
				<td>{{ $row->productQuantity }}</td>
			</tr>
				{{-- @if($row->productIsCombo)
					@foreach(get_combo_details($row->productId)->fk_products as $comboItem)
						<tr>
							<td></td>
							<td></td>
							<td>&#8594; {{ $comboItem->name }}</td>
							<td>{{ $comboItem->code }}</td>
						</tr>
					@endforeach
				@endif --}}
			@endif
		@endforeach
	</tbody>
</table>