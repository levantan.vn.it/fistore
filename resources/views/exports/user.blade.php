<table>
	<thead>
		<tr>
			<th>ID</th>
			<th>TÊN</th>
			<th>EMAIL</th>
			<th>ĐIỆN THOẠI</th>
			<th>LOẠI TÀI KHOẢN</th>
			<th>APP ID</th>
			<th>NGÀY TẠO</th>
			<th>EXTRA POINT</th>
		</tr>
	</thead>
	<tbody>
		@foreach($data as $row)
			<tr>
				<td>{{ $row->id }}</td>
				<td>{{ $row->name }}</td>
				<td>{{ $row->email }}</td>
				<td>{{ $row->phone }}</td>
				<td>{{ !is_null($row->provider) ? title_case($row->provider) : 'Tài khoản thường' }}</td>
				<td>{{ $row->social_id }}</td>
				<td>{{ date('d/m/y H:i:s', strtotime($row->created_at)) }}</td>
				<td>{{ $row->extra_point }}</td>
			</tr>
		@endforeach
	</tbody>
</table>