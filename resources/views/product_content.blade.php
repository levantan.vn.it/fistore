@extends('layouts.master')

@section('title', $data->title ?? $data->name)
@section('description', $data->description ?? str_length_limit($data->content, 200))
@section('keywords', $data->keywords ?? $data->tags)
@section('thumbnail', media_url('products', $data->thumbnail))

@section('header')
	{{-- XZoom --}}
	<link rel="stylesheet" href="{{ asset('libs/xzoom/1.0.8/xzoom.css') }}">
	<script src="{{ asset('libs/xzoom/1.0.8/xzoom.min.js') }}"></script>

	{{-- Owl Carousel --}}
	<script src="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/owl-carousel/2v2.2.1/owl.carousel.min.css') }}">

	{{-- Bar Rating Master --}}
	<script src="{{ asset('libs/bar-rating-master/jquery.barrating.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('libs/bar-rating-master/themes/bootstrap-stars.css') }}">
	<link rel="stylesheet" href="{{ asset('libs/bar-rating-master/themes/fontawesome-stars.css') }}">

	<style>
		.xzoom-thumbs:not(:nth-child(2)){
			display: none;
		}
		@media screen and (max-width: 760px){
			iframe {
				width: 100% !important;
				height: 480px !important;
			}
		}
		@media screen and (max-width: 480px){
			iframe {
				width: 100% !important;
				height: 320px !important;
			}
		}
		
		.block-single-content, .block-single-content {
			position: relative;
		}

		.block-single-content-body, .block-single-content-body {
			overflow: hidden;
		}
		.show-more {
			position: relative;
		}

		.show-more::before {
			height: 55px;
			content: "";
			background-image: -webkit-gradient(linear, 0% 100%, 0% 0%, from(#fff), color-stop(0.2, #fff), to(rgba(255,255,255,0)));
			display: block;
			position: absolute;
			top: -50px;
			width: 100%;
		}
		.show-more .readmore {
			width: 90px;
			display: block;
			overflow: hidden;
			position: relative;
			line-height: 30px;
			font-size: 14px;
			color: #fd6b90;
			margin: 10px auto;
			cursor: pointer;
		}
		.show-more .readmore:after {
			content: '';
			width: 0;
			right: 0;
			border-top: 6px solid #fd6b90;
			border-left: 6px solid transparent;
			border-right: 6px solid transparent;
			display: inline-block;
			vertical-align: middle;
			margin: -2px 0 0 5px;
		}
	</style>
@stop
@section('pixel_event')
	fbq('track', 'ViewContent');
@stop

@section('main')
	{!! Breadcrumbs::render('product.detail', $data) !!}

	<div class="block-single">
		<div class="row row-mx-10 mb-4">
			<div class="col-md-4">
				<div class="block-single-images">
					@if(count($data->fk_images) > 0)
					<div class="xzoom-container">
						<img class="xzoom" id="mainImage" width="100%" src="{{ media_url('products', $data->fk_images[0]->path) }}" xoriginal="{{ media_url('products', $data->fk_images[0]->path) }}">
					</div>
					<div class="xzoom-thumbs" id="xzoomThumbs_origin">
						@foreach($data->fk_images as $key => $image)
						<a href="{{ media_url('products', $image->path) }}">
							<img class="xzoom-gallery" height="50" src="{{ media_url('products', $image->path) }}"
							 {!! $key == 0 ? 'xpreview="'.media_url('products', $image->path).'"' : null !!}
							 alt="{{ $image->name }}" title="{{ $image->name }}">
						</a>
						@endforeach
					</div>
					@else
						@if(count($data->fk_versions) > 0)
							@if(count($data->fk_versions[0]->fk_images) > 0)
								<div class="xzoom-container">
									<img class="xzoom" id="mainImage" width="100%" src="{{ media_url('products', $data->fk_versions[0]->fk_images[0]->path) }}" xoriginal="{{ media_url('products', $data->fk_versions[0]->fk_images[0]->path) }}">
								</div>
							@endif
						@endif
					@endif

					@if(count($data->fk_versions) > 0)
						@foreach($data->fk_versions as $index => $versionImage)
							@if(count($versionImage->fk_images) > 0)
								
								<div class="xzoom-thumbs" id="xzoomThumbs_{{ $versionImage->id }}">
									@foreach($versionImage->fk_images as $key => $image)
									<a href="{{ media_url('products', $image->path) }}">
										<img class="xzoom-gallery" height="50" src="{{ media_url('products', $image->path) }}"
										 {!! $key == 0 ? 'xpreview="'.media_url('products', $image->path).'"' : null !!}
										 alt="{{ $image->name }}" title="{{ $image->name }}">
									</a>
									@endforeach
								</div>
							@endif
						@endforeach
					@endif
					<script>
						// Xzoom Slider
						$(document).ready(function() {
							$(".xzoom, .xzoom-gallery").xzoom({tint: '#333', Xoffset: 15});
						});
					</script>
				</div>
			</div>
			<div class="col-md-5 mb-4">
				<div class="block-single-info">
					@if(!$data->is_combo)
					<div class="block-single-brand">
						<a href="{{ brand_link($data->fk_brand->slug) }}">{{ $data->fk_brand->name }}</a>
					</div>
					@endif
					<div class="block-single-name">{{ $data->name }}</div>
					<div class="block-single-total-rate">
						@if(count($rating_point) > 0)
							@for($i = 1; $i <= array_sum($rating_point) / count($rating_point); $i ++)
								<i class="fa fa-star fa-lg" style="color: #ffc600"></i>
							@endfor
							@php
								$whole = number_format(array_sum($rating_point) / count($rating_point),1) - floor(number_format(array_sum($rating_point) / count($rating_point),1));
							@endphp
							@if($whole >= 0.25 && $whole <= 0.75)
								<i class="fa fa-star-half fa-lg" style="color: #ffc600"></i>
							@elseif($whole > 0.75)
								<i class="fa fa-star fa-lg" style="color: #ffc600"></i>
							@endif
						@endif
						<span class="fz-down-1">
							@if(count($rating_point) != 0)
							&emsp;({{ count($rating_point) }} đánh giá)
							@else
							(Chưa có đánh giá nào)
							@endif
						</span>
					</div>
					<div class="block-single-version-select" id="originalVersion">
						<div class="block-single-code">Mã sản phẩm: <span>{{ $data->code }}</span></div>
						<hr class="mb-1">
						<div class="block-single-price">
							@if($data->display_price)
								@if(is_null($hotdeal) || (!is_null($hotdeal->available_qty) && $hotdeal->available_qty <= 0))
									@if($data->price > 0)
										{{ number_format($data->price,0,',','.') }} VND
										@if($data->original_price && $data->original_price > $data->price)
											<strike style="font-size: 1rem;color: #676767">{{ number_format($data->original_price,0,',','.') }} VND</strike>
										@endif
									@else
										KHÔNG KINH DOANH
									@endif
								@else
									{{ number_format($hotdeal->price,0,',','.') }} VND
									<strike style="font-size: 1rem;color: #676767">{{ number_format($hotdeal->original_price,0,',','.') }} VND</strike>
								@endif
							@else
							LIÊN HỆ
							@endif
						</div>
					</div>
					@if($data->closed)
					<div class="alert alert-warning">Tạm hết hàng.</div>
					<form action="{{ route('contact.subscribe_instock_cameback') }}" method="POST" id="subscribeInstockCameback">
						@csrf
						<input type="hidden" name="id" value="{{ $data->id }}">
						<input type="hidden" name="code" value="{{ $data->code }}">
						<input type="hidden" name="name" value="{{ $data->name }}">

						<div class="form-group mb-3">
							<label class="mb-0">Liên hệ ngay khi có hàng:</label>
							<div class="input-group">
								<input type="email" name="email" class="form-control" placeholder="Nhập email liên hệ" required>
								<span class="input-group-btn">
									<button class="btn btn-primary"><i class="fa fa-envelope fa-lg"></i></button>
								</span>
							</div>
						</div>
					</form>
					@endif
					@if($data->display_price && !$data->closed && $data->price > 0)
						@if(count($data->fk_versions) > 0)
							@foreach($data->fk_versions as $version)
								<div class="block-single-version-select" id="version{{ $version->id }}Wrap" style="display: none;">
									<div class="block-single-code">Mã sản phẩm: <span>{{ $version->code }}</span></div>
									<hr class="mb-1">
									<div class="block-single-price">
										@if(is_null($hotdeal) || (!is_null($hotdeal->available_qty) && $hotdeal->available_qty <= 0))
											{{ $version->price ? number_format($version->price,0,',','.') : number_format($data->price,0,',','.') }} VND
											@if($data->original_price && $data->original_price > $data->price)
												<strike style="font-size: 1rem;color: #676767">{{ number_format($data->original_price,0,',','.') }} VND</strike>
											@endif
										@else
											{{ number_format($hotdeal->price,0,',','.') }} VND
											<strike style="font-size: 1rem;color: #676767">{{ number_format($hotdeal->original_price,0,',','.') }} VND</strike>
										@endif
									</div>
								</div>
							@endforeach
						@endif

						@if(is_null($hotdeal) || (!is_null($hotdeal->available_qty) && $hotdeal->available_qty <= 0))
						@else
							@if(!empty($hotdeal->quantity) && $hotdeal->started_at < date('Y-m-d H:i:s') && $hotdeal->expired_at >= date('Y-m-d H:i:s'))
							<div class="progress mb-2" style="font-size: .9rem; line-height: 1.5rem; position: relative;">
								<div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{ ($hotdeal->quantity - $hotdeal->available_qty) }}" aria-valuemin="0" aria-valuemax="{{ $hotdeal->quantity }}" style="width: {{ ($hotdeal->quantity - $hotdeal->available_qty) * 100 / $hotdeal->quantity }}%; height: 20px; background-color: #ffa500;"></div>
								<span style="position: absolute;width: 100%;">
									@if($hotdeal->quantity === $hotdeal->available_qty)
									Vừa mở bán
									@else
									Đã bán {{ $hotdeal->quantity - $hotdeal->available_qty }}
									@endif
								</span>
							</div>
							@endif
							
							<div class="hotdeal">
								Khuyến mãi hết hạn sau:
								<div class="hotdeal-countdown countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($hotdeal->expired_at)) }}" onload="countdown(event, true)">Loading</div>
							</div>
						@endif


						<form action="" method="POST" id="singleForm">
							@csrf
							<input type="hidden" name="id" value="{{ $data->id }}">
							@if(count($data->fk_versions) > 0)
							<div class="block-single-versions">
								<div class="mb-1">Tùy chọn: <span id="versionName"></span></div>
								@foreach($data->fk_versions as $key => $version)
								@if($version->color)
								<label class="color">
									<input type="radio" name="version_id" value="{{ $version->id }}" data-id="{{ $version->id }}" data-name="{{ $version->name }}" {!! $key == 0 ? ' class="required"' : null !!}{!! (!is_null($version->quantity) && $version->quantity == 0) ? ' disabled' : null !!}>
									<span style="background-color: #{{ $version->color }};"{!! (!is_null($version->quantity) && $version->quantity == 0) ? ' data-toggle="tooltip" data-placement="top" title="Hết hàng"' : null !!}></span>
								</label>
								@else
								<label class="text">
									<input type="radio" name="version_id" value="{{ $version->id }}" data-id="{{ $version->id }}" data-name="{{ $version->name }}" {!! $key == 0 ? ' class="required"' : null !!}{!! (!is_null($version->quantity) && $version->quantity == 0) ? ' disabled' : null !!}>
									<span{!! (!is_null($version->quantity) && $version->quantity == 0) ? ' data-toggle="tooltip" data-placement="top" title="Hết hàng"' : null !!}>{{ $version->name }}</span>
								</label>
								@endif
								@endforeach
								<div style="position: relative;"><label id="version_id-error" class="error" for="version_id" style="display: none;"></label></div>
							</div>
							<script>
								$("input[name=version_id]").change(function(){
									$(".block-single-version-select").hide();
									$("#version" + $(this).data("id") + "Wrap").show();
									$("#versionName").html($(this).data("name"));
									
									if($("#xzoomThumbs_" + $(this).data("id")).length > 0) {
										$(".xzoom-thumbs").hide();
										$("#xzoomThumbs_" + $(this).data("id")).show();
										$(".xzoom-gallery").removeClass("xactive");
										$("#xzoomThumbs_" + $(this).data("id")).find("img").first().addClass("xactive");
										$("#mainImage").attr("src", $("#xzoomThumbs_" + $(this).data("id")).find("img").first().attr("src"));
										$("#mainImage").attr("xoriginal", $("#xzoomThumbs_" + $(this).data("id")).find("img").first().attr("src"));
									}else{
										if($("#xzoomThumbs_origin").length > 0) {
											$(".xzoom-thumbs").hide();
											$("#xzoomThumbs_origin").show();
											$(".xzoom-gallery").removeClass("xactive");
											$("#xzoomThumbs_origin").find("img").first().addClass("xactive");
											$("#mainImage").attr("src", $("#xzoomThumbs_origin").find("img").first().attr("src"));
											$("#mainImage").attr("xoriginal", $("#xzoomThumbs_origin").find("img").first().attr("src"));
										}
									}
								});
							</script>
							@endif
							<div class="block-single-quantity">
								Số lượng:
								<div class="custom-number">
									<div class="input-group">
										<span class="input-group-btn">
											<button type="button" class="btn quantity-minus" onclick="quantity_sub(event);">-</button>
										</span>
										<input type="text" id="quantity" name="quantity" value="1" min="1" required>
										<span class="input-group-btn">
											<button type="button" class="btn quantity-plus" onclick="quantity_add(event);">+</button>
										</span>
									</div>
								</div>
								<label id="quantity-error" class="error" for="quantity" style="display: none;"></label>
							</div>
							
							@if($data->is_combo)
							<div class="block-single-combo-product-list mb-4">
								Các sản phẩm trong combo
								<table width="100%" cellpadding="5">
									<tbody>
										@foreach($data->fk_comboProducts as $singleProduct)
										<tr>
											<td width="70">
												<img src="{{ media_url('products', $singleProduct->thumbnail) }}" width="60">
											</td>
											<td>
												{{ $singleProduct->name }}
											</td>
											<td class="text-right">
												@if(count($singleProduct->fk_versions) > 0)
													<select name="combo_version_id[{{ $singleProduct->id }}]" class="custom-select">
														@foreach($singleProduct->fk_versions as $cb_version)
															@if(is_null($cb_version->quantity) || $cb_version->quantity > 0)
															<option value="{{ $cb_version->id }}">{{ $cb_version->name }}</option>
															@else
															<option value="{{ $cb_version->id }}" disabled>{{ $cb_version->name }} (Hết hàng)</option>
															@endif
														@endforeach
													</select>
												@endif
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							@endif

							@if($data->is_combo)
								@if($listproductss != '')
							<div class="block-single-combo-product-list mb-4">
								Mua kèm các sản phẩm bên dưới để được giá ưu đãi
								<table width="100%" cellpadding="5">
									<tbody>
									@foreach($listproductss as $lp)
										@php 
											$productinfo = $lp['productinfo'];
										@endphp
										<tr>
											<td width="20">
												<input type="radio" id="sanphamdikem{{ $lp->id }}" name="sanphamdikem" value="{{ $lp->id }}">
											</td>
											<td width="70">
												<img src="{{ media_url('products', $productinfo->thumbnail) }}" width="60">
											</td>
											<td>
												<label for="sanphamdikem{{ $lp->id }}">
													{{ $productinfo->name }}
												</label>
											</td>
											<td class="text-right block-single-price" style="font-size: 1rem;">
												{{ number_format($lp->price) }} VNĐ
											</td>
										</tr>
									@endforeach
									</tbody>
								</table>
							</div>
								@endif
							@endif

							<div class="block-single-actions">
								<button name="add_to_cart" value="on" class="btn btn-add-to-cart">Thêm vào giỏ hàng</button>
								<button name="buy" value="on" class="btn btn-buy">Mua ngay</button>
							</div>
						</form>
					@endif
					<div class="block-single-social">
						<div class="fb-like" data-href="{{ url()->current() }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				@if(!Agent::isPhone())
					@include('particles.single_policy')
				@endif
			</div>
		</div>
		
		@if($data->content)
		<div class="block-single-content mb-4">
			<div class="block-single-header">Mô tả sản phẩm</div>
			<div class="block-single-content-body" style="">
				{!! $data->content !!}</div>
			<p class="show-more" style="display: none;" onclick="readmore('block-single-content-body');">
				<a id="xem-them-bai-viet" href="javascript:;" class="readmore">Đọc thêm</a>
			</p>
		</div>
		@endif

		@if($data->properties)
		<div class="block-single-property mb-4">
			<div class="block-single-header">Thông tin chi tiết</div>
			<div class="block-single-property-body">
				{!! $data->properties !!}
			</div>
			<p class="show-more" style="display: none;" onclick="readmore('block-single-property-body');">
				<a id="xem-them-bai-viet" href="javascript:;" class="readmore">Đọc thêm</a>
			</p>
		</div>
		@endif

		@if(Agent::isPhone())
			@include('particles.single_policy')
		@endif

		@if(count($related) > 0)
		<div class="block-single-related mb-4">
			<div class="block-single-header">Gợi ý sản phẩm cho bạn</div>
			<div class="block-single-related-body">
				<div class="owl-carousel product-slider">
					@foreach($related as $item)
					<div class="item">
						@if($data->is_combo)
							@include('particles.combo_item')
						@else
							@include('particles.product_item')
						@endif
					</div>
					@endforeach
				</div>
			</div>
		</div>
		@endif

		@if(count($fullReviews) > 0)
		
			@include('particles.review_statistic')

			@include('particles.review_block')

		@else
		
		<div class="block-rating mb-5">
			@include('particles.review_rate_form')
		</div>

		@endif
	</div>

@stop

@section('footer')
	{{-- Xử lý "Read more" --}}
	<script>
		$(document).ready(function () {
			var height = 400;
			// TODO: Nếu là màn hình lớn thì lấy chiều dài / 2 || 400
			if($(window).width() > 768){
				height = $(window).width()/2 > 400 ? 400 : $(window).width()/2;
			} else { // Nếu là mobile thì lấy chiều cao / 2
				height = $(window).height()/2;
			}

			if ($('.block-single-content-body').length > 0) {
				// TODO: Nếu height của body lớn hơn height cho phép thì ẩn bớt và hiện read more
				if ($('.block-single-content-body').height() > height) {
					$('.block-single-content-body').height(height)
					$('.block-single-content .show-more').show()
				}
			}
			if ($('.block-single-property-body').length > 0) {
				// TODO: Nếu height của body lớn hơn height cho phép thì ẩn bớt và hiện read more
				if ($('.block-single-property-body').height() > height) {
					$('.block-single-property-body').height(height)
					$('.block-single-property .show-more').show()
				}
			}
		})
		function readmore(elementClass) {
			$('.' + elementClass).height('auto');
			$('.' + elementClass).parent().find('.show-more').hide()
		}
	</script>
	{{-- Product Slider --}}
	<script src="{{ asset('js/product-slider.handler.js') }}"></script>
	<script>
		$("#singleForm").validate({
			ignore:'',
			rules: {
				version_id: {
					required: true
				}
			},
			messages: {
				version_id: {
					required: "Vui lòng chọn màu sắc."
				}
			},
			submitHandler: function(form) {
				var request = $(form).serialize();
				//console.log(request);
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
				$.ajax({
					url : base_url + '/checkout/cart/add',
					data : request,
					type : 'POST',
					success : function (data) {
						{{-- MTAG Viewed Product Detail --}}
						try {
							mtagAddedProduct();
						} catch (error) {
						}

						if(data.redirect){
							window.location.href = data.redirect;
						}else{
							swal(data.title, data.msg, data.alert);
							$(".data-cart-count").html(data.count);
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
                    	console.log(xhr.responseText);
						swal('Lỗi!', xhr.responseText, 'error');
					}
				});
			}
		});
		$('#reviewModal').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget); // Button that triggered the modal
		  var recipient = button.data('id'); // Extract info from data-* attributes
		  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		  var modal = $(this);
		  modal.find('.modal-title').text('Trả lời đánh giá #' + recipient);
		  modal.find('.modal-body #parentCommentId').val(recipient);
		})
	</script>
	<script type="text/javascript">
	   $(function() {
	      $('#starRating').barrating({
	        theme: 'fontawesome-stars'
	      });
	   });
	</script>

	{{-- MTAG Viewed Product Detail --}}
	@include('api.mtag.viewed_product_detail')
	@include('api.mtag.added_product')
@stop