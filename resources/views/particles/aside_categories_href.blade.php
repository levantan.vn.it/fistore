<aside class="aside-products mb-4 d-none d-md-block">
	<div class="aside-header">
		<div class="aside-title">Danh mục sản phẩm</div>
	</div>
	<div class="aside-body">
		@foreach($categories as $item)
		<div class="mb-1 lh-2">
			<div class="text-uppercase{!! isset($category) && $category->id == $item->id || isset($category) && $category->parent_id == $item->id ? '' : ' collapsed' !!}" data-toggle="collapse" data-arrow="true" href="#categoryAside{{ $item->id }}" style="cursor: pointer">{{ $item->name }}</div>
			<div class="collapse{!! isset($category) && $category->id == $item->id || isset($category) && $category->parent_id == $item->id ? ' show' : null !!}" id="categoryAside{{ $item->id }}">
				<div>
					<a href="{{ category_link($item->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="{{ $item->id }}" class="custom-control-input" form="mainForm"{!! isset($category) && $category->id == $item->id ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Tất cả</span>
					</a>
				</div>
				@foreach($item->fk_childs as $child)
				<div>
					<a href="{{ category_link($child->slug) }}" class="custom-control custom-checkbox">
						<input type="radio" value="{{ $child->id }}" class="custom-control-input" form="mainForm"{!! isset($category) && $category->id == $child->id ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ $child->name }}</span>
					</a>
				</div>
				@endforeach
			</div>
		</div>
		@endforeach
	</div>
	{{-- <div class="aside-footer text-center">
		<button class="btn btn-link">Xem thêm <i class="fa fa-angle-down"></i></button>
	</div> --}}
</aside>