<div class="item-brand">
	<div class="item-thumbnail">
		<a href="http://fistore.vn/{{$item->slug}}">
			<img src="{{ media_url('product-brands', $item->thumbnail) }}" alt="{{ $item->name }}">
		</a>
	</div>
	<div class="item-info">
		<div class="item-name">
			<a href="{{ brand_link($item->slug) }}">{{ $item->name }}</a>
		</div>
	</div>
</div>