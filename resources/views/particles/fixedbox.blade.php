<div class="fixedbox" id="supportion">
	<a href="{{ url('page/dich-vu') }}" class="fixed-item mb-3">
		<img src="{{ asset('images/support-ico.png') }}">
		DỊCH VỤ
	</a>
	<a href="{{ route('contact') }}" class="fixed-item">
		<img src="{{ asset('images/contact-ico.png') }}">
		LIÊN HỆ
	</a>
</div>
<div class="fixedbox" id="position">
	<div class="fixed-item mt-4 d-none d-md-block">
		<img src="{{ asset('images/scrolltotop.png') }}" onclick="scroll_top()">
	</div>
</div>