@php
	$popupLG = get_setting('popup_lg');
	$popupSM = get_setting('popup_sm');
	$popupURL = get_setting('popup_url');
@endphp
@if((!empty($popupLG) || !empty($popupSM)) && (!$popupLG->disabled || !$popupSM->disabled))
<div id="popupOverlay">
	<div id="popup">
		@if(Agent::isPhone() && !empty($popupSM) && !$popupSM->disabled)
			<a href="{{ $popupURL->value }}">
				<img src="{{ media_url('popups', $popupSM->value) }}">
			</a>
		@elseif(!empty($popupLG) && !$popupLG->disabled)
			<a href="{{ $popupURL->value }}">
				<img src="{{ media_url('popups', $popupLG->value) }}">
			</a>
		@endif
		<span class="fa-stack fa-lg" onclick="popupOverlayEvent()" style="position: absolute;right: 0;top: 0;cursor: pointer;font-size: 1.8rem">
			<i class="fa fa-circle fa-stack-2x fa-inverse"></i>
			<i class="fa fa-remove fa-stack-1x"></i>
		</span>
	</div>
</div>


<style>
	#popupOverlay{
		background-color: rgba(91,89,77,.7);
		position: fixed;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		z-index: 998;
		display: none;
	}
	#popup{
		position: fixed;
		z-index: 999;
		width: 100%;
		height: 100%;
		max-width: 720px;
		max-height: 360px;
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		margin: auto;
		align-items: center;
		display: flex;
		justify-content: center;
	}
	#popup img{
		width: 100%;
	}
	#popupOverlay.show{
		display: block;
	}
	@media (max-width: 767px) {
		max-width: 375px;
		max-height: 480px;
	}
</style>
<script>
	function popupOverlayEvent(){
		$("#popupOverlay").removeClass("show");
	}
	$(document).ready(function(){
		if(!$.session.get("popupExist")){
			$("#popupOverlay").addClass("show");
			$.session.set("popupExist", true);
		}
	});
</script>
@endif