<aside class="block-articles mb-5">
	<div class="block-header">
		<div class="block-title">Bài viết xem nhiều</div>
	</div>
	<div class="block-body">
		@foreach($mostedArticles as $item)
		<div class="article-item-3">
			<img src="{{ media_url('articles', $item->thumbnail) }}" alt="{{ $item->title ?? $item->name }}" class="item-thumbnail">
			<a href="{{ article_link($item->slug) }}" class="item-title">{{ $item->name }}</a>
		</div>
		@endforeach
	</div>
</aside>