<div class="item-product hot-deal">
  <div class="item-thumbnail">
    <a href="{{ $item->target_url }}" target="_blank">
      <img src="{{ media_url('products', $item->thumbnail) }}" alt="{{ $item->name }}" title="{{ $item->name }}" width="100%">
    </a>
  </div>
  <div class="item-info">
    <div class="item-brand-name">
      <a href="{{ brand_link($item->brand_slug) }}">{{ str_length_limit($item->brand_name, 13) }}</a>
    </div>
    <div class="item-name">
      <a href="{{ $item->target_url }}" target="_blank">{{ str_length_limit($item->name, 17) }}</a>
    </div>

    @if(date('U', strtotime($item->started_at)) > date('U'))
      <div class="item-countdown">-<span class="countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->started_at)) }}" onload="countdown(event)">Loading...</span></div>
    @else
      <div class="item-countdown"><span class="countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->expired_at)) }}" onload="countdown(event)">Loading...</span></div>
    @endif
    
    <div class="item-action">
      <a href="{{ $item->target_url }}" target="_blank" class="btn btn-primary btn-block">
        @if(date('U', strtotime($item->started_at)) > date('U'))
          COMING SOON
        @else
          TRẢI NGHIỆM
        @endif
      </a>
    </div>
  </div>
</div>
