<aside class="aside-products mb-4">
	<div class="aside-header">
		<div class="aside-title">Danh mục sản phẩm</div>
	</div>
	<div class="aside-body">
		@foreach($categories as $item)
		<div class="mb-3">
			<div class="text-uppercase font-weight-bold">{{ $item->name }}</div>
			<div class="form-group mb-1">
				<label class="custom-control custom-checkbox">
					<input type="radio" name="category" value="{{ $item->id }}" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! request()->category == $item->id ? ' checked' : null !!}>
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description">Tất cả</span>
				</label>
			</div>
			@foreach($item->fk_childs as $child)
			<div class="form-group mb-1">
				<label class="custom-control custom-checkbox">
					<input type="radio" name="category" value="{{ $child->id }}" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! request()->category == $child->id ? ' checked' : null !!}>
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description">{{ $child->name }}</span>
				</label>
			</div>
			@endforeach

		</div>
		@endforeach
	</div>
	<div class="aside-footer text-center">
		<button class="btn btn-link">Xem thêm <i class="fa fa-angle-down"></i></button>
	</div>
</aside>