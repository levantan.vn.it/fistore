<aside class="aside-products mb-4 d-none d-md-block">
	<div class="aside-header">
		<div class="aside-title">Danh mục sản phẩm</div>
	</div>
	<div class="aside-body">
		@foreach($categories as $item)
		<div class="mb-1 lh-2">
			<div class="text-uppercase{!! request()->category == $item->id ? '' : ' collapsed' !!}" data-toggle="collapse" data-arrow="true" href="#categoryAside{{ $item->id }}" style="cursor: pointer">{{ $item->name }}</div>
			<div class="collapse{!! request()->category == $item->id ? ' show' : null !!}" id="categoryAside{{ $item->id }}">
				<div>
					<label class="custom-control custom-checkbox">
						<input type="radio" name="category" value="{{ $item->id }}" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! request()->category == $item->id ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Tất cả</span>
					</label>
				</div>
				@foreach($item->fk_childs as $child)
				<div>
					<label class="custom-control custom-checkbox">
						<input type="radio" name="category" value="{{ $child->id }}" class="custom-control-input" onchange="this.form.submit()" form="mainForm"{!! request()->category == $child->id ? ' checked' : null !!}>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">{{ $child->name }}</span>
					</label>
				</div>
				@endforeach
			</div>
		</div>
		@endforeach
	</div>
	{{-- <div class="aside-footer text-center">
		<button class="btn btn-link">Xem thêm <i class="fa fa-angle-down"></i></button>
	</div> --}}
</aside>