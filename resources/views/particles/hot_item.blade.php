<div class="item-product hot-deal">
  <div class="item-thumbnail">
    <a href="{{ product_link($item->fk_product) }}">
      <img src="{{ media_url('products', $item->fk_product->thumbnail) }}" alt="{{ $item->fk_product->title ?? $item->fk_product->name }}" title="{{ $item->fk_product->title ?? $item->fk_product->name }}">
    </a>
  </div>
  <div class="item-info">
    <div class="item-brand-name">
      <a href="{{ brand_link_by_product($item->fk_product) }}">{{ str_length_limit($item->fk_product->fk_brand->name, 13) }}</a>
    </div>
    <div class="item-name">
      <a href="{{ product_link($item->fk_product) }}">{{ str_length_limit($item->fk_product->name, 17) }}</a>
    </div>
    <div class="item-price">{{ number_format($item->price,0,',','.') }} đ
      @isset($item->original_price)
        <strike class="fz-down-1 text-muted">{{ number_format($item->original_price,0,',','.') }} đ</strike>
      @endisset
    </div>
    <div class="item-action">
      <a href="{{ product_link($item->fk_product) }}" class="btn btn-primary btn-block countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->expired_at)) }}" onload="countdown(event)">Loading...</a>
    </div>
  </div>
</div>
