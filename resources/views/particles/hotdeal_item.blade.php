<div class="item-product hot-deal">
  <div class="item-thumbnail">
    <a href="{{ product_link($item->slug) }}">
      <img src="{{ media_url('products', $item->thumbnail) }}" alt="{{ $item->name }}" title="{{ $item->name }}">
    </a>
    <label class="saleof">
      @if($item->started_at > date('Y-m-d H:i:s'))
      - {{ substr(number_format(round(($item->original_price-$item->price)/$item->original_price*100),0,',','.'), 0, 1) . str_replace([0,1,2,3,4,5,6,7,8,9], 'x', substr(number_format(round(($item->original_price-$item->price)/$item->original_price*100),0,',','.'), 1)) }}%
      @else
      - {{ round(($item->original_price-$item->price)/$item->original_price*100) }}%
      @endif
    </label>
  </div>
  <div class="item-info">
    {{-- <div class="item-brand-name">
      <a href="{{ brand_link($item->brand_slug) }}">{{ str_length_limit($item->brand_name, 15) }}</a>
    </div> --}}
    <div class="item-name">
      <a href="{{ product_link($item->slug) }}">{{ str_length_limit($item->name, 20) }}</a>
    </div>
    <div class="item-price">
      @if($item->started_at > date('Y-m-d H:i:s'))
        {{ substr(number_format($item->price,0,',','.'), 0, 1) . str_replace([0,1,2,3,4,5,6,7,8,9], 'x', substr(number_format($item->price,0,',','.'), 1)) }} đ
      @else
        {{ number_format($item->price,0,',','.') }} đ
      @endif
      @isset($item->original_price)
        <strike class="fz-down-1 text-muted">{{ number_format($item->original_price,0,',','.') }} đ</strike>
      @endisset
      <br/>
      @if($item->started_at > date('Y-m-d H:i:s'))
        <span class="countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->started_at)) }}" onload="countdown_compact(event)" style="color: {{ $item->started_at <= date('Y-m-d H:i:s') ? '#868e96' : '#A80B0B' }}">Loading...</span>
      @else
        <span class="countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->expired_at)) }}" onload="countdown_compact(event)" style="color: {{ $item->expired_at <= date('Y-m-d H:i:s') ? '#868e96' : '#A80B0B' }}">Loading...</span>
      @endif
    </div>
    @if(!empty($item->quantity) && $item->started_at < date('Y-m-d H:i:s') && $item->expired_at >= date('Y-m-d H:i:s'))
    <div class="progress" style="font-size: .9rem; line-height: 1.5rem; position: relative;">
      <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{ ($item->quantity - $item->available_qty) }}" aria-valuemin="0" aria-valuemax="{{ $item->quantity }}" style="width: {{ ($item->quantity - $item->available_qty) * 100 / $item->quantity }}%; height: 20px; background-color: #ffa500;"></div>
      <span style="position: absolute;width: 100%;">
        @if($item->quantity - $item->available_qty == 0)
        Vừa mở bán
        @else
        Đã bán {{ $item->quantity - $item->available_qty }}
        @endif
      </span>
    </div>
    @endif
    @if($item->started_at < date('Y-m-d H:i:s') && $item->expired_at >= date('Y-m-d H:i:s'))
    <div class="item-action">
      <a href="{{ product_link($item->slug) }}" class="btn btn-primary btn-block">MUA NGAY</a>
    </div>
    @endif
  </div>
</div>