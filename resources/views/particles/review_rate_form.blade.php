<div class="{!! count($fullReviews) > 0 ? 'collapse': null !!}" id="reviewCollapse">
<hr class="mb-5">
<div class="row">
	<div class="col-lg-8 m-auto">

		
		<div class="form-title">GỬI NHẬN XÉT CỦA BẠN</div>
		@auth('customer')
		<form action="{{ route('review.rate', $data->slug) }}" method="POST" id="reviewForm">
			@csrf
			<input type="hidden" name="product_id" value="{{ $data->id }}">


			<div class="form-group row">
				<label for="title" class="col-md-3">Mức độ hài lòng <span class="required">*</span></label>
				<div class="col-md-9">
					<select name="rating" id="starRating">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
						<option value="5" selected>5</option>
					</select>
				</div>
			</div>
			<div class="form-group row">
				<label for="title" class="col-md-3">Tiêu đề nhận xét <span class="required">*</span></label>
				<div class="col-md-9">
					<input type="text" name="title" class="form-control" required>
				</div>
			</div>
			<div class="form-group row">
				<label for="title" class="col-md-3">Nội dung nhận xét <span class="required">*</span></label>
				<div class="col-md-9">
					<textarea name="content" rows="4" class="form-control" required></textarea>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-3"></label>
				<div class="col-md-9">
					<button class="btn btn-primary" style="min-width: 200px">GỬI NHẬN XÉT</button>
					@if(count($fullReviews) > 0)
					<button class="btn btn-outline-secondary" type="button" data-toggle="collapse" data-target="#reviewCollapse">Đóng</button>
					@endif
				</div>
			</div>
		</form>
		@else
		<div class="form-group text-center">
			<label>Vui lòng đăng nhập để đăng nhận xét</label>
			<div>
				<button class="btn btn-primary" data-toggle="modal" data-target="#loginModal" style="min-width: 200px">ĐĂNG NHẬP</button>
			</div>
		</div>
		@endauth
		
	</div>
</div>
<hr class="mt-5 mb-0">
</div>