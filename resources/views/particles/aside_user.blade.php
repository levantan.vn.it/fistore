<aside class="block-user d-none d-md-block mt-4 mb-4">
	<div class="user-describe">
		<img src="{{ Auth::guard('customer')->user()->avatar ?? media_url('users', 'user.png') }}" class="user-avatar">
		<div class="user-info">
			<div class="user-name">{{ Auth::guard('customer')->user()->name }}</div>
			<div class="user-email">{{ Auth::guard('customer')->user()->email }}</div>
		</div>
	</div>

	<li class="menu-item profile">
		<a href="{{ route('user.profile') }}"><i class="fa fa-user-circle-o fa-fw fa-lg"></i>Thông tin cá nhân</a>
	</li>
	<li class="menu-item order">
		<a href="{{ route('user.order') }}"><i class="fa fa-calendar fa-fw fa-lg"></i>Quản lý đơn hàng</a>
	</li>
	<li class="menu-item address_book">
		<a href="{{ route('user.address_book') }}"><i class="fa fa-map-marker fa-fw fa-lg"></i>Sổ địa chỉ</a>
	</li>
	<li class="menu-item extra">
		<a href="{{ route('user.extra') }}"><i class="fa fa-usd fa-fw fa-lg"></i>Quản lý điểm</a>
	</li>
	<li class="menu-item">
		<a href="{{ route('logout') }}"><i class="fa fa-sign-out fa-fw fa-lg"></i>Thoát tài khoản</a>
	</li>
</aside>