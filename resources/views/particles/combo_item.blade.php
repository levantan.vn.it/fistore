<div class="item-product">
  <div class="item-thumbnail">
    <a href="{{ combo_link($item->slug) }}">
      <img src="{{ media_url('products', $item->thumbnail) }}" alt="{{ $item->title ?? $item->name }}" title="{{ $item->title ?? $item->name }}">
    </a>
  </div>
  <div class="item-info">
    <div class="item-name">
      <a href="{{ combo_link($item->slug) }}">{{ str_length_limit($item->name, 17) }}</a>
    </div>
    <div class="item-price">{{ number_format($item->price,0,',','.') }} đ
      @isset($item->original_price)
        <strike class="fz-down-1 text-muted">{{ number_format($item->original_price,0,',','.') }} đ</strike>
      @endisset
    </div>
    <div class="item-action">
      <a href="{{ combo_link($item->slug) }}" class="btn btn-primary btn-block">XEM NGAY</a>
    </div>
  </div>
</div>
