@if(count($fullReviews) > 0)
<div class="block-reviews mb-5">
	<div class="block-header">
		Chọn xem nhận xét
		<select name="sort" class="custom-select">
			<option value="usefullest">Hữu ích</option>
			<option value="newest">Mới nhất</option>
		</select>
		<select name="customer" class="custom-select">
			<option value="">Tất cả khách hàng</option>
			<option value="1">Khách hàng đã mua sản phẩm</option>
		</select>
		<select name="star" class="custom-select">
			<option value="">Tất cả sao</option>
			<option value="5">5 sao</option>
			<option value="4">4 sao</option>
			<option value="3">3 sao</option>
			<option value="2">2 sao</option>
			<option value="1">1 sao</option>
		</select>
	</div>
	<div class="block-body">
		@foreach($fullReviews as $item)
		<div class="review-item" id="reviewId{{ $item->id }}">
			<div class="row">
				<div class="review-author col-md-2">
					<div class="review-author-avatar">
						<img src="{{ $item->fk_user->avatar ?? media_url('users', 'user.png') }}">
					</div>
					<div class="review-author-name">{{ $item->fk_user->name }}</div>
					<div class="review-author-commented_at">{{ date('d/m/Y H:i', strtotime($item->created_at)) }}</div>
					<div class="review-author-from">Đã tích lũy được <strong>{{ $item->fk_user->extra_point }}</strong> điểm</div>
				</div>
				<div class="review-contain col-md-10">
					<div class="review-title">
						@for($i = 1; $i <= $item->rating; $i++)
						<i class="fa fa-star fa-lg"></i>
						@endfor
						<span class="title">{{ $item->title }}</span>
						@if($item->bought)
						<div class="text-success fz-down-1">Khách hàng đã mua sản phẩm <i class="fa fa-check-circle"></i></div>
						@endif
					</div>
					<div class="review-content">{{ $item->content }}</div>
					<div class="review-action">
						@auth('customer')
							<button class="btn btn-reply" data-toggle="collapse" data-target="#replyTo{{ $item->id }}">Gửi trả lời</button>
							@if($item->thanked > 0)
								<span>Được cảm ơn <strong>{{ $item->thanked }}</strong> lần</span>
							@endif
							@if(!$item->fk_thank->contains(Auth::guard('customer')->user()->id) && Auth::guard('customer')->user()->id != $item->user_id)
								<span>Nhận xét này hữu ích với bạn?</span>
								<form action="{{ route('review.thank', $data->slug) }}" method="POST" class="d-inline">
									@csrf
									<button class="btn btn-primary" name="thank" value="{{ $item->id }}"><i class="fa fa-thumbs-up"></i>&nbsp;&nbsp;Cảm ơn</button>
								</form>
							@endif
						@else
							<button class="btn btn-reply" data-toggle="modal" data-target="#loginModal">Đăng nhập để trả lời</button>
						@endif
					</div>
					<div class="review-reply collapse" id="replyTo{{ $item->id }}">
						<form action="{{ route('review.reply', $data->slug) }}" method="POST">
							@csrf
							<input type="hidden" name="parent_id" value="{{ $item->id }}">
							<div class="form-group">
								<textarea name="reply" rows="5" class="form-control" placeholder="Nhập nội dung trả lời" required></textarea>
							</div>
							<div class="form-group">
								<button class="btn btn-primary"><i class="fa fa-reply"></i>&nbsp;&nbsp;Gửi trả lời</button>
								<button class="btn btn-outline-secondary" data-toggle="collapse" data-target="#replyTo{{ $item->id }}">Hủy bỏ</button>
							</div>
						</form>
					</div>
					@if(count($item->fk_replies) > 0)
					<div class="review-replies">

						@foreach($item->fk_replies as $reply)
						<div class="review-reply-item">
							<div class="review-author-reply-avatar">
								<img src="{{ media_url('users', 'user.png') }}">
							</div>
							<div class="review-author-reply-contain">
								<div class="review-author-reply-name">{{ $reply->fk_user->name }}</div>
								<div class="review-author-reply-content">{{ $reply->content }}</div>
							</div>
						</div>
						@endforeach

					</div>
					@endif
				</div>
			</div>
		</div>
		@endforeach
	</div>
	<div class="block-footer">
		
		<ul class="pagination">
		    @if($fullReviews->currentPage() != 1)
		    <li class="page-item">
		        <a class="page-link" href="{{$fullReviews->url(1)}}">
		            <i class="fa fa-angle-double-left"></i>
		        </a>
		    </li>
		    @endif

		    @for($i = 1; $i <= $fullReviews->lastPage(); $i++)
		        @if($i <= ($fullReviews->currentPage() + 2) && $i >= ($fullReviews->currentPage() - 2))
		            @if($fullReviews->lastPage() > 1)
		                <li class="page-item{{($fullReviews->currentPage() == $i) ? ' active' : null}}">
		                    <a class="page-link" href="{{$fullReviews->url($i)}}">{{$i}}</a>
		                </li>
		            @endif
		        @endif
		    @endfor
		    
		    @if($fullReviews->currentPage() < $fullReviews->lastPage())
		    <li class="page-item">
		        <a class="page-link" href="{{$fullReviews->url($fullReviews->lastPage())}}">
		            <i class="fa fa-angle-double-right"></i>
		        </a>
		    </li>
		    @endif
		</ul>
		
	</div>
</div>
@endif

<script>
	$('#reviewModal').on('show.bs.modal', function (event) {
	  var button = $(event.relatedTarget); // Button that triggered the modal
	  var recipient = button.data('id'); // Extract info from data-* attributes
	  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
	  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
	  var modal = $(this);
	  modal.find('.modal-title').text('Trả lời đánh giá #' + recipient);
	  modal.find('.modal-body #parentCommentId').val(recipient);
	});
	$(function() {
	   $('#starRating').barrating({
	     theme: 'fontawesome-stars'
	   });
	});
</script>