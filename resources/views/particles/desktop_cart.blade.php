<table width="100%" cellpadding="10" class="cart-content desktop">
	<tbody>
		@foreach(Cart::content() as $item)
			@if($item->options->product1)
		<tr>
			<td width="120">
				<img src="{{ media_url('products', $item->options->product->thumbnail) }}"
					alt="{{ $item->name }}" title="{{ $item->name }}" width="100%" class="cart-product-thumbnail">
			</td>
			<td>
				<div class="cart-product-name">
					<a href="{{ product_link($item->options->product->slug) }}" target="_blank">
						{{ $item->name }}
					</a>
				</div>	
				<div class="cart-product-name">
					+ <a href="{{ product_link($item->options->product1->slug) }}" target="_blank">
						{{ $item->options->product1->name }}
					</a>
				</div>	
				<div class="cart-product-price">{{ number_format($item->price,0,',','.') }} VND</div>

				@isset($item->options->product->original_price)
				<strike class="cart-product-original_price">{{ number_format($item->options->product->original_price,0,',','.') }} VND</strike>
				@endisset

				@if(!is_null($item->options->hotdeal->expired_at) && date('U', strtotime($item->options->hotdeal->expired_at)) > date('U'))
				<div class="hotdeal">
					Hết hạn sau:
					<span class="hotdeal-countdown countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->options->hotdeal->expired_at)) }}" onload="countdown(event, true)">Loading</span>
				</div>
				@endif

				@if(!is_null($item->options->combo))
				<div>
					@foreach($item->options->combo as $singleProduct)
						<div>{{ $singleProduct->product_name }} {{ isset($singleProduct->version_id) ? ' - '.$singleProduct->version_name : null }}</div>
					@endforeach
				</div>
				@endif

				<div><button class="btn btn-remove" onclick="remove_cart_item(event)"><i class="fa fa-trash"></i> Xóa</button></div>
				<input type="hidden" name="rowId" value="{{ $item->rowId }}">
			</td>
			<td width="60">
				<div class="custom-number">
					<div class="input-group">
                    	<span class="input-group-btn">
							<button type="button" class="btn quantity-minus" onfocus="prev_value(event);" onclick="quantity_sub(event);update_cart(event);">-</button>
						</span>
                    	@if($item->options->salestatus == 1)
						<input type="text" id="quantity" name="quantity" value="{{ $item->qty }}" min="1" max="2" onfocus="prev_value(event);" onchange="update_cart(event)" required>
                    	<span class="input-group-btn">
							<button type="button" class="btn quantity-plus" max="2" onfocus="prev_value(event);" onclick="quantity_add(event);update_cart(event);">+</button>
						</span>
                    	@else
                    	<input type="text" id="quantity" name="quantity" value="{{ $item->qty }}" min="1" onfocus="prev_value(event);" onchange="update_cart(event)" required>
                    	<span class="input-group-btn">
							<button type="button" class="btn quantity-plus"  onfocus="prev_value(event);" onclick="quantity_add(event);update_cart(event);">+</button>
						</span>
                    	@endif
					</div>
				</div>
			</td>
		</tr>	
			@else
		<tr>
			<td width="120">
				<img src="{{ media_url('products', $item->options->product->thumbnail) }}"
					alt="{{ $item->name }}" title="{{ $item->name }}" width="100%" class="cart-product-thumbnail">
			</td>
			<td>
				<div class="cart-product-name">
					<a href="{{ product_link($item->options->product->slug) }}" target="_blank">
						{{ $item->name }}
					</a>
				</div>
				<div class="cart-product-price">{{ number_format($item->price,0,',','.') }} VND</div>

				@isset($item->options->product->original_price)
				<strike class="cart-product-original_price">{{ number_format($item->options->product->original_price,0,',','.') }} VND</strike>
				@endisset

				@if(!is_null($item->options->hotdeal->expired_at) && date('U', strtotime($item->options->hotdeal->expired_at)) > date('U'))
				<div class="hotdeal">
					Hết hạn sau:
					<span class="hotdeal-countdown countdown" data-countdown="{{ date('Y/m/d H:i:s', strtotime($item->options->hotdeal->expired_at)) }}" onload="countdown(event, true)">Loading</span>
				</div>
				@endif

				@if(!is_null($item->options->combo))
				<div>
					@foreach($item->options->combo as $singleProduct)
						<div>{{ $singleProduct->product_name }} {{ isset($singleProduct->version_id) ? ' - '.$singleProduct->version_name : null }}</div>
					@endforeach
				</div>
				@endif

				<div><button class="btn btn-remove" onclick="remove_cart_item(event)"><i class="fa fa-trash"></i> Xóa</button></div>
				<input type="hidden" name="rowId" value="{{ $item->rowId }}">
			</td>
			<td width="60">
				<div class="custom-number">
					<div class="input-group">
						<span class="input-group-btn">
							<button type="button" class="btn quantity-minus" onfocus="prev_value(event);" onclick="quantity_sub(event);update_cart(event);">-</button>
						</span>
                    	@if($item->options->salestatus == 1)
						<input type="text" id="quantity" name="quantity" value="{{ $item->qty }}" min="1" max="2" onfocus="prev_value(event);" onchange="update_cart(event)" required>
                    	<span class="input-group-btn">
							<button type="button" class="btn quantity-plus" max="2" onfocus="prev_value(event);" onclick="quantity_add(event,2);update_cart(event,2);">+</button>
						</span>
                    	@else
                    	<input type="text" id="quantity" name="quantity" value="{{ $item->qty }}" min="1" onfocus="prev_value(event);" onchange="update_cart(event)" required>
                    	<span class="input-group-btn">
							<button type="button" class="btn quantity-plus"  onfocus="prev_value(event);" onclick="quantity_add(event);update_cart(event);">+</button>
						</span>
                    	@endif
					</div>
				</div>
			</td>
		</tr>
			@endif
		@endforeach
	</tbody>
</table>