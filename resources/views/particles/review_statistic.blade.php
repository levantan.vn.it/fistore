<div class="block-rating mb-5">
	<div class="block-header block-single-header">
		<div class="block-title">Khách hàng nhận xét</div>
	</div>
	@if(count($fullReviews) > 0)
	<div class="block-body row">
		
		<div class="statistic col-md-one-fifth">
			<div class="statistic-title">Đánh giá trung bình</div>
			<div class="statistic-point">{{ number_format(array_sum($rating_point) / count($rating_point),1) }}/5</div>
			<div class="statistic-stars">
				@if(count($rating_point) > 0)
					@for($i = 1; $i <= array_sum($rating_point) / count($rating_point); $i ++)
						<i class="fa fa-star fa-lg"></i>
					@endfor
				@endif
				@if($whole >= 0.25 && $whole <= 0.75)
					<i class="fa fa-star-half fa-lg"></i>
				@elseif($whole > 0.75)
					<i class="fa fa-star fa-lg"></i>
				@endif
			</div>
			<div class="statistic-review-count">({{ count($fullReviews) }} nhận xét)</div>
		</div>
		<div class="chart col-md-two-fifth m-auto">
			<div class="chart-item">
				<div class="chart-item-before">5 <i class="fa fa-star"></i></div>
				<div class="chart-item-progress">
					<div class="progress">
						<div class="progress-bar" role="progressbar" style="width: {{ number_format($reviewStatistic->s5,0) }}%" aria-valuenow="{{ number_format($reviewStatistic->s5,0) }}" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>						
				<div class="chart-item-after">{{ number_format($reviewStatistic->s5,0) }}%</div>
			</div>

			<div class="chart-item">
				<div class="chart-item-before">4 <i class="fa fa-star"></i></div>
				<div class="chart-item-progress">
					<div class="progress">
						<div class="progress-bar" role="progressbar" style="width: {{ number_format($reviewStatistic->s4,0) }}%" aria-valuenow="{{ number_format($reviewStatistic->s4,0) }}" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>						
				<div class="chart-item-after">{{ number_format($reviewStatistic->s4,0) }}%</div>
			</div>

			<div class="chart-item">
				<div class="chart-item-before">3 <i class="fa fa-star"></i></div>
				<div class="chart-item-progress">
					<div class="progress">
						<div class="progress-bar" role="progressbar" style="width: {{ number_format($reviewStatistic->s3,0) }}%" aria-valuenow="{{ number_format($reviewStatistic->s3,0) }}" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>						
				<div class="chart-item-after">{{ number_format($reviewStatistic->s3,0) }}%</div>
			</div>

			<div class="chart-item">
				<div class="chart-item-before">2 <i class="fa fa-star"></i></div>
				<div class="chart-item-progress">
					<div class="progress">
						<div class="progress-bar" role="progressbar" style="width: {{ number_format($reviewStatistic->s2,0) }}%" aria-valuenow="{{ number_format($reviewStatistic->s2,0) }}" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>						
				<div class="chart-item-after">{{ number_format($reviewStatistic->s2,0) }}%</div>
			</div>

			<div class="chart-item">
				<div class="chart-item-before">1 <i class="fa fa-star"></i></div>
				<div class="chart-item-progress">
					<div class="progress">
						<div class="progress-bar" role="progressbar" style="width: {{ number_format($reviewStatistic->s1,0) }}%" aria-valuenow="{{ number_format($reviewStatistic->s1,0) }}" aria-valuemin="0" aria-valuemax="100"></div>
					</div>
				</div>						
				<div class="chart-item-after">{{ number_format($reviewStatistic->s1,0) }}%</div>
			</div>
		</div>
		<div class="review-form col-md-3">
			@auth('customer')
				@if($rated)
					Cảm ơn bạn đã nhận xét
				@else
					<button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#reviewCollapse">Viết nhận xét</button>
				@endif
			@else
			<button class="btn btn-primary btn-block" data-toggle="modal" data-target="#loginModal">Đăng nhập để nhận xét</button>
			@endauth
		</div>
	</div>
	@endif
	<div class="body-footer">
		@include('particles.review_rate_form')
	</div>
</div>