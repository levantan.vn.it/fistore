<script>
	function mtagAddedProduct()
	{
		digitalData.events.push({
			name: 'Added Product',
			product: {
				id: "{{ $data->id }}",
				url: "{{ product_link($data->slug) }}",
				imageUrl: "{{ media_url('products', $data->fk_images[0]->path ?? null) }}",
				thumbnailUrl: "{{ media_url('products', $data->fk_images[0]->path ?? null) }}",
				name: "{{ $data->name }}",
				categoryId: "{{ $data->category_id }}",
				category: "{{ optional($data->fk_category)->name ?? null }}",
				currency: "VND",
				unitPrice: {{ $data->original_price ?? $data->price }},
				unitSalePrice: {{ $data->price }},
				skuCode: "{{ $data->code }}",
				manufacturer: "{{ optional($data->fk_brand)->name ?? '' }}"
			},
			quantity: $("#quantity").val()
		});
	}
</script>