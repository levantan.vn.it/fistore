@auth('customer')
<script>
	window.digitalData =  window.digitalData || {};
	window.digitalData.user = window.digitalData.user || {};
	window.digitalData.user.email = '{{ Auth::guard('customer')->user()->email }}'
	window.digitalData.user.fullName = '{{ Auth::guard('customer')->user()->name }}';
</script>
@endauth