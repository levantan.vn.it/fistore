<script>
	var digitalData = (window.digitalData = window.digitalData || {});
	digitalData.events = digitalData.events || [];
	digitalData.events.push({
		category: 'Ecommerce',
		name: 'Completed Transaction',
		transaction: {
			orderId: "{{ $order->code }}",
			currency: "VND",
			subtotal: {{ $order->subtotal }},
			shippingCost: 0,
			total: {{ $order->total }},
			contactInfo: { 
				fullName: "{{ $order->name }}",
				address: "{{ $order->address }}, @if($order->ward_id != 0){{ get_ward($order->ward_id)->name }} ,@endif {{ get_district($order->district_id)->name }}, {{ get_province($order->province_id)->name }}", 
				phone: "{{ $order->phone }}",
				email: "{{ $order->email }}"
			},
			lineItems: [
				@foreach($order->fk_details as $row)
				{
					product: 
					{
						id: "{{ $row->product_id }}",
						url: "{{ product_link($row->fk_product->slug) }}",
						imageUrl: "{{ media_url('products', $row->fk_product->thumbnail) }}",
						thumbnailUrl: "{{ media_url('products', $row->fk_product->thumbnail) }}",
						name: "{{ $row->name }}",
						categoryId: "{{ $row->fk_product->category_id }}",
						category: "{{ $row->fk_product->fk_category->name ?? null }}",
						currency: "VND",
						unitPrice: {{ $row->original_price ?? $row->price }},
						unitSalePrice: {{ $row->price }},
						skuCode: "{{ $row->fk_product->code }}",
						manufacturer: "{{ $row->fk_product->fk_brand->name }}"
					},
					quantity: {{ $row->quantity }},
					subtotal: {{ $row->subtotal }}
				},
				@endforeach
			]
		}
	});

</script>