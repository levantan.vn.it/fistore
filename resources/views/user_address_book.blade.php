@extends('layouts.master')

@section('title', 'Sổ địa chỉ')
@section('description', null)
@section('keywords', null)

@section('header')
@stop

@section('main')
	{!! Breadcrumbs::render('user.address_book') !!}

	<div class="row">
		<div class="col-md-3">
			@include('particles.aside_user')
		</div>
		<div class="col-md-9">

			<div class="block-user mt-4 mb-5">
				<div class="block-header">
					<div class="block-title">Sổ địa chỉ</div>
				</div>
				<div class="block-body">
					<div class="row">
						<div class="col-md-6 create-address-border">
							<button class="btn" data-toggle="modal" data-target="#createAddressModal"><i class="fa fa-plus-square-o fa-lg"></i>&emsp;Thêm địa chỉ mới</button>
						</div>
					</div>
					@foreach($data as $item)
					<div class="address-item">
						<div class="item-header">
							<div class="item-name">
								<span class="name">{{ $item->name }}</span>
								@if($item->defaulted)
								&emsp;<span class="default-address"><i class="fa fa-check-circle"></i> Địa chỉ mặc định</span>
								@endif
							</div>
							<div class="item-action">
								<button class="btn btn-edit-address" data-toggle="modal" data-target="#editAddressModal{{ $item->id }}"><i class="fa fa-pencil-square-o"></i> Chỉnh sửa</button>
								@if(!$item->defaulted)
								| <button class="btn btn-edit-address"><i class="fa fa-trash-o"></i> Xóa</button>
								@endif
							</div>
						</div>
						<div class="item-body">
							<div>Địa chỉ: {{ $item->address }}, {{ $item->fk_ward->name }}, {{ $item->fk_district->name }}, {{ $item->fk_province->name }}</div>
							<div>Số điện thoại: {{ $item->phone }}</div>
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>

@stop

@section('footer')
	<div class="modal fade" id="createAddressModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content ">
				<div class="modal-header">
					<h5 class="modal-title">Thêm địa chỉ giao hàng mới</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form action="{{ route('user.address_book.create') }}" method="POST" id="createAddressForm">
						@csrf
						<div class="row row-mx-10">
							<div class="col-md-6">
								<div class="form-group">
									<label for="name">Họ tên <span class="required">*</span></label>
									<input type="text" name="name" class="form-control" placeholder="Nhập họ tên" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="phone">Điện thoại liên hệ <span class="required">*</span></label>
									<input type="text" name="phone" class="form-control" placeholder="Nhập số điện thoại" required>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="province">Tỉnh/Thành phố <span class="required">*</span></label>
									<select name="province" class="custom-select form-control" onchange="get_districts(event)" required>
										<option value="">Chọn Tỉnh/Thành phố</option>
										@foreach(get_provinces() as $province)
											<option value="{{ $province->id }}">{{ $province->name }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="district">Quận/Huyện <span class="required">*</span></label>
									<select name="district" class="custom-select form-control" onchange="get_wards(event)" required>
										<option value="">Chọn Quận/Huyện</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="ward">Phường/Xã <span class="required">*</span></label>
									<select name="ward" class="custom-select form-control" required>
										<option value="">Chọn Phường/Xã</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="address">Địa chỉ <span class="required">*</span></label>
									<textarea name="address" rows="3" class="form-control" placeholder="Vd: 52, đường Trần Hưng Đạo" required></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="mb-3">Để nhận hàng thuận tiện hơn, bạn vui lòng cho <strong>Shopa</strong> biết nơi nhận hàng cụ thể:</label>
							<div class="form-group">
								<label class="custom-control custom-radio">
									<input type="radio" name="address_type" value="0" class="custom-control-input" checked required>
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Nhà riêng/Chung cư</span>
								</label>
							</div>
							<div class="form-group">
								<label class="custom-control custom-radio">
									<input type="radio" name="address_type" value="1" class="custom-control-input">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Cơ quan/Công ty</span>
								</label>
							</div>

							<div class="form-group">
								<label class="custom-control custom-checkbox">
									<input type="checkbox" name="default" class="custom-control-input">
									<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Sử dụng làm địa chỉ mặc định</span>
								</label>
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" form="createAddressForm">Thêm địa chỉ</button>
					<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Đóng</button>
				</div>
			</div>
		</div>
	</div>

	@foreach($data as $item)
		<div class="modal fade" id="editAddressModal{{ $item->id }}" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content ">
					<form action="{{ route('user.address_book.update') }}" method="POST" class="edit-address-form">
						@csrf
						<input type="hidden" name="id" value="{{ $item->id }}">
						<div class="modal-header">
							<h5 class="modal-title">Chỉnh sửa địa chỉ có sẵn</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							<div class="row row-mx-10">
								<div class="col-md-6">
									<div class="form-group">
										<label for="name">Họ tên <span class="required">*</span></label>
										<input type="text" name="name" value="{{ $item->name }}" class="form-control" placeholder="Nhập họ tên" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="phone">Điện thoại liên hệ <span class="required">*</span></label>
										<input type="text" name="phone" value="{{ $item->phone }}" class="form-control" placeholder="Nhập số điện thoại" required>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="province">Tỉnh/Thành phố <span class="required">*</span></label>
										<select name="province" class="custom-select form-control" onchange="get_districts(event)" required>
											<option value="">Chọn Tỉnh/Thành phố</option>
											@foreach(get_provinces() as $province)
												<option value="{{ $province->id }}"{!! $item->province_id == $province->id ? ' selected' : null !!}>{{ $province->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="district">Quận/Huyện <span class="required">*</span></label>
										<select name="district" class="custom-select form-control" onchange="get_wards(event)" required>
											<option value="">Chọn Quận/Huyện</option>
											@foreach(get_districts_by_province($item->province_id) as $district)
												<option value="{{ $district->id }}"{!! $item->district_id == $district->id ? ' selected' : null !!}>{{ $district->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="ward">Phường/Xã <span class="required">*</span></label>
										<select name="ward" class="custom-select form-control">
											<option value="">Chọn Phường/Xã</option>
											@foreach(get_wards_by_district($item->district_id) as $ward)
												<option value="{{ $ward->id }}"{!! $item->ward_id == $ward->id ? ' selected' : null !!}>{{ $ward->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="address">Địa chỉ <span class="required">*</span></label>
										<textarea name="address" rows="3" class="form-control" placeholder="Vd: 52, đường Trần Hưng Đạo" required>{{ $item->address }}</textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="mb-3">Để nhận hàng thuận tiện hơn, bạn vui lòng cho <strong>Shopa</strong> biết nơi nhận hàng cụ thể:</label>
								<div class="form-group">
									<label class="custom-control custom-radio">
										<input type="radio" name="address_type" value="0" class="custom-control-input"{!! $item->address_type == 0 ? ' checked' : null !!} required>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Nhà riêng/Chung cư</span>
									</label>
								</div>
								<div class="form-group">
									<label class="custom-control custom-radio">
										<input type="radio" name="address_type" value="1" class="custom-control-input"{!! $item->address_type == 1 ? ' checked' : null !!}>
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Cơ quan/Công ty</span>
									</label>
								</div>

								@if(!$item->defaulted)
								<div class="form-group">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" name="default" class="custom-control-input">
										<span class="custom-control-indicator"></span>
										<span class="custom-control-description">Sử dụng làm địa chỉ mặc định</span>
									</label>
								</div>
								@else
								<input type="hidden" name="default" value="on">
								@endif
							</div>
						</div>
						<div class="modal-footer">
							<button class="btn btn-primary">Lưu thay đổi</button>
							<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Đóng</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	@endforeach
	
	<script src="{{ asset('js/areas.handler.js') }}"></script>
	<script>
		$("#createAddressForm").validate();
		$(".edit-address-form").validate();
		$("aside.block-user .address_book").addClass("active");
	</script>
@stop