@extends('layouts.clear')

@section('title', 'Hoàn tất đặt hàng')
@section('description', null)
@section('keywords', null)

@section('header')
@stop
@section('pixel_event')
	fbq('track', 'Purchase', {value: '{{ number_format($order->total,0) }}', currency: 'VND'});
@stop

@section('main')

	<div class="row">
		<div class="col-md-10 mx-auto my-5">
			<div class="block-checkout success-order">
				<div class="success-image"></div>
				<div class="success-title">Đặt hàng thành công!</div>
				<div class="success-caption">Cảm ơn bạn đã đặt hàng tại {{ get_config()->name }}.</div>
				@auth('customer')
					<div class="success-point-plus">Bạn đã được cộng <strong>+{{ $extra_point }}</strong> điểm tích lũy.</div>
				@endauth
				<div class="success-order-info">
					<p>Mã đơn hàng: <strong>{{ $order->code }}</strong></p>
					<div class="success-info-title">Thông tin đơn hàng</div>
					<table class="success-order-detail-info table table-hover table-striped">
						<thead>
							<tr>
								<th colspan="2">Sản phẩm</th>
								<th class="text-center">Số lượng</th>
								<th class="text-right">Thành tiền</th>
							</tr>
						</thead>
						<tbody>
							@foreach($order->fk_details as $row)
							<tr>
								<td width="80">
									<img src="{{ media_url('products', $row->fk_product->thumbnail) }}" width="100%">
								</td>
								<td>
									<a href="{{ product_link($row->fk_product->slug) }}" target="_blank" class="product-name">
										{{ $row->name }}
									</a>
									<div class="product-price">{{ number_format($row->price,0,',','.') }} VND</div>
									@if($row->original_price > 0)
										<strike class="product-original-price">{{ number_format($row->original_price,0,',','.') }} VND</strike>
									@endif
								</td>
								<td class="text-center">{{ $row->quantity }}</td>
								<td class="text-right" rowspan="{{ count($row->fk_comboItems) + 1 }}">
									<strong>{{ number_format($row->subtotal,0,',','.') }}</strong>
								</td>
							</tr>
								@foreach($row->fk_comboItems as $comboItem)
									<tr>
										<td width="80" class="text-center">
											<i class="fa fa-level-up fa-lg fa-rotate-90"></i>
										</td>
										<td>
											<a href="{{ product_link($comboItem->fk_product->slug) }}" target="_blank" class="product-name">
												{{ $comboItem->name }}
											</a>
											<div class="product-price">{{ number_format($comboItem->price,0,',','.') }} VND</div>
											@if($comboItem->original_price > 0)
												<strike class="product-original-price">{{ number_format($comboItem->original_price,0,',','.') }} VND</strike>
											@endif
										</td>
										<td class="text-center">{{ $comboItem->quantity}} <span class="text-danger">(x{{ $row->quantity  }})</span></td>
									</tr>
								@endforeach
							@endforeach
							@if($order->discounted > 0)
							<tr>
								<td colspan="3" class="text-right"><b>Giảm giá</b></td>
								<td class="text-right"><strong>-{{ number_format($order->discounted,0,',','.') }}</strong></td>
							</tr>
							@endif
							<tr>
								<td colspan="3" class="text-right"><b>Phí vận chuyển</b></td>
								<td class="text-right"><strong>{{ number_format($order->transport_price,0,',','.') }}</strong></td>
							</tr>
							<tr>
								<td colspan="3" class="text-right"><b>Thanh toán</b></td>
								<td class="text-right"><strong>{{ number_format($order->total,0,',','.') }}</strong></td>
							</tr>
						</tbody>
					</table>

					<hr class="my-5">

					<div class="success-info-title">Thông tin giao hàng</div>
					<table class="success-author-info table table-hover table-striped mb-5">
						<tbody>
							<tr>
								<td width="30%">Họ tên:</td>
								<td>{{ $order->name }}</td>
							</tr>
							<tr>
								<td width="30%">Số điện thoại:</td>
								<td>{{ $order->phone }}</td>
							</tr>
							<tr>
								<td width="30%">Địa chỉ</td>
								<td>{{ $order->address }}, @if($order->ward_id != 0){{ get_ward($order->ward_id)->name }} ,@endif {{ get_district($order->district_id)->name }}, {{ get_province($order->province_id)->name }}</td>
							</tr>
						</tbody>
					</table>
				</div>

				
				<div class="success-caption">Đơn hàng của quý khách sẽ được vận chuyển trong thời gian sớm nhất.<br/>
				Chân thành cảm ơn quý khách đã mua hàng.</div>
			</div>
		</div>
	</div>
@stop

@section('footer')
	@include('api.mtag.completed_transaction')
@stop