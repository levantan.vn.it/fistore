<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    public function fk_permissions()
    {
    	return $this->belongsToMany(Permission::class, 'role_permission_relationships');
    }
}
