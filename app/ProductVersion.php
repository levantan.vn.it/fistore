<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVersion extends Model
{
    protected $table = 'product_versions';

    /**
	 * Lấy danh sách ảnh sản phẩm
	 */
	public function fk_images()
    {
		return $this->hasMany(Media::class, 'target_id')->where('target_type', 'version')->orderBy('index', 'asc');
	}
}
