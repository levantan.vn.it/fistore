<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessToken extends Model
{
	protected $primaryKey = 'id';
    protected $table = 'access_tokens';
    protected $fillable = [
        'access_token'
    ];
    public $timestamps = true;
    
}
