<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'sales';

    public function fk_products()
    {
        return $this->belongsToMany(Product::class, 'sales_product', 'sales_id', 'product_id');
    }
}
