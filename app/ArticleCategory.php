<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $table = 'article_categories';

    public function fk_childs()
    {
    	return $this->hasMany(ArticleCategory::class, 'parent_id');
    }

    public function fk_articles($take = null)
    {
    	return $this->belongsToMany(Article::class, 'article_category_relationships', 'category_id', 'article_id')
    				->whereHidden(false)
    				->whereDraft(false)
    				->whereTrashed(false)
    				->orderBy('reposted_at', 'desc')
    				->orderBy('created_at', 'desc')
    				->take($take)
    				->get();
    }

    public function fk_paginated_articles($take = null)
    {
        return $this->belongsToMany(Article::class, 'article_category_relationships', 'category_id', 'article_id')
                    ->whereHidden(false)
                    ->whereDraft(false)
                    ->whereTrashed(false)
                    ->orderBy('reposted_at', 'desc')
                    ->orderBy('created_at', 'desc')
                    ->paginate($take);
    }
}
