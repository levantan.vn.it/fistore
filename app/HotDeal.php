<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotDeal extends Model
{
    protected $table = 'hot_deals';

    public function fk_product()
    {
    	return $this->hasOne(Product::class, 'id', 'product_id')->whereTrashed(false)->whereDrafted(false)->whereClosed(false)->where('display_price', true);
    }

    public function scopeOutOfStock($query)
    {
        return $query->where('available_qty', '<=', 0);
    }

    public function scopeIsAvail($query)
    {
    	return $query->where('available_qty', '>', 0)->orWhereNull('available_qty');
    }
}
