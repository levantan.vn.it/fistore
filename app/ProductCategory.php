<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';

    public function fk_childs()
    {
    	return $this->hasMany(ProductCategory::class, 'parent_id');
    }
}
