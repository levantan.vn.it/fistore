<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';

    public function fk_permissions()
    {
    	return $this->hasMany(Permission::class, 'module_id', 'id');
    }
}
