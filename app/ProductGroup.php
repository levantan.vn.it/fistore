<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model
{
    protected $table = 'product_groups';

    public function fk_childs()
    {
    	return $this->hasMany(ProductGroup::class, 'parent_id');
    }

    public function fk_products($take = null)
    {
    	return $this->belongsToMany(Product::class, 'product_group_relationships', 'group_id', 'product_id')
    				->whereHidden(false)
    				->whereDrafted(false)
    				->whereTrashed(false)
    				->orderBy('reposted_at', 'desc')
    				->orderBy('created_at', 'desc')
    				->take($take)
    				->get();
    }

    public function fk_paginated_products($take = null)
    {
        return $this->belongsToMany(Product::class, 'product_group_relationships', 'group_id', 'product_id')
                    ->whereHidden(false)
                    ->whereDrafted(false)
                    ->whereTrashed(false)
                    ->orderBy('reposted_at', 'desc')
                    ->orderBy('created_at', 'desc')
                    ->paginate($take);
    }
}
