<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductOrderDetail extends Model
{
    protected $table = 'product_order_details';

    public function fk_product()
    {
    	return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function fk_combo()
    {
    	return $this->hasOne(Combo::class, 'id', 'product_id');
    }

    public function fk_comboItems()
    {
    	return $this->hasMany(ProductOrderDetail::class, 'parent_id');
    }
}
