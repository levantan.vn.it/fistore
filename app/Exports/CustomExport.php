<?php 
namespace App\Exports;

use App\Permission;
use Maatwebsite\Excel\Concerns\FromCollection;

class CustomExport implements FromCollection
{


    public function collection()
    {
        return Permission::all();
    }
}