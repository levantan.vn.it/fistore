<?php

/**
 * Lấy danh mục sản phẩm dạng select
 * @param  integer $current danh mục hiện tại của sản phẩm
 * @return html          	thẻ select chứa danh mục sản phẩm
 */
function get_selectize_product_categories_control($current = null)
{
	$categories = \App\ProductCategory::whereNull('parent_id')->get();
	if(count($categories) > 0) {
		$html = 	'<div class="control-group">';
			$html .=	'<select id="categories" name="category">';
				$html .=	'<option value=""></option>';
				foreach($categories as $item) {
					$html .=	'<option value="'. $item->id .'"'. ($current === $item->id ? ' selected' : null) .'>'. $item->name .'</option>';
					foreach($item->fk_childs as $child) {
						$html .=	'<option value="'. $child->id .'"'. ($current === $child->id ? ' selected' : null) .'>&mdash; '. $child->name .'</option>';
						foreach($child->fk_childs as $grandchild) {
							$html .=	'<option value="'. $grandchild->id .'"'. ($current === $grandchild->id ? ' selected' : null) .'>&mdash;&mdash; '. $grandchild->name .'</option>';
						}
					}
				}
			$html .=	'</select>';
		$html .=	'</div>';
		$html .=	'<script>';
			$html .=	'$("#categories").selectize();';
		$html .=	'</script>';
		return $html;
	}
	return '<div class="alert alert-info">Chưa có danh mục nào được tạo.</div>';
}

/**
 * Lấy thương hiệu sản phẩm dạng select
 * @param  integer $current danh mục hiện tại của sản phẩm
 * @return html          	thẻ select chứa thương hiệu sản phẩm
 */
function get_selectize_product_brands_control($current = null)
{
	$brands = \App\ProductBrand::whereNull('parent_id')->get();
	if(count($brands) > 0) {

		$html = 	'<div class="control-group">';
			$html .=	'<select id="brands" name="brand" required>';
				$html .=	'<option value=""></option>';
				foreach($brands as $item) {
					$html .=	'<option value="'. $item->id .'"'. ($current === $item->id ? ' selected' : null) .'>'. $item->name .'</option>';
					foreach($item->fk_childs as $child) {
						$html .=	'<option value="'. $child->id .'"'. ($current === $child->id ? ' selected' : null) .'>&mdash; '. $child->name .'</option>';
						foreach($child->fk_childs as $grandchild) {
							$html .=	'<option value="'. $grandchild->id .'"'. ($current === $grandchild->id ? ' selected' : null) .'>&mdash;&mdash; '. $grandchild->name .'</option>';
						}
					}
				}
			$html .=	'</select>';
		$html .=	'</div>';
		$html .=	'<script>';
			$html .=	'$("#brands").selectize();';
		$html .=	'</script>';
		return $html;
	}
	return '<div class="alert alert-info">Chưa có thương hiệu nào được tạo.</div>';
}


/**
 * Lấy nhóm sản phẩm dạng checkbox
 * @param  App\Product::class $product 	danh mục hiện tại của sản phẩm
 * @return html          				thẻ checkbox chứa nhóm sản phẩm
 */
function get_checkbox_product_groups_control($product = null)
{
	$groups = \App\ProductGroup::whereNull('parent_id')->get();
	if(count($groups) > 0) {
		$html = null;
		foreach($groups as $item) {
			$html .=	'<div class="form-group">';
				$html .=	'<label class="custom-control custom-checkbox">';
					$html .=	'<input type="checkbox" name="groups[]" value="'. $item->id .'" class="custom-control-input"'. (!is_null($product) && $product->fk_groups->contains($item->id) ? ' checked' : null) .'>';
					$html .=	'<span class="custom-control-indicator"></span>';
					$html .=	'<span class="custom-control-description">'. $item->name .'</span>';
				$html .=	'</label>';
			$html .=	'</div>';
			foreach($item->fk_childs as $child) {
				$html .=	'<div class="form-group">';
					$html .=	'&emsp;';
					$html .=	'<label class="custom-control custom-checkbox">';
						$html .=	'<input type="checkbox" name="groups[]" value="'. $child->id .'" class="custom-control-input"'. (!is_null($product) && $product->fk_groups->contains($child->id) ? ' checked' : null) .'>';
						$html .=	'<span class="custom-control-indicator"></span>';
						$html .=	'<span class="custom-control-description">'. $child->name .'</span>';
					$html .=	'</label>';
				$html .=	'</div>';
				foreach($child->fk_childs as $grandchild) {
					$html .=	'<div class="form-group">';
						$html .=	'&emsp;&emsp;';
						$html .=	'<label class="custom-control custom-checkbox">';
							$html .=	'<input type="checkbox" name="groups[]" value="'. $grandchild->id .'" class="custom-control-input"'. (!is_null($product) && $product->fk_groups->contains($grandchild->id) ? ' checked' : null) .'>';
							$html .=	'<span class="custom-control-indicator"></span>';
							$html .=	'<span class="custom-control-description">'. $grandchild->name .'</span>';
						$html .=	'</label>';
					$html .=	'</div>';
				}
			}
		}
		return $html;
	}
	return '<div class="alert alert-info">Chưa có nhóm nào được tạo.</div>';
}