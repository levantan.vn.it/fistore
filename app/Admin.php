<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
	protected $fillable = [
		'name', 'email', 'password',
	];

	protected $hidden = [
		'password', 'remember_token',
	];

	public function fk_role()
	{
	    return $this->belongsTo(Role::class, 'role_id');
	}

	public function hasPermission(Permission $permission)
	{
	    return !! optional(optional($this->fk_role)->fk_permissions)->contains($permission);
	}

	public function isSuperAdmin()
	{
		if($this->fk_role->key === 'sadmns')
			return true;
		return false;
	}
}
