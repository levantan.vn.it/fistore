<?php

namespace App\Http\Controllers;

use App\SiteConfig;
use App\Contact;
use App\Page;
use App\PageGroup;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getContact()
    {
        $data = SiteConfig::first();
        return view('contact', compact('data'));
    }

    public function subscribeInstockCameback(Request $request)
    {
        $data = new Contact;
        $data->name = '';
        $data->email = $request->email;
        $data->phone = '';
        $data->subject = "$request->code - Liên hệ tôi ngay khi có hàng";
        $data->content = "Liên hệ tôi ngay khi sản phẩm $request->code ($request->name) có hàng!";
        $data->save();
        return back()->with([
            'alert'    => 'success',
            'title'    => 'Hoàn tất',
            'message' => 'Thông tin liên hệ của bạn đã được ghi nhận. Chúng tôi sẽ liên hệ bạn ngay khi có hàng!'
        ]);
    }

    public function sendContact(Request $request)
    {
        $data = new Contact;
        $data->name = $request->name;
        $data->email = $request->email;
        $data->phone = $request->phone;
        $data->subject = $request->subject;
        $data->content = $request->content;
        $data->save();

        return back()->with([
            'alert'    => 'success',
            'title'    => 'Hoàn tất',
            'message' => 'Thông tin liên hệ của bạn đã được gửi đi'
        ]);
    }

    public function showPageListInGroup($group_slug)
    {
        $list = PageGroup::whereSlug($group_slug)->firstOrFail();
        $child_group = PageGroup::where('parent_id', $list->id)->get()->pluck('id');
        $data = Page::whereHidden(false)->whereDrafted(false)->whereTrashed(false)->where(function ($query) use ($list, $child_group) {
            $query->where('group_id', $list->id)->orWhereIn('group_id', $child_group);
        })->orderBy('created_at', 'desc')->paginate(16);
        return view('show_page_in_group', compact('list', 'data'));
    }

    public function showPageDetail($page_slug)
    {
        $data = Page::whereHidden(false)->whereDrafted(false)->whereTrashed(false)->whereSlug($page_slug)->first();

        return view('show_page_content', compact('data'));
    }
}
