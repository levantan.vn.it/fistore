<?php

namespace App\Http\Controllers\Auth\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Validation\ValidationException;
use Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $maxAttempts = 10;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

	protected $redirectTo = '/admin';

    protected function guard()
    {
    	return Auth::guard('admin');
    }

    public function showLoginForm()
    {
    	return view('auth.admin.login');
    }

    public function login(Request $request)
    {
        // dd($request->all());
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            // Kiểm tra nếu tài khoản bị khóa logout và báo lỗi
            if($this->guard()->user()->disabled) {
                $this->guard()->logout();
                throw ValidationException::withMessages([
                    $this->username() => ['Tài khoản đã bị khóa. Vui lòng liên hệ quản trị để mở khóa tài khoản.'],
                ])->status(403);
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Thực hiện đăng xuất tài khoản Quản trị
     * Phương thức chỉ hoạt động đối với tài khoản Quản trị
     */
    public function logout() {
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }
}