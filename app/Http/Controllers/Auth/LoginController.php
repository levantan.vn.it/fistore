<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Socialite;
use Cart;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\Mail\SignUpSuccessMail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = null;

    protected function guard()
    {
        return \Auth::guard('customer');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->redirectTo = url()->previous();

        if (url()->previous() == env('APP_URL') . '/checkout/address')
            $this->redirectTo = '/checkout/address/list';

        $this->middleware('guest:customer')->except('logout');
    }

    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect('/login');
    }

    /**
     * Facebook Redirect
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    /**
     * Facebook Callback
     */
    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->stateless()->user();

        $auth = User::where('social_id', $user->id)->where('provider', $provider)->first();

        if ($auth) {
            Auth::guard('customer')->login($auth);
            if (Cart::count() > 0)
                return redirect('checkout/cart');
            return redirect('');
        } else {
            $check = User::whereNotNull('email')->where('email', $user->email)->first();
            if ($check) {
                Auth::guard('customer')->login($check);
                if (Cart::count() > 0)
                    return redirect('checkout/cart');
                return redirect('');
            } else {
                $data = new User;
                $data->name = $user->name;
                $data->social_id = $user->id;
                $data->provider = $provider;
                $data->email = $user->email;
                $data->avatar = $user->avatar;
                $data->remember_token = $user->token;
                $data->save();
                Auth::guard('customer')->login($data);

                if (!is_null($data->email))
                    \Mail::to($data->email)->send(new SignUpSuccessMail($data->toArray()));
                if (Cart::count() > 0)
                    return redirect('checkout/cart');
                return redirect('');

                //return redirect()->back();
            }
        }
    }
}
