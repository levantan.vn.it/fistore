<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Mail\SignUpSuccessMail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:customer');
    }

    protected function guard()
    {
        return \Auth::guard('customer');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        \Mail::to($data['email'])->send(new SignUpSuccessMail($user));
        // $this->customerUpdate($user);
        return $user;
    }

    public function customerUpdate($data)
    {
        if(empty($data->id)){
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi!',
                'msg' => 'Không tìm thấy khách hàng.'
            ]);
        }
        $id = $data->id;
        $data = User::where('id',$id)->first();
        if(empty($data)){
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi!',
                'msg' => 'Không tìm thấy khách hàng.'
            ]);
        }
        $data_result = [];
        $url = 'services/apexrest/afrfistore/customerUpdate';
        $params = [
            'customerName'   =>  $data->name,
            'customerCode'  =>  $data->id,
            'phoneNumber'   =>  $data->phone,
            'email'   =>  $data->email,
            'gender'   =>  $data->gender == 1?'Male':'Female',
            'dateOfBirth'   =>  $data->date_of_birth

        ];
        $access_totken = $this->getAccessTokenByDB();
        // $access_totken = "00DR0000001znou!AQcAQGgbujba0eAAhQB6lDdGlf_K6GWb2CCoMO71BhBbVImlCZeO1laCYlk2F9_UBMhH.TNmCfG8WSVWi7PuxKpTwOhhMxpV";
        $result = $this->getAPI($url,$params,$access_totken);
        
        if(!empty($result['errorCode']) && $result['errorCode'] == 'INVALID_SESSION_ID'){
            // Save log lỗi access token vào get lại access token mới
            $access_totken = $this->getAccessToken();
            $result = $this->getAPI($url,$params,$access_totken);
        }
        // dd($result);
        if(empty($result['success'])){
            if(!empty($result['errorMessages']['customerCode'])){
                $message = $result['errorMessages']['customerCode'];
            }elseif($result['errorMessages']['customerName']){
                $message = $result['errorMessages']['customerName'];
            }elseif($result['errorMessages']['phoneNumber']){
                $message = $result['errorMessages']['phoneNumber'];
            }elseif($result['errorMessages']['email']){
                $message = $result['errorMessages']['email'];
            }elseif($result['errorMessages']['gender']){
                $message = $result['errorMessages']['gender'];
            }elseif($result['errorMessages']['dateOfBirth']){
                $message = $result['errorMessages']['dateOfBirth'];
            }
            // save log lỗi
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi',
                'msg' => $message
            ]);
        }
        
        $customerId = $result['data'];
        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất!',
            'msg' => 'Cập nhật khách hàng thành công.',
            'data'  =>  $customerId
        ]);
    }
}
