<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\ValidationException;
use Auth;

class CheckoutLoginController extends Controller
{
    use AuthenticatesUsers;

	protected $redirectTo = '/checkout/address/list';

	public function __construct()
    {
        $this->middleware('guest:customer');
    }

    protected function guard()
    {
    	return Auth::guard('customer');
    }

    public function login()
    {
    	return view('checkout_account_login');
    }

    public function postLogin(Request $request)
    {
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            // Kiểm tra nếu tài khoản bị khóa logout và báo lỗi
            if($this->guard()->user()->disabled) {
                $this->guard()->logout();
                throw ValidationException::withMessages([
                    $this->username() => ['Tài khoản đã bị khóa. Vui lòng liên hệ quản trị để mở khóa tài khoản.'],
                ])->status(403);
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function checkEmailValid(Request $request)
    {
        $count = User::whereEmail($request->email)->get()->count();

        if($count > 0)
            return response()->json(true);
        return response()->json('Tài khoản không chính xác.');
    }

    public function checkPasswordValid(Request $request)
    {
        $user = User::whereEmail($request->email)->first();

        if($user->count() > 0) {
            if(\Hash::check($request->password, $user->password))
                return response()->json(true);
            return response()->json('Mật khẩu không chính xác.');
        }
    }
}
