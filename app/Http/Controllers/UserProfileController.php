<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    public function getProfile()
    {
    	$data = Auth::guard('customer')->user();
    	return view('user_profile', compact('data'));
    }

    public function updateProfile(Request $request)
    {
    	$data = User::find(Auth::guard('customer')->user()->id);
    	$data->name = $request->name;
    	$data->email = $request->email;
    	$data->phone = $request->phone;
    	$data->gender = $request->gender ?? null;
        if($request->filled('year') && $request->filled('month') && $request->filled('day'))
    	   $data->date_of_birth = $request->year .'-'. $request->month .'-'. $request->day;
    	$data->save();

    	if($request->filled('change_password')) {
    		if($request->old_password == $request->password)
    		    return back()->with(['password_error' => 'Mật khẩu mới phải khác mật khẩu cũ.']);

    		if(!(\Hash::check($request->old_password, Auth::guard('customer')->user()->password)))
    		    return back()->with(['old_password_error' => 'Mật khẫu cũ không chính xác.']);

    		$data = User::find(Auth::guard('customer')->user()->id);
    		$data->password = bcrypt($request->password);
    		$data->save();
    	}

    	return back()->with([
    		'alert'	=> 'success',
    		'title'	=> 'Hoàn tất',
    		'message'	=> 'Đã cập nhật thông tin tài khoản.'
    	]);
    }

    
}
