<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function index()
    {
        return redirect('/');
    }

    public function redirect1Level($l1)
    {
        switch ($l1) {
            case 'phan-ma-black-rouge-cheek-on.html':
                return redirect('/phan-ma-hong-black-rouge-cheek-on-SP00280.html');
                break;
            case 'son-kem- black-rouge-air-fit-chok-chok-tint.html':
                return redirect('/son-kem-black-rouge-air-fit-chok-chok-tint-SP00012.html');
                break;
            case 'son-kem-air-fit-velvet-tint.html':
                return redirect('/son-kem-black-rouge-air-fit-velvet-tint-SP00008.html');
                break;
            case 'son-kem-air-fit-velvet-tint-new.html':
                return redirect('/son-kem-black-rouge-air-fit-velvet-tint-SP00008.html');
                break;
            case 'kem-duong-trang.html':
                return redirect('/kem-duong-trang-black-rouge-vita-whitening-tone-up-cream-SP00032.html');
                break;
            case 'easy-duo-eyebrown-kit.html':
                return redirect('/bo-kit-chan-may-black-rouge-easy-duo-eyebrow-kit-SP00034.html');
                break;
            case 'up-down-triple-shading.html':
                return redirect('/bang-tao-khoi-va-highlight-black-rouge-up-down-triple-shading-SP00036.html');
                break;
            case 'black-rouge-diva-velvet-lipstick.html':
                return redirect('/son-black-rouge-diva-velvet-lipstick-BRL001001.html');
                break;
            case 'black-rouge-creamy-velvet-crayon.html':
                return redirect('/son-but-chi-black-rouge-creamy-velvet-crayon-SP00052.html');
                break;
            case 'sugar-cream-blusher.html':
                return redirect('/phan-ma-kem-black-rouge-sugar-cream-blusher-BRF008001.html');
                break;
            case 'edge-gel-eyeliner.html':
                return redirect('/gel-ke-mat-black-rouge-edge-gel-eyeliner-SP00056.html');
                break;
            case 'single-eyes.html':
                return redirect('/phan-mat-black-rouge-single-eyes-SP00058.html');
                break;
            case 'highclass-waterproof-gel-eyeliner.html':
                return redirect('/chi-ke-mat-black-rouge-highclass-waterproof-gel-eyeliner-SP00062.html');
                break;
            case 'gel-mat-kim-tuyen-pearllection.html':
                return redirect('/gel-mat-nhu-black-rouge-pearllection-eye-glitter-SP00068.html');
                break;
            case 'pure-clean-lip--eye-remover.html':
                return redirect('/tay-trang-cho-mat-va-moi-black-rouge-pure-clean-lip-eye-remover-BRD001001.html');
                break;
            case 'black-rouge-diva-black-lipstick.html':
                return redirect('/son-black-rouge-diva-black-lipstick-BRL002001.html');
                break;
            case 'one-touch-dual-lip-lacquer-plumping.html':
                return redirect('/son-2-dau-black-rouge-one-touch-dual-lip-lacquer-plumping-SP00022.html');
                break;
            case 'eye-step-shadow.html':
                return redirect('/phan-mat-3-mau-black-rouge-eye-step-shadow-SP00028.html');
                break;
            case 'the-saem-water-candy-tint.html':
                return redirect('/son-tint-the-saem-water-candy-tint-SAL001WAT.html');
                break;
            case 'nuoc-tay-trang-the-saem-healing-tea-garden-cleansing-water.html':
                return redirect('/nuoc-tay-trang-the-saem-healing-tea-garden-cleansing-water-JU-SK003GREEN.html');
                break;
            case 'sua-rua-mat-healing-tea-garden-cleansing-foam.html':
                return redirect('/sua-rua-mat-the-saem-healing-tea-garden-cleansing-foam-JU-SK002RTE.html');
                break;
            case 'mat-na-ngu-the-saem-fruit-punch.html':
                return redirect('/mat-na-ngu-the-saem-fruit-punch-sleeping-pack-JU-SM002APPLE.html');
                break;
            case 'xit-khoang-fresh-bamboo-essential-water-mist.html':
                return redirect('/xit-khoang-the-saem-fresh-bamboo-essential-water-mist-JU-SK001WATER.html');
                break;
            case 'kem-chong-nang-the-saem-eco-earth-power-green.html':
                return redirect('/kem-chong-nang-cho-da-nhay-cam-the-saem-eco-earth-power-green-JU-SK001GREEN.html');
                break;
            case 'kem-chong-nang-eco-earth-power-pink-sun-cream.html':
                return redirect('/kem-chong-nang-the-saem-eco-earth-power-pink-sun-cream-JU-SK001PINK.html');
                break;
            case 'the-saem-harakeke-eye-serum.html':
                return redirect('/tinh-chat-the-saem-urban-eco-harakeke-eye-serum-SAS005001.html');
                break;
            case 'the-saem-harakeke-emulsion-ex.html':
                return redirect('/sua-duong-the-saem-urban-eco-harakeke-emulsion-ex-SAS004001.html');
                break;
            case 'the-saem-harakeke-ampoule.html':
                return redirect('/tinh-chat-the-saem-urban-eco-harakeke-ampoule-SAS003001.html');
                break;
            case 'the-saem-harakeke-essence.html':
                return redirect('/tinh-chat-the-saem-urban-eco-harakeke-essence-SAS001001.html');
                break;
            case 'mat-na-the-saem-daily-super-seed.html':
                return redirect('/mat-na-the-saem-daily-super-seed-mask-sheet-JU-SM001bas.html');
                break;
            case 'son-xop-the-saem-mousse-candy-tint.html':
                return redirect('/son-tint-the-saem-mousse-candy-tint-SAL002003.html');
                break;
            case 'the-saem-harakeke-toner.html':
                return redirect('/nuoc-can-bang-da-the-saem-urban-eco-harakeke-fresh-toner-JU-SK004FRESH.html');
                break;
            case 'kem-chong-nang-mr-hero-perfect-sun-cream-mom-kids-spf50-pa.html':
                return redirect('/kem-chong-nang-mrhero-mom-kids-perfect-sun-cream-spf50-pa-SP00211.html');
                break;
            case 'son-kem-macqueen-glam-tint-lip-lips.html':
                return redirect('/son-kem-macqueen-glam-tint-lip-lips-SP00118.html');
                break;
            case 'mut-macqueen-blending-puff.html':
                return redirect('/mut-macqueen-blending-puff-SP00119.html');
                break;
            case 'macqueen-mineral-cc-cushion-cover-plus.html':
                return redirect('/phan-nuoc-macqueen-mineral-cc-cushion-cover-plus-SP00120.html');
                break;
            case 'tinh-dau-duong-moi-macqueen-volume-fix-essential-lip-oil.html':
                return redirect('/tinh-dau-duong-moi-macqueen-volume-fix-essential-lip-oil-SP00121.html');
                break;
            case 'son-but-chi-macqueen-retro-velvet-lip-pencil.html':
                return redirect('/son-but-chi-macqueen-retro-velvet-lip-pencil-SP00122.html');
                break;
            case 'macqueen-ruby-cell-puff.html':
                return redirect('/macqueen-ruby-cell-puff-SP00123.html');
                break;
            case 'mascara-macqueen-extreme-potencara-volume-poten.html':
                return redirect('/mascara-macqueen-extreme-potencara-volume-poten-SP00124.html');
                break;
            case 'mascara-macqueen-extreme-potencara-skinny-poten.html':
                return redirect('/mascara-macqueen-extreme-potencara-skinny-poten-SP00125.html');
                break;
            case 'ke-mat-nuoc-macqueen-waterproof-pen-eyeliner.html':
                return redirect('/ke-mat-nuoc-macqueen-waterproof-pen-eyeliner-SP00126.html');
                break;
            case 'son-tint-macqueen-cushion-tint.html':
                return redirect('/son-macqueen-cushion-tint-SP00134.html');
                break;
            case 'son-tin-macqueen-airfit-tint.html':
                return redirect('/son-macqueen-airfit-tint-MCL001001.html');
                break;
            case 'phan-mat-macqueen-new-york-eye-holic-stick-shadow.html':
                return redirect('/phan-mat-macqueen-new-york-eye-holic-stick-shadow-BL-MQF001001.html');
                break;
            case 'but-ke-chan-may-macqueen-my-gyeol-fit-tint-brown.html':
                return redirect('/but-ke-chan-may-macqueen-my-gyeol-fit-tint-brown-SP00129.html');
                break;
            case 'kem-che-khuyet-diem-macqueen-renewal-tea-tree-concealler-dual-veil.html':
                return redirect('/kem-che-khuyet-diem-macqueen-renewal-tea-tree-concealler-dual-veil-SP00127.html');
                break;
            case 'bong-tri-mun-cosrx-one-step-pimple-clear-pad.html':
                return redirect('/mieng-tay-da-chet-cosrx-one-step-pimple-clear-pad-CRXG002001.html');
                break;
            case 'cosrx-aha-bha-clarifying-treatment-toner.html':
                return redirect('/cosrx-aha-bha-clarifying-treatment-toner-CRXS002001.html');
                break;
            case 'cosrx-natural-bha-skin-returning-a-sol.html':
                return redirect('/cosrx-natural-bha-skin-returning-a-sol-CRXS010001.html');
                break;
            case 'cosrx-natural-bha-skin-returning-a-sol.html':
                return redirect('/');
                break;
            case 'cosrx-mela-14-white-ampule.html':
                return redirect('/cosrx-mela-14-white-ampule-CRXF006001.html');
                break;
            case 'cosrx-propolis-light-ampule.html':
                return redirect('/cosrx-propolis-light-ampule-CRXS005001.html');
                break;
            case 'cosrx-aha7-whitehead-power-liquid.html':
                return redirect('/cosrx-aha7-whitehead-power-liquid-CRXS011001.html');
                break;
            case 'cosrx-centella-blemish-ampule.html':
                return redirect('/cosrx-centella-blemish-ampule-CRXS004001.html');
                break;
            case 'cosrx-centella-water-alcohol-free-toner.html':
                return redirect('/cosrx-centella-water-alcohol-free-toner-CRXS008001.html');
                break;
            case 'kem-nen-aritaum-full-cover-foundation-spf-50-pa.html':
                return redirect('/kem-nen-aritaum-full-cover-foundation-spf-50-pa-JU-AF00501.html');
                break;
            case 'tay-te-bao-chet-moi-aritaum-ginger-sugar-lip-scrub.html':
                return redirect('/tay-te-bao-chet-moi-aritaum-ginger-sugar-lip-scrub-JU-AL001SCR.html');
                break;
            case 'phan-tao-khoi-aritaum-magic-contouring-cream.html':
                return redirect('/phan-tao-khoi-aritaum-magic-contouring-cream-JU-AF00601.html');
                break;
            case 'phan-tao-khoi-aritaum-magic-contouring-powder.html':
                return redirect('/phan-tao-khoi-aritaum-magic-contouring-powder-JU-AF00701.html');
                break;
            case 'chi-ke-may-aritaum-matte-formula.html':
                return redirect('/chi-ke-may-aritaum-matte-formula-JU-AE00101.html');
                break;
            case 'mat-na-aritaum-fresh-power-essence-mask.html':
                return redirect('/mat-na-aritaum-fresh-power-essence-mask-JU-AM001ALOE.html');
                break;
            case 'xit-khoang-aritaum-face-mist-shiro-maro.html':
                return redirect('/xit-khoang-aritaum-face-mist-shiro-maro-JU-AF004COL.html');
                break;
            case 'kem-nen-aritaum-all-day-last-foundation-spf30-pa.html':
                return redirect('/kem-nen-aritaum-all-day-last-foundation-spf30-pa-JU-AF00101.html');
                break;
            case 'kem-lot-aritaum-all-day-lasting-primer-spf-44-pa.html':
                return redirect('/kem-lot-aritaum-all-day-lasting-primer-spf-44-pa-JU-AF002PRI.html');
                break;
            case 'che-khuyet-diem-aritaum-all-day-tip-concealer.html':
                return redirect('/che-khuyet-diem-aritaum-all-day-tip-concealer-JU-AF00301.html');
                break;
            case 'mat-na-mediheal-i-p-i-lightmax-ampoule-mask.html':
                return redirect('/mat-na-mediheal-ipi-lightmax-ampoule-mask-MDM001IPI.html');
                break;
            case 'mat-na-n-m-f-aquaring-ampoule-mask.html':
                return redirect('/mat-na-nmf-aquaring-ampoule-mask-JU-MDM001NMF.html');
                break;
            case 'mat-na-mediheal-teatree-care-solution-essential-mask-ex.html':
                return redirect('/mat-na-mediheal-teatree-care-solution-essential-mask-ex-JU-MDM001TEA.html');
                break;
            case 'mat-na-than-hoat-tinh-mediheal-h-d-p-pore-stamping-charcoal-mineral-mask.html':
                return redirect('/mat-na-than-hoat-tinh-mediheal-hdp-pore-stamping-charcoal-mineral-mask-MDM004HDP.html');
                break;
            case 'mat-na-than-hoat-tinh-mediheal-w-h-p-white-hydrating-charcoal-mineral-mask.html':
                return redirect('/mat-na-mediheal-whp-white-hydrating-black-mask-ex-SP00173.html');
                break;
            case 'mat-na-2-buoc-mediheal-wrinkle-minus-smart-tox-mask.html':
                return redirect('/mat-na-2-buoc-mediheal-wrinkle-minus-smart-tox-mask-SP00172.html');
                break;
            case 'mat-na-2-buoc-mediheal-aqua-double-smart-filler-mask.html':
                return redirect('/mat-na-2-buoc-mediheal-aqua-double-smart-filler-mask-SP00170.html');
                break;
            case 'mat-na-2-buoc-mediheal-pore-down-smart-tox-mask.html':
                return redirect('/mat-na-2-buoc-mediheal-pore-down-smart-tox-mask-SP00169.html');
                break;
            case 'mat-na-mediheal-placenta-revital-essential-mask.html':
                return redirect('/mat-na-mediheal-placenta-revital-essential-mask-JU-MDM001REV.html');
                break;
            case 'mat-na-mediheal-vita-lightbeam-essential-mask-ex.html':
                return redirect('/mat-na-mediheal-vita-lightbeam-essential-mask-ex-JU-MDM001LIGHT.html');
                break;
            case 'mat-na-mediheal-collagen-impact-essential-mask.html':
                return redirect('/mat-na-mediheal-collagen-impact-essential-mask-JU-MDM001COL.html');
                break;
            case 'son-tint-lau-troi-pony-effect-deep-pure-lip-tint.html':
                return redirect('/mat-na-mediheal-collagen-impact-essential-mask-JU-MDM001COL.html');
                break;
            case 'stay-fit-matte-lip-colour.html':
                return redirect('/son-pony-effect-stay-fit-matte-lip-colour-SP00176.html');
                break;
            case 'pony-effect-outfit-velvet-lipstick.html':
                return redirect('/son-pony-effect-outfit-velvet-lipstick-SP00175.html');
                break;
            case 'pony-effect-everlasting-cushion.html':
                return redirect('/phan-nuoc-pony-effect-everlasting-cushion-SP00247.html');
                break;
            case 'bo-phan-mat-8-mau-shine-easy-glam-eyeshadow-palette.html':
                return redirect('/bo-phan-mat-8-mau-shine-easy-glam-eyeshadow-palette-SP00240.html');
                break;
            case 'conceptual-eyes-quad.html':
                return redirect('/phan-mat-pony-effect-conceptual-eyes-quad-SP00241.html');
                break;
            case 'kem-nen-pony-effect-waterproof-cushion-foundation-stick.html':
                return redirect('/kem-nen-pony-effect-waterproof-cushion-foundation-stick-SP00243.html');
                break;
            case 'son-kem-etude-house-liquid-lip.html':
                return redirect('/son-kem-etude-house-color-in-liquid-lips-mousse-EDL004RD302.html');
                break;
            case 'son-xam-etude-house-dear-darling-water-gel-tint-packed.html':
                return redirect('/son-xam-etude-house-dear-darling-water-gel-tint-packed-SP00180.html');
                break;
            case 'son-matte-chic-lip-lacquer-etude-house.html':
                return redirect('/son-etude-house-matte-chic-lip-lacquer-EDC005OR201.html');
                break;
            case 'mat-na-duong-trang-anti-dust-whitening-mask.html':
                return redirect('/mat-na-duong-trang-jayjun-anti-dust-whitening-mask.html');
                break;
            case 'mat-na-sang-da-real-water-brightening-black-mask.html':
                return redirect('/mat-na-sang-da-jayjun-real-water-brightening-black-mask.html');
                break;
            case 'Mat-na-Intensive-Shining-Mask.html':
                return redirect('/mat-na-jayjun-intensive-shining-mask-JJM003001.html');
                break;
            case 'mat-na-skin-fit-mask.html':
                return redirect('/mat-na-jayjun-skin-fit-mask.html');
                break;
            case 'mat-na-rose-blossom-mask.html':
                return redirect('/mat-na-jayjun-rose-blossom-mask-JJM005001.html');
                break;
            case 'mat-na-purple-fragrance-mask.html':
                return redirect('/mat-na-jayjun-purple-fragrance-mask.html');
                break;
            case 'mat-na-gold-snow-black-mask.html':
                return redirect('/mat-na-jayjun-gold-snow-black-mask-JJM001001.html');
                break;
            case 'mat-nan-anti-dust-therapy-mask.html':
                return redirect('/mat-na-jayjun-anti-dust-therapy-mask-JJM006001.html');
                break;
            case 'peripera-tay-trang-moi-ink-remover-2.html':
                return redirect('/tay-trang-moi-peripera-ink-remover-2-PPL003REM.html');
                break;
            case 'peripera-son-vivid-tint-water.html':
                return redirect('/son-peripera-vivid-tint-water-PPL004002.html');
                break;
            case 'peripera-cushion-kem-chong-nang-pure-milk.html':
                return redirect('/kem-chong-nang-dang-cushion-peripera-pure-milk-PPF001PURE.html');
                break;
            case 'peripera-son-ink-velvet.html':
                return redirect('/son-peripera-ink-velvet-SP00197.html');
                break;
            case 'peripera-mascara-nhieu-mau-ink-color.html':
                return redirect('/mascara-peripera-ink-color-SP00196.html');
                break;
            case 'peripera-cushion-trang-diem-dieu-chinh-mau-da-peripera-ink-lasting.html':
                return redirect('/phan-nuoc-dieu-chinh-mau-da-peripera-ink-lasting-PPF002PINK.html');
                break;
            case 'peripera-milk-moist-pact.html':
                return redirect('/phan-tuoi-peripera-milk-moist-pact-PPF003PIN.html');
                break;
            case 'huxley-secret-of-sahara-toner-extract-it.html':
                return redirect('/nuoc-can-bang-huxley-secret-of-sahara-toner-extract-it-BL-HUK001EXT.html');
                break;
            case 'huxley-secret-of-sahara-oil-essence-essence-like-oil-like.html':
                return redirect('/tinh-chat-chong-lao-hoa-huxley-secret-of-sahara-oil-essence-essence-like-oil-like-BL-HUK002OIL.html');
                break;
            case 'kem-duong-am-huxley-secret-of-sahara-cream-more-than-moist.html':
                return redirect('/kem-duong-am-huxley-secret-of-sahara-cream-more-than-moist-SP00205.html');
                break;
            case 'kem-chong-lao-hoa-huxley-secret-of-sahara-cream-anti-gravity.html':
                return redirect('/kem-chong-lao-hoa-huxley-secret-of-sahara-cream-anti-gravity-BL-HUK002ANTI.html');
                break;
            case 'kem-duong-am-huxley-secret-of-sahara-cream-fresh-and-more.html':
                return redirect('/kem-duong-am-chiet-xuat-xuong-rong-huxley-secret-of-sahara-cream-fresh-and-more-SP00203.html');
                break;
            case 'tay-trang-huxley-secret-of-sahara-cleansing-water-be-clean-be-moist.html':
                return redirect('/tay-trang-huxley-secret-of-sahara-cleansing-water-be-clean-be-moist-BL-HUK003MOIST.html');
                break;
            case 'tinh-chat-duong-am-huxley-secret-of-sahara-essence-grab-water.html':
                return redirect('/tinh-chat-duong-am-huxley-secret-of-sahara-essence-grab-water-SP00201.html');
                break;
            case 'kem-nen-suiskin-the-classic-blemish-balm.html':
                return redirect('/kem-nen-suiskin-the-classic-blemish-balm-BL-SSF001CLAS.html');
                break;
            case 'serum-suiskin-aha-serum-8.html':
                return redirect('/serum-suiskin-aha-serum-8-BL-SSK001AHA.html');
                break;
            case 'suiskin-a-c-control-toner.html':
                return redirect('/nuoc-can-bang-suiskin-ac-control-toner-BL-SSK002TONER.html');
                break;
            case 'gel-duong-suiskin-a-c-control-gel-lotion.html':
                return redirect('/gel-duong-suiskin-ac-control-gel-lotion-BL-SSK003LOT.html');
                break;
            case 'sua-rua-mat-suiskin-vitamin-powder-wash.html':
                return redirect('/sua-rua-mat-suiskin-vitamin-powder-wash-SP00211.html');
                break;
            case 'mat-na-suiskin-shaking-modeling-mask-blue.html':
                return redirect('/mat-na-suiskin-shaking-modeling-mask-blue-BL-SSM001SMS.html');
                break;
            case 'kem-duong-ca-hoi-suiskin-salmon-dn-dense-cream.html':
                return redirect('/kem-duong-ca-hoi-suiskin-salmon-dn-dense-cream-SP00208.html');
                break;
            case 'tay-trang-nooni-deep-cleanse.html':
                return redirect('/tay-trang-dang-bot-nooni-deep-cleanse-snowflake-mousse-cleanser-NNC002001.html');
                break;
            case 'deep-water-therapy-dual-moisturizing-toner.html':
                return redirect('/nuoc-can-bang-nooni-deep-water-therapy-dual-moisturizing-toner-NNC001001.html');
                break;
            case 'kem-duong-trang-nooni-radiance-tone-up-cream.html':
                return redirect('/kem-duong-trang-nooni-radiance-tone-up-cream-SP00222.html');
                break;
            case 'skin-pack-sheet.html':
                return redirect('/nooni-skin-pack-sheet-NNE002001.html');
                break;
            case 'marshamallow-whip-maker.html':
                return redirect('/dung-cu-tao-bot-nooni-marshamallow-whip-maker-NNO001001.html');
                break;
            case 'bonvivant-anti-gravity-contour-massage-stick.html':
                return redirect('/kem-duong-bonvivant-anti-gravity-contour-massage-stick-BVO001001.html');
                break;
            case 'anti-gravity-contour-v-massager.html':
                return redirect('/dung-cu-massager-nang-co-mat-bonvivant-anti-gravity-contour-v-massager-SP00230.html');
                break;
            case 'bonmask-hydro-nude-gel-mask.html':
                return redirect('/mat-na-bonvivant-bonmask-hydro-nude-gel-mask-SP00229.html');
                break;
            case 'botanical-mellow-clay-mask.html':
                return redirect('/mat-na-dat-set-bonvivant-botanical-mellow-clay-mask-BVM004002.html');
                break;
            case 'water-lock-sleeping-pack.html':
                return redirect('/mat-na-ngu-bonvivant-water-lock-sleeping-pack-BVE002001.html');
                break;
            case 'kem-chong-nang-innisfree-daily-uv-protection-essence.html':
                return redirect('/kem-chong-nang-innisfree-daily-uv-protection-essence-IFE002SHE.html');
                break;
            case 'kem-chong-nang-perfect-uv-protection-cream.html':
                return redirect('/kem-chong-nang-innisfree-perfect-uv-protection-cream.html');
                break;
            case 'laneige-son-2-mau-two-tone-lip-bar-1.html':
                return redirect('/son-2-mau-laneige-two-tone-lip-bar-LNL001003.html');
                break;
            case 'laneige-kem-duong-am-sau-water-bank-gel-cream-ex.html':
                return redirect('/kem-duong-am-laneige-water-bank-gel-cream-ex-LNS001WBG.html');
                break;
            case 'mat-na-ngu-dang-vien-laneige-white-plus-review-capsule-sleeping-mask.html':
                return redirect('/mat-na-ngu-dang-vien-laneige-white-plus-renew-capsule-sleeping-mask-LNM002WHI.html');
                break;
            case 'laneige-mat-na-ngu-cho-mat-eye-sleeping-mask.html':
                return redirect('/mat-na-ngu-cho-mat-laneige-special-care-eye-sleeping-mask.html');
                break;
            case 'mieng-chuom-nong-apieu-heat-on-hot-pack.html':
                return redirect('/mieng-chuom-nong-apieu-heat-on-hot-pack-APM004HHP.html');
                break;
            case 'mat-na-ngu-a-pieu-good-night-water-sleeping-mask.html':
                return redirect('/mat-na-ngu-apieu-good-night-water-sleeping-mask-SP00113.html');
                break;
            case 'son-duong-lam-day-moi-apieu-wasabiya-lip-plumer.html':
                return redirect('/son-duong-lam-day-moi-apieu-wasabiya-lip-plumer-SP00234.html');
                break;
            case 'mamonde-sua-rua-mat-hoa-sen-dang-bot.html':
                return redirect('/sua-rua-mat-mamonde-lotus-micro-cleansing-foam-MDC001001.html');
                break;
            case 'mamonde-nuoc-toner-hoa-hong-250ml.html':
                return redirect('/nuoc-can-bang-tinh-chat-hoa-hong-mamonde-rose-water-toner-MDS001001.html');
                break;
            case 'mamonde-son-new-creamy-tint-color-balm.html':
                return redirect('/son-but-chi-mamonde-creamy-tint-color-balm-intense-SP00227.html');
                break;
            case 'mat-na.html':
                return redirect('/phan-loai/mat-na');
                break;
            case 'san-pham-moi.html':
                return redirect('/san-pham/hang-moi-ve');
                break;
            case 'hang-ban-chay.html':
                return redirect('/san-pham/hang-ban-chay');
                break;
            case 'xu-huong.html':
                return redirect('/bai-viet-su-kien');
                break;
            case 'black-rouge.html':
                return redirect('/thuong-hieu/black-rouge');
                break;
            case '3ce.html':
                return redirect('/thuong-hieu/3ce');
                break;
            case 'i-m-meme.html':
                return redirect('/thuong-hieu/im-meme');
                break;
            case 'the-saem.html':
                return redirect('/thuong-hieu/the-saem');
                break;
            case 'macqueen.html':
                return redirect('/thuong-hieu/macqueen');
                break;
            case 'cosrx.html':
                return redirect('/thuong-hieu/cosrx');
                break;
            case 'aritaum.html':
                return redirect('/thuong-hieu/aritaum');
                break;
            case 'mediheal.html':
                return redirect('/thuong-hieu/mediheal');
                break;
            case 'pony-effect.html':
                return redirect('/thuong-hieu/pony-effect');
                break;
            case 'etude-house.html':
                return redirect('/thuong-hieu/etude-house');
                break;
            case 'jayjun.html':
                return redirect('/thuong-hieu/jayjun');
                break;
            case 'peripera.html':
                return redirect('/thuong-hieu/peripera');
                break;
            case 'huxley.html':
                return redirect('/thuong-hieu/huxley');
                break;
            case 'nooni.html':
                return redirect('/thuong-hieu/nooni');
                break;
            case 'suiskin.html':
                return redirect('/thuong-hieu/suiskin');
                break;
            case 'bonvivant.html':
                return redirect('/thuong-hieu/bonvivant');
                break;
            case 'innisfree.html':
                return redirect('/thuong-hieu/innisfree');
                break;
            case 'laneige.html':
                return redirect('/thuong-hieu/laneige');
                break;
            case 'a-pieu.html':
                return redirect('/thuong-hieu/apieu');
                break;
            case 'mamonde.html':
                return redirect('/thuong-hieu/mamonde');
                break;
            case 'combo-khuyen-mai.html':
                return redirect('/khuyen-mai/combo');
                break;
            case 'khuyen-mai.html':
                return redirect('/san-pham/khuyen-mai');
                break;
            case 'phan-ma-hong-3ce-take-a-layer-multi-pot.html':
                return redirect('/phan-ma-hong-3ce-take-a-layer-multi-pot-SP00066.html');
                break;
            case '3ce-love-glossy-lip-stick.html':
                return redirect('/son-3ce-love-glossy-lipstick-SP00064.html');
                break;
            case 'che-khuyet-diem-3ce-full-cover-concealer.html':
                return redirect('/che-khuyet-diem-3ce-full-cover-concealer-SP00100.html');
                break;
            case 'son-kem-li-3ce-velvet-lip-tint-new.html':
                return redirect('/son-kem-li-3ce-velvet-lip-tint-SP00082.html');
                break;
            case 'son-kem-3ce-velvet-lip-tint-new.html':
                return redirect('/son-kem-li-3ce-velvet-lip-tint-SP00082.html');
                break;
            case 'kem-trang-diem-3ce-bb-cream-back-to-baby.html':
                return redirect('/kem-trang-diem-3ce-bb-cream-back-to-baby-SP00288.html');
                break;
            case '3ce-skin-tone-comtrol-primer.html':
                return redirect('/kem-lot-3ce-skin-tone-control-primer-SP00030.html');
                break;
            case '3ce-natural-finish-loose-powder.html':
                return redirect('/phan-phu-3ce-natural-finish-loose-powder-SP00099.html');
                break;
            case '3ce-fitting-cushion-foundation.html':
                return redirect('/phan-nen-3ce-fitting-cushion-foundation-CEF021001.html');
                break;
            case 'son-mong-tay-take-a-layer-layering-nail-lacquer.html':
                return redirect('/son-mong-tay-3ce-take-a-layer-layering-nail-lacquer-SP00063.html');
                break;
            case 'thuoc-nhuom-toc-3ce-treatment-hair-tint.html':
                return redirect('/thuoc-nhuom-toc-3ce-treatment-hair-tint-SP00069.html');
                break;
            case '3ce-maison-kitsune-velvet-lip-tint.html':
                return redirect('/son-3ce-maison-kitsune-velvet-lip-tint-SP00071.html');
                break;
            case '3ce-drawing-lip-pen.html':
                return redirect('/son-moi-dang-chi-3ce-drawing-lip-pen-SP00072.html');
                break;
            case 'son-duong-moi-3ce-heart-pot-lip.html':
                return redirect('/son-duong-moi-3ce-heart-pot-lip-SP00074.html');
                break;
            case '3ce-blush-cushion.html':
                return redirect('/phan-ma-3ce-blush-cushion-SP00081.html');
                break;
            case 'son-kem-li-3ce-velvet-lip-tint.html':
                return redirect('/son-kem-li-3ce-velvet-lip-tint-SP00082.html');
                break;
            case '3ce-maison-kitsune-multi-color-palette.html':
                return redirect('/phan-mat-3ce-maison-kitsune-multi-color-palette-SP00085.html');
                break;
            case 'Son-mong-tay-3ce-red-recipe-long-lasting-nail-lacquer.html':
                return redirect('/son-mong-tay-3ce-red-recipe-long-lasting-nail-lacquer-SP00087.html');
                break;
            case '3ce-maison-kitsune-eye-switch.html':
                return redirect('/3ce-maison-kitsune-eye-switch-SP00091.html');
                break;
            case '3ce-maison-kitsune-velvet-lip-crayon.html':
                return redirect('/son-3ce-maison-kitsune-velvet-lip-crayon-SP00093.html');
                break;
            case 'bang-mat-9-mau-3ce-mood-on-and-on-recipe-multi-eye-color-palette.html':
                return redirect('/bang-mat-9-mau-3ce-mood-on-and-on-recipe-multi-eye-color-palette-SP00101.html');
                break;
            case '3ce-matte-lip-color.html':
                return redirect('/son-li-3ce-matte-lip-color-CEC007909B.html');
                break;
            case '3ce-love-cheek-maker.html':
                return redirect('/phan-ma-hong-3ce-love-cheek-maker-SP00107.html');
                break;
            case '3ce-love-duo-shadow.html':
                return redirect('/phan-ma-hong-3ce-love-duo-shadow-CEE017PIE.html');
                break;
            case '3ce-matte-fit-foundation.html':
                return redirect('/kem-nen-3ce-matte-fit-foundation-CEF009001.html');
                break;
            case '3ce-boosting-sun-mist.html':
                return redirect('/kem-chong-nang-dang-xit-3ce-boosting-sun-mist-CEF015SUN.html');
                break;
            case '3ce-fixer-mascara.html':
                return redirect('/3ce-fixer-mascara-SP00093.html');
                break;
            case '3ce-mood-recipe-long-lasting-nail-lacquer.html':
                return redirect('/3ce-mood-recipe-long-lasting-nail-lacquer-SP00068.html');
                break;
            case '3ce-mood-recipe-matte-lip-color.html':
                return redirect('/son-3ce-mood-recipe-matte-lip-color-SP00061.html');
                break;
            case '3ce-red-recipe-face-blush.html':
                return redirect('/phan-ma-3ce-red-recipe-face-blush-SP00043.html');
                break;
            case '3ce-maison-kitsune-square-mini-hand-mirror.html':
                return redirect('/guong-3ce-maison-kitsune-square-mini-hand-mirror-SP00042.html');
                break;
            case 'guong-3ce-mood-on-and-on.html':
                return redirect('/guong-3ce-mood-recipe-square-hand-mirror-CEO004ROBB.html');
                break;
            case '3ce-eye-brightener.html':
                return redirect('/che-khuyet-diem-mat-3ce-eye-brightener-SP00026.html');
                break;
            case '3ce-slim-fit-powder-pact.html':
                return redirect('/phan-nen-3ce-slim-fit-powder-pact-SP00023.html');
                break;
            case 'phan-ma-3ce-mood-recipe-face-blush.html':
                return redirect('/phan-ma-3ce-mood-on-and-on-face-blush-SP00095.html');
                break;
            case '3ce-soft-liplacquer.html':
                return redirect('/son-3ce-soft-lip-lacquer-SP00057.html');
                break;
            case '3ce-mood-recipe-triple-shadow.html':
                return redirect('/3ce-mood-recipe-triple-shadow-SP00021.html');
                break;
            case '3ce-eyebrow-mascara.html':
                return redirect('/3ce-eyebrow-mascara-SP00019.html');
                break;
            case '3ce-water-proof-cream-brow-brow-mascara.html':
                return redirect('/3ce-water-proof-cream-brow-brow-mascara-SP00049.html');
                break;
            case 'son-li-3ce-mood-on-and-on.html':
                return redirect('/son-li-3ce-mood-on-and-on-lipstick-SP00038.html');
                break;
            case '3ce-super-slim-pen-eyeliner.html':
                return redirect('/chi-ke-mat-3ce-super-slim-pen-eyeliner-CEE010BUR.html');
                break;
            case '3ce-red-recipe-lip-color.html':
                return redirect('/son-3ce-red-recipe-lip-color-SP00077.html');
                break;
            case '3ce-liquid-lip-color.html':
                return redirect('/son-3ce-liquid-lip-color-SP00053.html');
                break;
            case '3ce-back-to-baby-make-up-base.html':
                return redirect('/kem-lot-dieu-chinh-mau-da-3ce-back-to-baby-make-up-base-SP00106.html');
                break;
            case '3ce-glow-jam-stick.html':
                return redirect('/son-3ce-glow-jam-stick-SP00010.html');
                break;
            case '3ce-duo-countour-stick.html':
                return redirect('/3ce-duo-contour-stick-SP00088.html');
                break;
            case '3ce-love-velvet-lipstick.html':
                return redirect('/son-3ce-love-velvet-lipstick-SP00053.html');
                break;
            case 'son-3ce-take-a-layer-tinted-water-tint.html':
                return redirect('/son-3ce-take-a-layer-tinted-water-tint-SP00061.html');
                break;
            case 'i-m-meme-i-m-tic-toc-lipstick-satin-m.html':
                return redirect('/son-trai-tim-im-meme-tic-toc-lipstick-satin-MMC008001.html');
                break;
            case 'son-im-meme-im-tic-toc-tint-lip-velvet.html':
                return redirect('/son-bam-im-meme-im-tic-toc-cushion-velvet-tint-SP00014.html');
                break;
            case 'i-m-meme-i-m-tint-balm.html':
                return redirect('/son-duong-im-meme-im-tint-balm-SP00015.html');
                break;
            case 'bang-mat-4-mau-i-m-meme-i-m-eyeshadow-palette-quad.html':
                return redirect('/bang-mat-4-mau-im-meme-im-eyeshadow-palette-quad-SP00017.html');
                break;
            case 'i-m-meme-i-m-cushion-correctors.html':
                return redirect('/cushion-dieu-chinh-mau-da-tao-khoi-im-meme-im-cushion-corrector-SP00021.html');
                break;
            case 'i-m-meme-i-m-lipstick-water-fit.html':
                return redirect('/son-im-meme-im-lipstick-water-fit-SP00029.html');
                break;
            case 'i-m-meme-i-m-nude-cc.html':
                return redirect('/im-meme-im-nude-cc-SP00032.html');
                break;
            case 'i-m-meme-i-m-skin-bb.html':
                return redirect('/im-meme-im-skin-bb-SP00033.html');
                break;
            case 'i-m-meme-i-m-tic-toc-tint-lip.html':
                return redirect('/son-im-meme-im-tic-toc-tint-lip-SP00035.html');
                break;
            case 'i-m-meme-i-m-blusher.html':
                return redirect('/phan-ma-im-meme-im-blusher-SP00037.html');
                break;
            case 'i-m-meme-i-m-concealer.html':
                return redirect('/che-khuyet-diem-im-meme-im-concealer-SP00039.html');
                break;
            case 'i-m-meme-i-m-fix-lip.html':
                return redirect('/son-im-meme-im-fix-lip-SP00041.html');
                break;
            case 'i-m-meme-i-m-primer.html':
                return redirect('/kem-lot-im-meme-im-primer-SP00264.html');
                break;
            case 'i-m-meme-i-m-lip-eye-primer.html':
                return redirect('/kem-lot-cho-mat-va-moi-im-meme-lip-eye-primer-MMB006001.html');
                break;
            case 'son-but-chi-im-meme-im-matte-lip-crayon.html':
                return redirect('/son-but-chi-im-meme-im-matte-lip-crayon-SP00011.html');
                break;
            case 'i-m-meme-i-m-browcara.html':
                return redirect('/mascara-chan-may-im-meme-im-browcara-SP00026.html');
                break;
            case 'i-m-meme-tao-khoi-va-ma-hong-i-m-multi-stick.html':
                return redirect('/tao-khoi-va-ma-hong-im-meme-im-multi-stick-SP00046.html');
                break;
            case 'son-nuoc-im-meme-im-lip-lacquer.html':
                return redirect('/son-kem-im-meme-im-lip-lacquer-SP00049.html');
                break;
            case 'chi-mat-i-m-meme-i-m-eyebrow-pencil.html':
                return redirect('/ke-chan-may-im-meme-im-eyebrow-pencil-SP00019.html');
                break;

            case 'mat-na-duong-den-skinfood-black-sugar-mask.html':
                return redirect('/mat-na-skinfood-black-sugar-mask-wash-off-SP00115.html');
                break;
            case 'bot-chan-may-skinfood-choco-eyebrow-powder-cake.html':
                return redirect('/bot-chan-may-skinfood-choco-eyebrow-powder-cake-SP00253.html');
                break;
            case 'tinh-chat-duong-black-sugar-perfect-first-serum-2x.html':
                return redirect('/tinh-chat-duong-skinfood-black-sugar-perfect-first-serum-2x-SFS001BSP.html');
                break;
            case 'sua-rua-mat-enesti-suansu-foaming-cleanser.html':
                return redirect('/sua-rua-mat-enesti-suansu-rose-hip-oil-foaming-cleanser-SP00267.html');
                break;
            case 'sua-rua-mat-enesti-day-to-day-green-tea-foaming-cleanser.html':
                return redirect('/sua-rua-mat-enesti-day-to-day-green-tea-foaming-cleanser-SP00266.html');
                break;
            case 'sua-rua-mat-shiseido-senka-perfect-whip-cleansing-foam.html':
                return redirect('/sua-rua-mat-shiseido-senka-perfect-whip-cleansing-foam-SP00289.html');
                break;
            case 'dau-tay-trang-essenherb-soybean-90-deep-cleansing-oil.html':
                return redirect('/dau-tay-trang-essenherb-soybean-90-deep-cleansing-oil-SP00290.html');
                break;
            case 'xit-khoang-vichy-eau-thermale-mist.html':
                return redirect('/xit-khoang-vichy-eau-thermale-mist-SP00291.html');
                break;
            case 'xit-khoang-avene-thermal-spring-water.html':
                return redirect('/xit-khoang-avene-thermal-spring-water-SP00292.html');
                break;
            case 'sua-rua-mat-us-loha-rice-bran-cleansing-foam.html':
                return redirect('/sua-rua-mat-us-loha-rice-bran-cleansing-foam-SP00293.html');
                break;
            case 'xit-khoang-five-back-luminous-mist-fixer.html':
                return redirect('/xit-khoang-five-back-luminous-mist-fixer-SP00294.html');
                break;
            case 'nuoc-hoa-hong-us-loha-rose-water-toner.html':
                return redirect('/nuoc-hoa-hong-us-loha-rose-water-toner-SP00299.html');
                break;
            case 'son-thoi-espoir-no-wear-live.html':
                return redirect('/son-thoi-espoir-lipstick-no-wear-live-lipstick-EPC006001.html');
                break;
            case 'son-thoi-espoir-lipstick-noware-signature.html':
                return redirect('/son-thoi-espoir-lipstick-no-wear-signature-EPL002WIL.html');
                break;
            case 'son-li-espoir-no-wear-transfit.html':
                return redirect('/son-li-espoir-no-wear-transfit-lipstick-EPL005001.html');
                break;
            case 'kem-gai-bien-vi-kim-private21-spicule-ultra-dermatox-pill-cream.html':
                return redirect('/kem-gai-bien-vi-kim-private21-spicule-ultra-dermatox-pill-cream-SP00296.html');
                break;
            case 'kem-duong-am-avene-cicalfate-restorative-skin-cream.html':
                return redirect('/kem-duong-am-avene-cicalfate-restorative-skin-cream-AVS001RES.html');
                break;
            case 'bot-vitamin-c-let-s-cure-c-ster-high-performance-powder.html':
                return redirect('/bot-vitamin-c-lets-cure-c-ster-high-performance-powder-SP00154.html');
                break;
            case 'son-thoi-agapan-pit-a-pat-lipstick-red-limited.html':
                return redirect('/son-thoi-agapan-pit-a-pat-lipstick-red-limited-JU-AGL001R21.html');
                break;
            case 'phan-nuoc-agapan-extreme-cover-cushion-matte-finish-spf50.html':
                return redirect('/phan-nuoc-agapan-extreme-cover-cushion-matte-finish-spf50-pa-JU-AGF00121.html');
                break;
            case 'nuoc-hoa-hong-than-ky-some-by-mi-aha-bha-pha-30-days-miracle-toner.html':
                return redirect('/nuoc-can-bang-than-ky-some-by-mi-aha-bha-pha-30-days-miracle-toner-JU-SBMO00130DAYS.html');
                break;
            case 'gel-duong-am-skincube-snail-mucus-100-soothing-gel.html':
                return redirect('/gel-duong-am-skincube-snail-mucus-100-soothing-gel-SP00297.html');
                break;
            case 'mat-na-duong-mat-koelp-hydrogel-eye-patch.html':
                return redirect('/mat-na-duong-mat-koelp-hydrogel-eye-patch-SP00298.html');
                break;
            case 'mat-na-mieng-cho-da-va-mat-purederm-pads.html':
                return redirect('/mat-na-mieng-cho-da-va-mat-purederm-pads-SPM004VIN.html');
                break;
            case 'a-pieu.html':
                return redirect('/thuong-hieu/apieu');
                break;
            case 'etude-house.html':
                return redirect('/thuong-hieu/etude-house');
                break;
            case 'trang-diem-da-mat.html':
                return redirect('/phan-loai/trang-diem-da-mat');
                break;
            case 'peripera.html':
                return redirect('/thuong-hieu/peripera');
                break;
                
            default:
                return redirect('/'.$l1);
                break;
        }
    }

    public function redirect2Level($l1, $l2)
    {        
        return redirect('/'.$l1.'/'.$l2);
    }

    public function redirect3Level($l1, $l2, $l3)
    {        
        return redirect('/'.$l1.'/'.$l2.'/'.$l3);
    }
}