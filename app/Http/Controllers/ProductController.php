<?php

namespace App\Http\Controllers;

use App\Product;
use App\Comboprice;
use App\ProductCategory;
use App\ProductGroup;
use App\Review;
use App\Sales;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Trạng thái sản phẩm
     */
    protected $status = array(
        1 => 'Còn hàng',
        0 => 'Tạm hết',
        -1 => 'Ngừng kinh doanh',
    );
    
	public function showProductDetail($slug)
	{

        $data = Product::whereHidden(false)->whereDrafted(false)->whereTrashed(false)
                ->where(function ($query) {
                    $query->where('status', '!=' , -1) // bỏ sản phẩm có trạng thái là Ngừng kinh doanh
                        ->orWhereNull('status');
                })
                ->whereSlug($slug)->firstOrFail();        

        $hotdeal = $data->fk_hotdeal();
        
        // TODO: Nếu trạng thái sản phẩm là 'Tạm hết' thì set closed = true
        if ($data->status === 0) {
            $data->closed = true;
        }
        
    	if($data->fk_sales()->count() > 0){
            $sales = $data->fk_sales()->firstorfail();
            if($sales->status == 1 && (date('U', strtotime($sales->started_at)) <= date('U') && date('U', strtotime($sales->expired_at)) >= date('U'))){
                if($data->original_price > 0 ){
                    if($sales->price_type == 1){
                        $data->price = floatval($data->original_price) - (floatval($data->original_price)*$sales->price_down/100);
                    }else{
                        $data->price = floatval($data->original_price) -floatval($sales->price_down);
                    }
                }else{
                    if($sales->price_type == 1){
                        $data->original_price = $data->price;
                        $data->price = floatval($data->original_price) - (floatval($data->original_price)*$sales->price_down/100);
                    }else{
                        $data->price = floatval($data->original_price) -floatval($sales->price_down);
                    }
                }
            }
        }

        $related = Product::whereHidden(false)->whereDrafted(false)->whereTrashed(false)
            ->where(function ($query) {
                $query->where('status', '!=' , -1) // bỏ sản phẩm có trạng thái là Ngừng kinh doanh
                    ->orWhereNull('status');
            })
            ->where('category_id', $data->category_id)->where('id', '!=', $data->id)
            ->orderBy('reposted_at', 'desc')->orderBy('created_at', 'desc')->take(6)->get(); 

        $reviews = Review::whereTrashed(false)->where('product_id', $data->id)->whereNull('parent_id')->get();
        $fullReviews = Review::whereTrashed(false)->where('product_id', $data->id)->whereNull('parent_id')->orderBy('created_at', 'desc')->paginate(10);

        $rated = false;
        if(\Auth::guard('customer')->check()) {
            $rate_count = Review::whereTrashed(false)->where('user_id', \Auth::guard('customer')->user()->id)->get()->count();

            if($rate_count)
                $rated = true;
        }
		//echo var_dump($reviews);
        if(count($reviews) > 0) {
            $pluck = $reviews->pluck('rating')->toArray();

            $reviewStatistic = (object)[
                's5'    =>  ((array_count_values($pluck)['5'] ?? 0) * 100) / count($reviews),
                's4'    =>  ((array_count_values($pluck)['4'] ?? 0) * 100) / count($reviews),
                's3'    =>  ((array_count_values($pluck)['3'] ?? 0) * 100) / count($reviews),
                's2'    =>  ((array_count_values($pluck)['2'] ?? 0) * 100) / count($reviews),
                's1'    =>  ((array_count_values($pluck)['1'] ?? 0) * 100) / count($reviews),
            ];
        }
    
    	if($data->is_combo){
            if(Comboprice::where('combo_id',$data->id)->get()){
                $listproductss = Comboprice::where('combo_id',$data->id)->get();

                foreach ($listproductss as $key => $value) {
                    if(Product::find($value['product_id'])){
                        $productinfo = Product::find($value['product_id']);
                        $listproductss[$key]['productinfo'] = $productinfo;
                    }
                }
            }else{
                $listproductss = '';
            }
        }else{
            $listproductss = '';
        }

        $rating_point = $reviews->pluck('rating')->toArray();
	if(count($reviews) > 0)
        return view('product_content', compact('data', 'hotdeal', 'related', 'fullReviews', 'reviewStatistic', 'rated', 'rating_point','listproductss'));
	else
		return view('product_content', compact('data', 'hotdeal', 'related', 'fullReviews', 'rated', 'rating_point','listproductss'));
	}

	public function showAllProducts()
	{
        $data = Product::where('is_combo', false)->whereHidden(false)->whereDrafted(false)->whereTrashed(false)
            ->where(function ($query) {
                $query->where('status', '!=' , -1) // bỏ sản phẩm có trạng thái là Ngừng kinh doanh
                    ->orWhereNull('status');
            })
            ->orderBy('position', 'asc')->orderBy('reposted_at', 'desc')->orderBy('created_at')->paginate(20);
		return view('show-all-products', compact('data'));
	}

    public function showProductsInList($slug)
    {
        // Lấy ra đối tượng danh sách
        $list = ProductCategory::whereSlug($slug)->first();        
        if(is_null($list)) {
            $list = ProductGroup::whereSlug($slug)->first();   
            // Lấy ra các đối tượng con của danh sách
            $list_ids = [$list->id];
            if(count($list->fk_childs) > 0) {
                $list_ids = ProductGroup::where('parent_id', $list->id)->get()->pluck('id')->toArray();
                array_push($list_ids, $list->id);
            }

            // Lấy danh sách id sản phẩm
            $ids = \DB::table('product_group_relationships')
            ->whereIn('group_id', $list_ids)
            ->get()->pluck('product_id')->toArray();
        }else{
            // Lấy ra các đối tượng con của danh sách
            $list_ids = [$list->id];
            if(count($list->fk_childs) > 0) {
                $list_ids = ProductCategory::where('parent_id', $list->id)->get()->pluck('id')->toArray();
                array_push($list_ids, $list->id);
            }

            // Lấy danh sách id sản phẩm
            $ids = \DB::table('product_category_relationships')
            ->whereIn('category_id', $list_ids)
            ->get()->pluck('product_id')->toArray();
        }

        $data = Product::where('is_combo', false)->whereHidden(false)->whereDraft(false)->whereTrashed(false)
            ->where(function ($query) {
                $query->where('status', '!=' , -1) // bỏ sản phẩm có trạng thái là Ngừng kinh doanh
                    ->orWhereNull('status');
            })
            ->whereIn('id', $ids)->orderBy('position', 'asc')->orderBy('reposted_at', 'desc')->orderBy('created_at')->paginate(21);
    	return view('show-products-in-list', compact('list', 'data'));
    }
}
