<?php

namespace App\Http\Controllers;

use App\ProductBrand;
use App\ProductCategory;
use Illuminate\Http\Request;

class SaleOfController extends Controller
{
    public function showHotDealProducts()
    {
        $query = \App\HotDeal::whereDisabled(false)
                            ->join('products', 'products.id', '=', 'hot_deals.product_id')
                            ->join('product_brands', 'product_brands.id', '=', 'products.brand_id')
                            ->where('products.is_combo', false)
                            ->where('products.trashed', false)
                            ->where('products.drafted', false)
                            ->where('products.closed', false);

        if(request()->filled('category'))
            $query->where('products.category_id', request()->category);
        
        if(request()->filled('brand'))
            $query->where('products.brand_id', request()->brand);

        if(request()->status == 'ats') {
            $query->where('started_at', '>', date('Y-m-d H:i:s'))
                  ->where('expired_at', '>', date('Y-m-d H:i:s'));
        }else if(request()->status == 'expired') {
            $query->where(function($cond){
                   $cond->where('expired_at', '<', date('Y-m-d H:i:s'));
               });
        }else{
            $query->where('started_at', '<=', date('Y-m-d H:i:s'))
                  ->where('expired_at', '>', date('Y-m-d H:i:s'));
        }
        
        $query->select('products.name', 'products.slug', 'hot_deals.price', 'hot_deals.original_price', 'hot_deals.quantity', 'hot_deals.available_qty', 'products.thumbnail', 'started_at', 'expired_at', 'product_brands.slug as brand_slug', 'product_brands.name as brand_name');

        $data = $query->orderBy('hot_deals.created_at', 'desc')->paginate(20);

        $categories = ProductCategory::whereNull('parent_id')->get();
        $brands = ProductBrand::whereNull('parent_id')->get();

        return view('sale_of', compact('data', 'categories', 'brands'));
    }

    public function showFreeTrialProducts()
    {
        $query = \App\FreeTrial::whereDisabled(false)
                            ->where('expired_at', '>', date('Y-m-d H:i:s'))
                            ->join('products', 'products.id', '=', 'free_trial.product_id')
                            ->join('product_brands', 'product_brands.id', '=', 'products.brand_id')
                            ->where('products.is_combo', false)
                            ->where('products.trashed', false)
                            ->where('products.drafted', false);

        if(request()->filled('category'))
            $query->where('products.category_id', request()->category);
        
        if(request()->filled('brand'))
            $query->where('products.brand_id', request()->brand);
        
        $query->select('products.name', 'free_trial.quantity', 'products.thumbnail', 'started_at', 'expired_at', 'target_url', 'product_brands.slug as brand_slug', 'product_brands.name as brand_name');

        $data = $query->orderBy('free_trial.position', 'asc')->orderBy('free_trial.created_at', 'desc')->paginate(20);

        $categories = ProductCategory::whereNull('parent_id')->get();
        $brands = ProductBrand::whereNull('parent_id')->get();

        return view('sale_of', compact('data', 'categories', 'brands'));
    }

    public function showComboProducts()
    {
        $query = \App\Combo::where('products.is_combo', true)->where('products.hidden', false)
                ->join('combo_details', 'combo_details.combo_id', '=', 'products.id');

        if(request()->filled('category')) {
            $query->where('products.category_id', request()->category);
        }
        if(request()->filled('brand')) {
            $query->where('products.brand_id', request()->brand);
        }

        $data = $query->select('products.*')->orderBy('products.position', 'asc')->orderBy('products.reposted_at', 'desc')->orderBy('products.created_at', 'desc')->distinct()->paginate(20);

        $categories = ProductCategory::whereNull('parent_id')->get();
        $brands = ProductBrand::whereNull('parent_id')->get();

        return view('sale_of', compact('data', 'categories', 'brands'));
    }
}