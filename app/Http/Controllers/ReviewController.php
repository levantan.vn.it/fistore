<?php

namespace App\Http\Controllers;

use Auth;
use App\Review;
use App\ReviewThanked;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function rate(Request $request)
    {
    	$review = new Review;
    	$review->user_id = Auth::guard('customer')->user()->id;
    	$review->product_id = $request->product_id;
    	$review->title = $request->title;
    	$review->rating = $request->rating;
    	$review->content = $request->content;

    	$orderId = \App\ProductOrder::where('user_id', Auth::guard('customer')->user()->id)->get()->pluck('id');
    	$bought = \App\ProductOrderDetail::whereIn('order_id', $orderId)->where('product_id', $request->product_id)->get()->count();

    	$review->bought = $bought > 0 ? true : false;

    	$review->save();

    	return redirect()->to(url()->previous() . '#reviewId' . $review->id);
    }

    public function reply(Request $request)
    {
    	$review = Review::find($request->parent_id);

    	$reply = new Review;
    	$reply->user_id = Auth::guard('customer')->user()->id;
    	$reply->product_id = $review->product_id;
    	$reply->parent_id = $review->id;
    	$reply->content = $request->reply;

    	$reply->save();

    	return redirect()->to(url()->previous() . '#reviewId' . $review->id);
    }

    public function thank(Request $request)
    {
    	$review = Review::find($request->thank);
        if(!$review->fk_thank->contains(Auth::guard('customer')->user()->id)) {
        	$review->thanked += 1;
        	$review->save();
            
            $review->fk_thank()->sync(Auth::guard('customer')->user()->id);
        }

    	return redirect()->to(url()->previous() . '#reviewId' . $request->thank);
    }

    public function update(Request $request)
    {

    }
}
