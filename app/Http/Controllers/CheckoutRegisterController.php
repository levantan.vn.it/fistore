<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;

class CheckoutRegisterController extends Controller
{
	use RegistersUsers;

	protected $redirectTo = '/checkout/address/list';

	public function __construct()
    {
        $this->middleware('guest:customer');
    }

    protected function guard()
    {
    	return Auth::guard('customer');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);
    }

    public function register()
    {
    	return view('checkout_account_register');
    }

    public function postRegister(Request $request)
    {
    	// Kiểm tra dữ liệu nhập vào
    	$this->validator($request->all())->validate();

    	// Tạo tài khoản
    	$user = new User;
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->phone = $request->phone ?? null;
    	$user->password = Hash::make($request->password);
    	$user->save();

    	// Đăng nhập
    	$this->guard()->login($user);

    	// Chuyển hướng
    	return redirect($this->redirectTo);
    }
}
