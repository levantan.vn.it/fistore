<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function newsletter(Request $request)
    {
    	if(Newsletter::whereEmail($request->email)->get()->count() > 0){
    		return back()->with([
    			'alert' => 'warning',
    			'title' => 'Lỗi!',
    			'message' => 'Email đã được đăng ký trước đó.'
    		]);
    	}else{
    		$data = new Newsletter;
    		$data->email = $request->email;
    		$data->save();
    	}

    	return back()->with([
    		'alert' => 'success',
    		'title' => 'Thành công!',
    		'message' => 'Cảm ơn bạn đã đăng ký nhận thông tin tại ' . get_config()->name
    	]);
    }
}
