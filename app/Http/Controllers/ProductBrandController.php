<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductBrand;
use Illuminate\Http\Request;

class ProductBrandController extends Controller
{
    public function showBrandList()
    {
    	$data = ProductBrand::orderBy('name', 'asc')->get();
    	return view('brand_list', compact('data'));
    }

    public function showProductListInBrand($brand_slug)
    {
    	$brand = ProductBrand::whereSlug($brand_slug)->first();

        $brands = ProductBrand::whereNull('parent_id')->orderBy('index')->get();

    	$query = Product::whereHidden(0)
    				->whereDrafted(0)
    				->whereTrashed(0)
    				->where('brand_id', $brand->id);

    	if(request()->filled('sort')) {
    		if(request()->sort == 'min_cost') {
    			$query->orderBy('price', 'asc');
    		}
    		else if(request()->sort == 'max_cost') {
    			$query->orderBy('price', 'desc');
    		}
    	}
    	$data = $query->orderBy('position', 'asc')
                        ->orderBy('reposted_at', 'desc')
    					->orderBy('created_at', 'desc')
    					->paginate(20);
    	return view('brand_products', compact('brand', 'data', 'brands'));
    }
}
