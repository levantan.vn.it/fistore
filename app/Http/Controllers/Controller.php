<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\AccessToken;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function getAPI($url,$bodyData,$access_token,$name_api = ""){
    	$base_url = 'https://dmnc-afr--sandbox353.my.salesforce.com/';
        $base_url = 'https://dmnc-afr.my.salesforce.com/';
    	$url = $base_url.$url;
        $tmpData = $bodyData;
    	$bodyData = json_encode($bodyData);
    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    	'Authorization: Bearer '.$access_token,
	    	'Content-Type: application/json',
	    	'Content-Length: '.strlen($bodyData)
	    ));
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyData);
        curl_setopt($ch, CURLOPT_TIMEOUT, 2);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
		$server_output = curl_exec($ch);
		
        if ($server_output === false) {
            $info = curl_getinfo($ch);
            if ($info['http_code'] === 0) {
                $data_log = [
                    'name'  =>  $name_api,
                    'message'   =>   'Lỗi quá tải server. Bạn vui lòng gửi lại api.',
                    'status'    =>  2,
                    'data'  =>  serialize($tmpData),
                    'url'   =>  $url,
                    'send_again'    => 1,
                    'created_at'    =>  date('Y-m-d H:i:s'),
                    'updated_at'    =>  date('Y-m-d H:i:s')
                ];
                $this->save_logs($data_log);
                return false;
            }
        }
        curl_close ($ch);
		return json_decode($server_output, true);
    }

    public function getAccessToken(){
    	$url = 'https://dmnc-afr--sandbox353.my.salesforce.com/services/oauth2/token';
    	$params = [
    		'grant_type'	=>	'password',
    		'client_id'	=>	'3MVG9Iu66FKeHhIM9SJbqBt6ied0IopyQRM1JTI5gGFlc3tfYSJ.DdD.3J1ZEbSForFRXDRPiXO2QJ6oeL1J2',
    		'client_secret'	=>	'928269EB38B5BB7F4D29F3EF475A696430FAF5525F533D18E983CBF10F85FB35',
    		'username'	=>	'admin@dmnc-afr.afr.sandbox353',
    		'password'	=>	'afrIntegrationTesting01SesbfnWTfeQ6OlHAN878yltg'
    	];

        $url = 'https://dmnc-afr.my.salesforce.com/services/oauth2/token';
        $params = [
            'grant_type'    =>  'password',
            'client_id' =>  '3MVG9IHf89I1t8hr9HIFc4mS75Hve3PXK.Jnwbo8ZkwQ4ZvpH4ZB.mFqIvf4iS7FxjUyg3Kl0YEHnhLf9KZBV',
            'client_secret' =>  '4DE1FEC8AB5E38E899AA9217A5B43741A259F925C1AEABD16371FC4A951A96A6',
            'username'  =>  'api@dmnc-afr.afr',
            'password'  =>  'AfrDmnc@12309L4x1tAClS5ecQFMadvxIEO1'
        ];
    	$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_POST, count($params));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
		// Receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close ($ch);
        $result = json_decode($result, true);
    	if(!empty($result['access_token'])){
    		AccessToken::create($result);
    	}

    	return !empty($result['access_token'])?$result['access_token']:NULL;
    }

    public function getAccessTokenByDB(){
    	$access_token = AccessToken::orderBy('id','DESC')->first();
    	if(!empty($access_token))
    		return $access_token->access_token;
    	return $this->getAccessToken();
    }

    public function save_logs($data){
        $data['created_at'] =   date('Y-m-d H:i:s');
        $data['updated_at'] =   date('Y-m-d H:i:s');
        \DB::table('logs_api')->insert($data);
    }
}
