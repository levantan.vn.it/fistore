<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Illuminate\Http\Request;

class UserExtraPointController extends Controller
{
    public function getExtraPoint()
    {
    	$data = User::find(Auth::guard('customer')->user()->id);
    	return view('user_extra_point', compact('data'));
    }
}
