<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddressBook;
use Auth;
use Cart;

class CheckoutAddressController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware(function($request, $next){
            // Từ chối nếu giỏ hàng trống
            if(Cart::count() == 0)
                return redirect()->route('cart.show');

            return $next($request);
        });
    }

    protected $address_type = [
        0 => 'Nhà riêng/Chung cư',
        1 => 'Công ty/Cơ quan'
    ];

    /**
     * Hiển thị trang nhập địa chỉ
     * Khi mua hàng không cần tài khoản
     * Khi đã đăng nhập và không có địa chỉ có sẵn
     * Khi chọn Thêm địa chỉ mới
     * Khi Chỉnh sửa địa chỉ có sẵn
     */
    public function getAddress()
    {
        /**
         * Đã đăng nhập
         */
        if(Auth::guard('customer')->check()) {
            /**
             * Chỉnh sửa địa chỉ
             */
            if(request()->has('edit')) {
                $userAddress = AddressBook::find(request()->edit);
                return view('checkout_address', compact('userAddress'));
            }
        }

    	/**
         * Chưa đăng nhập
         * Đã đăng nhập và Chưa có địa chỉ nào
         */
        return view('checkout_address');
    }

    public function postAddress(Request $request)
    {
        /**
         * Nếu chọn địa chỉ có sẵn từ sổ
         */
        if($request->filled('address_id')) {
            /**
             * Tạo Session lưu địa chỉ
             */
            $address = AddressBook::find($request->address_id);
            $this->createAddressSession($address);
        }else{
            /**
             * Đã đăng nhập
             * Lưu địa chỉ vào sổ
             */
            if(Auth::guard('customer')->check()) {

                /**
                 * Sửa địa chỉ có sẵn
                 */
                if(request()->filled('address_id')) {
                    $address_book = AddressBook::find(request()->address_id);
                }
                /**
                 * Nhập địa chỉ mới
                 */
                else{
                    $address_book = new AddressBook;
                }

            	if(isset($request->ward)){
                	$ward = $request->ward;
                }else{
                	$ward = 0;
                }

                $address_book->user_id = Auth::guard('customer')->user()->id;
                $address_book->name = $request->name;
                $address_book->phone = $request->phone;
                $address_book->province_id = $request->province;
                $address_book->district_id = $request->district;
                $address_book->ward_id = $ward;
                $address_book->address = $request->address;
                $address_book->address_type = $request->address_type;
                $address_book->defaulted = $request->filled('default') ? true : false;
                $address_book->save();
            }

            /**
             * Tạo Session lưu địa chỉ
             */
            $request->province_id = $request->province;
            $request->district_id = $request->district;
            $request->ward_id = $request->ward;
            $this->createAddressSession($request);
        }


        /**
         * Chuyển hướng sang trang Thanh toán
         */
        return redirect()->route('checkout.payment.get');
    }

    public function createAddressSession($data)
    {
        session(['checkout' => (object)[
            'user_id'       =>  Auth::guard('customer')->check() ? Auth::guard('customer')->user()->id : null,
            'name'          => $data->name,
            'phone'         => $data->phone,
            'email'         => $data->email,
            'province_id'   => $data->province_id,
            'district_id'   => $data->district_id,
            'ward_id'       => $data->ward_id,
            'address'       => $data->address,
            'address_type'  => $data->address_type,
        ]]);
    }

    /**
     * Hiển thị trang chọn địa chỉ có sẵn
     */
    public function showAddressList()
    {
        /**
         * Chưa đăng nhập
         * Đã đăng nhập nhưng chưa có địa chỉ nào trong sổ
         * Chuyển hướng sang trang mua hàng không cần tài khoản
         */
        if(Auth::guard('customer')->guest() || count(Auth::guard('customer')->user()->fk_address_book) == 0){
            return redirect()->route('checkout.address.get');
        }

        $address_books = Auth::guard('customer')->user()->fk_address_book;
        return view('checkout_address_book', compact('address_books'));
    }
}
