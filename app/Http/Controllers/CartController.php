<?php

namespace App\Http\Controllers;

use Cart;
use App\Comboprice;
use App\Coupon;
use App\Product;
use App\Sales;
use Illuminate\Http\Request;
use stdClass;

class CartController extends Controller
{
    public function showCart()
    {
        $this->updateAllCartItems();
        return view('cart');
    }

    public function addToCart(Request $request)
    {
        /** Lấy sản phẩm */
        $product = Product::whereId($request->id)
            ->whereHidden(false)
            ->whereDrafted(false)
            ->whereTrashed(false)
            ->whereClosed(false)
            ->where('display_price', true)
            ->first();

        $hotdeal = $product->fk_hotdeal();

        if ($product->is_combo) {
            $combo = [];
            foreach ($product->fk_comboProducts as $key => $singleProduct) {
                $combo[$key]['product_id'] = $singleProduct->id;
                $combo[$key]['product_name'] = $singleProduct->name;
                if ($request->filled('combo_version_id.' . $singleProduct->id)) {
                    $combo[$key]['version_id'] = $request->combo_version_id[$singleProduct->id];
                    $combo[$key]['version_name'] = \App\ProductVersion::find($request->combo_version_id[$singleProduct->id])->name;
                }
                $combo[$key] = (object) $combo[$key];
            }
        } elseif (!is_null($hotdeal)) {
            $inventory = 0;
            foreach (Cart::content() as $item) {
                if ($item->options->hotdeal->id == $hotdeal->id) {
                    $inventory = $item->qty;
                    break;
                }
            }

            /** Kiểm tra số lượng tồn kho */
            if (!$this->checkInventory($hotdeal->quantity, $request->quantity, $inventory))
                return response('Số lượng hotdeal không đủ cung ứng.', 400);
            $version = \App\ProductVersion::find($request->version_id);
            //ducngo
            //Check tồn kho sản phẩm
            if (!$this->checkInventory($version->quantity, $request->quantity, $inventory))
                return response('Số lượng sản phẩm không đủ cung ứng.', 400);
        } else {
            if ($request->filled('version_id')) {
                $version = \App\ProductVersion::find($request->version_id);

                /** Kiểm tra thông tin tồn kho phiên bản */
                $inventory = 0;
                foreach (Cart::content() as $item) {
                    if ($item->options->version->id == $version->id) {
                        $inventory = $item->qty;
                        break;
                    }
                }

                /** Kiểm tra số lượng tồn kho */
                if (!$this->checkInventory($version->quantity, $request->quantity, $inventory))
                    return response('Số lượng sản phẩm không đủ cung ứng.', 400);
            } else {
                /** Kiểm tra thông tin tồn kho sản phẩm */
                /** Lấy số lượng hiện tại trong giỏ hàng */
                $inventory = 0;
                foreach (Cart::content() as $item) {
                    if ($item->options->code == $product->code) {
                        $inventory = $item->qty;
                        break;
                    }
                }

                /** Kiểm tra số lượng tồn kho */
                if (!$this->checkInventory($product->quantity, $request->quantity, $inventory))
                    return response('Số lượng sản phẩm không đủ cung ứng.', 400);
            }
        }

        $salestatus = 0;
        $quantity = $request->quantity;
        $quantitystatus = [
            'error' => false,
            'limited' => 0
        ];
        if ($product->fk_sales()->count() > 0) {
            $sales = $product->fk_sales()->firstorfail();
            if ($sales->status == 1 && (date('U', strtotime($sales->started_at)) <= date('U') && date('U', strtotime($sales->expired_at)) >= date('U'))) {
                $salestatus = 1;
                $quantitystatus['limited'] = $sales->limit;
                if ($product->original_price > 0) {
                    if ($sales->price_type == 1) {
                        $product->price = floatval($product->original_price) - (floatval($product->original_price) * $sales->price_down / 100);
                    } else {
                        $product->price = floatval($product->original_price) - floatval($sales->price_down);
                    }
                } else {
                    $product->original_price = $product->price;
                    if ($sales->price_type == 1) {
                        $product->price = floatval($product->original_price) - (floatval($product->original_price) * $sales->price_down / 100);
                    } else {
                        $product->price = floatval($product->original_price) - floatval($sales->price_down);
                    }
                }

                $countproduct = 0;
                foreach (Cart::content() as $item) {
                    if ($item->id == $product->id) {
                        if ((!is_null($sales->limit)) && intval($item->qty) + intval($request->quantity) > $sales->limit) {
                            $quantitystatus['error'] = true;
                        }
                    } else {
                        $countproduct++;
                    }
                }
                if ($countproduct == Cart::count()) {
                    if ((!is_null($sales->limit)) && intval($request->quantity) > $sales->limit) {
                        $quantitystatus['error'] = true;
                    } else {
                        $quantitystatus['error'] = false;
                    }
                }
            }
        }

        if ($request->has('buy')) {
            /** Hủy toàn bộ giỏ hàng */
            Cart::destroy();
        }

        /** Thêm sản phẩm vào giỏ */
        if ($quantitystatus['error']) {
            return response()->json([
                'alert'         =>  'error',
                'title'         =>  'Thông báo!',
                'msg'           =>  'Chương trình chỉ áp dụng cho tối đa ' . $quantitystatus['limited'] . ' sản phẩm trên mỗi lượt mua hàng.',
                'count'         =>  Cart::count(),
            ]);
        } else {
            if ($request->sanphamdikem) {
                $priceinfo = Comboprice::where('id', $request->sanphamdikem)->first();
                $productinfoo = Product::where('id', $priceinfo['product_id'])->first();
                Cart::add([
                    'id'            => $product->id,
                    'name'          => isset($version->name) ? $product->name . ' - ' . $version->name : $product->name,
                    'qty'           => $request->quantity,
                    'price'         => $priceinfo['price'],
                    'options'       => [
                        'code'          => $version->code ?? $product->code,
                        'product'       => (object) [
                            'code'          => $product->code,
                            'original_price' => $product->original_price ?? null,
                            'slug'          => $product->slug,
                            'thumbnail'     => $version->fk_images[0]->path ?? $product->thumbnail,
                        ],
                        'product1'       => (object) [
                            'name'          => $productinfoo['name'],
                            'code'          => $productinfoo['code'],
                            'slug'          => $productinfoo['slug'],
                            'thumbnail'     => $productinfoo['thumbnail'],
                        ],
                        'version'       => (object) [
                            'id'            => $version->id ?? null,
                            'name'          => $version->name ?? null,
                            'code'          => $version->code ?? null,
                        ],
                        'hotdeal'       => (object) [
                            'id'             => $hotdeal->id ?? null,
                            'original_price' => $hotdeal->original_price ?? null,
                            'expired_at'     => $hotdeal->expired_at ?? null,
                        ],
                        'combo'         => $combo ?? null,
                        'salestatus'    =>  $salestatus
                    ],
                ]);
            } else {
                Cart::add([
                    'id'            => $product->id,
                    'name'          => isset($version->name) ? $product->name . ' - ' . $version->name : $product->name,
                    'qty'           => $request->quantity,
                    'price'         => $hotdeal->price ?? ($version->price ?? $product->price),
                    'options'       => [
                        'code'          => $version->code ?? $product->code,
                        'product'       => (object) [
                            'code'          => $product->code,
                            'original_price' => $product->original_price ?? null,
                            'slug'          => $product->slug,
                            'thumbnail'     => $version->fk_images[0]->path ?? $product->thumbnail,
                        ],
                        'version'       => (object) [
                            'id'            => $version->id ?? null,
                            'name'          => $version->name ?? null,
                            'code'          => $version->code ?? null,
                        ],
                        'hotdeal'       => (object) [
                            'id'             => $hotdeal->id ?? null,
                            'original_price' => $hotdeal->original_price ?? null,
                            'expired_at'     => $hotdeal->expired_at ?? null,
                        ],
                        'combo'         => $combo ?? null,
                        'salestatus'    =>  $salestatus
                    ],
                ]);
            }

            if ($request->has('buy')) {
                /** Chuyển hướng sang trang giỏ hàng */
                return response()->json([
                    'redirect'      =>  'checkout/cart',
                ]);
            }

            return response()->json([
                'alert'         =>  'success',
                'title'         =>  'Thành công!',
                'msg'           =>  'Đã thêm sản phẩm vào giỏ hàng.',
                'count'         =>  Cart::count(),
            ]);
        }
    }

    public function updateCart(Request $request)
    {
        if ($request->ajax()) {
            /** Kiểm tra số lượng đầu vào hợp lệ */
            if ($request->qty < 1) {
                return response('Số lượng nhập vào không hợp lệ.', 400);
            }

            /** Lấy sản phẩm hiện tại trong giỏ hàng */
            $cartItem = Cart::get($request->rowId);

            /** Lấy sản phẩm trong db */
            $product = Product::find($cartItem->id);

            /** Sản phẩm Hotdeal */
            $hotdeal = $product->fk_hotdeal();

            /** Phiên bản sản phẩm */
            $version = \App\ProductVersion::where('product_id', $product->id)->whereId($cartItem->options->version->id)->first();

            if (!is_null($hotdeal)) {
                /** Kiểm tra số lượng tồn kho */
                if (!$this->checkInventory($hotdeal->quantity, $request->qty))
                    return response('Số lượng sản phẩm không đủ cung ứng.', 400);
            }
            /** Sản phẩm có nhiều phiên bản */
            else if (!is_null($version)) {
                /** Kiểm tra số lượng tồn kho */
                if (!$this->checkInventory($version->quantity, $request->qty))
                    return response('Số lượng sản phẩm không đủ cung ứng.', 400);
            } else {
                /** Kiểm tra số lượng tồn kho */
                if (!$this->checkInventory($product->quantity, $request->qty))
                    return response('Số lượng sản phẩm không đủ cung ứng.', 400);
            }

            /** Update số lượng */
            Cart::update($request->rowId, $request->qty);

            if (session()->has('coupon')) {
                $coupon = Coupon::where('code', session('coupon')->code)->first();
                if (!$this->checkCouponErrors($coupon)['error']) {
                    $temp = $this->checkCouponCode($coupon);
                    if (!empty($temp)) {
                        $discount = $temp->discount;
                        session(['coupon' => (object) [
                            'code'  => $coupon->code,
                            'discount' => $discount
                        ]]);
                    }
                }
            }

            return response()->json([
                'count'         => Cart::count(),
                'discount'      => isset($discount) ? number_format($discount, 0, ',', '.') : null,
                'subtotal'      => Cart::subtotal(0, ',', '.'),
                'total'         => number_format(Cart::subtotal(0, ',', '') - (session('coupon')->discount ?? 0), 0, ',', '.')
            ]);
        }
    }

    public function removeProductFromCart(Request $request)
    {
        if ($request->ajax()) {
            Cart::remove($request->rowId);

            if (session()->has('coupon')) {
                $coupon = Coupon::where('code', session('coupon')->code)->first();
                if (!$this->checkCouponErrors($coupon)['error']) {
                    $temp = $this->checkCouponCode($coupon);
                    if (!empty($temp)) {
                        $discount = $temp->discount;
                        session(['coupon' => (object) [
                            'code'  => $coupon->code,
                            'discount' => $discount
                        ]]);
                    }
                }
            }

            return response()->json([
                'alert'         =>  'success',
                'title'         =>  'Thành công!',
                'msg'           =>  'Đã xóa sản phẩm khỏi giỏ hàng.',
                'count'         =>  Cart::count(),
                'discount'      => isset($discount) ? number_format($discount, 0, ',', '.') : null,
                'subtotal'      => Cart::subtotal(0, ',', '.'),
                'total'         => number_format(Cart::subtotal(0, ',', '') - (session('coupon')->discount ?? 0), 0, ',', '.')
            ]);
        }
    }

    public function destroyCart()
    {
        Cart::destroy();

        return response()->json([
            'alert'         =>  'success',
            'title'         =>  'Thành công!',
            'msg'           =>  'Đã làm rỗng giỏ hàng.',
            'count'         =>  Cart::count(),
            'total'         =>  Cart::subtotal(0, ',', '.')
        ]);
    }

    public function updateAllCartItems()
    {
        foreach (Cart::content() as $cartItem) {
            /** Lấy sản phẩm trong db */
            $product = Product::find($cartItem->id);

            /** Sản phẩm Hotdeal */
            $hotdeal = $product->fk_hotdeal();

            if ($product->is_combo) {
                $combo = $cartItem->options->combo;
            } elseif (empty($product) || $product->trashed || $product->drafted || $product->hidden || $product->closed || !$product->display_price || (!empty($cartItem->options->version->id) && is_null(\App\ProductVersion::find($cartItem->options->version->id)))) {
                Cart::remove($cartItem->rowId);
            } else {
                $version = \App\ProductVersion::where('product_id', $product->id)->whereId($cartItem->options->version->id)->first();
                if (!is_null($hotdeal)) {
                    /** Kiểm tra số lượng tồn kho */
                    if (!is_null($hotdeal->quantity) && $hotdeal->quantity < $cartItem->qty) {
                        Cart::update($cartItem->rowId, $hotdeal->quantity);
                    }
                } elseif (!is_null($version)) {
                    /** Kiểm tra số lượng tồn kho */
                    if (!is_null($version->quantity) && $version->quantity < $cartItem->qty) {
                        Cart::update($cartItem->rowId, $version->quantity);
                    }
                } else {
                    /** Kiểm tra số lượng tồn kho */
                    if (!is_null($product->quantity) && $product->quantity < $cartItem->qty) {
                        Cart::update($cartItem->rowId, $product->quantity);
                    }
                }

                $salestatus = 0;
                if ($product->fk_sales()->count() > 0) {
                    $sales = $product->fk_sales()->firstorfail();

                    if ($sales->status == 1 && (date('U', strtotime($sales->started_at)) <= date('U') && date('U', strtotime($sales->expired_at)) >= date('U'))) {
                        $salestatus = 1;
                        if ($product->original_price > 0) {
                            if ($sales->price_type == 1) {
                                $product->price = floatval($product->original_price) - (floatval($product->original_price) * $sales->price_down / 100);
                            } else {
                                $product->price = floatval($product->original_price) - floatval($sales->price_down);
                            }
                        } else {
                            $product->original_price = $product->price;
                            if ($sales->price_type == 1) {
                                $product->price = floatval($product->original_price) - (floatval($product->original_price) * $sales->price_down / 100);
                            } else {
                                $product->price = floatval($product->original_price) - floatval($sales->price_down);
                            }
                        }
                    }
                }

                /** Update giỏ hàng */
                Cart::update($cartItem->rowId, [
                    'name'              => isset($version->name) ? $product->name . ' - ' . $version->name : $product->name,
                    'price'             => $hotdeal->price ?? ($version->price ?? $product->price),
                    'options'           =>  [
                        'code'          => $version->code ?? $product->code,
                        'product'       => (object) [
                            'code'          => $product->code,
                            'original_price' => $product->original_price ?? null,
                            'slug'          => $product->slug,
                            'thumbnail'     => $version->fk_images[0]->path ?? $product->thumbnail,
                        ],
                        'version'       => (object) [
                            'id'            => $version->id ?? null,
                            'name'          => $version->name ?? null,
                            'code'          => $version->code ?? null,
                        ],
                        'hotdeal'       => (object) [
                            'id'             => $hotdeal->id ?? null,
                            'original_price' => $hotdeal->original_price ?? null,
                            'expired_at'     => $hotdeal->expired_at ?? null,
                        ],
                        'combo'         => $combo ?? null,
                        'salestatus'    =>    $salestatus
                    ],
                ]);
            }
            unset($combo);
        }
    }

    public function checkInventory($product_qty, $request_qty, $inventory = 0)
    {
        if ((!is_null($product_qty)) && $product_qty < ($request_qty + $inventory))
            return false;
        return true;
    }

    public function applyCoupon(Request $request)
    {
        /** Lấy thông tin mã khuyến mãi */
        $coupon = Coupon::whereTrashed(false)->whereDisabled(false)->where('code', $request->coupon)->first();

        /** Kiểm tra và báo lỗi */
        $checkCouponErrors = $this->checkCouponErrors($coupon);
        if ($checkCouponErrors['error']) {
            session()->forget('coupon');
            return back()->withErrors(['coupon' => $checkCouponErrors['error_message']]);
        }

        $discount_obj = $this->checkCouponCode($coupon);
        $discount = $discount_obj->discount;
        if ($discount <= 0) {
            session()->forget('coupon');
            return back()->withErrors(['coupon' => 'Không có sản phẩm nào trong giỏ hàng được áp dụng khuyến mãi này.']);
        }

		// Lấy đơn vị để hiển thị
        $unit = 'đ';
        if ($discount_obj->unit == 1) {
            $unit = '%';
        }

        session(['coupon' => (object) [
            'id'  => $coupon->id,
            'code'  => $request->coupon,
            'discount' => $discount,
            'unit' => $unit,
            'type' => $discount_obj->type, //1: giảm bình thường, 2: giảm phí ship
        ]]);

        if (session()->has('coupon')) {
            // Cập nhật số lượng giới hạn mã khuyến mãi
            if (!is_null($coupon->limited)) {
                $coupon->limited -= 1;
                $coupon->save();
            }
        }

        return back();
    }

    public function calcDiscount($coupon, $price, $qty = 1)
    {
        if ($coupon->unit == 'static')
            $discount = $coupon->discount;
        else
            $discount = ($price * $coupon->discount) / 100;
        return $discount * $qty;
    }

    public function checkCouponCode($coupon)
    {
        $result = new stdClass();
        $result->type = 1; //cupon for discount
        $discount = 0;

        /** Lấy danh sách các đối tương được áp dụng khuyến mãi */
        $coupon_targets = $coupon->fk_targets;

        /** Áp dụng tất cả đơn hàng */
        if ($coupon->target_type === 0) {
            $discount = $this->calcDiscount($coupon, Cart::subtotal(0, ',', ''));
        }

        /** Áp dụng cho sản phẩm */
        if ($coupon->target_type === 1) {
            foreach (Cart::content() as $cartItem) {
                if ($coupon_targets->pluck('target_id')->contains($cartItem->id)) {
                    $discount += $this->calcDiscount($coupon, $cartItem->price, $cartItem->qty);
                }
            }
        }

        /** Áp dụng cho nhóm sản phẩm */
        if ($coupon->target_type === 2) {
            foreach ($coupon_targets as $target) {
                $productGroup = \App\ProductGroup::find($target->target_id)->fk_products();
                foreach (Cart::content() as $cartItem) {
                    if ($productGroup->contains($cartItem->id)) {
                        $discount += $this->calcDiscount($coupon, $cartItem->price, $cartItem->qty);
                    }
                }
            }
        }

        /** Áp dụng cho danh mục sản phẩm */
        if ($coupon->target_type === 3) {
            foreach ($coupon_targets as $target) {
                foreach (Cart::content() as $cartItem) {
                    if (Product::where('id', $cartItem->id)->where('category_id', $target->target_id)->get()->count() > 0) {
                        $discount += $this->calcDiscount($coupon, $cartItem->price, $cartItem->qty);
                    }
                }
            }
        }

        /** Áp dụng cho thương hiệu sản phẩm */
        if ($coupon->target_type === 4) {
            foreach ($coupon_targets as $target) {
                foreach (Cart::content() as $cartItem) {
                    if (Product::where('id', $cartItem->id)->where('brand_id', $target->target_id)->get()->count() > 0) {
                        $discount += $this->calcDiscount($coupon, $cartItem->price, $cartItem->qty);
                    }
                }
            }
        }
        /**ducngo */
        /** Áp dụng cho giảm phí ship */
        if ($coupon->target_type === 5) {
            $result->type = 2; //cupon for discount
            $discount = $coupon->discount;
        }
        $result->discount = $discount;
        $result->unit = $coupon->unit;
        return $result;
    }

    public function checkCouponErrors($coupon)
    {
        if (is_null($coupon))
            return array(
                'error' => true,
                'error_message' => 'Mã khuyến mãi không tồn tại.'
            );

        // if (count($coupon) == 0)
        //     return array(
        //         'error' => true,
        //         'error_message' => 'Mã khuyến mãi không tồn tại.'
        //     );

        if (\App\CouponUser::where('coupon_id', $coupon->id)->where('user_id', \Auth::guard('customer')->user()->id)->get()->count() > 0) {
            return array(
                'error' => true,
                'error_message' => 'Mã khuyến mãi chỉ sử dụng 1 lần cho mỗi khách hàng.'
            );
        }

        if (!is_null($coupon->limited) && $coupon->limited <= 0)
            return array(
                'error' => true,
                'error_message' => 'Mã khuyến mãi này đã được phát hết.'
            );

        if (date('U', strtotime($coupon->started_at)) > date('U') || date('U', strtotime($coupon->expired_at)) < date('U'))
            return array(
                'error' => true,
                'error_message' => 'Mã khuyến mãi không áp dụng ở thời điểm hiện tại.'
            );

        if ($coupon->min_value > Cart::subtotal(0, ',', ''))
            return array(
                'error' => true,
                'error_message' => 'Mã khuyến mãi này chỉ áp dụng đối với đơn hàng trị giá tối thiểu ' . number_format($coupon->min_value, 0, ',', '.') . 'đ'
            );

        return false;
    }
}
