<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Product API 
 */
use App\AccessCode;
use App\Product;
class ProductApiController extends ApiController
{
    public function productUpdate(Request $request)
    {
        $data = $request->all();
        if(empty($data) || !is_array($data)){
            $mes = ['errorCode'=>'Format data not supported.'];
            return $this->sendErrorResponse($mes);
        }
        if(empty($data['accessCode'])){
        	$mes = ['accessCode'=>'Access Code is required'];
        	return $this->sendErrorResponse($mes);
        }
        if(empty($data['productCode'])){
        	$mes = ['productCode'=>'productCode is required'];
        	return $this->sendErrorResponse($mes);
        }
        if(empty($data['productName'])){
        	$mes = ['productName'=>'productName is required'];
        	return $this->sendErrorResponse($mes);
        }
        try {
        	$accessCode = AccessCode::where('access_code',$data['accessCode'])->first();
        	if(empty($accessCode)){
        		$mes = ['accessCode'=>'Access Code expired or invalid'];
        		return $this->sendErrorResponse($mes);
        	}
        } catch (\Illuminate\Database\QueryException $e) {
    		$mes = ['accessCode'=>'Access Code expired or invalid'];
    		return $this->sendErrorResponse($mes);
        }
        
        $product = Product::where('external_id',$data['productCode'])->first();
        if(empty($product)){
        	$mes = ['productCode'=>"Could not find product with productCode '".$data['productCode']."'"];
        	return $this->sendErrorResponse($mes);
        }
        $product->name = $data['productName'];
        try {
        	$update =  $product->save();
        	if(empty($update)){
        		$mes = ['productCode'=>"Could not update with productCode '".$data['productCode']."'"];
        		return $this->sendErrorResponse($mes);
        	}
        } catch (\Illuminate\Database\QueryException $e) {
        	$mes = ['productCode'=>"Could not update with productCode '".$data['productCode']."'"];
        	return $this->sendErrorResponse($mes);
        }
        
        $result = ['productCode'=>$data['productCode']];
        return $this->sendSuccessResponse($result);
    }
}
