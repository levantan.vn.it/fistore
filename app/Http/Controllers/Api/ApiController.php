<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Base API 
 * by: ducngo
 */
class ApiController extends Controller
{
    /**
     * Dynamic data for request
     */
    public $data = null;
    /**
     * Time execution
     */
    public $time = null;
    /**
     * Status
     * ok
     * error
     */
    public $status = null;
    /**
     * Message
     * ok => empty
     * error => message error
     */
    public $message = null;

    /**
     * uniqid for tracer
     */
    public $request_id = null;

    function __construct()
    {
        $this->time = $this->getTimeToMicroseconds();
        //var_dump($this->time);
        $this->request_id = uniqid();
        $this->status = "ok";
    }
    /**
     * Build data for resquest with:
     * Time execution
     * Status
     * Data
     */
    public function send_response()
    {
        $result = (object) array(
            "request_id" => $this->request_id,
            "status" => $this->status,
            "time" => round($this->getTimeMili($this->getTimeToMicroseconds(), $this->time),3),
            "message" => $this->message,
            "data" => $this->data
        );
        return response()->json($result);
    }

    /**
     * Send error respon
     */
    public function sendErrorResponse($message)
    {
        $result = array(
            'success' => false,
            'errorMessages' => $message,
            'code' => 400,
        );
        return response()->json($result);
    }
    public function sendSuccessResponse($data)
    {
        $result = array(
            'success' => true,
            'data' => $data,
            'code' => 200,
        );
        return response()->json($result);
    }

    public function log($mess)
    {
        print_r($mess);
    }
    /**
     * Format 'Y-m-d H:i:s.u'
     * Return diff milisecond
     */
    private function getTimeMili($date1, $date2)
    {
        $date1 = \DateTime::createFromFormat('Y-m-d H:i:s.u', $date1);
        $date2 = \DateTime::createFromFormat('Y-m-d H:i:s.u', $date2);

        $interval = $date2->diff($date1);

        return  $interval->s + $interval->f;
    }
    /**
     * Get current time
     */
    private function getTimeToMicroseconds() {
        $t = microtime(true);
        $micro = sprintf("%06d", ($t - floor($t)) * 1000000);
        $d = new \DateTime(date('Y-m-d H:i:s.' . $micro, $t));
    
        return $d->format("Y-m-d H:i:s.u"); 
    }
}
