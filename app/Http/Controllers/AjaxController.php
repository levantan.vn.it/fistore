<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use File;

class AjaxController extends Controller
{
    public function getDistrict(Request $request)
    {
        $html = '';
        if ($request->ajax()) {
            if($request->filled('province_id')){
                $districts = \App\District::where('province_id', $request->province_id)->get();
                $html = '<option value="">Chọn Quận/Huyện</option>';
                foreach($districts as $item) {
                    $html.= '<option value="' . $item->id . '">' . $item->name . '</option>';
                }
            }else{
                $html = '<option value=""></option>';
            }
        }
        echo $html;
    }

    public function getWard(Request $request)
    {
        $html = '';
        if ($request->ajax()) {
            if($request->filled('district_id')){
                $wards = \App\Ward::where('district_id', $request->district_id)->get();
                $html = '<option value="">Chọn Phường/Xã</option>';
                foreach($wards as $item) {
                    $html.= '<option value="' . $item->id . '">' . $item->name . '</option>';
                }
            }else{
                $html = '<option value=""></option>';
            }
        }
        echo $html;
    }
    public function deliveryOrder()
    {
        $name_api = "API Delivery Order";
        try{
            $data_customer = session('data_customer');
            $check = $this->customerUpdate($data_customer);
            if($check){

                $product_delivery = session('product_delivery');
                $orderDetail = session('order');
                
                if(empty($product_delivery) || empty($orderDetail)){
                    $data_log = [
                        'name'  =>  $name_api,
                        'message'   =>  "Lỗi không tìm thấy order đồng bộ",
                        'status'    =>  2
                    ];
                    $this->save_logs($data_log);
                    session()->forget('data_customer');
                    session()->forget('product_delivery');
                    session()->forget('order');
                    // save log lỗi
                    echo 'false0';return;
                }
                $data_result = [];
                $url = 'services/apexrest/afrfistore/deliveryOrder';
                $paymentMethod = "Cash";
                $handOverMethod = "Drop-Off";
                $cargoInsuranceType = "Premium(100% Value)";
                $purchaseDate = date('Y-m-d');
                $deliveryTargetDate = date('Y-m-d', strtotime($purchaseDate . ' +3 day'));
                $productLocator  = "DMNC Warehouse";
                $shippingService = "SDP(DHL Parcel Metro)";
                $shippingTariff = $orderDetail['transport_price'];
                $customerCode = $orderDetail['user_id'];
                $vasType = "Testing";
                $vasRequirement = "Test Requirement";
                $cod = $orderDetail['total'];
                $deliveryAddress = $orderDetail['address'];
                $params = [
                    'webOrderCode'   =>  $orderDetail['code'],
                    'products'  =>  $product_delivery,
                    'productLocator'   =>  $productLocator,
                    'customerCode'   =>  (string)$customerCode,
                    'vasType'   =>  $vasType,
                    'vasRequirement'   =>  $vasRequirement,
                    'shippingService'   =>  $shippingService,
                    'cod'   =>  number_format($cod,0,'.',''),
                    'shippingTariff'   =>  number_format($shippingTariff,0,'.',''),
                    'purchaseDate'   =>  $purchaseDate,
                    'deliveryTargetDate'   =>  $deliveryTargetDate,
                    'deliveryAddress'   =>  $deliveryAddress,
                    'paymentMethod'   =>  $paymentMethod,
                    'handOverMethod'   =>  $handOverMethod,
                    'cargoInsuranceType'   =>  $cargoInsuranceType

                ];
                if($check === 123){
                    $data_log = [
                        'name'  =>  $name_api,
                        'message'   =>   'Lỗi quá tải server. Bạn vui lòng gửi lại api.',
                        'status'    =>  2,
                        'data'  =>  serialize($params),
                        'url'   =>  "https://dmnc-afr.my.salesforce.com/".$url,
                        'send_again'    => 1,
                        'created_at'    =>  date('Y-m-d H:i:s'),
                        'updated_at'    =>  date('Y-m-d H:i:s')
                    ];
                    $this->save_logs($data_log);
                    return "false5";
                }
                // dd($params);
                $access_totken = $this->getAccessTokenByDB();
                // $access_totken = "00DR0000001znou!AQcAQGgbujba0eAAhQB6lDdGlf_K6GWb2CCoMO71BhBbVImlCZeO1laCYlk2F9_UBMhH.TNmCfG8WSVWi7PuxKpTwOhhMxpV";
                $result = $this->getAPI($url,$params,$access_totken,$name_api);
                if(empty($result)){
                    return false;
                }
                if(!empty($result[0]) && !empty($result[0]['errorCode']) && $result[0]['errorCode'] == 'INVALID_SESSION_ID'){
                    // Save log lỗi access token vào get lại access token mới
                    $data_log = [
                        'name'  =>  "API Delivery Order",
                        'message'   =>  "Lỗi access token hết hạn. Tự động lấy mã token mới",
                        'status'    =>  2
                    ];
                    $this->save_logs($data_log);
                    $access_totken = $this->getAccessToken();
                    $result = $this->getAPI($url,$params,$access_totken,$name_api);
                }
                if(empty($result)){
                    return false;
                }
                if(empty($result['success'])){
                    $message = "Lỗi đồng bộ thông tin khách hàng thất bại.";
                    if(!empty($result['errorMessages']['products'])){
                        $message = $result['errorMessages']['products'];
                    }elseif(!empty($result['errorMessages']['webOrderCode'])){
                        $message = $result['errorMessages']['webOrderCode'];
                    }elseif(!empty($result['errorMessages']['shippingTariff'])){
                        $message = $result['errorMessages']['shippingTariff'];
                    }elseif(!empty($result['errorMessages']['paymentMethod'])){
                        $message = $result['errorMessages']['paymentMethod'];
                    }elseif(!empty($result['errorMessages']['cod'])){
                        $message = $result['errorMessages']['cod'];
                    }elseif(!empty($result['errorMessages']['deliveryAddress'])){
                        $message = $result['errorMessages']['deliveryAddress'];
                    }
                    $data_log = [
                        'name'  =>  $name_api,
                        'message'   =>  $message,
                        'status'    =>  2
                    ];
                    $this->save_logs($data_log);
                    session()->forget('data_customer');
                    session()->forget('product_delivery');
                    session()->forget('order');
                    // save log lỗi
                    echo 'false1';return;
                }
                session()->forget('data_customer');
                session()->forget('product_delivery');
                session()->forget('order');
                $orderResult = $result['data'];
                echo 'true';return;
            }else{
                session()->forget('data_customer');
                session()->forget('product_delivery');
                session()->forget('order');
                echo 'false2';return;
                // save log error
            }
        }catch (\Exception $e) {
            $data_log = [
                'name'  =>  "API Delivery Order",
                'message'   =>  $e->getMessage(),
                'status'    =>  2
            ];
            $this->save_logs($data_log);
            return "false4";
        }
            
        
    }

    public function customerUpdate($data)
    {
        $name_api = "API Customer Update";
        try{
            if(empty($data)){
                return false;
            }
            $data_result = [];
            $url = 'services/apexrest/afrfistore/customerUpdate';
            $params = [
                'customerName'   =>  $data['name'],
                'customerCode'  =>  (string)$data['id'],
                'phoneNumber'   =>  $data['phone'],
                'email'   =>  $data['email'],
                'gender'   =>  $data['gender'] == 1?'Male':'Female',
                'dateOfBirth'   =>  date('Y-m-d',strtotime($data['date_of_birth']))

            ];
            $access_totken = $this->getAccessTokenByDB();
            $result = $this->getAPI($url,$params,$access_totken,$name_api);

            if(!empty($result[0]) && !empty($result[0]['errorCode']) && $result[0]['errorCode'] == 'INVALID_SESSION_ID'){
                // Save log lỗi access token vào get lại access token mới
                $data_log = [
                    'name'  =>  "API Customer Update",
                    'message'   =>  "Lỗi access token hết hạn. Tự động lấy mã token mới",
                    'status'    =>  2
                ];
                $this->save_logs($data_log);
                $access_totken = $this->getAccessToken();
                $result = $this->getAPI($url,$params,$access_totken,$name_api);
            }
            if(empty($result)){
                return 123;
            }
            if(empty($result['success'])){
                $message = "Lỗi đồng bộ thông tin khách hàng thất bại.";
                if(!empty($result['errorMessages']['customerCode'])){
                    $message = $result['errorMessages']['customerCode'];
                }elseif(!empty($result['errorMessages']['customerName'])){
                    $message = $result['errorMessages']['customerName'];
                }elseif(!empty($result['errorMessages']['phoneNumber'])){
                    $message = $result['errorMessages']['phoneNumber'];
                }elseif(!empty($result['errorMessages']['email'])){
                    $message = $result['errorMessages']['email'];
                }elseif(!empty($result['errorMessages']['gender'])){
                    $message = $result['errorMessages']['gender'];
                }elseif(!empty($result['errorMessages']['dateOfBirth'])){
                    $message = $result['errorMessages']['dateOfBirth'];
                }
                $data_log = [
                    'name'  =>  "API Customer Update",
                    'message'   =>  $message,
                    'status'    =>  2
                ];
                $this->save_logs($data_log);
                // save log lỗi
                return false;
            }
            $data_log = [
                'name'  =>  "API Customer Update",
                'message'   =>   'Đồng bộ thông tin khách hàng('.$data['name'].') thành công.',
                'status'    =>  1
            ];
            $this->save_logs($data_log);
            $customerId = $result['data'];
            // save log success
            return true;
        }catch (\Exception $e) {
            $data_log = [
                'name'  =>  "API Customer Update",
                'message'   =>  $e->getMessage(),
                'status'    =>  2
            ];
            $this->save_logs($data_log);
            return false;
        }
    }
}


