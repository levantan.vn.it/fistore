<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

use App\Sales;
use App\Product;
use App\ProductCategory;

use DB;

class SaleController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/sales/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];

    public function index(Request $request){
        $this->authorize('sale-access');
        $data = Sales::orderBy('created_at', 'desc')->get();
        return view('admin.sale-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('sale-access');
        $products = Product::where('is_combo', false)->whereTrashed(false)->whereDrafted(false)->get();
        return view('admin.sale-create', compact('products'));
    }

    public function store(Request $request)
    {
        $this->authorize('sale-access');
    
        if(!isset($request->price_down)){
            $request->price_down = 0;
        }

        $data               = new Sales;
        $data->name         = $request->name;
        $data->slug         = $this->generateSlug($request->name);
        $data->price_type   = $request->price_type;
        $data->price_down   = str_replace(',', '', $request->price_down);
        $data->image_border = '';
        $data->started_at   = substr($request->started_at, 6, 4) . '-' . substr($request->started_at, 3, 2) . '-' . substr($request->started_at, 0, 2) . ' ' . str_replace(' ', '', $request->started_at_time);
        $data->expired_at   = substr($request->expired_at, 6, 4) . '-' . substr($request->expired_at, 3, 2) . '-' . substr($request->expired_at, 0, 2) . ' ' . str_replace(' ', '', $request->expired_at_time);
        $data->status = $request->has('status') ? 1 : 0;
        $data->limit = $request->limit;

        
        $data->save();

        $data->fk_products()->sync($request->products);

        return redirect()->route('admin.sale.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã thêm chương trình khuyến mãi mới.'
        ]);
    }

    public function generateSlug($name)
    {
        $slug = str_slug($name);

        $count = DB::table('sales')->where('slug', $slug);

        if(!$count){
            return $slug;
        }else{
            $i = 2;
            
            while (DB::table('sales')->where('slug', $slug . '-' . $i)->count() > 0) {
                $i++;
            }

            return $slug . '-' . $i;
        }
    }

    public function edit($id)
    {
        $this->authorize('sale-access');
        $data = Sales::find($id);
        $products = Product::where('is_combo', false)->whereTrashed(false)->whereDrafted(false)->get();
        return view('admin.sale-edit', compact('data','products'));
    }

    public function update(Request $request, $id)
    {

        $this->authorize('sale-access');
        $data = Sales::find($id);
    
        if(!isset($request->price_down)){
            $request->price_down = 0;
        }

        $data->name         = $request->name;
        $data->price_type   = $request->price_type;
        $data->price_down   = str_replace(',', '', $request->price_down);
        $data->started_at   = substr($request->started_at, 6, 4) . '-' . substr($request->started_at, 3, 2) . '-' . substr($request->started_at, 0, 2) . ' ' . str_replace(' ', '', $request->started_at_time);
        $data->expired_at   = substr($request->expired_at, 6, 4) . '-' . substr($request->expired_at, 3, 2) . '-' . substr($request->expired_at, 0, 2) . ' ' . str_replace(' ', '', $request->expired_at_time);
        $data->status       = $request->has('status') ? 1 : 0;
        $data->limit = $request->limit;

        /** Đăng lên đầu trang */
        $data->save();
    
        
   

        /** Insert Contain Product */
        $data->fk_products()->sync($request->products);

        return redirect()->route('admin.sale.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin chương trình khuyến mãi.'
        ]);
    }

    public function delete(Request $request, $id)
    {
        $this->authorize('sale-access');
        $data = Sales::find($id);
        /** Xóa ảnh thumbnail */
        if(File::exists($this->path.$data->image_border)) File::delete($this->path.$data->image_border);
        /** Xóa sản phẩm */
        Sales::destroy($id);

        //$data->fk_products()->delete();

        return redirect()->route('admin.sale.index')->with([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã xóa bỏ 1 chương trình khuyến mãi.'
        ]);
    }
}
