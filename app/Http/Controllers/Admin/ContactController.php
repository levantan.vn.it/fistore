<?php

namespace App\Http\Controllers\Admin;

use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index()
    {
        $this->authorize('contact-access');
        $data = Contact::orderBy('created_at', 'desc')->get();
        return view('admin.contact-list', compact('data'));
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('contact-access');
        if($request->ajax()){
            Contact::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
