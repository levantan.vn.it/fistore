<?php

namespace App\Http\Controllers\Admin;

use App\SiteConfig;
use App\Ward;
use App\District;
use App\Province;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ConfigurationController extends Controller
{
    protected $path = 'media/';

    public function show()
    {
        $this->authorize('config-access');
    	$data = SiteConfig::find(1);
    	return view('admin.config-show', compact('data'));
    }

    public function edit()
    {
        $this->authorize('config-access');
    	$data = SiteConfig::find(1);
        $provinces = Province::all();
        $districts = District::where('province_id', $data->province_id)->get();
        $wards = Ward::where('district_id', $data->district_id)->get();
    	return view('admin.config-edit', compact('data', 'provinces', 'districts', 'wards'));
    }

    public function update(Request $request)
    {
        $this->authorize('config-access');
    	$data = SiteConfig::find(1);

        /** Upload Logo */
        $logo = $data->logo;
        if($request->hasFile('logo')) {
            /** Xóa logo cũ */
            if(File::exists($this->path.$data->logo) && $data->logo != 'logo.png') File::delete($this->path.$data->logo);
            /** Upload logo mới */
            $file = $request->file('logo');
            $name = str_slug($request->name) . '-logo-' . rand(1000, 9999);
            $logo = upload_file($file, $name, $this->path, [1024, 1024]);
        }

        /** Upload Favicon */
        $favicon = $data->favicon;
        if($request->hasFile('favicon')) {
            /** Xóa favicon cũ */
            if(File::exists($this->path.$data->favicon) && $data->favicon != 'favicon.png') File::delete($this->path.$data->favicon);
            /** Upload favicon mới */
            $file = $request->file('favicon');
            $name = str_slug($request->name) . '-favicon-' . rand(1000, 9999);
            $favicon = upload_file($file, $name, $this->path, [128, 128]);
        }

        $data->name = $request->name;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->keywords = $request->keywords;
        $data->domain = $request->domain;
        $data->author = $request->author;
        $data->introduce = $request->introduce;
        $data->contact_email = $request->contact_email;
        $data->support_email = $request->support_email;
        $data->hotline_1 = $request->hotline_1;
        $data->hotline_2 = $request->hotline_2;
        $data->fax = $request->fax;
        $data->address = $request->address;
        $data->province_id = $request->province;
        $data->district_id = $request->district;
        $data->ward_id = $request->ward;
        $data->map = $request->map ?? null;
        $data->logo = $logo;
        $data->favicon = $favicon;
        $data->save();

        return back()->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!.',
            'msg'   =>  'Đã lưu thay đổi.'
        ]);
    }
}
