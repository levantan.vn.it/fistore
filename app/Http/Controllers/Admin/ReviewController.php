<?php

namespace App\Http\Controllers\Admin;

use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('review-access');
        if($request->has('trash'))
            $data = Review::where('trashed', 1);
        else
            $data = Review::where('trashed', 0);
        $data = $data->orderBy('created_at', 'desc')->get();

        return view('admin.review-list', compact('data'));
    }

    public function trash(Request $request)
    {
        $this->authorize('review-access');
        if($request->ajax()){
            Review::whereId($request->id)->update(['trashed' => true]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã chuyển 1 mục vào thùng rác.'
            ]);
        }
    }

    public function restore(Request $request)
    {
        $this->authorize('review-access');
        if($request->ajax()){
            Review::whereId($request->id)->update(['trashed' => false]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã khôi phục 1 mục.'
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('review-access');
        if($request->ajax()){
            Review::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
