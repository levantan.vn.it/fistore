<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\ProductOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('customer-access');

        $query = User::whereNotNull('id');

        if($request->filled('email'))
            $query->where('email', 'LIKE', '%'.$request->email.'%');
        if($request->filled('name'))
            $query->where('name', 'LIKE', '%'.str_replace(' ', '%', $request->name).'%');
        if($request->filled('provider')) {
            if($request->provider == 'null')
                $query->whereNull('provider');
            else
                $query->where('provider', $request->provider);
        }
        if($request->filled('created_at_from'))
            $query->where('created_at', '>=', substr($request->created_at_from, -4).'-'.substr($request->created_at_from, 3, 2).'-'.substr($request->created_at_from, 0,2));
        if($request->filled('created_at_to'))
            $query->where('created_at', '<=', substr($request->created_at_to, -4).'-'.substr($request->created_at_to, 3, 2).'-'.substr($request->created_at_to, 0,2).' 23:59:59');

        $data = $query->orderBy('created_at', 'desc')->paginate($request->show ?? 25);
        $data->appends($request->input())->links();

        // Export Excel
        if($request->filled('export'))
            return $this->export($data);

        return view('admin.customer-list', compact('data'));
    }
    public function show($id)
    {
        $this->authorize('customer-access');
        $data = User::find($id);

        $orderHistory = ProductOrder::where('user_id', $id)->orderBy('created_at', 'desc')->get();

        $orderCount = ProductOrder::where('user_id', $id)->get()->count();
        $buyingOrderCount = ProductOrder::where('user_id', $id)->where('cancelled', false)->where('status', '<', 3)->get()->count();
        $completedOrderCount = ProductOrder::where('user_id', $id)->where('cancelled', false)->where('status', 3)->get()->count();
        $cancelledOrderCount = ProductOrder::where('user_id', $id)->where('cancelled', true)->get()->count();
        $sumBoughtOrderCost = array_sum(ProductOrder::where('user_id', $id)->where('cancelled', false)->where('status', 3)->get()->pluck('total')->toArray());

        $statistics = [
            'orderCount' => $orderCount,
            'buyingOrderCount' => $buyingOrderCount,
            'completedOrderCount' => $completedOrderCount,
            'cancelledOrderCount' => $cancelledOrderCount,
            'sumBoughtOrderCost' => $sumBoughtOrderCost
        ];
        return view('admin.customer-show', compact('data', 'statistics', 'orderHistory'));
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('customer-access');

        if($request->ajax()){
            if(User::destroy($id)){
                return response()->json([
                    'alert' => 'success',
                    'title' => 'Hoàn tất',
                    'msg'   => 'Đã xóa người dùng.'
                ]);
            }
        }
    }

    public function disable(Request $request)
    {
        $this->authorize('customer-access');

        if($request->ajax()){
            $data = User::find($request->id);
            $data->disabled = true;
            if($data->save()){
                return response()->json([
                    'alert' => 'success',
                    'title' => 'Hoàn tất',
                    'msg'   => 'Đã vô hiệu hóa người dùng.'
                ]);
            }
        }
    }

    public function activate(Request $request)
    {
        $this->authorize('customer-access');

        if($request->ajax()){
            $data = User::find($request->id);
            $data->disabled = false;
            if($data->save()){
                return response()->json([
                    'alert' => 'success',
                    'title' => 'Hoàn tất',
                    'msg'   => 'Đã mở khóa người dùng.'
                ]);
            }
        }
    }

    public function export($data)
    {
        return \Excel::download(new \App\Exports\UserExport($data), 'KhachHang_'.date('U').'.xlsx');
    }

    
}
