<?php

namespace App\Http\Controllers\Admin;

use App\Tag;
use App\Article;
use App\ArticleCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ArticleController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/articles/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];

    public function index(Request $request)
    {
        $this->authorize('article-access');
        if($request->has('trash'))
            $data = Article::where('trashed', 1);
        else
            $data = Article::where('trashed', 0);
        $data = $data->orderBy('reposted_at', 'desc')->orderBy('created_at', 'desc')->get();
        return view('admin.article-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('article-access');
        $categories = ArticleCategory::whereNull('parent_id')->get();
        $tags = Tag::all();
        return view('admin.article-create', compact('categories', 'tags'));
    }

    public function store(Request $request)
    {
        $this->authorize('article-access');

        /** Upload Thumbnail */
        $thumbnail = null;
        if($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        /** Insert Tags */
        $tags = null;
        if($request->filled('tags')) {
            foreach($request->tags as $item) {
                if(Tag::where('keyword', $item)->get()->count() == 0) {
                    $tag = new Tag;
                    $tag->keyword = $item;
                    $tag->slug = str_slug($item);
                    $tag->save();
                }
                $tags = implode(',', $request->tags);
            }
        }

        $data = new Article;
        $data->name = $request->name;
        $data->content = $request->content;
        $data->slug = $request->slug;
        $data->thumbnail = $thumbnail;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->author_id = \Auth::guard('admin')->user()->id;
        $data->tags = $tags;
        $data->hidden = $request->filled('show') ? false : true;
        $data->drafted = $request->filled('draft') ? true : false;
        $data->featured = $request->filled('featured') ? true : false;
        $data->reposted_at = date('Y-m-d H:i:s');
        $data->save();


        /** Insert Contain Categories */
        $data->fk_categories()->sync($request->categories);

        return redirect()->route('admin.article.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã thêm bài viết mới'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('article-access');
        $data = Article::find($id);
        $categories = ArticleCategory::whereNull('parent_id')->get();
        $tags = Tag::all();
        return view('admin.article-edit', compact('data', 'categories', 'tags'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('article-access');
        $data = Article::find($id);

        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if($request->hasFile('thumbnail')) {
            /** Xóa thumbnail cũ */
            if(File::exists($this->path.$data->thumbnail)) File::delete($this->path.$data->thumbnail);
            /** Upload thumbnail mới */
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        /** Insert Tags */
        $tags = null;
        if($request->filled('tags')) {
            foreach($request->tags as $item) {
                if(Tag::where('keyword', $item)->get()->count() == 0) {
                    $tag = new Tag;
                    $tag->keyword = $item;
                    $tag->slug = str_slug($item);
                    $tag->save();
                }
                $tags = implode(',', $request->tags);
            }
        }

        $data->name = $request->name;
        $data->content = $request->content;
        $data->slug = $request->slug;
        $data->thumbnail = $thumbnail;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->tags = $tags;
        $data->hidden = $request->filled('show') ? false : true;
        $data->drafted = $request->filled('draft') ? true : false;
        $data->featured = $request->filled('featured') ? true : false;

        /** Đăng lên đầu trang */
        if($request->filled('reposted')) $data->reposted_at = date('Y-m-d H:i:s');
        $data->save();

        /** Insert Contain Categories */
        $data->fk_categories()->sync($request->categories);

        return redirect()->route('admin.article.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin bài viết.'
        ]);
    }

    public function trash(Request $request)
    {
        $this->authorize('article-access');
        if($request->ajax()){
            Article::whereId($request->id)->update(['trashed' => true]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã chuyển 1 mục vào thùng rác.'
            ]);
        }
    }

    public function restore(Request $request)
    {
        $this->authorize('article-access');
        if($request->ajax()){
            Article::whereId($request->id)->update(['trashed' => false]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã khôi phục 1 mục.'
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('article-access');
        if($request->ajax()){
            $data = Article::find($id);
            /** Xóa ảnh thumbnail */
            if(File::exists($this->path.$data->thumbnail)) File::delete($this->path.$data->thumbnail);
            /** Xóa sản phẩm */
            Article::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
