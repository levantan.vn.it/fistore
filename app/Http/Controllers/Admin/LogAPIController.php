<?php

namespace App\Http\Controllers\Admin;

use App\Tag;
use App\LogAPI;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class LogAPIController extends Controller
{

    public function index(Request $request)
    {
        // $this->authorize('article-access');

        $data = new LogAPI();
        if ($request->filled('name')){
            $data = $data->where('name','LIKE','%'.$request->name.'%');
        }
        if ($request->filled('message')){
            $data = $data->where('message','LIKE','%'.$request->message.'%');
        }
        if ($request->filled('message')){
            if($request->status == 'error'){
                $data = $data->where('status',2);
            }else{
                $data = $data->where('status',1);
            }
            
        }
        $data = $data->orderBy('id','desc');
        $data = $data->paginate(15);
        return view('admin.logs.list', compact('data'));
    }

    public function sendAgain(Request $request)
    {
        $data = LogAPI::find($id);
        $data->send_again = 0;
        $data->save();
        if(empty($data)){
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi',
                'msg' => 'Không tìm thấy log.'
            ]);
        }
        $data_send = $data->data;
        if(empty($data_send)){
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi',
                'msg' => 'Log này không có data để gửi lại.'
            ]);
        }
        if(empty($data->url)){
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi',
                'msg' => 'Log này không có url để gửi lại.'
            ]);
        }
        $url = $data->url;
        $name_api = $data->name;
        $access_totken = $this->getAccessTokenByDB();
        // $access_totken = "00DR0000001znou!AQcAQGgbujba0eAAhQB6lDdGlf_K6GWb2CCoMO71BhBbVImlCZeO1laCYlk2F9_UBMhH.TNmCfG8WSVWi7PuxKpTwOhhMxpV";
        $result = $this->getAPI($url,$params,$access_totken,$name_api);
        if(!empty($result[0]['errorCode']) && $result[0]['errorCode'] == 'INVALID_SESSION_ID'){
            // Save log lỗi access token vào get lại access token mới
            $data_log = [
                'name'  =>  "API Stock Search",
                'message'   =>  "Lỗi access token hết hạn. Tự động lấy mã token mới",
                'status'    =>  2
            ];
            $this->save_logs($data_log);
            $access_totken = $this->getAccessToken();
            $result = $this->getAPI($url,$params,$access_totken,$name_api);
        }
        if(empty($result)){
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi',
                'msg' => "Kết nối quá tải. Vui lòng thử lại."
            ]);
        }
        if(empty($result['success'])){
            $message = 'Đã có lỗi xảy ra.';
            if(!empty($result['errorMessages']) && count($$result['errorMessages'])){
                foreach ($result['errorMessages'] as $key => $value) {
                    $message = $value;
                }
            }
            $data_log = [
                'name'  =>  $name_api,
                'message'   =>  $message,
                'status'    =>  2
            ];
            $this->save_logs($data_log);
        }else{
            $data_log = [
                'name'  =>  $name_api,
                'message'   =>   "Gọi lại api: ".$name_api." thành công",
                'status'    =>  1
            ];
            $this->save_logs($data_log);
        }

        
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('article-access');
        if($request->ajax()){
            $data = Article::find($id);
            /** Xóa ảnh thumbnail */
            if(File::exists($this->path.$data->thumbnail)) File::delete($this->path.$data->thumbnail);
            /** Xóa sản phẩm */
            Article::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
