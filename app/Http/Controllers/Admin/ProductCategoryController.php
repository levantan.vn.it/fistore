<?php

namespace App\Http\Controllers\Admin;

use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ProductCategoryController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/product-categories/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];

    public function index()
    {
        $this->authorize('product-cat-access');
        $data = ProductCategory::whereNull('parent_id')->get();
        return view('admin.product-category-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('product-cat-access');
        $categories = ProductCategory::whereNull('parent_id')->get();
        return view('admin.product-category-create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->authorize('product-cat-access');

        /** Upload Thumbnail */
        $thumbnail = null;
        if($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        $data = new ProductCategory;
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->thumbnail = $thumbnail;
        $data->pinned = $request->has('pinned') ? true : false;
        $data->save();

        return redirect()->route('admin.product.category.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã tạo mới chuyên mục sản phẩm.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('product-cat-access');
        $data = ProductCategory::find($id);
        $categories = ProductCategory::whereNull('parent_id')->where('id', '!=', $id)->get();
        return view('admin.product-category-edit', compact('data', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('product-cat-access');
        $data = ProductCategory::find($id);

        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if($request->hasFile('thumbnail')) {
            /** Xóa thumbnail cũ */
            if(File::exists($this->path.$data->thumbnail)) File::delete($this->path.$data->thumbnail);
            /** Upload thumbnail mới */
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->thumbnail = $thumbnail;
        $data->pinned = $request->has('pinned') ? true : false;
        $data->save();

        return redirect()->route('admin.product.category.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin chuyên mục sản phẩm.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('product-cat-access');
        if($request->ajax()){
            ProductCategory::destroy($id);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function checkNameExists(Request $request)
    {
      return response()->json(true);
        // if($request->ajax()) {
        //     $query = ProductCategory::whereName($request->name);
        //     if($request->has('id'))
        //         $query->where('id', '!=', $request->id);
        //
        //     if($query->get()->count() == 0)
        //         return response()->json(true);
        // }
        // return response()->json('Mục này đã tồn tại.');
    }

    public function checkSlugExists(Request $request)
    {
        if($request->ajax()) {
            $query = ProductCategory::whereSlug($request->slug);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0)
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }
}
