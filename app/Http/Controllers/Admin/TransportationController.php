<?php

namespace App\Http\Controllers\Admin;

use App\Province;
use App\District;
use App\Ward;
use App\Transportation;
use App\TransportationArea;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransportationController extends Controller
{
    public function index()
    {
        $this->authorize('transportation-access');
        $data = Transportation::all();
        $provinces = Province::all();

        return view('admin.transportations', compact('data', 'provinces'));
    }

    public function store(Request $request)
    {
        $this->authorize('transportation-access');
        $data = new Transportation;
        $data->province_id = $request->province_id;
        $data->name = $request->name;
        $data->min_order = str_replace(',', '', $request->min_order);
        $data->max_order = $request->filled('max_order') ? str_replace(',', '', $request->max_order) : 99999999999;
        $data->price = str_replace(',', '', $request->price);
        $data->save();

        $districts = District::where('province_id', $request->province_id)->get();
        foreach($districts as $district) {
            $area = new TransportationArea;
            $area->transport_id = $data->id;
            $area->province_id = $request->province_id;
            $area->district_id = $district->id;
            $area->min_order = $data->min_order;
            $area->max_order = $data->max_order;
            $area->price = $data->price;
            $area->save();
        }

        return back()->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã thêm khu vực vận chuyển mới'
        ]);
    }

    public function update(Request $request, $id)
    {
        $this->authorize('transportation-access');
        $data = Transportation::find($id);
        $data->name = $request->name;
        $data->min_order = str_replace(',', '', $request->min_order);
        $data->max_order = $request->filled('max_order') ? str_replace(',', '', $request->max_order) : 99999999999;
        $data->price = str_replace(',', '', $request->price);
        $data->save();

        if($id > 0) {
            foreach($data->fk_transportation_areas as $area){
                $area = TransportationArea::find($area->id);
                $area->district_id = $request->district_id[$area->id];
                $area->price = str_replace(',', '', $request->district_price[$area->id]);
                $area->denied = !empty($request->district_deny[$area->id]) ? true : false;
                $area->save();
            }
        }

        return back()->with([
            'alert' => 'success', 
            'title' => 'Hoàn tất', 
            'msg' => 'Đã cập nhật thông tin vận chuyển.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('transportation-access');
        if($request->ajax()){
            Transportation::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function setting(Request $request)
    {
        $this->authorize('transportation-access');
        $setting = \App\Setting::where('key', 'default_shipping_price')->first();
        if($request->has('default_shipping_price'))
            $setting->value = str_replace(',', '', $request->default_shipping_price);
        $setting->disabled = $request->disabled ? true : false;
        $setting->save();
        return back()->with([
            'alert' => 'success', 
            'title' => 'Hoàn tất', 
            'msg' => 'Đã lưu cài đặt vận chuyển.'
        ]);
    }
}
