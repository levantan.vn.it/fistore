<?php

namespace App\Http\Controllers\Admin;

use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExtraPointController extends Controller
{
    public function show()
    {
        $this->authorize('extra-point-access');
        $money_to_extra_point = Setting::where('key', 'money_to_extra_point')->first()->value;
        $extra_point_to_money = Setting::where('key', 'extra_point_to_money')->first()->value;
    	return view('admin.extra-point-show', compact('money_to_extra_point', 'extra_point_to_money'));
    }

    public function update(Request $request)
    {
        $this->authorize('extra-point-access');
        $money_to_extra_point = Setting::where('key', 'money_to_extra_point')->update(['value' => str_replace(',', '', $request->money_to_extra_point)]);
    	$extra_point_to_money = Setting::where('key', 'extra_point_to_money')->update(['value' => str_replace(',', '', $request->extra_point_to_money)]);

        return back()->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!.',
            'msg'   =>  'Đã lưu thay đổi.'
        ]);
    }
}
