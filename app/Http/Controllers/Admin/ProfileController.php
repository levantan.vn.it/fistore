<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ProfileController extends Controller
{
    public function show()
    {
    	$data = Admin::find(Auth::guard('admin')->user()->id);
    	return view('admin.profile-show', compact('data'));
    }

    public function edit()
    {
        $data = Admin::find(Auth::guard('admin')->user()->id);
    	return view('admin.profile-edit', compact('data'));
    }

    public function update(Request $request)
    {
        $data = Admin::find(Auth::guard('admin')->user()->id);
        $data->name = $request->name;
        $data->phone = $request->phone;
        $data->save();

        return back()->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!.',
            'msg'   =>  'Đã cập nhật hồ sơ.'
        ]);
    }

    public function editPassword()
    {
        return view('admin.profile-password-edit');
    }

    public function updatePassword(Request $request)
    {
        if($request->old_password == $request->password)
            return back()->with(['new_password_error' => 'Mật khẩu mới phải khác mật khẩu cũ.']);

        if(!(\Hash::check($request->old_password, Auth::guard('admin')->user()->password)))
            return back()->with(['password_error' => 'Mật khẫu cũ không chính xác.']);

        $data = Admin::find(Auth::guard('admin')->user()->id);
        $data->password = bcrypt($request->password);
        $data->save();

        return redirect(url('admin/profile'))->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!.',
            'msg'   =>  'Đã cập nhật mật khẩu mới.'
        ]);
    }
}
