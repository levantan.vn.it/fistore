<?php

namespace App\Http\Controllers\Admin;

use App\Page;
use App\PageGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class PageController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/pages/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];

    public function index(Request $request)
    {
        $this->authorize('page-access');
        if($request->has('trash'))
            $data = Page::where('trashed', true)->orderBy('updated_at', 'desc')->get();
        else
            $data = Page::where('trashed', false)->orderBy('created_at', 'desc')->get();
        return view('admin.page-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('page-access');
        $page_groups = PageGroup::whereNull('parent_id')->get();
        return view('admin.page-create', compact('page_groups'));
    }

    public function store(Request $request)
    {
        $this->authorize('page-access');

        /** Upload Thumbnail */
        $thumbnail = null;
        if($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        $data = new Page;
        $data->name = $request->name;
        $data->content = $request->content;
        $data->slug = $request->slug;
        $data->thumbnail = $thumbnail;
        $data->author_id = \Auth::guard('admin')->user()->id;
        $data->group_id = $request->group;
        $data->hidden = $request->has('show') ? false : true;
        $data->drafted = $request->has('draft') ? true : false;
        $data->save();

        return redirect()->route('admin.page.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã thêm trang mới.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('page-access');
        $data = Page::find($id);
        $page_groups = PageGroup::whereNull('parent_id')->get();
        return view('admin.page-edit', compact('data', 'page_groups'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('page-access');
        $data = Page::find($id);

        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if($request->hasFile('thumbnail')) {
            /** Xóa thumbnail cũ */
            if(File::exists($this->path.$data->thumbnail)) File::delete($this->path.$data->thumbnail);
            /** Upload thumbnail mới */
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        $data->name = $request->name;
        $data->content = $request->content;
        $data->slug = $request->slug;
        $data->thumbnail = $thumbnail;
        $data->group_id = $request->group;
        $data->hidden = $request->has('show') ? false : true;
        $data->drafted = $request->has('draft') ? true : false;
        $data->save();

        return redirect()->route('admin.page.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật nội dung trang.'
        ]);
    }

    public function trash(Request $request)
    {
        $this->authorize('page-access');
        if($request->ajax()){
            Page::whereId($request->id)->update(['trashed' => true]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã chuyển 1 mục vào thùng rác.'
            ]);
        }
    }

    public function restore(Request $request)
    {
        $this->authorize('page-access');
        if($request->ajax()){
            Page::whereId($request->id)->update(['trashed' => false]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã khôi phục 1 mục.'
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('page-access');
        if($request->ajax()){
            Page::destroy($id);

            /** Xóa ảnh thumbnail */
            if(File::exists($this->path.$data->thumbnail)) File::delete($this->path.$data->thumbnail);

            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
