<?php

namespace App\Http\Controllers\Admin;

use App\CommentReport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentReportController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('report-access');
        $data = CommentReport::orderBy('created_at', 'desc')->get();

        return view('admin.comment-report-list', compact('data'));
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('report-access');
        if($request->ajax()){
            CommentReport::destroy($id);
            
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }
}
