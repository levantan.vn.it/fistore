<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Setting;


class PopupController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/popups/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];

    public function index(Request $request){
        $this->authorize('popup-access');

        $popupLG = Setting::where('key', 'popup_lg')->first();
        $popupSM = Setting::where('key', 'popup_sm')->first();
        $popupURL = Setting::where('key', 'popup_url')->first();
        $productBorder = Setting::where('key', 'product_border')->first();
     	
     	return view('admin.popup',compact('popupLG', 'popupSM', 'productBorder', 'popupURL'));
    }

    public function savepopup(Request $request){

    	$this->authorize('popup-access');

        if($request->hasFile('popup_lg')) {
            $popupLGFile = $request->file('popup_lg');
            $popupLG = upload_file($popupLGFile, 'popup_lg', $this->path, $this->dimensions);
            $this->upsert('popup_lg', 'POPUP cho Máy tính', $popupLG, $request->has('popup_lg_sts') ? false : true);
        }

        if($request->hasFile('popup_sm')) {
            $popupSMFile = $request->file('popup_sm');
            $popupSM = upload_file($popupSMFile, 'popup_sm', $this->path, $this->dimensions);
            $this->upsert('popup_sm', 'POPUP cho Mobile', $popupSM, $request->has('popup_sm_sts') ? false : true);
        }

        if($request->hasFile('product_border')) {
            $productBorderFile = $request->file('product_border');
            $productBorder = upload_file($productBorderFile, 'product_border', $this->path, $this->dimensions);
            $this->upsert('product_border', 'Product Border', $productBorder, $request->has('product_border_sts') ? false : true);
        }

        if($request->filled('popup_url')) {
            $this->upsert('popup_url', 'POPUP URL', $request->popup_url);
        }

        // Update status
        if($request->has('popup_lg_sts')) {
            $this->updateStatus('popup_lg', false);
        }else{
            $this->updateStatus('popup_lg', true);
        }

        if($request->has('popup_sm_sts')) {
            $this->updateStatus('popup_sm', false);
        }else{
            $this->updateStatus('popup_sm', true);
        }

        if($request->has('product_border_sts')) {
            $this->updateStatus('product_border', false);
        }else{
            $this->updateStatus('product_border', true);
        }

        return redirect()->route('admin.popup')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã lưu thay đổi.'
        ]);

    }

    public function upsert($key, $name, $value, $disabled = false)
    {
        $up = Setting::where('key', $key)->first();
        if(empty($up)) {
            $sert = new Setting;
            $sert->key = $key;
            $sert->name = $name;
            $sert->value = $value;
            $sert->disabled = $disabled;
            $sert->save();
        }else{
            $up->value = $value;
            $up->disabled = $disabled;
            $up->save();
        }
    }

    public function updateStatus($key, $status)
    {
        $up = Setting::where('key', $key)->update(['disabled' => $status]);
    }
}
