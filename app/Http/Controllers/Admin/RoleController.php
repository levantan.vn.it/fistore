<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function index()
    {
        $this->authorize('permission-access');
        $data = Role::all();
        return view('admin.role-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('permission-access');
        return view('admin.role-create');
    }

    public function store(Request $request)
    {
        $this->authorize('permission-access');
        $data = new Role;
        $data->name = $request->name;
        $data->key = $request->key;
        $data->level = $request->level;
        $data->comment = $request->comment;
        $data->save();

        return redirect()->route('admin.role.index')->with([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã tạo nhóm quyền mới.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('permission-access');

        $data = Role::find($id);

        // Kiểm tra cấp độ quyền
        // Nếu cấp quyền thấp hơn thì từ chối truy cập
        if(\Auth::guard('admin')->user()->fk_role->level >= $data->level)
            abort(403);

        return view('admin.role-edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('permission-access');

        $data = Role::find($id);

        // Kiểm tra cấp độ quyền
        // Nếu cấp quyền thấp hơn thì từ chối truy cập
        if(\Auth::guard('admin')->user()->fk_role->level >= $data->level)
            abort(403);

        $data->name = $request->name;
        $data->key = $request->key;
        $data->level = $request->level;
        $data->comment = $request->comment;
        $data->save();

        return redirect()->route('admin.role.index')->with([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã cập nhật thông tin nhóm quyền.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('permission-access');
        if($request->ajax()){
            Role::destroy($id);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ nhóm quyền.'
            ]);
        }
    }

    public function checkKeyExists(Request $request)
    {
        if($request->ajax()){
            $query = Role::where('key', $request->key);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);
            if($query->get()->count() > 0)
                return response()->json('Key đã tồn tại.');
            return response()->json(true);
        }
    }
}
