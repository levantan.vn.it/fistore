<?php

namespace App\Http\Controllers\Admin;

use App\Combo;
use App\Comboprice;
use App\Product;
use App\ProductCategory;
use App\ProductBrand;
use App\Tag;
use App\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ComboController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/products/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];

    public function index(Request $request)
    {
        $this->authorize('combo-access');
        $data = Combo::where('is_combo', true)->orderBy('position')->orderBy('reposted_at', 'desc')->get();
        return view('admin.combo-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('combo-access');
        $products = Product::where('is_combo', false)->whereTrashed(false)->whereDrafted(false)->get();
        $tags = Tag::all();
        $code = $this->makeCodeFormat();
        return view('admin.combo-create', compact('tags', 'code', 'products'));
    }

    public function store(Request $request)
    {
        $this->authorize('combo-access');
        /** Upload Thumbnail */
        $thumbnail = 'default.jpg';
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        } else {
            // TOTO: Nếu không có hình thumnail thì sẽ lấy hình đầu tiên trong ds hình mô tả (nếu có)
            if ($request->hasFile('images')) {
                $file = $request->file('images.0'); // Lấy hình đầu tiên trong ds hình
                $name = $request->slug;
                $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
            }
        }

        /** Insert Tags */
        $tags = null;
        if ($request->has('tags')) {
            foreach ($request->tags as $item) {
                if (Tag::where('keyword', $item)->get()->count() == 0) {
                    $tag = new Tag;
                    $tag->keyword = $item;
                    $tag->slug = str_slug($item);
                    $tag->save();
                }
                $tags = implode(',', $request->tags);
            }
        }

        $data = new Combo;
        $data->name = $request->name;
        $data->code = $request->code ?? $this->makeCodeFormat();
        $data->slug = $request->slug;
        $data->price = str_replace(',', '', $request->price);
        $data->original_price = !empty($request->original_price) ? str_replace(',', '', $request->original_price) : null;
        $data->thumbnail = $thumbnail;
        $data->content = $request->content;
        $data->tags = $tags;
        $data->quantity = $request->quantity;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->is_combo = true;
        $data->hidden = $request->has('show') ? false : true;
        $data->reposted_at = date('Y-m-d H:i:s');
        $data->save();

        /** Insert Contain Product */
        $data->fk_products()->sync($request->products);

        /** Upload Product Images */
        if ($request->hasFile('images')) {
            $i = 0; // Thứ tự của hình, title, index
            foreach ($request->file('images') as $key => $file) {
                $index = $request->input('indexes.' . $i);
                $name = $request->input('names.' . $i);
                if (!$index) {
                    $index = $key + 1;
                }

                if (!$name) {
                    $name = $request->slug . '-' . $index;
                }
                $slug = upload_file($file, $request->name . strtolower(str_random(5)), $this->path, $this->dimensions);

                $media = new Media;
                $media->target_id = $data->id;
                $media->target_type = 'product';
                $media->name = $name;
                $media->size = $file->getClientSize();
                $media->type = $file->getClientMimeType();
                $media->path = $slug;
                $media->index = $index;
                $media->save();
                $i++;
            }
        }

        return redirect()->route('admin.combo.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã thêm gói khuyến mãi mới.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('combo-access');
        $data = Combo::find($id);
        $products = Product::where('is_combo', false)->whereTrashed(false)->whereDrafted(false)->get();
        $tags = Tag::all();
        return view('admin.combo-edit', compact('data', 'tags', 'products'));
    }

    public function addproduct(Request $request, $id)
    {
        $data = new Comboprice;
        $data->combo_id = $id;
        $data->product_id = $request->productss;
        $data->price = str_replace(',', '', $request->pricee);

        if ($data->save()) {
            return 1;
        }
    }

    public function getproduct(Request $request, $id)
    {
        $listproductss = Comboprice::where('combo_id', $id)->get();

        $html = '';

        foreach ($listproductss as $value) {
            $productinfo = Product::find($value['product_id']);
            $html1 = '<tr>
                <td>' . $productinfo->name . '</td>
                <td>' . number_format($value->price) . '</td>
                <td><button class="btn btn-danger" onclick="deletecomboproduct(' . $value->id . ');"><i class="fa fa-trash"></i></button></td>
            </tr>';

            $html .= $html1;
        }

        return $html;
    }

    public function deleteproduct($id)
    {
        if (Comboprice::where('id', $id)->get()) {
            Comboprice::where('id', $id)->delete();

            return 1;
        }
    }

    public function update(Request $request, $id)
    {

        $this->authorize('combo-access');
        $data = Combo::find($id);

        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if ($request->hasFile('thumbnail')) {
            /** Xóa thumbnail cũ */
            if (File::exists($this->path . $data->thumbnail)) File::delete($this->path . $data->thumbnail);
            /** Upload thumbnail mới */
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        /** Insert Tags */
        $tags = null;
        if ($request->filled('tags')) {
            foreach ($request->tags as $item) {
                if (Tag::where('keyword', $item)->get()->count() == 0) {
                    $tag = new Tag;
                    $tag->keyword = $item;
                    $tag->slug = str_slug($item);
                    $tag->save();
                }
                $tags = implode(',', $request->tags);
            }
        }

        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->price = str_replace(',', '', $request->price);
        $data->original_price = !empty($request->original_price) ? str_replace(',', '', $request->original_price) : null;
        $data->thumbnail = $thumbnail;
        $data->content = $request->content;
        $data->tags = $tags;
        $data->quantity = $request->quantity;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->hidden = $request->has('show') ? false : true;

        /** Đăng lên đầu trang */
        if ($request->has('repost')) $data->reposted_at = date('Y-m-d H:i:s');
        $data->save();

        /** Insert Contain Product */
        $data->fk_products()->sync($request->products);

        /** Upload Product Images */
        /** Lấy danh sách ảnh sản phẩm */
        $images = Media::where('target_id', $id)->where('target_type', 'product')->get();
        /** Xóa những ảnh đã loại bỏ */
        if ($request->filled('old_images')) {
            foreach ($images as $image) {
                if (!in_array($image->id, $request->old_images)) {
                    if (File::exists($this->path . $image->path))
                        File::delete($this->path . $image->path);
                    Media::destroy($image->id);
                } else {
                    // TODO: Cập nhật lại thứ tự và tiêu đề của ds hình cũ
                    $image->name = $request->input('image_name' . $image->id);
                    $image->index = $request->input('image_index' . $image->id);
                    $image->save();
                }
            }
        } else {
            foreach ($images as $image) {
                if (File::exists($this->path . $image->path)) File::delete($this->path . $image->path);
                Media::destroy($image->id);
            }
        }

        /** Upload hình ảnh mới */
        if ($request->hasFile('images')) {
            $i = 0; // Thứ tự của hình, title, index
            foreach ($request->file('images') as $file) {
                $index = $request->input('indexes.' . $i);
                $name = $request->input('names.' . $i);
                if (!$index) {
                    $index = Media::where('target_id', $id)->where('target_type', 'product')->max('index') + 1;
                }

                if (!$name) {
                    $name = $request->slug . '-' . $index;
                }
                $slug = upload_file($file, $name . strtolower(str_random(5)), $this->path, $this->dimensions);

                $media = new Media;
                $media->target_id = $id;
                $media->target_type = 'product';
                $media->name = $name;
                $media->size = $file->getClientSize();
                $media->type = $file->getClientMimeType();
                $media->path = $slug;
                $media->index = $index;
                $media->save();
                $i++;
            }
        }

        return redirect()->route('admin.combo.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin gói khuyến mãi.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('combo-access');
        if ($request->ajax()) {
            $data = Combo::find($id);
            /** Xóa ảnh thumbnail */
            if (File::exists($this->path . $data->thumbnail)) File::delete($this->path . $data->thumbnail);
            /** Xóa ảnh sản phẩm */
            $medias = Media::where('target_id', $id)->where('target_type', 'product')->get();
            foreach ($medias as $media) {
                if (File::exists($this->path . $media->path)) File::delete($this->path . $media->path);
                Media::destroy($media->id);
            }
            /** Xóa sản phẩm */
            Combo::destroy($id);

            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function makeCodeFormat()
    {
        $start = 'CB';
        $max = Combo::max('id');
        return $start . make_rand_string($max + 1);
    }

    public function position(Request $request)
    {
        $current = Combo::find($request->id);
        $cur_pos = $current->position;
        $new_pos = $request->position;

        if ($cur_pos < $new_pos) {
            $before = Combo::where('is_combo', true)->where('position', '>', $cur_pos)->where('position', '<=', $new_pos)->where('id', '!=', $current->id)->orderBy('position')->get();
            foreach ($before as $bf) {
                $up = Combo::find($bf->id);
                $up->position -= 1;
                $up->save();
            }
        } else if ($cur_pos > $new_pos) {
            $after = Combo::where('is_combo', true)->where('position', '<', $cur_pos)->where('position', '>=', $new_pos)->where('id', '!=', $current->id)->orderBy('position')->get();
            foreach ($after as $at) {
                $dn = Combo::find($at->id);
                $dn->position += 1;
                $dn->save();
            }
        }

        $current->position = $new_pos;
        $current->save();

        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã cập nhật thứ tự hiển thị.'
        ]);
    }
}
