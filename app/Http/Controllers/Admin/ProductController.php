<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\ProductGroup;
use App\ProductCategory;
use App\ProductBrand;
use App\ProductVersion;
use App\Tag;
use App\Media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/products/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];

    /**
     * Trạng thái sản phẩm
     */
    protected $status = array(
        1 => 'Còn hàng',
        0 => 'Tạm hết',
        -1 => 'Ngừng kinh doanh',
    );

    public function index(Request $request)
    {

        $kind = '';
        if ($request->filled('category')) {
            $kind = 'category';
            $this->authorize('product-access');
        } elseif ($request->filled('brand')) {
            $kind = 'brand';
            $this->authorize('brand-access');
        }

        if ($request->filled('trash'))
            $query = Product::where('is_combo', false)->where('trashed', 1);
        else
            $query = Product::where('is_combo', false)->where('trashed', 0);
        //select những trường cần thiết
        $query->select('products.id', 'products.thumbnail', 'products.slug', 'products.name', 'products.drafted',
            'products.code', 'products.closed', 'products.hidden', 'products.position', 'products.position_in_cat',
            'products.price', 'products.original_price', 'products.display_price', 'products.quantity', 'products.category_id', 'products.brand_id','products.real_stock_quantity','products.external_id');
        if ($request->filled('code')) {
            // TODO: Lấy ds các product id có version code cần filter
            $productVersion = ProductVersion::where('code', 'LIKE', $request->code)->get()->pluck('product_id');
            
            // TODO: Lọc ds product theo code của product hoặc theo code version của product
            $query->where('code', 'LIKE', $request->code)
                ->orWhere(function ($string) use ($request, $productVersion) {
                    $string->whereIn('id', $productVersion);
                });
        }
        if ($request->filled('name'))
            $query->where('name', 'LIKE', '%' . str_replace(' ', '%', $request->name) . '%');
        if ($request->filled('status')) {
            if ($request->status == 'hidden_price')
                $query->where('display_price', false);
            else
                $query->where($request->status, true);
        }

        // TODO: Lọc theo nhóm sản phẩm
        if ($request->filled('group')) {
            $requestGroups = ProductGroup::join('product_group_relationships', function ($string) use ($request) {
                $string->where('group_id', $request->group);
            })->get()->pluck('product_id');
            
            $query->where(function ($string) use ($request, $requestGroups) {
                $string->whereIn('id', $requestGroups);
            });
        }

        if ($request->filled('category')) {
            $requestCats = ProductCategory::where('parent_id', $request->category)->get()->pluck('id');
            $query->where(function ($string) use ($request, $requestCats) {
                $string->where('category_id', $request->category)->orWhereIn('category_id', $requestCats);
            });
        }

        if ($request->filled('brand')) {
            $requestBrands = ProductBrand::where('parent_id', $request->brand)->get()->pluck('id');
            $query->where(function ($string) use ($request, $requestBrands) {
                $string->where('brand_id', $request->brand)->orWhereIn('brand_id', $requestBrands);
            });
        }

        $data = $query->orderBy('position', 'asc')
            ->orderBy('reposted_at', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate($request->show ?? 25);
        $data->appends(request()->input())->links();

        foreach ($data as $vl) {
            if ($vl->fk_sales()->count() > 0) {
                $sales = $vl->fk_sales()->firstorfail();
                if ($sales->status == 1 && (date('U', strtotime($sales->started_at)) <= date('U') && date('U', strtotime($sales->expired_at)) >= date('U'))) {
                    if ($vl->original_price > 0) {
                        if ($sales->price_type == 1) {
                            $vl->price = floatval($vl->original_price) - (floatval($vl->original_price) * $sales->price_down / 100);
                        } else {
                            $vl->price = floatval($vl->original_price) - floatval($sales->price_down);
                        }
                    } else {
                        if ($sales->price_type == 1) {
                            $vl->original_price = $vl->price;
                            $vl->price = floatval($vl->original_price) - (floatval($vl->original_price) * $sales->price_down / 100);
                        } else {
                            $vl->price = floatval($vl->original_price) - floatval($sales->price_down);
                        }
                    }
                }
            }
        }
        $listallcategories = get_all_categories();
        $listallbrands = get_all_brands();
        $listallgroups = get_all_groups();

        return view('admin.product-list', compact('data', 'listallcategories', 'listallbrands', 'listallgroups', 'kind'));
    }

    public function create()
    {
        $this->authorize('product-access');
        $groups = ProductGroup::all();
        $categories = ProductCategory::whereNull('parent_id')->get();
        $brands = ProductBrand::whereNull('parent_id')->get();
        $tags = Tag::all();
        $code = $this->makeCodeFormat();
        $status = $this->status;

        return view('admin.product-create',
         compact('groups', 'categories', 'brands', 'tags', 'code', 'status'));
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->authorize('product-access');

        $code =  $request->code ?? $this->makeCodeFormat();
        // TODO: Check duplicate Product Code
        if (Product::where('code', $code)->exists()) {
            return redirect()->route('admin.product.index')->with([
                'alert' =>  'error',
                'title' =>  'Lỗi!',
                'msg'   =>  'Mã sản phẩm đã tồn tại'
            ]);
        }

        /** Upload Thumbnail */
        $thumbnail = 'default.jpg';
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        /** Insert Tags */
        $tags = null;
        if ($request->has('tags')) {
            foreach ($request->tags as $item) {
                if (Tag::where('keyword', $item)->get()->count() == 0) {
                    $tag = new Tag;
                    $tag->keyword = $item;
                    $tag->slug = str_slug($item);
                    $tag->save();
                }
                $tags = implode(',', $request->tags);
            }
        }

        $data = new Product;
        $data->name = $request->name;
        $data->code = $code;
        $data->slug = $request->slug;
        $data->price = str_replace(',', '', $request->price);
        $data->original_price = !empty($request->original_price) ? str_replace(',', '', $request->original_price) : null;
        $data->thumbnail = $thumbnail;
        $data->content = $request->content;
        $data->properties = $request->properties;
        $data->category_id = $request->category;
        $data->brand_id = $request->brand;
        $data->tags = $tags;
        $data->quantity = $request->quantity;
        $data->external_id = $request->external_id??'';
        $data->title = $request->title;
        $data->description = $request->description;
        $data->keywords = $request->keywords;
        $data->display_price = $request->has('display_price') ? true : false;
        $data->status = $request->status;
        $data->closed = $request->has('close') ? true : false;
        $data->hidden = $request->has('show') ? false : true;
        $data->drafted = $request->has('draft') ? true : false;
        $data->reposted_at = date('Y-m-d H:i:s');
        $data->save();

        /** Insert Product Version */
        $this->storeVersion($request, $data->id);

        /** Insert Contain Groups */
        $data->fk_groups()->sync($request->groups);

        /** Upload Product Images */
        if ($request->hasFile('images')) {

            $i = 0; // Thứ tự của hình, title, index
            foreach ($request->file('images') as $key => $file) {
                $index = $request->input('indexes.' . $i);
                $name = $request->input('names.' . $i);
                if (!$index) {
                    $index = $key + 1;
                }

                if (!$name) {
                    $name = $request->slug . '-' . $index;
                }
                $slug = upload_file($file, $name . strtolower(str_random(5)), $this->path, $this->dimensions);

                $media = new Media;
                $media->target_id = $data->id;
                $media->target_type = 'product';
                $media->name = $name;
                $media->size = $file->getClientSize();
                $media->type = $file->getClientMimeType();
                $media->path = $slug;
                $media->index = $index;
                $media->save();
                $i++;
            }
        }

        // TOTO: Nếu không có hình thumnail thì sẽ lấy hình đầu tiên trong ds hình mô tả (nếu có)
        if (!$request->hasFile('thumbnail')) {
            $list_images = Media::where('target_id', $data->id)->where('target_type', 'product')->orderBy('index', 'asc')->get();
            if (count($list_images) > 0) {
                $data->thumbnail = $list_images[0]->path;
                $data->save();
            }
        }

        return redirect()->route('admin.product.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã thêm sản phẩm mới'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('product-access');
        $data = Product::find($id);
        $groups = ProductGroup::all();
        $categories = ProductCategory::whereNull('parent_id')->get();
        $brands = ProductBrand::whereNull('parent_id')->get();
        $tags = Tag::all();
        $versions = $data->fk_versions;
        $status = $this->status;

        return view('admin.product-edit',
         compact('data', 'groups', 'categories', 'brands', 'tags', 'versions', 'status'));
    }

    public function getinfo($id, Request $request)
    {
        $data = Product::find($id);

        $result = array(
            'price'             =>  str_replace(',', '', $data->price),
            'original_price'    =>  !empty($data->original_price) ? str_replace(',', '', $data->original_price) : 'N/A',
            'brand_id'          =>  $data->brand_id,
            'category_id'       =>  $data->category_id,
            'name'              =>  $data->name,
            'quantity'          =>  !empty($data->quantity) ? str_replace(',', '', $data->quantity) : 'Không giới hạn',
            'real_stock_quantity'          =>  !empty($data->real_stock_quantity) ? $data->real_stock_quantity : '',
            'group_id'          =>  $data->fk_groups
        );

        return $result;
    }

    public function updateinfo($id, Request $request)
    {
        $data = Product::find($id);

        if ($request->product_original_price != 'N/A') {
            $product_original_price = $request->product_original_price;
        } else {
            $product_original_price = NULL;
        }

        if ($request->quantity != 'Không giới hạn') {
            $quantity = $request->quantity;
        } else {
            $quantity = NULL;
        }

        $data->name = $request->product_name;
        $data->price = str_replace(',', '', $request->product_price);
        $data->original_price = $product_original_price;
        $data->quantity = $quantity;
        $data->real_stock_quantity = $request->real_stock_quantity??0;
        $data->category_id = $request->product_kind;
        $data->brand_id = $request->product_brand;
        if ($data->save()) {
            if ($request->product_group != 0) {
                $data->fk_groups()->sync($request->product_group);
            }
            return 1;
        }
    }

    public function updatestatus($id, Request $request)
    {
        $data = Product::find($id);

        if ($data->hidden != $request->status) {
            $data->hidden = $request->status;

            if ($data->save()) {
                return 1;
            }
        }
    }

    public function update(Request $request, $id)
    {

        $this->authorize('product-access');
        $data = Product::find($id);

        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if ($request->hasFile('thumbnail')) {
            /** Xóa thumbnail cũ */
            if (File::exists($this->path . $data->thumbnail)) File::delete($this->path . $data->thumbnail);
            /** Upload thumbnail mới */
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }

        /** Insert Tags */
        $tags = null;
        if ($request->filled('tags')) {
            foreach ($request->tags as $item) {
                if (Tag::where('keyword', $item)->get()->count() == 0) {
                    $tag = new Tag;
                    $tag->keyword = $item;
                    $tag->slug = str_slug($item);
                    $tag->save();
                }
                $tags = implode(',', $request->tags);
            }
        }

        $data->name = $request->name;
        $data->code = $request->code;
        $data->slug = $request->slug;
        $data->price = str_replace(',', '', $request->price);
        $data->original_price = !empty($request->original_price) ? str_replace(',', '', $request->original_price) : null;
        $data->thumbnail = $thumbnail;
        $data->content = $request->content;
        $data->properties = $request->properties;
        $data->category_id = $request->category;
        $data->brand_id = $request->brand;
        $data->tags = $tags;
        $data->quantity = $request->quantity;
        $data->real_stock_quantity = $request->real_stock_quantity??0;
        $data->external_id = $request->external_id??'';
        $data->title = $request->title;
        $data->description = $request->description;
        $data->keywords = $request->keywords;
        $data->display_price = $request->has('display_price') ? true : false;
        $data->status = $request->status;
        $data->closed = $request->has('close') ? true : false;
        $data->hidden = $request->has('show') ? false : true;
        $data->drafted = $request->has('draft') ? true : false;

        /** Đăng lên đầu trang */
        if ($request->has('repost')) $data->reposted_at = date('Y-m-d H:i:s');
        $data->save();

        /** Update Product Version */
        $this->updateVersion($request, $data->id);

        /** Insert Contain Groups */
        $data->fk_groups()->sync($request->groups);

        /** Upload Product Images */
        /** Lấy danh sách ảnh sản phẩm */
        $images = Media::where('target_id', $id)->where('target_type', 'product')->get();
        /** Xóa những ảnh đã loại bỏ */
        if ($request->filled('old_images')) {
            foreach ($images as $image) {
                if (!in_array($image->id, $request->old_images)) {
                    if (File::exists($this->path . $image->path))
                        File::delete($this->path . $image->path);
                    Media::destroy($image->id);
                } else {
                    // TODO: Cập nhật lại thứ tự và tiêu đề của ds hình cũ
                    $image->name = $request->input('image_name' . $image->id);
                    $image->index = $request->input('image_index' . $image->id);
                    $image->save();
                }
            }
        } else {
            foreach ($images as $image) {
                if (File::exists($this->path . $image->path)) File::delete($this->path . $image->path);
                Media::destroy($image->id);
            }
        }

        /** Upload hình ảnh mới */
        if ($request->hasFile('images')) {
            $i = 0; // Thứ tự của hình, title, index
            foreach ($request->file('images') as $file) {
                $index = $request->input('indexes.' . $i);
                $name = $request->input('names.' . $i);
                if (!$index) {
                    $index = Media::where('target_id', $id)->where('target_type', 'product')->max('index') + 1;
                }

                if (!$name) {
                    $name = $request->slug . '-' . $index;
                }
                $slug = upload_file($file, $name . strtolower(str_random(5)), $this->path, $this->dimensions);

                $media = new Media;
                $media->target_id = $id;
                $media->target_type = 'product';
                $media->name = $name;
                $media->size = $file->getClientSize();
                $media->type = $file->getClientMimeType();
                $media->path = $slug;
                $media->index = $index;
                $media->save();

                $i++;
            }
        }


        // TOTO: Nếu không có hình thumnail thì sẽ lấy hình đầu tiên trong ds hình mô tả (nếu có)
        if (!$request->hasFile('thumbnail')) {
            $list_images = Media::where('target_id', $data->id)->where('target_type', 'product')->orderBy('index', 'asc')->get();
            if (count($list_images) > 0) {
                $data->thumbnail = $list_images[0]->path;
                $data->save();
            }
        }

        // Cập nhật số lượng sản phẩm chính
        $data = DB::select('call products_updatequantity (?)', array($id));

        return redirect()->route('admin.product.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin sản phẩm.'
        ]);
    }

    public function trash(Request $request)
    {
        $this->authorize('product-access');
        if ($request->ajax()) {
            Product::whereId($request->id)->update(['trashed' => true]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã chuyển 1 mục vào thùng rác.'
            ]);
        }
    }

    public function restore(Request $request)
    {
        $this->authorize('product-access');
        if ($request->ajax()) {
            Product::whereId($request->id)->update(['trashed' => false]);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã khôi phục 1 mục.'
            ]);
        }
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('product-access');
        if ($request->ajax()) {
            $data = Product::find($id);
            /** Xóa ảnh thumbnail */
            if (File::exists($this->path . $data->thumbnail)) File::delete($this->path . $data->thumbnail);
            /** Xóa ảnh sản phẩm */
            $medias = Media::where('target_id', $id)->where('target_type', 'product')->get();
            foreach ($medias as $media) {
                if (File::exists($this->path . $media->path)) File::delete($this->path . $media->path);
                Media::destroy($media->id);
            }
            /** Xóa sản phẩm */
            Product::destroy($id);

            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function makeCodeFormat()
    {
        $start = 'SP';
        $max = Product::max('id');
        return $start . make_rand_string($max + 1);
    }

    public function storeVersion($request, $product_id)
    {
        if ($request->filled('version_name')) {
            foreach ($request->version_name as $key => $value) {
                $data = new ProductVersion;
                $data->product_id = $product_id;
                $data->name = $request->version_name[$key];
                $data->code = $request->version_code[$key];
                $data->color = $request->version_color[$key];
                $data->price = empty($request->version_price[$key]) ? null : str_replace(',', '', $request->version_price[$key]);
                $data->quantity = $request->version_quantity[$key] ?? null;
                $data->external_id = $request->version_external_id[$key]??null;
                $data->save();

                if (!empty($request->version_images[$key])) {
                    foreach ($request->version_images[$key] as $i => $versionImage) {
                        // TODO: Lấy title, index của version vị trí $i
                        $index = $request->version_image_indexes[$key][$i];
                        $name = $request->version_image_names[$key][$i];
                        if (!$index) {
                            $index = $i + 1;
                        }

                        if (!$name) {
                            $name = $request->slug . '-' . str_slug($request->version_name[$key]) . '-' . str_random(5);
                        }
                        $slug = upload_file($versionImage, $name . strtolower(str_random(5)), $this->path, $this->dimensions);

                        $media = new Media;
                        $media->target_id = $data->id;
                        $media->target_type = 'version';
                        $media->name = $name;
                        $media->size = $versionImage->getClientSize();
                        $media->type = $versionImage->getClientMimeType();
                        $media->path = $slug;
                        $media->index = $index;
                        $media->save();
                    }
                }
            }
        }
    }

    public function updateVersion($request, $product_id)
    {
        // Xóa các phiên bản đã bỏ qua
        if ($request->filled('version_id')) {
            ProductVersion::where('product_id', $product_id)->whereNotIn('id', $request->version_id)->delete();
            // $medias = Media::where('target_type', 'version')->whereNotIn('target_id', $request->version_id)->get();
            // foreach($medias as $media) {
            //     if(File::exists($this->path.$media->path)) File::delete($this->path.$media->path);
            //     Media::destroy($media->id);
            // }
        }

        // Update các phiên bản cũ
        if ($request->filled('version_id')) {
            foreach ($request->version_id as $key => $item_id) {
                if (!empty($request->version_id[$key])) {
                    $update = ProductVersion::find($item_id);
                    $update->name = $request->version_name[$key];
                    $update->code = $request->version_code[$key];
                    $update->color = $request->version_color[$key] ?? null;
                    $update->price = empty($request->version_price[$key]) ? null : str_replace(',', '', $request->version_price[$key]);
                    $update->quantity = $request->version_quantity[$key] ?? null;
                    $update->real_stock_quantity = $request->version_real_stock_quantity[$key] ?? null;
                    $update->external_id = $request->version_external_id[$key]??null;
                    $update->save();

                    // Nếu media chứa target_type = 'version' và target_id = $version_id
                    // Và id không tồn tại trong $request->version_images_old[$version_id] thì xóa
                    $old_imgs = Media::where('target_type', 'version')->where('target_id', $item_id)->get();
                    if (!empty($request->version_images_old)) {
                        foreach ($old_imgs as $image) {
                            if (!in_array($image->id, $request->version_images_old)) {
                                if (File::exists($this->path . $image->path))
                                    File::delete($this->path . $image->path);
                                Media::destroy($image->id);
                            } else {
                                // TODO: Cập nhật lại thứ tự và tiêu đề của ds hình cũ
                                $image->name = $request->input('version_image_name' . $image->id);
                                $image->index = $request->input('version_image_index' . $image->id);
                                $image->save();
                            }
                        }
                    } else {
                        foreach ($old_imgs as $image) {
                            if (File::exists($this->path . $image->path)) File::delete($this->path . $image->path);
                            Media::destroy($image->id);
                        }
                    }

                    // Upload hình ảnh mới của version
                    if (!empty($request->version_images[$key])) {
                        foreach ($request->version_images[$key] as $i => $versionImage) {
                            // TODO: Lấy title, index của version vị trí $i
                            $index = $request->version_image_indexes[$key][$i];
                            $name = $request->version_image_names[$key][$i];
                            if (!$index) {
                                $index = Media::where('target_type', 'version')->where('target_id', $item_id)->orderBy('index', 'desc')->first()->index ?? 1;
                            }

                            if (!$name) {
                                $name = $request->slug . '-' . str_slug($request->version_name[$key]) . '-' . str_random(5);
                            }
                            $slug = upload_file($versionImage, $name . strtolower(str_random(5)), $this->path, $this->dimensions);

                            $media = new Media;
                            $media->target_id = $item_id;
                            $media->target_type = 'version';
                            $media->name = $name;
                            $media->size = $versionImage->getClientSize();
                            $media->type = $versionImage->getClientMimeType();
                            $media->path = $slug;
                            $media->index = $index;
                            $media->save();
                        }
                    }
                }
            }
        }

        // Thêm các phiên bản mới
        if ($request->filled('version_name')) {
            foreach ($request->version_name as $key => $item) {
                if (empty($request->version_id[$key])) {
                    $data = new ProductVersion;
                    $data->product_id = $product_id;
                    $data->name = $request->version_name[$key];
                    $data->code = $request->version_code[$key];
                    $data->color = $request->version_color[$key];
                    $data->price = empty($request->version_price[$key]) ? null : str_replace(',', '', $request->version_price[$key]);
                    $data->quantity = $request->version_quantity[$key] ?? null;
                    $data->save();

                    if (!empty($request->version_images[$key])) {
                        foreach ($request->version_images[$key] as $i => $versionImage) {
                            $index = $i + 1;
                            $name = $request->slug . '-' . str_slug($request->version_name[$key]) . '-' . str_random(5);
                            $slug = upload_file($versionImage, $name, $this->path, $this->dimensions);

                            $media = new Media;
                            $media->target_id = $data->id;
                            $media->target_type = 'version';
                            $media->name = $name;
                            $media->size = $versionImage->getClientSize();
                            $media->type = $versionImage->getClientMimeType();
                            $media->path = $slug;
                            $media->index = $index;
                            $media->save();
                        }
                    }
                }
            }
        }
    }

    public function position(Request $request)
    {
        $current = Product::find($request->id);
        $cur_pos = $current->position;
        $new_pos = $request->position;

        if ($cur_pos < $new_pos) {
            $before = Product::where('is_combo', false)->where('position', '>', $cur_pos)->where('position', '<=', $new_pos)->where('id', '!=', $current->id)->orderBy('position')->get();
            foreach ($before as $bf) {
                $up = Product::find($bf->id);
                $up->position -= 1;
                $up->save();
            }
        } else if ($cur_pos > $new_pos) {
            $after = Product::where('is_combo', false)->where('position', '<', $cur_pos)->where('position', '>=', $new_pos)->where('id', '!=', $current->id)->orderBy('position')->get();
            foreach ($after as $at) {
                $dn = Product::find($at->id);
                $dn->position += 1;
                $dn->save();
            }
        }

        $current->position = $new_pos;
        $current->save();

        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã cập nhật thứ tự hiển thị.'
        ]);
    }

    public function positionInCat(Request $request)
    {
        $current = Product::find($request->id);
        $cur_pos = $current->position_in_cat;
        $new_pos = $request->position;

        if ($cur_pos < $new_pos) {
            $before = Product::where('is_combo', false)->where('position_in_cat', '>', $cur_pos)->where('position_in_cat', '<=', $new_pos)->where('id', '!=', $current->id)->where('category_id', $current->category_id)->orderBy('position_in_cat')->get();
            foreach ($before as $bf) {
                $up = Product::find($bf->id);
                $up->position_in_cat -= 1;
                $up->save();
            }
        } else if ($cur_pos > $new_pos) {
            $after = Product::where('is_combo', false)->where('position_in_cat', '<', $cur_pos)->where('position_in_cat', '>=', $new_pos)->where('id', '!=', $current->id)->where('category_id', $current->category_id)->orderBy('position_in_cat')->get();
            foreach ($after as $at) {
                $dn = Product::find($at->id);
                $dn->position_in_cat += 1;
                $dn->save();
            }
        }

        $current->position_in_cat = $new_pos;
        $current->save();

        return response()->json([
            'alert' => 'success',
            'title' => 'Hoàn tất',
            'msg' => 'Đã cập nhật thứ tự hiển thị trong danh mục sản phẩm.'
        ]);
    }


    public function fecthStock(Request $request)
    {
        $this->authorize('product-access');
        $name_api = "API Stock Search";
        try{
            if(empty($request->productId)){
                $data_log = [
                    'name'  =>  "API Stock Search",
                    'message'   =>  "Không tìm thấy sản phẩm để gọi API stock search",
                    'status'    =>  2
                ];
                $this->save_logs($data_log);
                return response()->json([
                    'alert' => 'error',
                    'title' => 'Lỗi!',
                    'msg' => 'Không tìm thấy sản phẩm.'
                ]);
            }
            $productId = $request->productId;
            $data = Product::where('id',$productId)->first();
            if(empty($data)){
                $data_log = [
                    'name'  =>  "API Stock Search",
                    'message'   =>  "Không tìm thấy sản phẩm với id ".$productId." để gọi API stock search",
                    'status'    =>  2
                ];
                $this->save_logs($data_log);
                return response()->json([
                    'alert' => 'error',
                    'title' => 'Lỗi!',
                    'msg' => 'Không tìm thấy sản phẩm.'
                ]);
            }
            $productCode = $data->external_id;
            $versions = $data->fk_versions()->pluck('external_id')->toArray();
            $data_result = [];
            $url = 'services/apexrest/afrfistore/stockSearch';

            if(empty($versions) || !count($versions)){
                // $productCode = 'testPDCode01';
                
                $params = [
                    'productCode'   =>  $productCode
                ];
                $access_totken = $this->getAccessTokenByDB();
                // $access_totken = "00DR0000001znou!AQcAQGgbujba0eAAhQB6lDdGlf_K6GWb2CCoMO71BhBbVImlCZeO1laCYlk2F9_UBMhH.TNmCfG8WSVWi7PuxKpTwOhhMxpV";
                $result = $this->getAPI($url,$params,$access_totken,$name_api);
                
                if(!empty($result[0]['errorCode']) && $result[0]['errorCode'] == 'INVALID_SESSION_ID'){
                    // Save log lỗi access token vào get lại access token mới
                    $data_log = [
                        'name'  =>  "API Stock Search",
                        'message'   =>  "Lỗi access token hết hạn. Tự động lấy mã token mới",
                        'status'    =>  2
                    ];
                    $this->save_logs($data_log);
                    $access_totken = $this->getAccessToken();
                    $result = $this->getAPI($url,$params,$access_totken,$name_api);
                }
                if(empty($result)){
                    return response()->json([
                        'alert' => 'error',
                        'title' => 'Lỗi',
                        'msg' => "Kết nối quá tải. Vui lòng thử lại."
                    ]);
                }
                if(empty($result['success'])){
                    $message = $result['errorMessages']['productCode'];
                    // save log lỗi
                    $data_log = [
                        'name'  =>  "API Stock Search",
                        'message'   =>  $message,
                        'status'    =>  2
                    ];
                    $this->save_logs($data_log);
                    return response()->json([
                        'alert' => 'error',
                        'title' => 'Lỗi',
                        'msg' => $message
                    ]);
                }
                
                $productStoc = [];
                $productStoc = $result['data'];
                $productStoc['productCode'] = $productCode;
                $productStoc['success'] = true;
                $productStoc['message'] = 'Đồng bộ sản phẩm with productCode '.$productCode.' trong kho thành công.';
                $data_result[] = $productStoc;
                $data_log = [
                    'name'  =>  "API Stock Search",
                    'message'   =>   'Đồng bộ sản phẩm with productCode '.$productCode.' trong kho thành công.',
                    'status'    =>  1
                ];
                $this->save_logs($data_log);
                return response()->json([
                    'alert' => 'success',
                    'title' => 'Hoàn tất!',
                    'msg' => 'Đồng bộ sản phẩm trong kho thành công.',
                    'data'  =>  $data_result
                ]);
            }else{
                foreach ($versions as $key) {
                    // $productCode = 'testPDCode01';
                    
                    $params = [
                        'productCode'   =>  $key
                    ];
                    $access_totken = $this->getAccessTokenByDB();
                    // $access_totken = "00DR0000001znou!AQcAQGgbujba0eAAhQB6lDdGlf_K6GWb2CCoMO71BhBbVImlCZeO1laCYlk2F9_UBMhH.TNmCfG8WSVWi7PuxKpTwOhhMxpV";
                    $result = $this->getAPI($url,$params,$access_totken,$name_api);
                    
                    if(!empty($result[0]['errorCode']) && $result[0]['errorCode'] == 'INVALID_SESSION_ID'){
                        // Save log lỗi access token vào get lại access token mới
                        $data_log = [
                            'name'  =>  "API Stock Search",
                            'message'   =>  "Lỗi access token hết hạn. Tự động lấy mã token mới",
                            'status'    =>  2
                        ];
                        $this->save_logs($data_log);
                        $access_totken = $this->getAccessToken();
                        $result = $this->getAPI($url,$params,$access_totken,$name_api);
                    }
                    if(empty($result)){
                        return response()->json([
                            'alert' => 'error',
                            'title' => 'Lỗi',
                            'msg' => "Kết nối quá tải. Vui lòng thử lại."
                        ]);
                    }
                    if(empty($result['success'])){
                        $message = $result['errorMessages']['productCode'];
                        // save log lỗi
                        // return response()->json([
                        //     'alert' => 'error',
                        //     'title' => 'Lỗi',
                        //     'msg' => $message
                        // ]);
                        $productStoc = [
                            'productCode' => $key,
                            'productStockQuantity'  =>  0,
                            'message'   =>  "Could not find product with productCode ".$key,
                            'success'   =>  false
                        ];
                        $data_result[] = $productStoc;
                        $data_log = [
                            'name'  =>  "API Stock Search",
                            'message'   =>  $message,
                            'status'    =>  2
                        ];
                        $this->save_logs($data_log);
                    }else{
                        $productStoc = [];
                        $productStoc = $result['data'];
                        $productStoc['productCode'] = $key;
                        $productStoc['success'] = true;
                        $productStoc['message'] = 'Đồng bộ sản phẩm with productCode '.$key.' trong kho thành công.';
                        $data_result[] = $productStoc;
                        $data_log = [
                            'name'  =>  "API Stock Search",
                            'message'   =>   'Đồng bộ sản phẩm with productCode '.$key.' trong kho thành công.',
                            'status'    =>  1
                        ];
                        $this->save_logs($data_log);
                    }
                }
                return response()->json([
                    'alert' => 'success',
                    'title' => 'Hoàn tất!',
                    'msg' => 'Đồng bộ sản phẩm trong kho thành công.',
                    'data'  =>  $data_result
                ]);
            }
            
        }catch (\Exception $e) {
            $data_log = [
                'name'  =>  "API Stock Search",
                'message'   =>  $e->getMessage(),
                'status'    =>  2
            ];
            $this->save_logs($data_log);
            return response()->json([
                'alert' => 'error',
                'title' => 'Lỗi!',
                'msg' => 'Không tìm thấy sản phẩm.'
            ]);
        }
    }
}
