<?php

namespace App\Http\Controllers\Admin;

use App\ProductBrand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;

class ProductBrandController extends Controller
{
    /**
     * Upload file path
     */
    protected $path = 'media/product-brands/';

    /**
     * Upload file size
     */
    protected $dimensions = [1024, 1024];

    public function index()
    {
        $this->authorize('brand-access');
        $data = ProductBrand::whereNull('parent_id')->orderBy('index')->get();
        return view('admin.product-brand-list', compact('data'));
    }

    public function create()
    {
        $this->authorize('brand-access');
        $brands = ProductBrand::whereNull('parent_id')->get();
        return view('admin.product-brand-create', compact('brands'));
    }

    public function store(Request $request)
    {
        $this->authorize('brand-access');

        /** Upload Thumbnail */
        $thumbnail = null;
        if($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }
        
        $data = new ProductBrand;
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->thumbnail = $thumbnail;
        $data->pinned = $request->has('pinned') ? true : false;
        $data->index = $request->index ?? 99;
        $data->save();

        return redirect()->route('admin.product.brand.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã tạo mới chuyên mục sản phẩm.'
        ]);
    }

    public function edit($id)
    {
        $this->authorize('brand-access');
        $data = ProductBrand::find($id);
        $brands = ProductBrand::whereNull('parent_id')->where('id', '!=', $id)->get();
        return view('admin.product-brand-edit', compact('data', 'brands'));
    }

    public function update(Request $request, $id)
    {
        $this->authorize('brand-access');
        
        $data = ProductBrand::find($id);

        /** Upload Thumbnail */
        $thumbnail = $data->thumbnail;
        if($request->hasFile('thumbnail')) {
            /** Xóa thumbnail cũ */
            if(File::exists($this->path.$data->thumbnail)) File::delete($this->path.$data->thumbnail);
            /** Upload thumbnail mới */
            $file = $request->file('thumbnail');
            $name = $request->slug;
            $thumbnail = upload_file($file, $name, $this->path, $this->dimensions);
        }
        
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->parent_id = $request->parent;
        $data->title = $request->title;
        $data->description = $request->description;
        $data->thumbnail = $thumbnail;
        $data->pinned = $request->has('pinned') ? true : false;
        $data->index = $request->index ?? 99;
        $data->save();

        return redirect()->route('admin.product.brand.index')->with([
            'alert' =>  'success',
            'title' =>  'Hoàn tất!',
            'msg'   =>  'Đã cập nhật thông tin chuyên mục sản phẩm.'
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('brand-access');
        if($request->ajax()){
            ProductBrand::destroy($id);
            return response()->json([
                'alert' => 'success',
                'title' => 'Hoàn tất',
                'msg' => 'Đã xóa bỏ 1 mục.'
            ]);
        }
    }

    public function checkNameExists(Request $request)
    {
        if($request->ajax()) {
            $query = ProductBrand::whereName($request->name);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0) 
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }

    public function checkSlugExists(Request $request)
    {
        if($request->ajax()) {
            $query = ProductBrand::whereSlug($request->slug);
            if($request->has('id'))
                $query->where('id', '!=', $request->id);

            if($query->get()->count() == 0) 
                return response()->json(true);
        }
        return response()->json('Mục này đã tồn tại.');
    }
}
