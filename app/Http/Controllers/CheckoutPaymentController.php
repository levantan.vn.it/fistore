<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Combo;
use App\Product;
use App\ProductOrder;
use App\ProductOrderDetail;
use App\User;
use Cart;
use Auth;
use App\Mail\OrderCompletedMail;
use Illuminate\Support\Facades\DB;
use App\Version;
class CheckoutPaymentController extends Controller
{
    public function __construct(Request $request)
    {
        $this->middleware(function ($request, $next) {
            // Từ chối nếu không tồn tại thông tin mua hàng đã lưu
            if (!session()->has('checkout'))
                return redirect()->route('cart.show');

            // Từ chối nếu Sesion Id người dùng không trùng với Id người dùng đang đăng nhập
            if (Auth::guard('customer')->check() && session('checkout')->user_id != Auth::guard('customer')->user()->id)
                return redirect()->route('cart.show');

            // Từ chối nếu tồn tại Session Id trong khi người dùng chưa đăng nhập
            if (Auth::guard('customer')->guest() && !is_null(session('checkout')->user_id))
                return redirect()->route('cart.show');

            // Từ chối nếu giỏ hàng trống
            if (Cart::count() == 0)
                return redirect()->route('cart.show');

            return $next($request);
        });
    }

    public function getShippingFee()
    {
        $shippingFee = \App\TransportationArea::where('district_id', session('checkout')->district_id)
            ->whereDenied(false)
            ->where('min_order', '<=', Cart::subtotal(0, ',', ''))
            ->where('max_order', '>=', Cart::subtotal(0, ',', ''))
            ->first();
        if (is_null($shippingFee))
            $shippingFee = get_setting('default_shipping_price')->value;
        else
            $shippingFee = $shippingFee->price;

        return $shippingFee;
    }

    public function getPayment()
    {
        $shippingFee = $this->getShippingFee();
        return view('checkout_payment', compact('shippingFee'));
    }

    public function postPayment(Request $request)
    {
        // Cập nhật thông tin khách hàng
        $data_customer = [
            'name' =>   session('checkout')->name,
            'id'    =>  Auth::guard('customer')->check() ? Auth::guard('customer')->user()->id : null,
            'phone' =>  session('checkout')->phone,
            'email' =>  Auth::guard('customer')->check() ? Auth::guard('customer')->user()->email : session('checkout')->email,
            'gender'    =>  Auth::guard('customer')->check() ? Auth::guard('customer')->user()->gender : null,
            'date_of_birth' =>  Auth::guard('customer')->check() ? Auth::guard('customer')->user()->date_of_birth : null,
        ];
        session(['data_customer' => $data_customer]);
        // Tạo đơn hàng
        $order = $this->createOrder($request);

        // Lưu thông tin chi tiết đơn hàng
        $this->createOrderDetail($request, $order->id,$order);

        // Nếu người dùng đăng nhập
        try {
            // Tính điểm Extra
            if (Auth::guard('customer')->check())
                $extra_point = $this->plusExtraPoint($order->total);

            if (session()->has('coupon') && \Auth::guard('customer')->check()) {
                // Cập nhật user đã sử dụng mã khuyến mãi
                $coupon_user = new \App\CouponUser;
                $coupon_user->user_id = \Auth::guard('customer')->user()->id;
                $coupon_user->coupon_id = session('coupon')->id;
                $coupon_user->save();
            }

            // Gửi mail cho khách hàng
            $this->sendNotification($order, Cart::content());
        } catch (\Exception $e) {
            \DB::table('logs')->insert(
                [
                    'name' => 'Lỗi mua hàng',
                    'description' => $e->getMessage(),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            );
        }

        // Xóa Session lưu thông tin đơn hàng
        session()->forget('coupon');
        session()->forget('checkout');

        // Xóa giỏ hàng
        Cart::destroy();

        return view('checkout_completed', compact('order', 'extra_point'));
    }

    public function createOrder(Request $request)
    {
        $data                     = new ProductOrder;
        $data->code             = $this->makeCodeFormat();
        $data->user_id             = Auth::guard('customer')->check() ? Auth::guard('customer')->user()->id : null;
        $data->name             = session('checkout')->name;
        $data->phone            = session('checkout')->phone;
        $data->email             = Auth::guard('customer')->check() ? Auth::guard('customer')->user()->email : session('checkout')->email;
        $data->address             = session('checkout')->address;
        if (isset(session('checkout')->ward_id)) {
            $data->ward_id             = session('checkout')->ward_id;
        } else {
            $data->ward_id             = 0;
        }
        $data->district_id         = session('checkout')->district_id;
        $data->province_id         = session('checkout')->province_id;
        $data->quantity         = Cart::count();

        $data->transport_price     = $this->getShippingFee();
        $discount_price = session('coupon')->discount ?? 0;
        if (isset(session('coupon')->type) && session('coupon')->type == 2) {
            if (session('coupon')->unit == '%') {
                $discount_price = $data->transport_price * session('coupon')->discount / 100;
            }

            $discount_price = $data->transport_price < $discount_price ? $data->transport_price : $discount_price;
        }

        $data->discounted         = $discount_price;

        $data->subtotal         = Cart::subtotal(0, ',', '');
        $data->total             = Cart::subtotal(0, ',', '') - $discount_price + ($data->transport_price ?? 0);

        $data->note             = $request->filled('note') ? substr($request->note, 0, 200) : null;

        $data->payment_method    = $request->payment_method;
        $data->payment_bank        = $request->atm_bank ?? null;
        $data->save();
        session(['order' => $data]);
        return $data;
    }

    public function createOrderDetail(Request $request, $orderId,$order)
    {
        $product_delivery = [];
        foreach (Cart::content() as $cartItem) {
            $data                     = new ProductOrderDetail;
            $data->order_id         = $orderId;
            $data->product_id         = $cartItem->id;
            $data->product_id         = $cartItem->id;
            $data->version_id = $cartItem->options->version->id;
            $data->code             = $cartItem->options->code;
            $data->name             = $cartItem->name;
            $data->quantity         = $cartItem->qty;
            $data->price            = $cartItem->price;
            $data->original_price   = $cartItem->options->hotdeal->original_price ?? $cartItem->options->product->original_price;
            $data->subtotal         = $cartItem->price * $cartItem->qty;
            $data->save();
            $product = Product::where('id', $cartItem->id)->first();
            if(!empty($cartItem->options->version->id)){
                $version = Version::find($cartItem->options->version->id);
                if(!empty($version)){
                    $product_delivery[] = [
                        'productCode' =>    $version->external_id,
                        'sellingPrice'  =>  $cartItem->price,
                        'sellingDeductAmount'   =>   $cartItem->options->hotdeal->original_price ?? $cartItem->options->product->original_price,
                        'purchasedQuantity' =>  $cartItem->qty
                    ];
                }
            }else{
                if(!empty($product)){
                    $product_delivery[] = [
                        'productCode' =>    $product->external_id,
                        'sellingPrice'  =>  $cartItem->price,
                        'sellingDeductAmount'   =>   $cartItem->options->hotdeal->original_price ?? $cartItem->options->product->original_price,
                        'purchasedQuantity' =>  $cartItem->qty
                    ];
                }
            }
            
            
            // Tên sản phẩm kèm phiên bản
            if (!is_null($cartItem->options->combo)) {
                //$combo = Combo::where('id', $cartItem->id)->first();
                foreach ($cartItem->options->combo as $comboItem) {
                    $product = Product::where('id', $comboItem->product_id)->first();
                    $detail                   = new ProductOrderDetail;
                    $detail->order_id         = $orderId;
                    $detail->product_id       = $product->id;
                    $detail->code             = $product->code;
                    $detail->name             = $comboItem->product_name;
                    if (isset($comboItem->version_id))
                        $detail->name .= ' - ' . $comboItem->version_name;
                    $detail->quantity         = 1;
                    $detail->parent_id        = $data->id;
                    $detail->price            = $product->price;
                    $detail->original_price   = $product->original_price;
                    $detail->subtotal         = 0;
                    $detail->save();
                    
                }
            }


            // Cập nhật số lượng sản phẩm
            $this->minusProductQuantity($cartItem->id, $cartItem->options->version->id, $cartItem->options->hotdeal->id, $cartItem->qty);

            // Cập nhật số lượng sản phẩm chính
            $data = DB::select('call products_updatequantity (?)', array($cartItem->id));
        }
        
        session(['product_delivery' => $product_delivery]);
        
        // $check = $this->deliveryOrder($order,$product_delivery);
    }

    public function plusExtraPoint($total)
    {
        $point = number_format($total / get_setting('money_to_extra_point')->value, 0, ',', '');

        $data = User::find(Auth::guard('customer')->user()->id);
        $data->extra_point += $point;
        $data->save();
        return $point;
    }

    public function minusProductQuantity($product_id, $version_id, $hotdeal_id, $quantity)
    {
        /**
         * Sản phẩm hot deal
         * Số lượng deal có giới hạn và lớn hơn 0
         * Cập nhật số lượng
         */
        if (!is_null($hotdeal_id)) {
            $hotdeal = \App\HotDeal::find($hotdeal_id);
            if (!is_null($hotdeal->available_qty) || $hotdeal->available_qty > 0) {
                $hotdeal->available_qty -= $quantity;
                $hotdeal->save();
            }
        } else {
            /**
             * Sản phẩm có nhiều phiên bản
             * Số lượng phiên bản có giới hạn và lớn hơn 0
             * Cập nhật số lượng
             */
            if (!is_null($version_id)) {
                $version = \App\ProductVersion::find($version_id);
                if (!is_null($version->quantity) || $version->quantity > 0) {
                    $version->quantity -= $quantity;
                    $version->save();
                }
            }
        }

        /**
         * Số lượng sản phẩm có giới hạn và lớn hơn 0
         * Cập nhất số lượng
         */
        $product = Product::find($product_id);
        if (!is_null($product->quantity) || $product->quantity > 0) {
            $product->quantity -= $quantity;
            $product->save();
        }
    }

    public function makeCodeFormat()
    {
        $begin     = 'SPDH';
        $end     = ProductOrder::max('id') + 1;
        return $begin . make_rand_string($end);
    }

    public function sendNotification($order, $orderDetail)
    {
        if (!is_null($order->email) && !empty($order->email))
            \Mail::to($order->email)->send(new OrderCompletedMail($order, $orderDetail));
    }

    
}
