<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use App\ProductBrand;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    public function showProductListInCategory($category_slug)
    {
    	$category = ProductCategory::whereSlug($category_slug)->first();
        $child_cats = $category->fk_childs->pluck('id');

    	$categories = ProductCategory::whereNull('parent_id')->get();
    	$brands = ProductBrand::whereNull('parent_id')->orderBy('index')->get();

    	$query = Product::whereHidden(0)
    				->whereDrafted(0)
    				->whereTrashed(0)
    				->where(function($string) use ($category, $child_cats){
                        $string->where('category_id', $category->id)->orWhereIn('category_id', $child_cats);
                    });

    	if(request()->filled('brand')) {
    		$query->where('brand_id', request()->brand);
    	}

    	if(request()->filled('sort')) {
    		if(request()->sort == 'min_cost') {
    			$query->orderBy('price', 'asc');
    		}
    		else if(request()->sort == 'max_cost') {
    			$query->orderBy('price', 'desc');
    		}
    	}
    	$data = $query->orderBy('position_in_cat', 'asc')
                        ->orderBy('reposted_at', 'desc')
    					->orderBy('created_at', 'desc')
    					->paginate(20);
    	return view('category_products', compact('category', 'categories', 'brands', 'data'));
    }
}
