<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    protected $table = 'slides';

    public function fk_images()
    {
    	return $this->hasMany(Media::class, 'target_id')->where('target_type', 'slide')->orderBy('index', 'asc')->orderBy('created_at', 'asc');
    }
}
