<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    // public function fk_categories()
    // {
    // 	return $this->belongsToMany(ProductCategory::class, 'product_category_relationships', 'product_id', 'category_id');
    // }

    public function fk_category()
    {
        return $this->hasOne(ProductCategory::class, 'id', 'category_id');
    }

    public function fk_brand()
    {
        return $this->hasOne(ProductBrand::class, 'id', 'brand_id');
    }

    public function fk_groups()
    {
    	return $this->belongsToMany(ProductGroup::class, 'product_group_relationships', 'product_id', 'group_id');
    }

    /**
	 * Lấy danh sách ảnh sản phẩm
	 */
	public function fk_images()
    {
		return $this->hasMany(Media::class, 'target_id')->where('target_type', 'product')->orderBy('index', 'asc');
	}

    /**
     * Lấy danh sách phiên bản sản phẩm
     */
    public function fk_versions()
    {
        return $this->hasMany(ProductVersion::class, 'product_id');
    }

    public function fk_hotdeal()
    {
        return $this->hasMany(HotDeal::class, 'product_id')
                            ->whereDisabled(false)
                            ->where(function($query){
                                $query->whereNull('quantity')->orWhere('quantity', '>', 0);
                            })
                            ->where('started_at', '<=', date('Y-m-d H:i:s'))
                            ->where('expired_at', '>', date('Y-m-d H:i:s'))
                            ->orderBy('created_at', 'desc')
                            ->first();
    }

    public function fk_combos()
    {
        return $this->belongsToMany(Combo::class, 'combo_details');
    }

    public function fk_comboProducts()
    {
        return $this->belongsToMany(Product::class, 'combo_details', 'combo_id', 'product_id');
    }

	public function fk_sales()
    {
        return $this->belongsToMany(Sales::class, 'sales_product')->orderBy('expired_at', 'desc');
    }

    public function fk_salesProducts()
    {
        return $this->belongsToMany(Product::class, 'sales_product', 'sale_id', 'product_id');
    }
}