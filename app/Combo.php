<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Combo extends Model
{
    protected $table = 'products';

    public function fk_products()
    {
    	return $this->belongsToMany(Product::class, 'combo_details', 'combo_id', 'product_id');
    }

    /**
	 * Lấy danh sách ảnh sản phẩm
	 */
	public function fk_images()
    {
  		return $this->hasMany(Media::class, 'target_id')->where('target_type', 'product')->orderBy('index', 'asc');
	}
}
