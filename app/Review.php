<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = 'reviews';

    public function fk_user()
    {
    	return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function fk_product()
    {
    	return $this->hasOne(Product::class, 'id', 'product_id');
    }

    public function fk_replies()
    {
    	return $this->hasMany(Review::class, 'parent_id');
    }

    public function fk_thank()
    {
        return $this->belongsToMany(User::class, 'review_thanked', 'review_id', 'user_id');
    }
}
