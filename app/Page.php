<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    public function fk_author()
    {
    	return $this->hasOne(Admin::class, 'id', 'author_id');
    }

    public function fk_group()
    {
    	return $this->hasOne(PageGroup::class, 'id', 'group_id');
    }
}
